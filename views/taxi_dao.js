var mongoose = require('mongoose');
var mongoosastic = require('mongoosastic');
var moment = require('moment-jalaali');
var geolib = require('geolib');
var autoIncrement = require('mongoose-auto-increment');

var config = require('./../config/config');
var turnover_dao = require('./../dao/turnover_dao');

var db = mongoose.createConnection(config.database);
autoIncrement.initialize(db);

var ObjectId = mongoose.Schema.Types.ObjectId;

// var taxiSchema = mongoose.Schema({
//
//     username: {type: String, es_indexed: true},
//     password: String,
//     model: {type: String, es_indexed: true},
//     driver_name: {type: String, es_indexed: true},
//     driver_phone_number: {type: String, es_indexed: true},
//     driver_avatar: {type: String, default: ""},
//     car_color: {type: String, default: ""},
//     location_lat: Number,
//     is_standby: {type: Boolean, default: false},
//     location_lan: Number,
//     year: Number,
//     is_online: Number,
//     state: String,
//     socket_id: String,
//     car_code: {type: String, es_indexed: true},
//     car_code_base: {type: Number, es_indexed: true},
//     taxi_code: String,
//     is_block: Boolean,
//     last_free_time: String
//
// });

var taxiSchema = mongoose.Schema({

  organization_id: ObjectId,
  organization_title: String,
  username: {type: String, es_indexed: true, unique: true},
  password: String,
  model: {type: String, es_indexed: true, default: ""},
  car_color: {type: String, default: "", es_indexed: true},
  driver_name: {type: String, es_indexed: true, default: ""},
  driver_phone_number: {type: String, es_indexed: true},
  driver_avatar: {type: String, default: ""},
  location_lat: Number,
  is_standby: {type: Boolean, default: false},
  location_lan: Number,
  year: String,
  is_online: String,
  state: String,
  socket_id: String,
  car_code: {type: String, es_indexed: true},
  car_code_base: {type: String, es_indexed: true},
  taxi_code: {type: String, es_indexed: true},
  is_block: Boolean,
  last_free_time: String,
  loc: {type: [Number], index: '2dsphere'},
  station_id: {type: ObjectId, default: null},
  station_title: {type: String, default: "ucab", es_indexed: true},
  is_apple: {type: Boolean, default: false},
  device_id: {type: String, default: ""},
  account_number: {type: String, default: "", es_indexed: true},
  account_name: {type: String, default: "", es_indexed: true},
  account_bank: {type: String, default: "", es_indexed: true},
  passenger_invite_code: {type: String, default: "", es_indexed: true},
  driver_invite_code: {type: String, default: "", es_indexed: true},
  email: {type: String, default: "", es_indexed: true},
  credit: {type: Number, default: 0, es_indexed: true},
  service_type: {type: String, default: "normal"},
  service_type_id: {type: ObjectId},
  rate: {type: Number, default: 5}

});

var serviceTypeSchema = mongoose.Schema({

  service_title: {type: String, default: ""},
  commission_rate: {type: Number, default: 13},
  explanation: {type: String, default: ""},

});

var taxiOnlineSchema = mongoose.Schema({

  taxi_id: {type: ObjectId},
  start_time: {type: Number, default: 0},
  finish_time: {type: Number, default: 0},
  date: {type: String, default: ""}

});

taxiSchema.plugin(mongoosastic, {
  hosts: [
    '127.0.0.1:9200'
  ]
});

var Taxi = db.model('Taxi', taxiSchema);

var TaxiOnline = db.model('TaxiOnline', taxiOnlineSchema);

var ServiceType = db.model('ServiceType', serviceTypeSchema);

taxiSchema.plugin(autoIncrement.plugin, {
  model: 'Taxi',
  field: 'taxi_code',
  startAt: 225,
  incrementBy: 5
});

// var stream = Taxi.synchronize(function(err){
//     console.log(err);
// })
//     , count = 0;
// stream.on('data', function (err, doc) {
//     count++;
// });
// stream.on('close', function () {
//     console.log('indexed ' + count + ' documents from LeadSearch!');
// });
// stream.on('error', function (err) {
//     console.log(err);
// });

module.exports.registerStationTaxi = function (station_id, station_title, username, password, model, year, driver_name, driver_phone_number, car_code, car_code_base, driver_avatar, car_color, callback) {

  var taxi = new Taxi({
    station_id: station_id,
    station_title: station_title,
    username: username,
    password: password,
    model: model,
    car_color: car_color,
    driver_name: driver_name,
    driver_phone_number: driver_phone_number,
    car_code: car_code,
    car_code_base: car_code_base,
    location_lat: 0,
    location_lan: 0,
    loc: [0, 0],
    year: year,
    is_online: 0,
    state: "free",
    socket_id: '',
    is_block: false,
    driver_avatar: driver_avatar
  });

  taxi.save(function (err, result) {

    if (!err && result) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.registerOrganizationTaxi = function (organization_id, organization_title, username, password, model, year, driver_name, driver_phone_number, car_code, car_code_base, driver_avatar, car_color, callback) {

  var taxi = new Taxi({
    organization_id: organization_id,
    organization_title: organization_title,
    username: username,
    password: password,
    model: model,
    car_color: car_color,
    driver_name: driver_name,
    driver_phone_number: driver_phone_number,
    car_code: car_code,
    car_code_base: car_code_base,
    location_lat: 0,
    location_lan: 0,
    loc: [0, 0],
    year: year,
    is_online: 0,
    state: "free",
    socket_id: '',
    is_block: false,
    driver_avatar: driver_avatar
  });

  taxi.save(function (err, result) {

    if (!err && result) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.registerTaxi = function (username, email, password, model, year, driver_name, driver_phone_number, car_code, car_code_base, driver_avatar, car_color, station_id, station_title, service_type, service_id, driver_invite_code, passenger_invite_code, account_number, account_name, account_bank, callback) {

  var taxi = new Taxi({

    username: username,
    password: password,
    model: model,
    car_color: car_color,
    driver_name: driver_name,
    driver_phone_number: driver_phone_number,
    car_code: car_code,
    car_code_base: car_code_base,
    location_lat: 0,
    location_lan: 0,
    loc: [0, 0],
    year: year,
    is_online: 0,
    state: "free",
    socket_id: '',
    is_block: false,
    driver_avatar: driver_avatar,
    station_id: station_id,
    station_title: station_title,
    service_type: service_type,
    service_type_id: service_id,
    email: email,
    passenger_invite_code: passenger_invite_code,
    driver_invite_code: driver_invite_code,
    account_number: account_number,
    account_bank: account_bank,
    account_name: account_name,

  });

  taxi.save(function (err, result) {

    if (!err && result) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.getTaxiInfo = function (taxi_id, callback) {

  Taxi.findOne({_id: taxi_id}, function (err, result) {

    if (!err && result) {
      callback(result);
    } else {
      callback(false);
    }

  });

};

module.exports.getTaxiByUsername = function (username, callback) {

  Taxi.findOne({username: username}, function (err, result) {

    if (!err) {
      console.log(result);
      callback(result);
    }

  });

};

module.exports.getTaxisLocation = function (callback) {

  //Taxi.find({is_online: 1}, {_id: 1, location_lan:1, location_lat: 1}, function(err, taxis_lists){

  Taxi.find({is_online: 1, state: "free", is_block: false}, function (err, taxis_lists) {

    if (taxis_lists) {
      console.log(err);
      callback(taxis_lists);

    } else {
      callback(false);
    }
  });
};

module.exports.setSocketId = function (taxi_id, socket_id, callback) {

  Taxi.update({_id: taxi_id}, {$set: {socket_id: socket_id}}, function (err, result) {

    if (!err) {
      callback(true);
    }
  });

};

module.exports.updateTaxiAvatar = function (taxi_id, taxi_avatar, callback) {

  Taxi.update({_id: taxi_id}, {$set: {driver_avatar: taxi_avatar}}, function (err, result) {

    if (result) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.setTaxiOnline = function (taxi_id, callback) {

  Taxi.update({_id: taxi_id}, {$set: {is_online: 1}}, function (err, result) {

    if (!err) {
      callback(true);
    }
  });

};

module.exports.setTaxiLocation = function (taxi_id, location_lat, location_lan, callback) {

  location_lat = parseFloat(location_lat);
  location_lan = parseFloat(location_lan);

  Taxi.update({_id: taxi_id}, {
    $set: {
      location_lat: location_lat,
      location_lan: location_lan,
      loc: [location_lan, location_lat]
    }
  }, function (err, result) {

    if (!err) {
      callback(true);
    }

  });

};


module.exports.getOrganizationTaxisList = function (page, per_page, organization_id, callback) {

  var page_number = parseInt(page);
  var taxis_in_page = parseInt(per_page);
  var start = (page_number - 1) * taxis_in_page;

  // get the data and return them

  Taxi.find({organization_id: organization_id})
    .sort({'_id': -1})
    .skip(start)
    .limit(taxis_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });


};

module.exports.getTaxisList = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var taxis_in_page = parseInt(per_page);
  var start = (page_number - 1) * taxis_in_page;

  // get the data and return them

  Taxi.find({})
    .sort({'_id': -1})
    .skip(start)
    .limit(taxis_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });


};

module.exports.getTaxisListAll = function (callback) {

  // get the data and return them

  Taxi.find({}, function (err, result) {

    if (err) {

      callback(false);

    } else {

      callback(result);
    }
  });


};

module.exports.setTaxiCredit = function (taxi_id, credit, callback) {

  Taxi.update({_id: taxi_id}, {$set: {credit: credit}}, function (err, result) {

    console.log(result);

    if (!err) {

      callback(true);

    }

  })

};

module.exports.getTaxiCredits = function (callback) {

  Taxi.find({credit: {$ne: 0}}, function (err, result) {

    if (!err) {

      callback(result);

    }

  })

}

module.exports.getStationTaxiCredits = function (station_id, callback) {

  Taxi.find({credit: {$ne: 0}, station_id: station_id}, function (err, result) {

    if (!err) {

      callback(result);

    }

  })

}

module.exports.updateTaxiLocation = function (taxi_id, location_lat, location_lan, callback) {

  location_lan = parseFloat(location_lan);
  location_lat = parseFloat(location_lat);

  Taxi.update({_id: taxi_id}, {
    $set: {
      location_lat: location_lat,
      location_lan: location_lan,
      is_online: 1,
      loc: [location_lan, location_lat]
    }
  }, function (err, result) {

    if (!err) {

      callback(true);

    }

  });

};

module.exports.setTaxiOffline = function (taxi_id, callback) {


  Taxi.update({_id: taxi_id}, {$set: {is_online: 0}}, function (err, result) {

    if (!err) callback(true);

  });

};

module.exports.setTaxiFree = function (taxi_id, callback) {

  Taxi.update({_id: taxi_id}, {$set: {state: "free"}}, function (err, result) {

    if (!err) callback(true);

  });

};

module.exports.editTaxiInfo = function (taxi_id, username, color, model, driver_name, driver_phone_number, year, car_code, car_code_base, car_color, station_id, station_title, service_type, service_type_id, account_number, account_name, account_bank, callback) {

  Taxi.update({_id: taxi_id}, {
    username: username,
    color: color,
    model: model,
    driver_name: driver_name,
    driver_phone_number: driver_phone_number,
    year: year,
    car_code: car_code,
    car_code_base: car_code_base,
    car_color: car_color,
    station_id: station_id,
    station_title: station_title,
    service_type: service_type,
    service_type_id: service_type_id,
    account_number: account_number,
    account_name: account_name,
    account_bank: account_bank

  }, function (err, result) {

    if (!err) {
      callback(true);
    }

  });

};

module.exports.setTaxiFreeOnStandBy = function (taxi_id, callback) {

  Taxi.update({
    _id: taxi_id,
    state: "standby"
  }, {$set: {state: "free"}}, function (err, result) {

    if (!err) {

      if (result.nModified == 0) {

        callback(false);

      } else {

        callback(true);

      }

    }

  });

};

module.exports.search = function (query, callback) {

  Taxi.search({
    query_string: {
      query: query
    }
  }, function (err, results) {

    callback(results.hits.hits);

  });

};

module.exports.setTaxiBusy = function (taxi_id, callback) {

  Taxi.update({
    _id: taxi_id,
    state: "standby"
  }, {$set: {state: "busy"}}, function (err, result) {

    if (!err) callback(true);

  });

};

module.exports.blockTaxi = function (taxi_id, callback) {

  Taxi.update({_id: taxi_id}, {$set: {is_block: true}}, function (err, result) {

    if (!err) {
      callback(true);
    }

  });

};

module.exports.unblockTaxi = function (taxi_id, callback) {

  Taxi.update({_id: taxi_id}, {$set: {is_block: false}}, function (err, result) {

    if (!err) {
      callback(true);
    }

  });

};

module.exports.setTaxiOfflineBySocket = function (socket_id) {

  Taxi.update({socket_id: socket_id}, {$set: {is_online: 0}}, function (err, result) {});

};

module.exports.getTaxiWaitTime = function (taxi_id, callback) {

  Taxi.findOne({_id: taxi_id}, function (err, taxi_data) {

    var now = moment();
    var start_time = moment(taxi_data.last_free_time, "jYYYY/jM/jD HH:mm:ss");
    var difference_time = now.diff(start_time, "seconds");

    callback(difference_time);

  });

};


module.exports.setLastFreeTime = function (taxi_id, callback) {

  var full_date = moment().format("jYYYY/jM/jD HH:mm:ss");

  Taxi.findOne({_id: taxi_id}, function (err, taxi_data) {

    if (!err) {

      if (taxi_data.state == "free") {

        Taxi.update({_id: taxi_id}, {$set: {last_free_time: full_date}}, function (err, result) {

          if (!err) {

            callback(true);

          }

        });

      }

    }

  });

};

// set taxi stand by

module.exports.setTaxiStandBy = function (taxi_id, callback) {

  Taxi.update({_id: taxi_id}, {$set: {state: "standby"}}, function (err, result) {

    if (result) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.setAllTaxisOffline = function (callback) {

  Taxi.update({}, {$set: {is_online: 0}}, {multi: true}, function (err, result) {

    if (!err) {

      callback(true);

    }

  })

};

module.exports.getTaxisBoundsLocation = function (min_lat, max_lat, min_lan, max_lan, callback) {

  Taxi.find({
      is_online: 1,
      state: "free",
      is_block: false,
      location_lat: {"$gt": min_lat, "$lt": max_lat},
      location_lan: {"$gt": min_lan, "$lt": max_lan}
    },
    function (err, taxis_lists) {

      if (taxis_lists) {

        callback(taxis_lists);

      } else {
        callback(false);
      }
    });

};

module.exports.getStationAroundTaxis = function (station_id, location_lat, location_lan, callback) {
  console.log("getStation AroundTaxis.....>" + station_id);
  location_lan = parseFloat(location_lan);
  location_lat = parseFloat(location_lat);


  Taxi
    .find({
      loc: {$geoWithin: {$centerSphere: [[location_lan, location_lat], 1 / 3963.2]}},
      is_online: 1,
      state: "free",
      is_block: false,
      station_id: station_id
    })
    .limit(20)
    .exec(function (err, result) {
      if (err) {
        console.log(err);
      } else {
        callback(result);
      }
    });

  // Taxi.find({
  //         is_online: 1,
  //         state: "free",
  //         is_block: false,
  //         location_lat: {"$gt": min_lat, "$lt": max_lat},
  //         location_lan: {"$gt": min_lan, "$lt": max_lan}
  //     },
  //     function(err, taxis_lists){
  //
  //         if(taxis_lists) {
  //
  //             callback(taxis_lists);
  //
  //         } else {
  //             callback(false);
  //         }
  //     });

};

module.exports.getAroundTaxisForStation = function (station_id, location_lat, location_lan, callback) {

  location_lan = parseFloat(location_lan);
  location_lat = parseFloat(location_lat);

  Taxi
    .find({
      loc: {$geoWithin: {$centerSphere: [[location_lan, location_lat], 1 / 3963.2]}},
      is_online: 1,
      state: "free",
      is_block: false,
      station_id: {$ne: station_id},
      organization_id: {$exists: false}

    })
    .limit(20)
    .exec(function (err, result) {
      if (err) {
        console.log(err);
      } else {
        callback(result);
      }
    });

  // Taxi.find({
  //         is_online: 1,
  //         state: "free",
  //         is_block: false,
  //         location_lat: {"$gt": min_lat, "$lt": max_lat},
  //         location_lan: {"$gt": min_lan, "$lt": max_lan}
  //     },
  //     function(err, taxis_lists){
  //
  //         if(taxis_lists) {
  //
  //             callback(taxis_lists);
  //
  //         } else {
  //             callback(false);
  //         }
  //     });

};

module.exports.getOrganizationAroundTaxis = function (organization_id, location_lat, location_lan, callback) {
  console.log("getOrganizationAroundTaxis.....>" + organization_id);
  location_lan = parseFloat(location_lan);
  location_lat = parseFloat(location_lat);


  Taxi
    .find({
      loc: {$geoWithin: {$centerSphere: [[location_lan, location_lat], 1 / 3963.2]}},
      is_online: 1,
      state: "free",
      is_block: false,
      organization_id: organization_id
    })
    .limit(20)
    .exec(function (err, result) {
      if (err) {
        console.log(err);
      } else {
        callback(result);
      }
    });

  // Taxi.find({
  //         is_online: 1,
  //         state: "free",
  //         is_block: false,
  //         location_lat: {"$gt": min_lat, "$lt": max_lat},
  //         location_lan: {"$gt": min_lan, "$lt": max_lan}
  //     },
  //     function(err, taxis_lists){
  //
  //         if(taxis_lists) {
  //
  //             callback(taxis_lists);
  //
  //         } else {
  //             callback(false);
  //         }
  //     });

};

module.exports.getAroundTaxis = function (service_type, location_lat, location_lan, callback) {

  location_lan = parseFloat(location_lan);
  location_lat = parseFloat(location_lat);

  Taxi
    .find({
      loc: {$geoWithin: {$centerSphere: [[location_lan, location_lat], 0.55 / 3963.2]}},
      is_online: 1,
      service_type: service_type,
      state: "free",
      is_block: false,
      organization_id: {$exists: false}
    })
    .limit(20)
    .exec(function (err, result) {
      if (err) {
        console.log(err);
      } else {
        callback(result);
      }
    });

  // Taxi.find({
  //         is_online: 1,
  //         state: "free",
  //         is_block: false,
  //         location_lat: {"$gt": min_lat, "$lt": max_lat},
  //         location_lan: {"$gt": min_lan, "$lt": max_lan}
  //     },
  //     function(err, taxis_lists){
  //
  //         if(taxis_lists) {
  //
  //             callback(taxis_lists);
  //
  //         } else {
  //             callback(false);
  //         }
  //     });

};

module.exports.getAroundTaxisTwo = function (service_type, location_lat, location_lan, refused_taxis_list, callback) {

    location_lan = parseFloat(location_lan);
    location_lat = parseFloat(location_lat);
    Taxi
        .find({
            loc: { $nearSphere: { $geometry: { type: "Point", coordinates: [ location_lan, location_lat ] }, $maxDistance: 1500 }},
            is_online: 1,
            service_type: service_type,
            state: "free",
            is_block: false,
            organization_id: {$exists: false},
            _id: { $nin: refused_taxis_list}
        })
        .limit(1)
        .exec(function (err, result) {
            if (err) {
                console.log(err);
            } else {
                callback(result);
            }
        });

};



module.exports.changeTaxiPassword = function (taxi_id, password, callback) {

  Taxi.update({_id: taxi_id}, {$set: {password: password}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  })

};

module.exports.getTaxisCount = function (callback) {

  Taxi.find({}).count().exec(function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

// getting driver

module.exports.getTaxiDriverAvatar = function (taxi_id, callback) {

  Taxi.findOne({_id: taxi_id}, function (err, taxi_data) {

    if (!err && taxi_data) {

      callback(taxi_data.driver_avatar);

    } else {

      callback(false);

    }

  });

};

// set start taxi time

module.exports.setTaxiOnlineStartTime = function (taxi_id, start_time, date, callback) {

  var new_taxi_online_time = new TaxiOnline({

    taxi_id: taxi_id,
    start_time: start_time,
    finish_time: 0,
    date: date

  });

  new_taxi_online_time.save(function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

// set finish online time

module.exports.setTaxiOnlineFinishTime = function (taxi_id, finish_time, today, yesterday, callback) {

  TaxiOnline.find({
    taxi_id: taxi_id,
    finish_time: 0,
    date: yesterday
  }, function (err, result) {

    if (!err) {

      if (result.length > 0) {

        TaxiOnline.update({
          taxi_id: taxi_id,
          finish_time: 0,
          date: yesterday
        }, {$set: {finish_time: 21600}}, {multi: true}, function (err, result) {

          if (!err) {

            var new_taxi_online_time = new TaxiOnline({

              taxi_id: taxi_id,
              start_time: 0,
              finish_time: finish_time,
              date: today

            });

            new_taxi_online_time.save(function (err, result) {

              if (!err) {

                callback(true);

              } else {

                callback(false);

              }

            });

          }

        })

      } else {

        TaxiOnline.update({
          taxi_id: taxi_id,
          finish_time: 0,
          date: today,
          start_time: {$lt: finish_time}
        }, {$set: {finish_time: finish_time}}, function (err, result) {

          if (!err) {

            TaxiOnline.remove({
              taxi_id: taxi_id,
              finish_time: 0,
              date: today,
              start_time: {$lt: finish_time}
            }, function (err, result) {

              callback(true);

            });

          }

        });

      }

    }

  });

};

module.exports.getTaxiInfoBySocketId = function (socket_id, callback) {

  Taxi.findOne({socket_id: socket_id}, function (err, taxi_data) {

    if (!err) {

      callback(taxi_data);

    }

  });

};

module.exports.getOnlineTaxisList = function (callback) {

  Taxi.find({is_online: 1, is_block: false}, function (err, taxis_list) {

    if (!err) {

      callback(taxis_list);

    } else {

      callback(false);

    }

  })

};

module.exports.getTaxiOnlineDates = function (taxi_id, callback) {

  TaxiOnline.find({taxi_id: taxi_id}, {date: 1}, function (err, result) {

    if (!err) {

      callback(result);

    } else {

      callback(false);

    }

  })

};

module.exports.getDateTaxiOnlineStat = function (taxi_id, date, callback) {

  TaxiOnline.find({taxi_id: taxi_id, date: date}, function (err, result) {

    if (!err) {

      callback(result);

    } else {

      console.log(err);

      callback(false);

    }

  })

};

module.exports.getStationTaxisList = function (station_id, page, per_page, callback) {

  var page_number = parseInt(page);
  var taxis_in_page = parseInt(per_page);
  var start = (page_number - 1) * taxis_in_page;

  // get the data and return them

  Taxi.find({station_id: station_id})
    .sort({'_id': -1})
    .skip(start)
    .limit(taxis_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });


};

module.exports.getStationTaxisListAll = function (station_id, callback) {

  Taxi.find({station_id: station_id}, function (err, result) {

    if (!err) {

      callback(result);

    }

  })

};

module.exports.getTaxiPhoneNumbers = function (callback) {

  Taxi.find({}, {driver_name: 1, driver_phone_number: 1}, function (err, result) {

    if (!err) {

      callback(result);

    }

  })

};

module.exports.updateTaxiAppleInfo = function (taxi_id, is_apple, device_id, callback) {

  Taxi.update({_id: taxi_id}, {
    $set: {
      is_apple: is_apple,
      device_id: device_id
    }
  }, function (err, result) {

    if (!err) {

      callback(true);

    }

  });

};

module.exports.setTaxiInfo = function (taxi_id, account_number, driver_name, callback) {

  Taxi.update({_id: taxi_id}, {
    $set: {
      account_number: account_number,
      driver_name: driver_name
    }
  }, function (err, result) {

    if (!err) {

      callback(true);

    }

  });

};

module.exports.updateTaxiRate = function (taxi_id, rate, callback) {

  Taxi.update({_id: taxi_id}, {$set: {rate: rate}}, function (err, result) {

    if (!err) {

      callback(true);

    }

  })

};

module.exports.createNewServiceType = function (service_title, commission_rate, explanation, callback) {

  var new_service = new ServiceType({

    service_title: service_title,
    commission_rate: commission_rate,
    explanation: explanation

  });

  new_service.save(function (err, service_data) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  })

};

module.exports.updateServiceType = function (service_id, commission_rate, explanation, callback) {

  ServiceType.update({_id: service_id}, {
    $set: {
      commission_rate: commission_rate,
      explanation: explanation
    }
  }, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.getServiceTypeData = function (service_id, callback) {

  ServiceType.findOne({_id: service_id}, function (err, service_data) {

    if (!err) {

      callback(service_data);

    }
    ;

  });

};

module.exports.getAllServiceTypes = function (callback) {

  ServiceType.find({}, function (err, result) {

    if (!err) {

      callback(result);

    } else {

      callback(false);

    }

  })

};

module.exports.updateAccountInfo = function (taxi_id, account_number, account_name, account_bank, callback) {

  Taxi.update({_id: taxi_id}, {
    $set: {

      account_number: account_number,
      account_name: account_name,
      account_bank: account_bank,

    }
  }, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  })

}


module.exports.getTaxisListOnlineSort = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var taxis_in_page = parseInt(per_page);
  var start = (page_number - 1) * taxis_in_page;

  // get the data and return them

  Taxi.find({})
    .sort({'is_online': -1})
    .skip(start)
    .limit(taxis_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });


};

module.exports.getTaxisListBusySort = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var taxis_in_page = parseInt(per_page);
  var start = (page_number - 1) * taxis_in_page;

  // get the data and return them

  Taxi.find({$or: [{state: "stand_by"}, {state: "busy"}]})
    .sort({'_id': -1})
    .skip(start)
    .limit(taxis_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });


};


module.exports.increaseTaxiCredit = function (taxi_id, credit, callback) {

  Taxi.update({_id: taxi_id}, {$inc: {credit: credit}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

// decrease taxi credit

module.exports.decreaseTaxiCredit = function (taxi_id, credit, callback) {

  Taxi.update({_id: taxi_id}, {$inc: {credit: -credit}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.fixTemp = function () {

  // set taxi credit to zero

  Taxi.find({credit: {$ne: 0}}, function (err, taxis) {

    for (var i = 0; i < taxis.length; i++) {

      (function (cntr) {

        var taxi_obj = taxis[cntr].toObject();

        // set credit to zero

        Taxi.update({_id: taxi_obj._id}, {$set: {credit: 0}}, function (err, result) {

          console.log("TAXI CREDIT TO ZERO");

          // calculate new credit

          var new_credit = 0;

          // get all taxi turnovers

          turnover_dao.getTaxiTurnOverList(taxi_obj._id, 1, 400, function (turnovers) {

              for(var j=0; j<turnovers.length; j++) {

                if(turnovers[j].turnover_type == "debtor") {

                  new_credit = new_credit - turnovers[j].amount;

                } else if (turnovers[j].turnover_type == "creditor") {

                  new_credit = new_credit + turnovers[j].amount;

                }

              }

            // update taxi credit

            Taxi.update({_id: taxi_obj._id}, {$set: {credit: new_credit}}, function(err, result){});


          });

        });

      })(i);
    }

  });


}


module.exports.setBankData = function (callback) {

  Taxi.find({account_bank: {$exists: false}}, function (err, result) {

    console.log(err);
    console.log(result);

    for (var i = 0; i < result.length; i++) {
      (function (cntr) {

        var invite_code = makeid(6);

        Taxi.update({_id: result[cntr]._id.toString()}, {$set: {account_bank: "", account_name: "", driver_invite_code: invite_code, passenger_invite_code: invite_code}}, {upsert: true}, function (err, result) {

          console.log(err);
          console.log(result);

          if (cntr == result.length - 1) {

            callback(true);

          }

        });

      })(i);
    }

  });

};

module.exports.getAllBusyStandByTaxis = function (callback) {
  
  Taxi.find({$or: [{state: "free"}, {state: "standby"}]}, function (err, result) {

    if(!err) {

      callback(result);

    }

  })
  
}

function makeid(length) {

  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  for( var i=0; i < length; i++ )
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};