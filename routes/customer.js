var path = require("path");
var customerController = require("../lib/controller/customer_controller");
var supportController = require("../lib/controller/support_controller");
var placeController = require("../lib/controller/place_controller");
var walletController = require("../lib/controller/wallet_controller");
var commentController = require("../lib/controller/comment_controller");
var travelController = require("../lib/controller/travel_controller");
var promotionController = require("../lib/controller/promotion_controller");
var consts = require("../lib/config/consts");
var aa = require("../lib/aa/aa");

module.exports = function (app) {

  app.post('/customer/customerregister/', function (req, res) {

    customerController.registerCustomer(req, function (result) {

      res.json({result: result});

    });

  });

  app.post('/customer/resendcode/', function (req, res) {

    customerController.updateCode(req, function (result) {

      res.json({result: result});

    });

  });

  app.post('/customer/codeverification/', function (req, res) {

    console.log("HERE SHIT");
    console.log(req.body);

    customerController.verifyCode(req, function (result) {

      res.json({result: result});

    });

  });

  app.post('/customer/calculatetravelcost/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      console.log(req.body);

      customerController.calculateTravelCost(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: {result: false}});

    }

  });

  app.post('/customer/savetravel/', function (req, res) {

    console.log("SAVE TRAVEL REQUEST");

    console.log(req.body);

    if (aa.customerAuthenticate(req)) {

      aa.isCustomerValid(req.body.customer_id, function (is_customer_valid) {

        if (is_customer_valid) {

          travelController.createTravel(req, function (result) {

            res.json({result: result});

          });

        } else {

          res.json({
            result: {
              result: false,
              message: "نام کاربری شما مسدود شده است. شما نمی‌توانید از این سرویس استفاده نمیایید."
            }
          });

        }

      });


    } else {

      res.json({
        result: {
          result: false,
          message: "لطفا از برنامه خارج و دوباره وارد شوید."
        }
      });

    }

  });

  app.post('/customer/travelslist/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      customerController.customerTravelsList(req, function (result) {

        console.log(result);

        res.json({result: result});

      });

    } else {

      res.json({result: false});

    }

  });

  app.post('/customer/updateinfo/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      customerController.updateInfo(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: false});

    }

  });

  app.post('/customer/updateavatar/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      customerController.updateAvatar(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: {result: false, avatar: null}});

    }

  });

  app.post('/customer/getinfo/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      customerController.getCustomerInfo(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: false});

    }

  });

  app.post('/customer/sendmessage/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      supportController.saveCustomerTemplateMessage(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: false});

    }

  });

  app.post('/customer/gettemplatemessages/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      supportController.getCustomerTemplateMessages(function (messages, passenger_phone) {

        res.json({result: true, messages: messages, passenger_phone: passenger_phone});

      });

    } else {

      res.json({result: false, messages: null, passenger_phone: null});

    }

  });

  app.post('/customer/checkappupdate/', function (req, res) {

    customerController.checkAppVersion(req, function (update_status) {

      res.json({result: update_status});

    });

  });

  app.post('/customer/checkiosappupdate/', function (req, res) {

    customerController.checkIosVersion(req, function (update_status) {

      res.json({result: update_status});

    });

  });

  // get place name by location

  app.post('/customer/getlocationplacename/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      placeController.getLocationPlaceName(req, function (place_name) {

        res.json({result: place_name});

      });

    } else {

      res.json({result: {result: false}});

    }

  });

  // search place

  app.post('/customer/searchplace/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      placeController.searchPlaceName(req, function (places) {

        res.json({result: {result: true, places: places}});

      });

    } else {

      res.json({result: {result: false, places: null}});

    }

  });

  // get taxis around customer

  app.post('/customer/getaroundtaxis/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      customerController.getAroundTaxis(req, function (taxis_list) {

        res.json({result: taxis_list});

      });

    } else {

      res.json({result: {result: false}});

    }

  });

  // rate to travel

  app.post('/customer/ratetravel/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      console.log("REQ SEND");
      console.log(req.body);

      travelController.travelCommented(req, function (rate_travel) {

        res.json({result: rate_travel});

      });

    } else {

      res.json({result: false});

    }

  });

  app.post('/customer/comments/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      commentController.getComments(req, function (rate_travel) {

        res.json({result: rate_travel});

      });

    } else {

      res.json({result: {result: false}});

    }

  });

  // payment request

  // app.post('/customer/paymentrequest/', function (req, res) {
  //
  //   if (aa.customerAuthenticate(req)) {
  //
  //     walletController.addMoneyToWallet(req, function (payment_result, transaction_id) {
  //
  //       if (payment_result) {
  //
  //         res.json({
  //           result: true,
  //           payment_result: payment_result,
  //           transaction_id: transaction_id
  //         });
  //
  //       }
  //
  //       else
  //         res.json({result: false, payment_result: null, transaction_id: null});
  //
  //     });
  //
  //   } else {
  //
  //     res.json({result: false, payment_result: null, transaction_id: null});
  //
  //   }
  //
  // });

  // ipg payment result

  app.get('/customer/paymentresult/ipg/', function (req, res) {


    walletController.getIpgPaymentResult(req, function (result) {

      res.sendFile(path.join(__dirname + '/../views/payment_success.html'));

    });

  });

  // ussd payment result

  app.get('/customer/paymentresult/ussd/', function (req, res) {


    walletController.getUssdPaymentResult(req, res);

  });

  // payment verification

  app.post('/customer/paymentverification/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      walletController.verifyPaymentResult(req, function (wallet_data, transaction_id) {

        if (wallet_data)
          res.json({
            result: true,
            wallet_data: wallet_data,
            transaction_id: transaction_id
          });
        else
          res.json({result: false, wallet_data: null, transaction_id: null});

      });

    } else {

      res.json({result: false, wallet_data: null, transaction_id: null});

    }


  });

  // get transactions

  app.post('/customer/transactions/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      walletController.getCustomerTransactions(req, function (result) {

        res.json({result: true, transactions: result});

      });
    } else {

      res.json({result: false});

    }

  });

  // get saman transactions

  app.post('/customer/samantransactions/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      walletController.getCustomerSamanTransactions(req, function (result) {

        res.json({result: true, transactions: result});

      });
    } else {

      res.json({result: false});

    }

  });

  // get wallet data

  app.post('/customer/wallet/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      walletController.getWalletInfo(req, function (result) {

        if (result)
          res.json({result: true, wallet_data: result});
        else
          res.json({result: false, wallet_data: null});

      });

    } else {

      res.json({result: false, wallet_data: null});

    }

  });

  // verify discount code

  app.post('/customer/discountverification/', function (req, res) {

    console.log("REQUEST COMING");

    if (aa.customerAuthenticate(req)) {

      promotionController.checkDiscount(req, function (result) {
        console.log(result);

        if (result)
          res.json({result: true, discount_amount: result});
        else
          res.json({result: false, discount_amount: null});

      });

    } else {

      res.json({result: false, discount_amount: null});

    }
  });

  // get avatar of user

  app.post('/customer/avatarpath/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      customerController.getCustomerAvatar(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: {result: false, avatar: null}});

    }

  });

  // add favorite place

  app.post('/customer/addfavplace/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

      customerController.addFavoritePlace(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: {result: false}});

    }

  });

  // remove favorite place

  app.post('/customer/removefavplace/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

        customerController.removeFavoritePlace(req, function (result) {

            res.json({result: result});

        });

    } else {

        res.json({result: {result: false}});

    }

  });

  // get favorite place

  app.post('/customer/getfavplaces/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

        customerController.getCustomerFavoritePlaces(req, function (result) {

            res.json({result: result});

        });

    } else {

        res.json({result: {result: false}});

    }

  });

  // get travel info

  app.post('/customer/gettravelinfo/', function (req, res) {

    if (aa.customerAuthenticate(req)) {

        customerController.getTravelInfoForCustomer(req, function (result) {

            res.json({result: result});

        });

    } else {

        res.json({result: {result: false}});

    }

  });

  // test part of a+ version

  app.post('/customer/checkaplusupdate/', function (req, res) {

    // customerController.checkAppVersion(req, function (update_status) {

      res.json({result: {result: "no_update", message: "", url: ""}});

    // res.json({result: {result: "force_update", message: "", url: "http://192.168.1.2/app.apk"}});


    // });

  });

  // share travel

  app.get('/ftsd12W00q/:travel_code', function (req, res) {

      // if (aa.customerAuthenticate(req)) {

          travelController.getTravelInfoByCode(req, function (data) {

            console.log(data);

              res.render('share_travel_page', {travel_data: data});

          });

      // } else {
      //
      //     res.json({result: {result: false}});
      //
      // }

  });

  app.post('/customer/paymentrequest/', function (req, res) {

    // if (aa.customerAuthenticate(req)) {

      walletController.saveNewSamanTransaction(req, function (payment_result, transaction_id) {

        if (payment_result) {

          res.json({
            result: true,
            payment_result: payment_result,
            transaction_id: transaction_id
          });

        }

        else
          res.json({result: false, payment_result: null, transaction_id: null});

      });

    // } else {

      // res.json({result: false, payment_result: null, transaction_id: null});

    // }

  });

  app.get('/payment/confirmation/:customer_id/:amount/:res_num/', function (req, res) {

    var customer_id = req.params.customer_id;
    var amount = req.params.amount;
    var res_num = req.params.res_num;
    var bank_amount = parseInt(amount) * 10;

    customerController.getCustomerInfo(req, function (customer_data) {

      if(customer_data) {

        res.render('payment_confirmation', {customer: customer_data, amount: amount, bank_amount: bank_amount, res_num: res_num, mid: consts.SAMAN_MERCHANT_ID, callback_url: consts.SAMAN_CALLBACK_URL})

      }

    })


  });

  app.post('/payment/callback/', function (req, res) {

    console.log(req.body);

    walletController.getSamanPaymentResult(req, function (result, payment_info, customer_data) {

      if(result) {

        res.render('payment_success_test', {customer: customer_data, payment: payment_info})

      } else {

        res.render('payment_failure')

      }


    });


  });

  ///////////////////////////////////////////////
  ///////////////////////////////////////////////

  app.get('/customer/payment/', function (req, res) {
    if (aa.getcustomerAuthenticate(req)) {

      taxiController.getPayment(req, function (pay) {
//  console.log(req.body);
//  var args = {
//  MID:10859888,
// Amount:10000 ,
// ResNum:123489524156 ,
// RedirectURL:'www.spintaxi.ir'
// };
// var args = {
// mid:10859888  ,
// amount:req.body.Amount ,
// refid:123489524156 ,
// url:"www.spintaxi.ir"
// };
        console.log(req.query.amount);
        console.log(req.query.token);
        console.log(req.query.customer_id);
        console.log(req.query.app_name);
        console.log(req.body);
        let refId = uniqid();
        let customer_id = req.query.customer_id ;
        let app_name  =req.query.app_name ;
        let ResNum = refId ;
        let amount= req.query.amount * 10;
        console.log(refId);
        console.log('+++-++-*/*-+-*//++-*/-++-//');
        console.log(ResNum);
        console.log('==============================');
        console.log(uniqid());

        var args = {
          mid:'10859888',
          amount: amount  ,
          refid: refId ,
          url:"http://94.232.174.159:8080/customer/paymenttest/"
        };
        wallet_dao.getWalletInfoByCustomerId(customer_id, function (wallet_data) {

          if (!wallet_data) {
            console.log('not found wallet ');
            // customer has no wallet
          } else {
            wallet_dao.saveSamanpreTransaction(customer_id , wallet_data._id, ResNum ,amount, function (save_transaction_data) {

              if (!save_transaction_data) {

                console.log('not found transaction data');
                // customer pay money but error in update data
              } else {

                // do nothing
                console.log("pre  payment is ok");

              }
            });
          }
        });
        console.log(args.mid+' '+args.amount +' '+args.refid  +' '+args.url +' ' +'------------------');

        res.render('payment/payment',{args:args});

      });
    } else {

      res.json({result: {result: false}});
//
    }
  });


  app.post('/customer/paymenttest/',  function (req, res) {

    console.log(req.body);
    let RefNum= req.body.RefNum;
    console.log(RefNum);
    let ResNum =req.body.ResNum;
    let State= req.body.State;
    let StateCode = req.body.StateCode;
    let CID = req.body.CID;
    let TRACENO =req.body.TRACENO;
    let RRN= req.body.RRN;
    let SecurePan = req.body.SecurePan;

    wallet_dao.findOneAndUpdateSamanafterTransaction( ResNum, RefNum  ,State ,StateCode,CID,TRACENO,RRN,SecurePan, function (save_Samantransaction_data) {

      if (!save_Samantransaction_data) {
        // customer pay money but error in update data
      } else {

        // do nothing
        console.log("after  payment is ok");

      }

    });

    if (req.body.State=='OK') {

      var url = "https://sep.shaparak.ir/payments/referencepayment.asmx?WSDL";
      var arg = {String_1:RefNum ,String_2:"10859888"};
      var options ={};
      Soap.createClient(url,options,  function(err, client) {
        var respond ;

        try {

          respond = client.verifyTransaction ;

          respond(arg,  function(err, result) {

            wallet_dao.findispaydSamanPayTransaction ( ResNum, function(findtrue){


              if (findtrue) {
                console.log('im   true ');

              }else {
                console.log(' im not true ');
              }


              console.log(result.result.$value);
              if (result.result.$value > 0  && findtrue  ) {
                let  amount = result.result.$value /8 ;


                wallet_dao.getWalletInfoByCustomerId(findtrue.customer_id, function (wallet_data) {

                  if (!wallet_data) {
                    // customer has no wallet
                  } else {
                    var  param ={
                      amount:amount,
                      ResNum:ResNum,
                    };

                    //  let amount=result.result.$value;

                    wallet_dao.incrementWalletMoney(wallet_data._id, amount, function (update_money_data) {

                      // do nothing

                      res.render('payment/success',{param:param});

                      console.log("ussd payment is ok");
                    });
                  }

                });

                wallet_dao.findOneAndUpdateSamanPayTransaction (ResNum , true, amount, function (save_Samanpaytransaction_data) {

                  if (!save_Samanpaytransaction_data) {
                    // customer pay money but error in update data
                  } else {

                    // do nothing
                    console.log("after  payment is ok");

                  }
                });

              }

              else {

                res.render('payment/fail');


                if (result.result.$value== -18){
                  console.log('ip server eshtebah ast ');

                }
                if(result.result.$value== -18){
                  console.log('');
                }

              }

              console.log('.................................');
              console.log(result);

              console.log('--------------------------------');
            });
          });
        } catch (ex) {
          console.log(ex);
        }

      });

// taxiController.getPayment(req, function (pay) {
//  console.log(req.body);
//         var args = {
//  MID:10859888,
// Amount:10000 ,
// ResNum:123489524156 ,
// RedirectURL:'www.spintaxi.ir'
// };
// var args = {
// mid:10859888  ,
// amount:req.body.Amount ,
// refid:123489524156 ,
// url:"www.spintaxi.ir"
// };
//     console.log(args.mid +' '+args.amount +' '+args.refid  +' '+args.url +' ' +'--------------------------------');
//
// res.render('station/payment',{args:args});
      console.log(req.body);

// });
    }
  });



};
