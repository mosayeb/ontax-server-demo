var customerController = require("../lib/controller/customer_controller");
var taxiController = require("../lib/controller/taxi_controller");
var travelController = require("../lib/controller/travel_controller");
var turnOverController = require("../lib/controller/turnover_controller");
var supportController = require("../lib/controller/support_controller");
var walletController = require("../lib/controller/wallet_controller");
var consts = require("../lib/config/consts");
var aa = require('../lib/aa/aa');

module.exports = function (app) {

  app.post('/taxi/login/', function (req, res) {

    console.log("TAXI LOGIN RESULT");

    taxiController.loginTaxi(req, function (result) {

      console.log(result);

      console.log("TAXI LOGIN RESULT");

      res.json({result: result});

    });

  });

  app.post('/taxi/travelslist/', function (req, res) {

    if (aa.taxiAuthenticate(req)) {

      taxiController.travelsList(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: false});

    }

  });

  app.post('/taxi/setoffline/', function (req, res) {

    if (aa.taxiAuthenticate(req)) {

      taxiController.setTaxiOffline(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: false});

    }

  });

  app.post('/taxi/setonline/', function (req, res) {

    if (aa.taxiAuthenticate(req)) {

      taxiController.setTaxiOnline(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: false});

    }

  });

  app.post('/taxi/travelfinished/', function (req, res) {

    if (aa.taxiAuthenticate(req)) {

      travelController.travelFinished(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: false});

    }

  });

  app.post('/taxi/avatarpath/', function (req, res) {

    if (aa.taxiAuthenticate(req)) {

      taxiController.getTaxiDriverAvatar(req, function (result) {

        res.json({result: result});

      });

    } else {

      res.json({result: false});

    }

  });

  app.post('/taxi/checkdriverupdate/', function (req, res) {

    taxiController.checkAppVersion(req, function (update_status) {

      res.json({result: update_status});

    });

  });

  app.post('/taxi/gettaxiinfo/', function (req, res) {

    console.log(req.body);

      if (aa.taxiAuthenticate(req)) {

          taxiController.getTaxiInfo(req, function (taxi_data) {

            console.log(taxi_data);

              res.json({result: {result: true, taxi_data: taxi_data}});

          });

      } else {

          res.json({result: {result: false}});

      }


  });

  app.post('/taxi/checkdriveriosupdate/', function (req, res) {

      taxiController.checkIosAppVersion(req, function (update_status) {

          res.json({result: update_status});

      });

  });

  app.post('/taxi/updatetaxiinfo/', function (req, res) {

      if (aa.taxiAuthenticate(req)) {

          taxiController.setTaxiInfo(req, function (taxi_data) {

              res.json({result: true});

          });

      } else {

          res.json({result: false});

      }

  });

  app.post('/taxi/info/', function (req, res) {

      if (aa.taxiAuthenticate(req)) {

          taxiController.getTaxiInfo(req, function (taxi_data) {

              res.json({result: {result: true, taxi_data: taxi_data}});

          });

      } else {

          res.json({result: {result: false, taxi_data: null}});

      }

  });

  app.post('/taxi/appopen/', function (req, res) {

      if (aa.taxiAuthenticate(req)) {

          taxiController.getOpenAppInfo(req, function (data) {

              // supportController.getDriverTelegramLink(function (telegram_link) {

                // if(telegram_link) {


                  // res.json({result: {result: true, taxi_data: data.taxi_data, support_data: data.support_phone, banners: data.banners, telegram_link: telegram_link}});
                  res.json({result: {result: true, taxi_data: data.taxi_data, support_data: data.support_phone}});

                // }

              // });

          });

      } else {

          res.json({result: {result: false, taxi_data: null, support_data: data.support_phone, banners: null, telegram_link: null}});

      }

  });

  app.post('/taxi/updateaccountinfo/', function (req, res) {

      if (aa.taxiAuthenticate(req)) {

        console.log(req.body);

          taxiController.updateAccountInfo(req, function (data) {

              res.json({result: true});

          });

      } else {

          res.json({result: false});

      }

  });

  app.post('/taxi/gettravelfactor/', function (req, res) {

    if (aa.taxiAuthenticate(req)) {

        console.log("HERE WHAT WE ATR");

      travelController.getTravelFactor(req, function (travel_data) {

          console.log("HERE WHAT WE ATR 22");

          console.log(travel_data);


          res.json({result: true, travel_data: travel_data});

      });

    } else {

      res.json({result: false, travel_data: null});

    }

  });

  app.post('/taxi/income/', function (req, res) {

    if (aa.taxiAuthenticate(req)) {

      taxiController.getTaxiIncomeInfo(req, function (income_data) {

        res.json({result: true, income_date: income_data});

      });

    } else {

      res.json({result: false, income_date: null});

    }

  });

  app.post('/taxi/gettravelinfo/', function (req, res) {

    if (aa.taxiAuthenticate(req)) {

      travelController.getTravelInfo(req, function (travel_data) {

        res.json({result: true, travel_data: travel_data});

      });

    } else {

      res.json({result: false, travel_data: null});

    }

  });

  app.post('/taxi/transactions/', function (req, res) {

    if (aa.taxiAuthenticate(req)) {

      turnOverController.getTaxiTransactions(req, function (transactions) {

        res.json({result: true, transactions: transactions});

      });

    } else {

      res.json({result: false, transactions: []});

    }

  });


//------------------------------------------------

  app.post('/taxi/paymentrequest/', function (req, res) {
    
        // if (aa.customerAuthenticate(req)) {
          console.log('/taxi/paymentrequest/');
          walletController.savetaxiNewSamanTransaction(req, function (payment_result, transaction_id) {
    
            if (payment_result) {
    
              res.json({
                result: true,
                payment_result: payment_result,
                transaction_id: transaction_id
              });
    
            }
    
            else
              res.json({result: false, payment_result: null, transaction_id: null});
    
          });
    
        // } else {
    
          // res.json({result: false, payment_result: null, transaction_id: null});
    
        // }
    
      });


      app.get('/payment/taxiconfirmation/:taxi_id/:amount/:res_num/', function (req, res) {
        
            var taxi_id = req.params.taxi_id;
            var amount = req.params.amount;
            var res_num = req.params.res_num;
            var bank_amount = parseInt(amount) * 10;
        
            taxiController.getTaxiInfo(req, function (customer_data) {
        
              if(customer_data) {
        
                res.render('payment_confirmation', {customer: customer_data, amount: amount, bank_amount: bank_amount, res_num: res_num, mid: consts.SAMAN_MERCHANT_ID, callback_url: consts.SAMAN_TAXI_CALLBACK_URL})
        
              }
        
            })
        
        
          });

    app.post('/taxi/gettravelinfo/', function (req, res) {

        if (aa.taxiAuthenticate(req)) {

            travelController.getTravelInfo(req, function (travel_data) {

                res.json({result: true, travel_data: travel_data});

            });

        } else {

            res.json({result: false, travel_data: {}});

        }

    });

    app.post('/taxi/havenewtravel/', function (req, res) {

        if (aa.taxiAuthenticate(req)) {

            taxiController.HasTaxiNewTravel(req, function (travel_data) {

                if(travel_data) {

                    res.json({result: true, travel_data: travel_data});

                } else {

                    res.json({result: false, travel_data: {}});

                }

            });

        } else {

            res.json({result: false, travel_data: {}});

        }

    });
        
          app.post('/payment/taxicallback/', function (req, res) {
            
                console.log(req.body);
            
                walletController.gettaxiSamanPaymentResult(req, function (result, payment_info, taxi_data) {
            
                  if(result) {
            
                    res.render('payment_success_test', {customer: taxi_data, payment: payment_info})
            
                  } else {
            
                    res.render('payment_failure')
            
                  }
            
            
                });
            
            
              });


          app.post('/taxi/paymentverification/', function (req, res) {
             console.log(req.body);
            if (aa.taxiAuthenticate(req)) {
    
             walletController.verifytaxiPaymentResult(req, function (taxi_data, transaction_id) {
    
                if (taxi_data)
                  res.json({
                  result: true,
                  taxi_data: taxi_data,
                  transaction_id: transaction_id
               });
                 else
               res.json({result: false, taxi_data: null, transaction_id: null});
    
              });
    
             } else {
    
               res.json({result: false, taxi_data: null, transaction_id: null});
    
             }
    
    
            });
    
};



