var customerController = require("../lib/controller/customer_controller");
var taxiController = require("../lib/controller/taxi_controller");
var travelController = require("../lib/controller/travel_controller");
var supportController = require("../lib/controller/support_controller");
var commentController = require("../lib/controller/comment_controller");
var placeController = require("../lib/controller/place_controller");
var promotionController = require("../lib/controller/promotion_controller");
var reportController = require("../lib/controller/report_controller");
var walletController = require("../lib/controller/wallet_controller");
var station_controller = require("../lib/controller/station_controller");
var turnOverController = require("../lib/controller/turnover_controller");
var taxi_dao = require("../lib/dao/taxi_dao");
var aa = require("../lib/aa/aa");
var utils = require("../lib/tools/utils");
var passport = require('passport');
var Account = require('./../lib/dao/aa_dao').Account;

module.exports = function (app) {

    app.get('/', function (req, res) {
        console.log(req.user);

        if (req.user) {
            if (req.user.account_type == 'admin') {

                reportController.getDashboardAllStatistics(function (result) {

                    if (result) {

                        console.log(result);

                        res.render('dashboard', {stat: result});

                    }

                });

            } else if (req.user.account_type == 'station')
                res.redirect('/station/');
        }

        else {

            res.render('layout')

        }


    });

    /*

     AUTHENTICATION PART

     */

    app.post('/login/', passport.authenticate('local'), function (req, res) {

        res.redirect(req.get('referer'));

    });

    app.post('/register/user/', function (req, res) {

        console.log(req.body);

        var username = req.body.username;
        var password = req.body.password;
        var account_type = req.body.account_type;

        if (account_type == "admin") {

            Account.register(new Account({

                username: username,
                account_type: account_type,
                station_id: null


            }), password, function (err, account) {

                if (err) {

                    return res.render('register', {account: account});

                }

                res.redirect(req.get('referer'));

                // passport.authenticate('local')(req, res, function () {
                //
                //   res.redirect('/');
                //
                // });

            });

        } else if (account_type == "station") {

            station_controller.registerStation(req, function (result, station_id) {

                Account.register(new Account({

                    username: username,
                    account_type: account_type,
                    station_id: station_id


                }), password, function (err, account) {

                    if (err) {

                        console.log(err);

                        return res.render('register', {account: account});

                    }

                    res.redirect(req.get('referer'));

                    // passport.authenticate('local')(req, res, function () {

                    // res.redirect('/station/');

                    // });

                });

            });

        } else if (account_type == "operator") {

            Account.register(new Account({

                username: username,
                account_type: account_type,
                station_id: null


            }), password, function (err, account) {

                if (err) {

                    return res.render('register', {account: account});

                }

                res.redirect(req.get('referer'));

                // passport.authenticate('local')(req, res, function () {

                // res.redirect('/');

                // });

            });

        }
        ;

    });

    app.get('/panel/logout/', function (req, res) {

        req.logout();
        res.redirect('/');

    });

    app.get('/panel/changepassword/', function (req, res) {

        res.render('change_password');

    });

    app.post('/panel/changepassword/', function (req, res) {

        supportController.changePassword(req, function(result){

            if(result) {

              req.logout();
              res.redirect('/');

            }

        });

    });

    /*

     ******** TAXI PART ********

     */

    app.get('/panel/taxi/', function (req, res) {

        res.render('taxi_index');

    });

    app.get('/panel/taxi/add/', function (req, res) {

        station_controller.getAllStations(function (stations) {

            taxi_dao.getAllServiceTypes(function (service_types) {

                res.render('taxi_register', {stations: stations, services: service_types});

            });

        });

    });

    app.get('/panel/taxi/info/:taxi_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var taxi_id = req.params.taxi_id;
        var path = "/panel/taxi/info/" +
            "" + taxi_id + "/";

        taxiController.getTaxiInfo(req, function (taxi_data) {

            taxiController.allTaxiTravels(req, function (travels) {

                taxiController.getTaxiComments(req, function (comments) {

                    taxiController.getTaxiOnlineStats(req, function (taxi_stat) {

                        taxiController.getTaxiIncomeInfo(req, function (income) {

                            if (taxi_stat) {

                                if (taxi_data) {

                                    res.render('taxi_info', {
                                        taxi: taxi_data,
                                        travels: travels,
                                        is_pagination: true,
                                        comments: comments,
                                        taxi_stat: taxi_stat,
                                        income: income,
                                        path: path,
                                        next_page: page + 1,
                                        prev_page: page - 1
                                    })

                                }

                            }

                        });

                    });

                });

            });

        });

    });

    app.get('/panel/taxi/edit/:taxi_id/', function (req, res) {

        taxiController.getTaxiInfo(req, function (taxi_data) {

            station_controller.getAllStations(function (stations) {

                taxi_dao.getAllServiceTypes(function (service_types) {


                    if (taxi_data) {

                        res.render('taxi_edit', {taxi: taxi_data, stations: stations, services: service_types});

                    }

                });

            });

        });

    });

    app.post('/panel/taxi/edit/', function (req, res) {

        taxiController.editTaxi(req, function (result) {

            if (result) {

                res.redirect('/')

            }

        });

    });

    app.get('/panel/taxi/block/:taxi_id', function (req, res) {

        var taxi_id = req.params.taxi_id;

        aa.blockTaxi(taxi_id, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.get('/panel/taxi/free/:taxi_id', function (req, res) {

        var taxi_id = req.params.taxi_id;

        taxiController.setTaxiFree(taxi_id, function (result) {

            if (result) {

                res.redirect('/');

            }

        });

    });

    app.get('/panel/taxi/unblock/:taxi_id', function (req, res) {

        var taxi_id = req.params.taxi_id;

        aa.unblockTaxi(taxi_id, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.post('/panel/taxi/add/', function (req, res) {

        console.log("WE ARE HERE");
        console.log(req.body);

        taxiController.registerTaxi(req, function (result) {

            if (result) {

                res.redirect('/panel/taxi/list/1/10/')

            } else {

                res.redirect('/panel/taxi/list/1/10/')

            }

        });

    });

    app.get('/panel/taxi/list/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);

        taxiController.getTaxisList(req, function (result) {

            if (result) {

                res.render('taxis_list', {
                    taxis: result,
                    next_page: page + 1,
                    prev_page: page - 1
                });

            }

        });

    });

    app.get('/panel/taxi/travels/:taxi_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var taxi_id = req.params.taxi_id;
        var path = "/panel/taxi/travels/" + taxi_id + "/";

        taxiController.travelsList(req, function (travels_list) {

            if (travels_list) {

                res.render('travels_list', {
                    travels: travels_list,
                    path: path,
                    next_page: page + 1,
                    prev_page: page - 1,
                    is_pagination: true
                });

            }

        });

    });


    app.get('/panel/taxi/comments/:taxi_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);

        taxiController.getTaxiComments(req, function (comments) {

            if (comments) {

                res.render('taxi_comments_list', {
                    comments: comments,
                    next_page: page + 1,
                    prev_page: page - 1
                });

            }

        });

    });

    app.post('/panel/taxi/changepassword/', function (req, res) {

        taxiController.changeTaxiPassword(req, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.post('/panel/taxi/updateavatar/', function (req, res) {

        taxiController.updateTaxiAvatar(req, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.post('/panel/taxi/stat/datelist/', function (req, res) {

        taxiController.getTaxiInfo(req, function (taxi_data) {

            taxiController.getTaxiStatByDate(req, function (result) {

                taxiController.getTaxiOnlineStats(req, function (taxi_stat) {

                    if (result) {

                        res.render('taxi_stat', {
                            result: result,
                            taxi_stat: taxi_stat,
                            taxi: taxi_data,
                            start_date: req.body.start_date,
                            finish_date: req.body.finish_date,
                        });

                    }

                });

            });

        });

    });

    app.post('/panel/taxi/search/', function (req, res) {

        taxiController.search(req, function (search_result) {

            if (search_result) {

                res.render('taxis_list', {taxis: search_result})

            }

        });

    });

    /*

     ********** TRAVELS PART *************

     */

    app.get('/panel/travel/', function (req, res) {

        res.render('travel_index');

    });

    app.get('/panel/travel/:travel_id/', function (req, res) {

        supportController.getTravelCompleteInfo(req, function (result) {

            console.log(result);

            res.render('travel_page', {travel_data: result});

        });

    });

    app.post('/panel/travel/search/', function (req, res) {

        travelController.search(req, function (search_result) {

            if (search_result) {

                res.render('travels_list', {
                    travels: search_result,
                    next_page: 1,
                    prev_page: 1,
                    path: "",
                    is_pagination: false
                });

            }

        });

    });

    app.post('/panel/travel/edittravelcost/', function (req, res) {

        travelController.changeTravelCost(req, function (change_cost_result) {

            if (change_cost_result) {


            }

            supportController.getTravelCompleteInfo(req, function (result) {

                res.render('travel_page', {travel_data: result});

            });

        });

    });

    app.get('/panel/travel/list/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var path = '/panel/travel/list/';

        travelController.getTravelsList(req, function (result) {

            if (result) {

                res.render('travels_list', {
                    travels: result,
                    next_page: page + 1,
                    prev_page: page - 1,
                    path: path,
                    is_pagination: true
                });

            }

        });

    });

    app.post('/panel/travel/datelist/', function (req, res) {

        var start_date = req.body.start_date;
        var finish_date = req.body.finish_date;
        start_date = utils.replaceDate(start_date, "/", "_");
        finish_date = utils.replaceDate(finish_date, "/", "_");
        var path = "/panel/travel/datelist/" + start_date + "/" + finish_date + "/1/10/";

        res.redirect(path);

    });

    app.get('/panel/travel/datelist/:start_date/:finish_date/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var url_start_date = req.params.start_date;
        var url_finish_date = req.params.finish_date;

        var path = '/panel/travel/datelist/' + url_start_date + "/" + url_finish_date + "/";

        travelController.getTravelsListByDate(req, function (result) {

            travelController.getTravelsStatusByDate(req, function (travels_stat) {

                if (result) {

                    res.render('travels_list', {
                        travels: result,
                        stat: travels_stat,
                        next_page: page + 1,
                        prev_page: page - 1,
                        path: path,
                        is_pagination: true
                    });


                }

            });

        });

    });

    app.post('/panel/travel/list/filter/', function (req, res) {

      var page = 1;
      var travel_state = req.body.travel_state;
      var path = "/panel/travel/list/filter/" + travel_state + "/";

      travelController.getTravelsListByFilter(req, function (result) {

        if (result) {

          res.render('travels_list', {
            travels: result,
            is_pagination: true,
            path: path,
            next_page: page + 1,
            prev_page: page - 1
          });

        }

      });

    });

    app.get('/panel/travel/list/filter/:travel_state/:page/:per_page/', function (req, res) {

      var page = parseInt(req.params.page);
      var travel_state = req.params.travel_state;
      var path = "/panel/travel/list/filter/" + travel_state + "/";

      travelController.getTravelsListByFilter(req, function (result) {

            if (result) {

                res.render('travels_list', {
                travels: result,
                  is_pagination: true,
                  path: path,
                  next_page: page + 1,
                  prev_page: page - 1

                });

            }

        });

    });

    /*

     ******** CUSTOMERS PART *********

     */

    app.get('/panel/customer/', function (req, res) {

        res.render('customer_index');

    });

    app.get('/panel/customer/info/:customer_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var customer_id = req.params.customer_id;
        var path = "/panel/customer/info/" +
            "" + customer_id + "/";

        customerController.getCustomerInfo(req, function (customer_data) {

            customerController.travelsList(req, function (travels) {

                walletController.getWalletInfo(req, function (wallet_data) {

                    if (customer_data) {

                        res.render('customer_info', {
                            customer: customer_data,
                            travels: travels,
                            is_pagination: true,
                            money: wallet_data.money,
                            path: path,
                            next_page: page + 1,
                            prev_page: page - 1
                        });

                    }

                });

            });

        });

    });

    app.post('/panel/customer/money/set/', function (req, res) {

        walletController.addMoneyToWalletBySupport(req, function (add_money_result) {

            res.redirect(req.get('referer'));

        });

    });

    app.post('/panel/customer/travels/filter/', function (req, res) {

        customerController.travelsListByFilter(req, function (travels) {

            res.render('travels_list', {
                travels: travels,
                is_pagination: false
            });

        });

    });

    app.get('/panel/customer/list/:page/:per_page/', function (req, res) {

        console.log("GET CUSTOMERS LIST");

        var page = parseInt(req.params.page);

        customerController.getCustomersList(req, function (customers_list) {

            console.log("GET CUSTOMER");
            console.log(customers_list);

            if (customers_list) {

                res.render('customers_list', {
                    customers: customers_list,
                    next_page: page + 1,
                    prev_page: page - 1
                });

            }

        });

    });

    app.get('/panel/customer/travels/:customer_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var customer_id = req.params.customer_id;
        var path = "/panel/customer/travels/" + customer_id + "/";

        customerController.travelsList(req, function (travels_list) {

            if (travels_list) {

                res.render('travels_list', {
                    travels: travels_list,
                    path: path,
                    next_page: page + 1,
                    prev_page: page - 1,
                    is_pagination: true
                });

            }

        });

    });

    app.get('/panel/customer/edit/:customer_id/', function (req, res) {

        customerController.getCustomerInfo(req, function (customer_data) {

            if (customer_data) {

                res.render('customer_edit', {customer: customer_data})

            }

        });

    });

    app.post('/panel/customer/edit/', function (req, res) {

        customerController.editCustomer(req, function (result) {

            if (result) {

                res.redirect('/')

            }

        });

    });

    app.get('/panel/customer/block/:customer_id', function (req, res) {

        var customer_id = req.params.customer_id;

        aa.blockCustomer(customer_id, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.get('/panel/customer/unblock/:customer_id', function (req, res) {

        var customer_id = req.params.customer_id;

        aa.unblockCustomer(customer_id, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.post('/panel/customer/search/', function (req, res) {

        customerController.search(req, function (search_result) {

            if (search_result) {

                res.render('customers_list', {customers: search_result})

            }

        });

    });

    app.get('/panel/customer/transaction/:customer_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var customer_id = req.params.customer_id;
        var path = "/panel/customer/transaction/" + customer_id + "/";

        walletController.getCustomerTransactions(req, function (trans_list) {

            if (trans_list) {

                res.render('transactions_list', {
                    trans: trans_list,
                    path: path,
                    next_page: page + 1,
                    prev_page: page - 1,
                    is_pagination: true
                });

            }

        });

    });

    /*

     SUPPORT PART

     */

    app.get('/panel/support/', function (req, res) {

        res.render('support_index');

    });

    app.get('/panel/support/messages/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);

        supportController.getTemplateMessages(function (template_messages) {

            supportController.getCustomerTemplateMessagesList(req, function (messages_list) {

                if (messages_list) {

                    res.render('messages_list', {
                        messages: messages_list,
                        template_messages: template_messages,
                        next_page: page + 1,
                        prev_page: page - 1
                    });

                }

            });

        });

    });

    app.post('/panel/support/message/addtemplatemessage/', function (req, res) {

        supportController.createTemplateMessage(req, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.get('/panel/support/places/:page/:per_page/', function (req, res) {

        placeController.getPlacesList(req, function (places_list) {

            if (places_list) {

                res.render('places_list', {places: places_list});

            }

        });

    });

    app.get('/panel/support/places/add/', function (req, res) {

        res.render('place_add');

    });

    app.post('/panel/support/places/add/', function (req, res) {

        placeController.addNewPlace(req, function (add_place_result) {

            if (add_place_result) {

                res.render('place_add');

            }

        });

    });

    app.get('/panel/support/factors/list/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);

        placeController.getFactorsList(req, function (factors_list) {

            if (factors_list) {

                res.render('factors_list', {
                    factors: factors_list,
                    next_page: page + 1,
                    prev_page: page - 1
                });
                // res.render('factors_list', {factors: factors_list});

            }

        });

    });

    app.get('/panel/support/factors/add/', function (req, res) {

        placeController.addNewFactor(req, function (add_factor_result) {

            placeController.getServiceFactor(function (service_factor) {

                placeController.getAllTimeFactors(function (factors) {

                    placeController.getAllDistanceFactors(function (distance_factors) {

                        supportController.getBaseFactor(function (base_factor) {

                          res.render('factor_add', {

                            service_factor: service_factor,
                            base_factor: base_factor,
                            factors: factors,
                            distance_factors: distance_factors

                          });


                        })
                    });

                });

            });

        });

        // placeController.getAllPlaces(function (places) {
        //
        //   res.render('factor_add', {places: places});
        //
        // });

    });

    app.post('/panel/support/factors/add/', function (req, res) {

        placeController.addNewFactor(req, function (add_factor_result) {

            placeController.getServiceFactor(function (service_factor) {

                placeController.getAllTimeFactors(function (factors) {

                    placeController.getAllDistanceFactors(function (distance_factors) {

                        res.render('factor_add', {
                            service_factor: service_factor,
                            factors: factors,
                            distance_factors: distance_factors
                        });

                    });

                });

            });

        });

    });

    app.post('/panel/support/updatecostfactors/', function (req, res) {

        placeController.updateServiceFactor(req, function (add_factor_result) {

            // placeController.getBaseFactor(function (base_factor) {
            //
            //     placeController.getAllTimeFactors(function (factors) {
            //
            //         res.render('factor_add', {base_factor: base_factor, factors: factors});
            //
            //     });
            //
            // });

            res.redirect('/panel/support/factors/add/');

        });

    });

    app.get('/panel/support/factors/edit/:factor_id/', function (req, res) {

        placeController.getFactorDataById(req, function (factor_data) {

            console.log("WE ARE HERE BABY");
            res.render('factor_edit', {factor: factor_data});

        });

    });

    app.post('/panel/support/factors/edit/', function (req, res) {

        placeController.editFactorCost(req, function (factor_edit_result) {

            if (factor_edit_result) {

                res.redirect('/panel/support/factors/list/1/10/');

            }

        });

    });

    app.get('/panel/support/version/updateversion/', function (req, res) {

        supportController.getAppCurrentVersion(function (current_version) {

            res.render('version_info', {version: current_version});

        });

    });

    app.post('/panel/support/version/updateversion/', function (req, res) {

        supportController.updateAppVersion(req, function (update_version_result) {

            if (update_version_result) {

                res.redirect('/panel/support/version/updateversion/');

            }

        });

    });

    // app.post('/panel/support/driver/updatedrivertelegramlink/', function (req, res) {
    //
    //     supportController.updateDriverTelegramLink(req, function (update_telegram_result) {
    //
    //         if (update_telegram_result) {
    //
    //             res.redirect('/panel/support/driver/updatedriverbanner/');
    //
    //         }
    //
    //     });
    //
    // });

    app.get('/panel/support/updatesupportphone/', function (req, res) {

        supportController.getSupportPhone(function (support_phone) {

            res.render('support_phone', {support_phone: support_phone});

        });

    });

    app.post('/panel/support/updatesupportphone/', function (req, res) {

        supportController.updateSupportPhone(req, function (update_support_result) {

            if (update_support_result) {

              res.redirect(req.get('referer'));

            }

        });

    });

    app.post('/panel/support/updateinputfactor/', function (req, res) {

    supportController.updateInputFactor(req, function (update_input_result) {

      if (update_input_result) {

        res.redirect(req.get('referer'));

      }

    });

  });

    app.post('/panel/support/version/setappdeprecated/', function (req, res) {

        supportController.setAppDeprecated(req, function (set_result) {

            if (set_result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.get('/panel/support/version/updateiosversion/', function (req, res) {

        supportController.getIosCurrentVersion(function (current_version) {

            res.render('version_ios_info', {version: current_version});

        });

    });

    app.post('/panel/support/version/updateiosversion/', function (req, res) {

        supportController.updateIosVersion(req, function (update_version_result) {

            if (update_version_result) {

                res.redirect('/panel/support/version/updateiosversion/');

            }

        });

    });

    app.get('/panel/support/version/updatedriverversion/', function (req, res) {

        supportController.getDriverCurrentVersion(function (current_version) {

            res.render('version_driver_info', {version: current_version});

        });

    });

    app.get('/panel/support/version/updateiosdriverversion/', function (req, res) {

        supportController.getIosDriverCurrentVersion(function (current_version) {

            res.render('version_driver_ios_info', {version: current_version});

        });

    });

    app.post('/panel/support/version/updatedriverversion/', function (req, res) {

        supportController.updateDriverVersion(req, function (update_version_result) {

            if (update_version_result) {

                res.redirect('/panel/support/version/updatedriverversion/');

            }

        });

    });

    app.post('/panel/support/version/updateiosdriverversion/', function (req, res) {

        supportController.updateIosDriverVersion(req, function (update_version_result) {

            if (update_version_result) {

                res.redirect('/panel/support/version/updateiosdriverversion/');

            }

        });

    });

    app.get('/panel/support/notify/', function (req, res) {

        supportController.getAppCurrentNotify(function (current_notify) {

            res.render('notify_info', {notify: current_notify});

        });

    });

    app.post('/panel/support/notify/updatenotify/', function (req, res) {

        supportController.updateAppNotify(req, function (update_notify_result) {

            if (update_notify_result) {

                res.redirect('/panel/support/notify/');

            }

        });

    });

    app.get('/panel/support/export/customer/phones/', function (req, res) {

        supportController.exportCustomerNumbers(function (exported_files) {

            res.setHeader('Content-Type', 'application/vnd.openxmlformats');

            res.setHeader("Content-Disposition", "attachment; filename=" + "Report_Customers.xlsx");

            res.end(exported_files, 'binary');

        });

    });

    app.get('/panel/support/export/taxi/phones/', function (req, res) {

        supportController.exportTaxisNumbers(function (exported_files) {

            res.setHeader('Content-Type', 'application/vnd.openxmlformats');

            res.setHeader("Content-Disposition", "attachment; filename=" + "Report_Taxis.xlsx");

            res.end(exported_files, 'binary');

        });

    });

    /*
     ASYNC PART
     */

    app.post('/panel/async/places/add/', function (req, res) {

        console.log(req.body);

        placeController.addNewPlace(req, function (add_place_result) {

            if (add_place_result) {

                res.json({result: add_place_result});

            }

        });

    });

    app.post('/panel/async/place/edit/', function (req, res) {

        console.log(req.body);

        placeController.updatePlace(req, function (edit_place_result) {

            if (edit_place_result) {

                res.json({result: edit_place_result});

            }

        });

    });

    app.post('/panel/async/place/delete/', function (req, res) {

        console.log(req.body);

        placeController.removePlace(req, function (remove_place_result) {

            if (remove_place_result) {

                res.json({result: remove_place_result});

            }

        });

    });

    app.get('/panel/support/activate/', function (req, res) {

        supportController.getSystemActivated(function (system_active_data) {

            res.render('system_active', {active_data: system_active_data});

        });

    });

    app.post('/panel/support/activate/updatesystemactive/', function (req, res) {

        supportController.setSystemStatus(req, function (active_result) {

            if (active_result) {

                res.redirect('/panel/support/');

            }

        });

    });

    app.post('/panel/async/place/getplaceinfo/', function (req, res) {

        console.log(req.body);

        placeController.getPlaceInfo(req, function (place_info) {

            if (place_info) {

                res.json({result: place_info});

            }

        });

    });

    app.get('/panel/async/places/all/', function (req, res) {

        placeController.getAllPlaces(function (places) {

            console.log(places);

            if (places) {

                res.json({result: places});

            }

        });

    });

    app.get('/panel/async/travel/', function (req, res) {

        supportController.getTravelCompleteInfo(req, function (result) {

            res.json(result);

        });

    });

    app.get('/panel/async/taxi/', function (req, res) {

        taxiController.getTaxiInfo(req, function (result) {

            res.json(result);

        });

    });

    app.get('/panel/async/taxis/', function (req, res) {

        taxiController.getTaxisListAll(req, function (search_result) {

            if (search_result) {

                res.json(search_result);

            }

        });

    });

    app.post('/panel/async/factors/add/', function (req, res) {

        console.log(req.body);

        placeController.addNewFactor(req, function (add_factor_result, state) {

            // placeController.getAllPlaces(function (places) {

            res.json({result: add_factor_result, state: state});

            // res.render('factor_add', {places: places, is_latest_added: add_factor_result});

            // });

        });

    });

    app.post('/panel/travel/cost/set/', function (req, res) {

        console.log(req.body);

        travelController.changeTravelCost(req, function (result) {

            res.redirect(req.get('referer'));

        });

    });

    app.post('/panel/async/promotions/discount_code/addplace/', function (req, res) {

        promotionController.addDiscountPlace(req, function (result) {

            console.log(result);

            res.json({result: result});

        });

    });

    app.post('/panel/async/promotions/discount_code/removeplace/', function (req, res) {

        promotionController.removeDiscountPlace(req, function (result) {

            res.json(result);

        });

    });

    app.get('/panel/async/place/search', function (req, res) {

        placeController.searchPlaceName‌ByPanel(req, function (places) {

            res.jsonp(places);

        });


        // promotionController.removeDiscountPlace(req, function (result) {
        //
        //   res.json(result);
        //
        // });

    });

    app.post('/panel/async/taxi/stat/', function (req, res) {

        taxiController.getDateTaxiOnlineStats(req, function (places) {

            res.jsonp(places);

        });


        // promotionController.removeDiscountPlace(req, function (result) {
        //
        //   res.json(result);
        //
        // });

    });

    app.post('/panel/support/message/removetemplatemessage/', function (req, res) {

        supportController.removeTemplateMessage(req, function (result) {

            res.json(result);

        });

    });

    app.post('/panel/comment/add/', function (req, res) {

        commentController.defineNewComment(req, function (comment_data) {

            res.redirect(req.get('referer'));


        });

    });

    app.get('/panel/comment/remove/:comment_id/', function (req, res) {

        commentController.removeComment(req, function (comment_data) {

            res.redirect(req.get('referer'));

        });

    });

    app.get('/panel/comment/list/', function (req, res) {

        commentController.getComments(req, function (result) {

            res.render('comments_list', {comments: result.comments, is_add: true});

        });

    });

    app.get('/panel/support/promotions/discount_code/new/', function (req, res) {

        placeController.getAllPlaces(function (places) {

            res.render('discount_add', {places: places});

        });

    });

    app.get('/panel/support/promotions/discount_code/edit/:discount_id/', function (req, res) {

        promotionController.getDiscountFullInfo(req, function (discount_data, discount_places) {

            placeController.getAllPlaces(function (places) {

                res.render('discount_edit', {
                    discount: discount_data,
                    discount_places: discount_places,
                    places: places
                });

            });

        });

    });

    app.post('/panel/support/promotions/discount_code/edit/', function (req, res) {

        promotionController.editDiscount(req, function (discount_data) {

            res.redirect('/panel/support/promotions/discount_code/list/1/10/');

        });

    });

    app.get('/panel/support/promotions/discount_code/list/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);

        promotionController.getDiscountsList(req, function (discount_codes) {

            if (discount_codes) {

                res.render('discount_list', {
                    discount_codes: discount_codes,
                    next_page: page + 1,
                    prev_page: page - 1
                });

            }

        });

    });

    app.get('/panel/support/factors/list/:page/:per_page/', function (req, res) {

        placeController.getFactorsList(req, function (factors_list) {

            if (factors_list) {

                res.render('factors_list', {
                    factors: factors_list,
                    next_page: page + 1,
                    prev_page: page - 1
                });

            }

        });

    });

    app.post('/panel/support/promotions/discount_code/new/', function (req, res) {

        promotionController.newDiscount(req, function (discount_data) {

            placeController.getAllPlaces(function (places) {

                res.render('discount_add', {places: places});

            });

        });

    });

    /*

     Reports

     */

    app.get('/panel/report/getallstat/', function (req, res) {

        reportController.getAllStatistics(function (result) {

            if (result) {

                res.render('report_stat', {stat: result});

            }

        });

    });

    app.get('/panel/report/getfinancereport/', function (req, res) {

        turnOverController.getTurnOverStat(req, function (data) {

            taxiController.getTaxiCredits(function (taxis) {

                res.render('report_finance', {data: data, taxis: taxis});

            });

        })


    });

    app.post('/panel/report/getfinancereport/', function (req, res) {

        var start_date = req.body.start_date;
        var finish_date = req.body.finish_date;

        reportController.getFinanceReport(req, function (report_data) {

            if (report_data) {

                res.render('report_finance', {
                    report_data: report_data,
                    finish_date: finish_date,
                    start_date: start_date
                });

            }

        });

    });

    app.post('/panel/report/topdrivers/', function (req, res) {

        var start_date = req.body.start_date;
        var finish_date = req.body.finish_date;
        start_date = utils.replaceDate(start_date, "/", "_");
        finish_date = utils.replaceDate(finish_date, "/", "_");
        var path = "/panel/report/topdrivers/" + start_date + "/" + finish_date + "/";

        res.redirect(path);

    });

    app.get('/panel/report/topdrivers/:start_date/:finish_date/', function (req, res) {

        var url_start_date = req.params.start_date;
        var url_finish_date = req.params.finish_date;
        var start_date = utils.replaceDate(url_start_date, "ـ", "/");
        var finish_date = utils.replaceDate(url_finish_date, "ـ", "/");

        console.log("WE ARE HERE BOY");

        reportController.getTopDriversIncomeByDate(req, function (taxis) {

            if (taxis) {

                res.render('report_top_drivers', {

                    taxis: taxis,
                    start_date: start_date,
                    finish_date: finish_date

                });

            }

        });

    });

    app.get('/panel/report/taxis/', function (req, res) {

        res.render('report_taxis');


    });

    app.post('/panel/taxi/setfree/', function (req, res) {

        var taxi_id = req.body.taxi_id;

        taxiController.setTaxiFree(taxi_id, function (result) {

            res.redirect(req.get('referer'));

        });

    });

    app.post('/panel/support/factors/time/set/', function (req, res) {

        placeController.setTimeFactor(req, function (result) {

            res.redirect('/panel/support/factors/add/');

        });

    });

    app.post('/panel/support/factors/distance/set/', function (req, res) {

        placeController.setDistanceFactor(req, function (result) {

            res.redirect('/panel/support/factors/add/');

        });

    });

    app.post('/panel/restore/', function (req, res) {

        supportController.restoreData(req, function (result) {

            res.redirect(req.get('referer'));

        });

    });

    app.get('/panel/restore/', function (req, res) {

        supportController.backupLatestList(function (back_up_list) {

            if (back_up_list) {

                res.render('restore', {backups: back_up_list});

            }

        });

    });

    /*

     STATIONS PART

     */

    app.get('/panel/station/add/', function (req, res) {

        res.render('station_add');

    });

    app.get('/panel/station/list/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);

        station_controller.getStationsList(req, function (result) {

            if (result) {

                res.render('station_list', {
                    stations: result,
                    next_page: page + 1,
                    prev_page: page - 1
                });

            }

        });

    });

    app.get('/panel/station/travels/:station_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var per_page = parseInt(req.params.per_page);
        var station_id = req.params.station_id;
        var path = "/panel/station/travels/" + station_id + "/";

        travelController.getStationTravelsList(station_id, page, per_page, function (travels_list) {

            if (travels_list) {

                res.render('travels_list', {
                    travels: travels_list,
                    path: path,
                    next_page: page + 1,
                    prev_page: page - 1,
                    is_pagination: true
                });

            }

        });

    });

    app.get('/panel/station/taxis/:station_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var per_page = parseInt(req.params.per_page);
        var station_id = req.params.station_id;
        var path = "/panel/station/taxis/" + station_id + "/";

        taxiController.getStationTaxisList(station_id, page, per_page, function (taxis_list) {

            if (taxis_list) {

                res.render('taxis_list', {
                    taxis: taxis_list,
                    path: path,
                    next_page: page + 1,
                    prev_page: page - 1,
                    is_pagination: true
                });

            }

        });

    });

    app.get('/panel/station/:station_id/', function (req, res) {

        var station_id = req.params.station_id;

        station_controller.getStationData(station_id, function (station_data) {

            if (station_data) {

                res.render('station_info', {
                    station: station_data
                });

            }

        });

    });

    app.post('/panel/station/edit/', function (req, res) {

        station_controller.editStation(req, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.post('/panel/station/changepassword/', function (req, res) {

        station_controller.changePassword(req, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.get('/panel/station/financestat/:station_id/', function (req, res) {

        var station_id = req.params.station_id;

        station_controller.getStationData(station_id, function (station_data) {

          turnOverController.getStationFinanceStat(req, function (data) {

            if (data) {

              taxiController.getStationTaxisCredit(req, function (taxis) {

                res.render('station_report_finance', {data: data, taxis: taxis, station: station_data});

              });

            }

          });

        });

    });

    app.get('/panel/station/turnoverlist/:station_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var per_page = parseInt(req.params.per_page);
        var station_id = req.params.station_id;
        var path = "/panel/station/turnoverlist/" + station_id + "/";

        turnOverController.getStationTaxisTransactions(req, function (transactions) {

          if (transactions) {

            res.render('station_turnover_list', {
              transactions: transactions,
              path: path,
              next_page: page + 1,
              prev_page: page - 1,
              is_pagination: true
            });

          }

        });

    });

    // end of station part

    app.post('/panel/customer/phone/search/', function (req, res) {

        customerController.phoneSearch(req, function (search_result) {

            if (search_result) {

                res.render('customers_list', {customers: search_result})

            }

        });

    });

    // service part

    app.get('/panel/support/service/add/', function (req, res) {

        res.render('service_add');

    });

    app.post('/panel/support/service/add/', function (req, res) {

        supportController.createNewServiceType(req, function (result) {

            res.redirect('/panel/support/service/list/');

        })

    });

    app.get('/panel/support/service/edit/:service_id/', function (req, res) {

        supportController.getServiceTypeData(req, function (service_data) {

            res.render('service_edit', {service: service_data});

        });

    });

    app.post('/panel/support/service/edit/', function (req, res) {

        supportController.updateServiceType(req, function (result) {

            if (result) {

                res.redirect('/panel/support/service/list/');

            }

        });

    });

    app.get('/panel/support/service/list/', function (req, res) {

        supportController.getAllServiceTypes(function (result) {

            res.render('service_list', {service_types: result});

        });

    });

    app.get('/panel/turnover/list/:page/:per_page', function (req, res) {

        turnOverController.getTurnOverList(req, function (result) {

            var page = parseInt(req.params.page);
            var path = "/panel/turnover/list/";

            res.render('turn_over_list', {
                turn_overs: result,
                path: path,
                next_page: page + 1,
                prev_page: page - 1,
                is_pagination: true
            });

        });

    });

    app.get('/panel/taxi/turnoverlist/:taxi_id/:page/:per_page', function (req, res) {

        turnOverController.getTaxiTransactions(req, function (turn_overs) {

            var page = parseInt(req.params.page);
            var taxi_id = req.params.taxi_id;
            var path = "/panel/taxi/turnoverlist/" + taxi_id + "/";

            taxiController.getTaxiIncomeInfo(req, function (income) {

                taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                    res.render('turn_over_taxi_list', {
                        turn_overs: turn_overs,
                        path: path,
                        income: income,
                        taxi: taxi_data,
                        next_page: page + 1,
                        prev_page: page - 1,
                        is_pagination: true
                    });

                });

            });


        });

    });

    // get taxi money info

    app.get('/panel/taxi/income/:taxi_id/', function (req, res) {

        turnOverController.getTaxiTransactions(req, function (turn_overs) {

            var page = parseInt(req.params.page);
            var taxi_id = req.params.taxi_id;
            var path = "/panel/taxi/turnoverlist/" + taxi_id + "/";

            taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                res.render('turn_over_taxi_list', {
                    turn_overs: turn_overs,
                    path: path,
                    taxi: taxi_data,
                    next_page: page + 1,
                    prev_page: page - 1,
                    is_pagination: true
                });

            });

        });

    });

    app.get('/panel/taxi/ponylist/:taxi_id/:page/:per_page', function (req, res) {

        turnOverController.getTaxiPonies(req, function (ponies) {

            var page = parseInt(req.params.page);
            var taxi_id = req.params.taxi_id;
            var path = "/panel/taxi/ponylist/" + taxi_id + "/";

            taxiController.getTaxiIncomeInfo(req, function (income) {

                res.render('pony_taxi_list', {
                    ponies: ponies,
                    income: income,
                    path: path,
                    next_page: page + 1,
                    prev_page: page - 1,
                    is_pagination: true
                });

            });


        });

    });

    app.get('/panel/pony/list/:page/:per_page', function (req, res) {

        turnOverController.getPonyList(req, function (ponies) {

            var page = parseInt(req.params.page);
            var path = "/panel/pony/list/";

                res.render('pony_list', {
                    ponies: ponies,
                    path: path,
                    next_page: page + 1,
                    prev_page: page - 1,
                    is_pagination: true
                });

        });

    });

    app.post('/panel/taxi/pony/createpony/', function (req, res) {

        turnOverController.createTaxiPony(req, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.get('/panel/monitor/livetaxi/', function (req, res) {

        res.render('monitor_live_taxis')

    });


    app.get('/panel/report/canceledtravel', function (req, res) {

        reportController.getCanceledTravels(req , function (error, result) {

            if (error)
            {
                res.json(error);
                return
            }
            res.render("report_canceled_travel" , {data : result});

        });


    });

    app.post('/panel/support/factors/base/set/', function (req, res) {

        placeController.updateBaseFactor(req, function (add_factor_result) {

            // placeController.getBaseFactor(function (base_factor) {
            //
            //     placeController.getAllTimeFactors(function (factors) {
            //
            //         res.render('factor_add', {base_factor: base_factor, factors: factors});
            //
            //     });
            //
            // });

            res.redirect('/panel/support/factors/add/');

        });

    });

    app.get('/panel/report/toptaxisrefusedtravel', function (req, res) {

        reportController.getTopTaxisRefusedTravels(req , function (error, result) {

            if (error)
            {
                res.json(error);
                return
            }
            res.render("report_top_refused_drivers" , {data : result});

        });


    });

    app.get('/panel/report/toptaxisstandbytravel', function (req, res) {

        reportController.getTopTaxisStandByTravels(req , function (error, result) {

            if (error)
            {
                res.json(error);
                return
            }
            res.render("report_top_standby_drivers" , {data : result});

        });


    });

    app.get('/panel/taxi/travels/refused', function (req, res) {



        travelController.getDriverRefusedTravelsListByDate(req , function (result) {

            if (result)

            {
                res.render('travels_list', {
                    travels: result,
                    path: "",
                    next_page: 1,
                    prev_page: 1,
                    is_pagination: false
                });
            }

        });


    });

    app.get('/panel/taxi/travels/standby', function (req, res) {

        travelController.getDriverStandByTravelsListByDate(req , function (result) {

            if (result)

            {
                res.render('travels_list', {
                    travels: result,
                    path: "",
                    next_page: 1,
                    prev_page: 1,
                    is_pagination: false
                });

            }

        });


    });

};


