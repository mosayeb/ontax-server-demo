var customerController = require("../lib/controller/customer_controller");
var taxiController = require("../lib/controller/taxi_controller");
var travelController = require("../lib/controller/travel_controller");
var supportController = require("../lib/controller/support_controller");
var commentController = require("../lib/controller/comment_controller");
var placeController = require("../lib/controller/place_controller");
var promotionController = require("../lib/controller/promotion_controller");
var reportController = require("../lib/controller/report_controller");
var walletController = require("../lib/controller/wallet_controller");
var stationController = require("../lib/controller/station_controller");
var turnOverController = require("../lib/controller/turnover_controller");
var aa_js = require("../lib/aa/aa.js")
var aa = require("../lib/tools/aa");
var utils = require("../lib/tools/utils");
var passport = require('passport');
var Account = require('./../lib/dao/aa_dao').Account;
var taxi_dao = require('./../lib/dao/taxi_dao');


module.exports = function (app) {

  /*             start  borna panels                */


    app.post('/station/login/',passport.authenticate('local'),  function (req, res) {
        if(req.user) {
            stationController.getStationData(req.user.station_id,function(result){
                if(result)
                {
                    var token = aa_js.createNewToken(req.user.station_id);
                    var result ={result: true, station_data: {_id:req.user.station_id,token:token,title:result.title}};
                    res.json(result);
                }
                else{
                    res.json({result: false, station_data: null});
                }
            });


        } else {

            rea.json({result: false, station_data: null});

        }

    });

    app.post('/station/stationregister/', function (req, res) {

        stationController.registerStation(req, function (result) {

            res.json({result: result});

        });

    });

    app.post('/station/registerstationtaxi/', function (req, res) {

        if(aa_js.stationAuthenticate(req))
        {
            console.log('true');
            taxiController.registerStationTaxi(req, function (result) {

                res.json({result: result});

            });
        }


    });

    app.post('/station/registerstationsubscriber/', function (req, res) {

        if(aa_js.stationAuthenticate(req))
        {
            stationController.registerStationSubscriber(req, function (result) {

                res.json({result: result});

            });
        }


    });

    app.post('/station/getsubscriberinfo',function(req,res){
        if(aa_js.stationAuthenticate(req))
        {
            customerController.getCustomerInfoBySubscriberNum(req, function (result) {
                if(result)
                {
                    res.json({result:true, subscriber_info:result})
                }
                 else res.json({result:false, subscriber_info:null})

            });
        }
    });

    app.post('/station/getstationtaxis/', function (req, res) {

        if(aa_js.stationAuthenticate(req))
        {
            console.log(req.body);
            taxiController.getStationTaxisList(req.body.station_id,req.body.page,req.body.per_page, function (result) {

                res.json({result: result});

            });
        }


    });

    app.post('/station/resendcode/', function (req, res) {

        stationController.updateCode(req, function (result) {

            res.json({result: result});

        });

    });

    app.post('/station/codeverification/', function (req, res) {

        stationController.verifyCode(req, function (result) {

            res.json({result: result});

        });

    });

    app.post('/station/calculatetravelcost/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            stationController.calculateTravelCost(req, function (result) {

                res.json({result: result});

            });

        } else {

            res.json({result: {result: false}});

        }

    });


    app.post('/station/savetravel/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            aa_js.isStationValid(req.body.station_id, function (is_station_valid) {

                if (is_station_valid) {
                    console.log("after validation");
                    travelController.createStationTravel(req, function (result) {

                        res.json({result: result});

                    });

                } else {

                    res.json({result: {result: false, message: "نام کاربری شما مسدود شده است. شما نمی‌توانید از این سرویس استفاده نمیایید."}});

                }

            });



        } else {

            res.json({result: {result: false, message: "لطفا از برنامه خارج و دوباره وارد شوید."}});

        }

    });

    ///////////////////////////////
    /*

        REPORTS

     */
    //////////////////////////////

    app.get('/station/report/getfinancereport/', function (req, res) {

        var station_id = req.user.station_id;

        turnOverController.getStationTurnOverStat(req, function (data) {

            taxiController.getStationTaxisCredit(station_id, function (taxis) {

                res.render('station/report_finance', {data: data, taxis: taxis});

            });

        })


    });

    app.post('/station/travelslist/', function (req, res) {
        console.log("station services list comes");
        if (aa_js.stationAuthenticate(req)) {
            console.log("station services list comes after authentication");
            stationController.travelsList(req, function (result) {
                res.json({result: result});

            });

        } else {

            res.json({result: false});

        }

    });

    app.post('/station/updateinfo/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            stationController.updateInfo(req, function (result) {

                res.json({result: result});

            });

        } else {

            res.json({result: false});

        }

    });

    app.post('/station/updateavatar/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            stationController.updateAvatar(req, function (result) {

                res.json({result: result});

            });

        } else {

            res.json({result: {result: false, avatar: null}});

        }

    });

    app.post('/station/getinfo/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            stationController.getStationInfo(req, function (result) {

                res.json({result: result});

            });

        } else {

            res.json({result: false});

        }

    });

    app.post('/station/sendmessage/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            supportController.saveStationTemplateMessage(req, function (result) {

                res.json({result: result});

            });

        } else {

            res.json({result: false});

        }

    });

    app.post('/station/gettemplatemessages/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            supportController.getTemplateMessages(function (messages) {

                res.json({result: true, messages: messages});

            });

        } else {

            res.json({result: false, messages: null});

        }

    });

    app.post('/station/checkappupdate/', function (req, res) {

        stationController.checkAppVersion(req, function (update_status) {

            res.json({result: update_status});

        });

    });

    // get place name by location

    app.post('/station/getlocationplacename/', function (req, res) {

        console.log("GET LOCATIOPN PLACE NAME");
        console.log(req.body);
        if (aa_js.stationAuthenticate(req)) {
            placeController.getLocationPlaceName(req, function (place_name) {

                console.log(place_name);

                res.json({result: place_name});

            });

        } else {

            res.json({result: {result: false}});

        }

    });

    // search place

    app.post('/station/searchplace/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            placeController.searchPlaceName(req, function (places) {

                res.json({result: {result: true, places: places}});

            });

        } else {

            res.json({result: {result: false, places: null}});

        }

    });

    // get station taxis around station

    app.post('/station/getstationaroundtaxis/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            stationController.getStationAroundTaxis(req, function (taxis_list) {

                res.json({result: taxis_list});

            });

        } else {

            res.json({result: {result: false}});

        }

    });

    // get taxis around station

    app.post('/station/getaroundtaxis/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            stationController.getAroundTaxis(req, function (taxis_list) {

                res.json({result: taxis_list});

            });

        } else {

            res.json({result: {result: false}});

        }

    });

    // rate to travel

    app.post('/station/ratetravel/', function (req, res) {

        console.log(req.body);

        if (aa_js.stationAuthenticate(req)) {

            travelController.travelCommented(req, function (rate_travel) {

                res.json({result: rate_travel});

            });

        } else {

            res.json({result: false});

        }

    });

    app.post('/station/comments/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            commentController.getComments(req, function (rate_travel) {

                res.json({result: rate_travel});

            });

        } else {

            res.json({result: {result: false}});

        }

    });

    // payment request

    app.post('/station/paymentrequest/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            walletController.addMoneyToStationWallet(req, function (payment_result, transaction_id) {

                console.log("WE ARE IN PAYMENT RESULT");
                console.log(payment_result);

                if (payment_result) {

                    console.log("IN CALL BACK");

                    res.json({
                        result: true,
                        payment_result: payment_result,
                        transaction_id: transaction_id
                    });

                }

                else
                    res.json({result: false, payment_result: null, transaction_id: null});

            });

        } else {

            res.json({result: false, payment_result: null, transaction_id: null});

        }

    });

    // ipg payment result

    app.get('/station/paymentresult/ipg/', function (req, res) {


        walletController.getIpgPaymentResult(req, function (result) {

            res.sendFile(path.join(__dirname + '/../views/payment_success.html'));

        });

    });

    // ussd payment result

    app.get('/station/paymentresult/ussd/', function (req, res) {

        console.log("USSD PAYMENT RESULT");

        walletController.getUssdPaymentResult(req, res);

    });

    // payment verification

    app.post('/station/paymentverification/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            walletController.verifyPaymentResult(req, function (wallet_data, transaction_id) {

                if (wallet_data)
                    res.json({
                        result: true,
                        wallet_data: wallet_data,
                        transaction_id: transaction_id
                    });
                else
                    res.json({result: false, wallet_data: null, transaction_id: null});

            });

        } else {

            res.json({result: false, wallet_data: null, transaction_id: null});

        }


    });

    // get transactions

    app.post('/station/transactions/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            walletController.getStationTransactions(req, function (result) {

                res.json({result: true, transactions: result});

            });
        } else {

            res.json({result: false});

        }

    });

    // get wallet data

    app.post('/station/wallet/', function (req, res) {

        console.log("/station/wallet/");
        console.log(req.body);


        if (aa_js.stationAuthenticate(req)) {

            walletController.getStationWalletInfo(req, function (result) {

                if (result)
                    res.json({result: true, wallet_data: result});
                else
                    res.json({result: false, wallet_data: null});

            });

        } else {

            res.json({result: false, wallet_data: null});

        }

    });

    // verify discount code

    app.post('/station/discountverification/', function (req, res) {

        if (aa_js.stationAuthenticate(req)) {

            promotionController.checkDiscount(req, function (result) {

                if (result)
                    res.json({result: true, discount_amount: result});
                else
                    res.json({result: false, discount_amount: null});

            });

        } else {

            res.json({result: false, discount_amount: null});

        }
    });

    app.get('/station/searchPN',function(req,res){
        placeController.searchPlaceName‌ByPanel(req,function(result){
            res.json(result);
        });
    });

    app.post('/station/getPlaceInfo',function(req,res){
        console.log('place location '+req.body.place_id );
        placeController.getPlaceInfo(req,function(result){
            console.log('location resives');
            res.json(result);
        });
    });

    app.post('/station/getSubscribers',function(req,res){

        if(aa_js.stationAuthenticate(req))
        {
            console.log(req.body);
            customerController.getStationSubscriberList(req.body.station_id,req.body.page,req.body.per_page, function (result) {

                res.json({result: result});

            });
        }
    });

    /*             end  borna panels                */

    app.get('/station/', function (req, res) {

        // if(req.user)
        //     if(req.user.is_admin)
        //         res.render('layout');
        //
        // else

        res.render('station/layout');

        // reportController.getAllStatistics(function (result) {
        //
        //   if (result) {
        //
        //     res.render('station/dashboard', {stat: result});
        //
        //   }
        //
        // });

    });

    app.get('/panel/taxi/info/:taxi_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var taxi_id = req.params.taxi_id;
        var path = "/panel/taxi/info/" +
            "" + taxi_id + "/";

        taxiController.getTaxiInfo(req, function (taxi_data) {

            taxiController.travelsList(req, function (travels) {

                taxiController.getTaxiComments(req, function (comments) {

                    taxiController.getTaxiOnlineStats(req, function (taxi_stat) {

                        if(taxi_stat) {

                            if (taxi_data) {

                                res.render('taxi_info', {
                                    taxi: taxi_data,
                                    travels: travels,
                                    is_pagination: true,
                                    comments: comments,
                                    path: path,
                                    next_page: page + 1,
                                    prev_page: page - 1
                                })

                            }

                        }

                    });

                });

            });

        });

    });

    app.get('/station/taxi/block/:taxi_id', function (req, res) {

        var taxi_id = req.params.taxi_id;

        aa.blockTaxi(taxi_id, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.get('/station/taxi/free/:taxi_id', function (req, res) {

        var taxi_id = req.params.taxi_id;

        taxiController.setTaxiFree(taxi_id, function (result) {

            if (result) {

                res.redirect('/');

            }

        });

    });

    app.get('/station/taxi/unblock/:taxi_id', function (req, res) {

        var taxi_id = req.params.taxi_id;

        aa.unblockTaxi(taxi_id, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.get('/station/taxi/list/:page/:per_page/', function (req, res) {

        if(req.user) {

            var page = parseInt(req.params.page);
            var per_page = parseInt(req.params.per_page);
            var station_id = req.user.station_id;

            taxiController.getStationTaxisList(station_id, page, per_page, function (result) {

                if (result) {

                    res.render('station/taxis_list', {
                        taxis: result,
                        next_page: page + 1,
                        prev_page: page - 1
                    });

                }

            });

        } else {

            res.redirect('/');

        }

    });

    app.get('/station/async/taxis/', function (req, res) {

        console.log(req);

        var station_id = req.user.station_id;

        taxiController.getStationTaxisListAll(station_id, function (search_result) {

            if (search_result) {

                res.json(search_result);

            }

        });

    });

    app.get('/station/travel/list/:page/:per_page/', function (req, res) {

        if(req.user) {

            var page = parseInt(req.params.page);
            var per_page = parseInt(req.params.per_page);
            var station_id = req.user.station_id;
            var path = '/station/travel/list/';

            travelController.getStationTravelsList(station_id, page, per_page, function (result) {

                if (result) {

                    res.render('station/travels_list', {
                        travels: result,
                        next_page: page + 1,
                        prev_page: page - 1,
                        path: path,
                        is_pagination: true,
                        user: req.user
                    });

                }

            });

        } else {

            res.redirect('/');

        }



    });

    app.get('/station/taxi/info/:taxi_id/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var taxi_id = req.params.taxi_id;
        var path = "/station/taxi/info/" +
            "" + taxi_id + "/";

        taxiController.getTaxiInfo(req, function (taxi_data) {

            taxiController.travelsList(req, function (travels) {

                taxiController.getTaxiComments(req, function (comments) {

                    taxiController.getTaxiOnlineStats(req, function (taxi_stat) {

                        if(taxi_stat) {

                            if (taxi_data) {

                                res.render('station/taxi_info', {
                                    taxi: taxi_data,
                                    travels: travels,
                                    is_pagination: true,
                                    comments: comments,
                                    taxi_stat: taxi_stat,
                                    path: path,
                                    next_page: page + 1,
                                    prev_page: page - 1
                                })

                            }

                        }

                    });

                });

            });

        });

    });

    app.get('/station/taxi/edit/:taxi_id/', function (req, res) {

        taxiController.getTaxiInfo(req, function (taxi_data) {

            console.log(taxi_data);

            res.render('station/taxi_edit', {taxi: taxi_data});

        });

    });

    app.post('/station/taxi/edit/', function (req, res) {

        taxiController.editTaxi(req, function (result) {

            if (result) {

                res.redirect('/')

            }

        });

    });

    app.post('/station/taxi/changepassword/', function (req, res) {

        taxiController.changeTaxiPassword(req, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.post('/station/taxi/updateavatar/', function (req, res) {

        taxiController.updateTaxiAvatar(req, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.post('/station/travel/datelist/', function (req, res) {

        var start_date = req.body.start_date;
        var finish_date = req.body.finish_date;
        start_date = utils.replaceDate(start_date, "/", "_");
        finish_date = utils.replaceDate(finish_date, "/", "_");
        var path = "/station/travel/datelist/" + start_date + "/" + finish_date + "/1/10/";

        res.redirect(path);

    });

    app.get('/station/travel/datelist/:start_date/:finish_date/:page/:per_page/', function (req, res) {

        var page = parseInt(req.params.page);
        var url_start_date = req.params.start_date;
        var url_finish_date = req.params.finish_date;

        var path = '/station/travel/datelist/' + url_start_date + "/" + url_finish_date + "/";

        travelController.getStationTravelsListByDate(req, function (result) {

            if (result) {

                res.render('station/travels_list', {
                    travels: result,
                    next_page: page + 1,
                    prev_page: page - 1,
                    path: path,
                    is_pagination: true,
                    user: req.user
                });

            }

        });

    });

    app.post('/station/travel/list/filter/', function (req, res) {

        travelController.getStationTravelsListByFilter(req, function (result) {

            if (result) {

                res.render('station/travels_list', {travels: result});

            }

        });

    });

    app.get('/station/travel/:travel_id/', function (req, res) {

        supportController.getTravelCompleteInfo(req, function (result) {

            res.render('station/travel_page', {travel_data: result});

        });

    });

    app.post('/station/travel/cost/set/', function (req, res) {

        travelController.changeTravelCost(req, function (result) {

            res.redirect(req.get('referer'));

        });

    });

    app.post('/station/taxi/setfree/', function (req, res) {

        var taxi_id = req.body.taxi_id;

        taxiController.setTaxiFree(taxi_id, function (result) {

            res.redirect(req.get('referer'));

        });

    });

    app.get('/station/edit/', function (req, res) {

        var station_id = req.user.station_id;

        stationController.getStationData(station_id, function (station_data) {

            if (station_data) {

                res.render('station/station_info', {
                    station: station_data
                });

            }

        });


    });

    app.post('/station/edit/', function (req, res) {

        stationController.editStation(req, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.post('/station/changepassword/', function (req, res) {

        stationController.changePassword(req, function (result) {

            if (result) {

                res.redirect(req.get('referer'));

            }

        });

    });

    app.get('/station/taxi/add/', function (req, res) {

        var station_id = req.user.station_id;

        stationController.getStationData(station_id, function (station_data) {

            taxi_dao.getAllServiceTypes(function (service_types) {

              res.render('station/taxi_register', {station: station_data, services: service_types});

            });

        });

    });

    app.post('/station/taxi/add/', function (req, res) {

        taxiController.registerTaxi(req, function (result) {

            if (result) {

                res.redirect('/station/taxi/list/1/10/')

            } else {

                res.redirect('/station/taxi/list/1/10/')

            }

        });

    });


};



