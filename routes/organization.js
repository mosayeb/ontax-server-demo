var path = require("path");
var organizationController = require("../lib/controller/organization_controller");
var supportController = require("../lib/controller/support_controller");
var taxiController = require("../lib/controller/taxi_controller");
var placeController = require("../lib/controller/place_controller");
var walletController = require("../lib/controller/wallet_controller");
var commentController = require("../lib/controller/comment_controller");
var travelController = require("../lib/controller/travel_controller");
var promotionController = require("../lib/controller/promotion_controller");
var aa = require("../lib/aa/aa");

module.exports = function (app) {

    app.post('/organization/login/', function (req, res) {

        organizationController.loginOrganization(req, function (result) {

            res.json({result: result});

        });

    });
    app.post('/organization/organizationregister/', function (req, res) {

        organizationController.registerOrganization(req, function (result) {

            res.json({result: result});

        });

    });

    app.post('/organization/registerorganizationtaxi/', function (req, res) {

        if(aa.organizationAuthenticate(req))
        {
            taxiController.registerOrganizationTaxi(req, function (result) {

                res.json({result: result});

            });
        }


    });

    app.post('/organization/getorganizationtaxis/', function (req, res) {

        if(aa.organizationAuthenticate(req))
        {
            taxiController.getorganizationtaxis(req, function (result) {

                res.json({result: result});

            });
        }


    });

    app.post('/organization/resendcode/', function (req, res) {

        organizationController.updateCode(req, function (result) {

            res.json({result: result});

        });

    });

    app.post('/organization/codeverification/', function (req, res) {

        organizationController.verifyCode(req, function (result) {

            res.json({result: result});

        });

    });

    app.post('/organization/calculatetravelcost/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            organizationController.calculateTravelCost(req, function (result) {

                res.json({result: result});

            });

        } else {

            res.json({result: {result: false}});

        }

    });


    app.post('/organization/savetravel/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            aa.isOrganizationValid(req.body.organization_id, function (is_organization_valid) {

                if (is_organization_valid) {
                    console.log("after validation");
                    travelController.createOrganizationTravel(req, function (result) {

                        res.json({result: result});

                    });

                } else {

                    res.json({result: {result: false, message: "نام کاربری شما مسدود شده است. شما نمی‌توانید از این سرویس استفاده نمیایید."}});

                }

            });



        } else {

            res.json({result: {result: false, message: "لطفا از برنامه خارج و دوباره وارد شوید."}});

        }

    });

    app.post('/organization/travelslist/', function (req, res) {
        console.log("organization services list comes");
        if (aa.organizationAuthenticate(req)) {
            console.log("organization services list comes after authentication");
            organizationController.travelsList(req, function (result) {
                res.json({result: result});

            });

        } else {

            res.json({result: false});

        }

    });

    app.post('/organization/updateinfo/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            organizationController.updateInfo(req, function (result) {

                res.json({result: result});

            });

        } else {

            res.json({result: false});

        }

    });

    app.post('/organization/updateavatar/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            organizationController.updateAvatar(req, function (result) {

                res.json({result: result});

            });

        } else {

            res.json({result: {result: false, avatar: null}});

        }

    });

    app.post('/organization/getinfo/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            organizationController.getOrganizationInfo(req, function (result) {

                res.json({result: result});

            });

        } else {

            res.json({result: false});

        }

    });

    app.post('/organization/sendmessage/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            supportController.saveOrganizationTemplateMessage(req, function (result) {

                res.json({result: result});

            });

        } else {

            res.json({result: false});

        }

    });

    app.post('/organization/gettemplatemessages/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            supportController.getTemplateMessages(function (messages) {

                res.json({result: true, messages: messages});

            });

        } else {

            res.json({result: false, messages: null});

        }

    });

    app.post('/organization/checkappupdate/', function (req, res) {

        organizationController.checkAppVersion(req, function (update_status) {

            res.json({result: update_status});

        });

    });

    // get place name by location

    app.post('/organization/getlocationplacename/', function (req, res) {

        console.log("GET LOCATIOPN PLACE NAME");
        console.log(req.body);

        if (aa.organizationAuthenticate(req)) {

            placeController.getLocationPlaceName(req, function (place_name) {

                console.log(place_name);

                res.json({result: place_name});

            });

        } else {

            res.json({result: {result: false}});

        }

    });

    // search place

    app.post('/organization/searchplace/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            placeController.searchPlaceName(req, function (places) {

                res.json({result: {result: true, places: places}});

            });

        } else {

            res.json({result: {result: false, places: null}});

        }

    });


    // get organization taxis around organization

    app.post('/organization/getorganizationaroundtaxis/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            organizationController.getOrganizationAroundTaxis(req, function (taxis_list) {

                res.json({result: taxis_list});

            });

        } else {

            res.json({result: {result: false}});

        }

    });

    // get taxis around organization

    app.post('/organization/getaroundtaxis/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            organizationController.getAroundTaxis(req, function (taxis_list) {

                res.json({result: taxis_list});

            });

        } else {

            res.json({result: {result: false}});

        }

    });

    // rate to travel

    app.post('/organization/ratetravel/', function (req, res) {

        console.log(req.body);

        if (aa.organizationAuthenticate(req)) {

            travelController.travelCommented(req, function (rate_travel) {

                res.json({result: rate_travel});

            });

        } else {

            res.json({result: false});

        }

    });

    app.post('/organization/comments/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            commentController.getComments(req, function (rate_travel) {

                res.json({result: rate_travel});

            });

        } else {

            res.json({result: {result: false}});

        }

    });

    // payment request

    app.post('/organization/paymentrequest/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            walletController.addMoneyToOrganizationWallet(req, function (payment_result, transaction_id) {

                console.log("WE ARE IN PAYMENT RESULT");
                console.log(payment_result);

                if (payment_result) {

                    console.log("IN CALL BACK");

                    res.json({
                        result: true,
                        payment_result: payment_result,
                        transaction_id: transaction_id
                    });

                }

                else
                    res.json({result: false, payment_result: null, transaction_id: null});

            });

        } else {

            res.json({result: false, payment_result: null, transaction_id: null});

        }

    });

    // ipg payment result

    app.get('/organization/paymentresult/ipg/', function (req, res) {


        walletController.getIpgPaymentResult(req, function (result) {

            res.sendFile(path.join(__dirname + '/../views/payment_success.html'));

        });

    });

    // ussd payment result

    app.get('/organization/paymentresult/ussd/', function (req, res) {

        console.log("USSD PAYMENT RESULT");

        walletController.getUssdPaymentResult(req, res);

    });

    // payment verification

    app.post('/organization/paymentverification/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            walletController.verifyPaymentResult(req, function (wallet_data, transaction_id) {

                if (wallet_data)
                    res.json({
                        result: true,
                        wallet_data: wallet_data,
                        transaction_id: transaction_id
                    });
                else
                    res.json({result: false, wallet_data: null, transaction_id: null});

            });

        } else {

            res.json({result: false, wallet_data: null, transaction_id: null});

        }


    });

    // get transactions

    app.post('/organization/transactions/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            walletController.getOrganizationTransactions(req, function (result) {

                res.json({result: true, transactions: result});

            });
        } else {

            res.json({result: false});

        }

    });

    // get wallet data

    app.post('/organization/wallet/', function (req, res) {

        console.log("/organization/wallet/");
        console.log(req.body);


        if (aa.organizationAuthenticate(req)) {

            walletController.getOrganizationWalletInfo(req, function (result) {

                if (result)
                    res.json({result: true, wallet_data: result});
                else
                    res.json({result: false, wallet_data: null});

            });

        } else {

            res.json({result: false, wallet_data: null});

        }

    });

    // verify discount code

    app.post('/organization/discountverification/', function (req, res) {

        if (aa.organizationAuthenticate(req)) {

            promotionController.checkDiscount(req, function (result) {

                if (result)
                    res.json({result: true, discount_amount: result});
                else
                    res.json({result: false, discount_amount: null});

            });

        } else {

            res.json({result: false, discount_amount: null});

        }
    });

    app.get('/organization/searchPN',function(req,res){
        placeController.searchPlaceName‌ByPanel(req,function(result){
            res.json(result);
        });
    });

    app.post('/organization/getPlaceInfo',function(req,res){
        console.log('place location '+req.body.place_id );
        placeController.getPlaceInfo(req,function(result){
            console.log('location resives');
            res.json(result);
        });
    });
};
