var customerController = require("../lib/controller/customer_controller");
var taxiController = require("../lib/controller/taxi_controller");
var travelController = require("../lib/controller/travel_controller");
var supportController = require("../lib/controller/support_controller");
var commentController = require("../lib/controller/comment_controller");
var placeController = require("../lib/controller/place_controller");
var promotionController = require("../lib/controller/promotion_controller");
var reportController = require("../lib/controller/report_controller");
var aa = require("../lib/tools/aa");
var utils = require("../lib/tools/utils");
var passport = require('passport');
var Account = require('./../lib/dao/aa_dao').Account;

module.exports = function (app) {

  app.post('/web/calculatetravelcost/', function (req, res) {

    customerController.calculateTravelCost(req, function (result) {

      res.json({result: result});

    });

  });

  app.post('/web/async/place/getplaceinfo/', function (req, res) {

    placeController.getPlaceInfo(req, function (places) {

      console.log(places);

      res.json(places);

    });

  });

};
