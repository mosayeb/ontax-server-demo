var app = require('express')();
var http = require("http").Server(app);
var express = require('express');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var io = require("socket.io")(http);
var bodyParser = require('body-parser');
var customerRoutes = require('./routes/customer');
var panelRoutes = require('./routes/panel');
var taxiRoutes = require('./routes/taxi');
var stationRoutes = require('./routes/station');
var organizationRoutes = require('./routes/organization');
var multer = require("multer");
var socketHandler = require('./lib/handler/socket_handler');
var start_handler = require('./lib/handler/start_handler');
var customer_controller = require('./lib/controller/customer_controller');
var map_tools = require('./lib/tools/map_tools');
var utils = require('./lib/tools/utils');
var path = require("path");
var Account = require('./lib/dao/aa_dao').Account;
app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({limit: '10mb', extended: false }));

app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(__dirname + '/public'));
app.use('/public', express.static(__dirname + '/public'));
app.use(multer({
    dest: './public/images'
}));


passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

app.use(function(req,res,next){
    res.locals.user = req.user;
    next();
});


module.exports = app;

customerRoutes(app);
panelRoutes(app);
taxiRoutes(app);
stationRoutes(app);
organizationRoutes(app);
socketHandler(io);

http.listen(9606, function(){

    start_handler.setAllTaxisOffline(function (result) {

        // utils.periodicBackUp(function (result) {

          // if(result) {

            //  start_handler.changePlaceData(function (result) {});

            console.log("Server Is Listening On Port 8005");

          // }

        // });

    });

});

