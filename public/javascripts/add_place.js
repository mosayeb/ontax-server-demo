
$(document).ready(function () {


    var map;
    var infowindow;

    setTimeout(function () {
        var mapCanvas = document.getElementById("add-place-map");
        var mapOptions = {
            center: new google.maps.LatLng(31.33809669065171, 48.6864280491136), zoom: 13
        };
        map = new google.maps.Map(mapCanvas, mapOptions);
        infowindow = new google.maps.InfoWindow();

        google.maps.event.addListener(map, 'click', function( event ){

            $('#place_lat').val(event.latLng.lat());
            $('#place_lan').val(event.latLng.lng());
            $('#place_name').val("");
            $('#place_id').val("");

        });

        $.ajax(
            {
                url: "/panel/async/places/all/",
                success: function(result){

                    var places = result.result;

                    for(var i=0; i<places.length; i++) {
                        (function () {

                            var place_marker = new google.maps.LatLng(places[i].loc[1], places[i].loc[0]);

                            marker = new google.maps.Marker({
                                position: place_marker,
                                map: map,
                                animation: google.maps.Animation.DROP,
                                title: places[i].place_name
                            });

                            marker._id = places[i]._id;

                            marker.addListener('click', function() {

                                var marker_lat = this.getPosition().lat();
                                var marker_lan = this.getPosition().lng();
                                var marker_title = this.title;
                                var marker_id = this._id;

                                $('#place_lat').val(marker_lat);
                                $('#place_lan').val(marker_lan);
                                $('#place_name').val(marker_title);
                                $('#place_id').val(marker_id);

                            });

                        })();

                    }

                }
            }
        );

    }, 3000);

    $("#place_register").click(function(){

        var place_lat = $('#place_lat').val();
        var place_lan = $('#place_lan').val();
        var place_name = $('#place_name').val();
        $.ajax(
            {
                url: "/panel/async/places/add/",
                type: "POST",
                data: {place_lat: place_lat, place_lan: place_lan, place_name: place_name},
                success: function(result) {

                    $('#place_lat').val("");
                    $('#place_lan').val("");
                    $('#place_name').val("");

                    var place_marker = new google.maps.LatLng(place_lat, place_lan);
                    marker = new google.maps.Marker({
                        position: place_marker,
                        map: map,
                        title: place_name
                    });

                    marker.addListener('click', function() {

                        $('#place_lat').val(place_lat);
                        $('#place_lan').val(place_lan);
                        $('#place_name').val(place_name);

                    });
                }
            }
        );
    });

    $("#place_edit").click(function(){

        var place_lat = $('#place_lat').val();
        var place_lan = $('#place_lan').val();
        var place_name = $('#place_name').val();
        var place_id = $('#place_id').val();
        $.ajax(
            {
                url: "/panel/async/place/edit/",
                type: "POST",
                data: {place_id: place_id, place_lat: place_lat, place_lan: place_lan, place_name: place_name},
                success: function(result) {

                    location.reload();
                }
            }
        );
    });

    $("#place_delete").click(function(){

        var place_id = $('#place_id').val();

        $.ajax(
            {
                url: "/panel/async/place/delete/",
                type: "POST",
                data: {place_id: place_id},
                success: function(result) {

                    location.reload();
                }
            }
        );
    });

});

// google.maps.event.addListener(map, 'click', function( event ){
//     alert( "Latitude: "+event.latLng.lat()+" "+", longitude: "+event.latLng.lng() );
// });


// var new_position = new google.maps.LatLng(taxi_location_data.location_lat, taxi_location_data.location_lan);


