
$(document).ready(function () {

    var map;
    var infowindow;

    setTimeout(function () {
        var mapCanvas = document.getElementById("add-factor-map");
        var mapOptions = {
            center: new google.maps.LatLng(32.669604, 51.667983), zoom: 13
        };
        map = new google.maps.Map(mapCanvas, mapOptions);
        infowindow = new google.maps.InfoWindow();

        // google.maps.event.addListener(map, 'click', function( event ){
        //
        //     $('#place_lat').val(event.latLng.lat());
        //     $('#place_lan').val(event.latLng.lng());
        //     $('#place_name').val("");
        //     $('#place_id').val("");
        //
        // });

        $.ajax(
            {
                url: "/panel/async/places/all/",
                success: function(result){

                    var places = result.result;

                    for(var i=0; i<places.length; i++) {
                        (function () {

                            var place_marker = new google.maps.LatLng(places[i].place_lat, places[i].place_lan);

                            marker = new google.maps.Marker({
                                position: place_marker,
                                map: map,
                                animation: google.maps.Animation.DROP,
                                title: places[i].place_name
                            });

                            marker._id = places[i]._id;

                            marker.addListener('click', function() {

                                // var marker_lat = this.getPosition().lat();
                                // var marker_lan = this.getPosition().lng();
                                // var marker_title = this.title;
                                var marker_id = this._id;
                                var first_place_val = $("#first_place").val();
                                var second_place = $("#second_place").val();
                                if(first_place_val == "none") {
                                    $("#first_place").val(marker_id);
                                } else {
                                    $("#second_place").val(marker_id);
                                }

                                // $('#place_lat').val(marker_lat);
                                // $('#place_lan').val(marker_lan);
                                // $('#place_name').val(marker_title);
                                // $('#place_id').val(marker_id);

                            });

                        })();

                    }

                }
            }
        );

    }, 3000);

    $("#factor_register").click(function(){

        var first_place_id = $('#first_place').val();
        var second_place_id = $('#second_place').val();
        var factor_cost = $('#factor_cost').val();

        $.ajax(
            {
                url: "/panel/async/factors/add/",
                type: "POST",
                data: {first_place_id: first_place_id, second_place_id: second_place_id, cost: factor_cost},
                success: function(result) {

                    $("#first_place").val("none");
                    $("#second_place").val("none");
                    $("#factor_cost").val("");
                }
            }
        );
    });
    //
    // $("#place_edit").click(function(){
    //
    //     var place_lat = $('#place_lat').val();
    //     var place_lan = $('#place_lan').val();
    //     var place_name = $('#place_name').val();
    //     var place_id = $('#place_id').val();
    //     $.ajax(
    //         {
    //             url: "/panel/async/place/edit/",
    //             type: "POST",
    //             data: {place_id: place_id, place_lat: place_lat, place_lan: place_lan, place_name: place_name},
    //             success: function(result) {
    //
    //                 location.reload();
    //             }
    //         }
    //     );
    // });
    //
    // $("#place_delete").click(function(){
    //
    //     var place_id = $('#place_id').val();
    //
    //     $.ajax(
    //         {
    //             url: "/panel/async/place/delete/",
    //             type: "POST",
    //             data: {place_id: place_id},
    //             success: function(result) {
    //
    //                 location.reload();
    //             }
    //         }
    //     );
    // });

    $('#first_place').on('change', function() {

        var place_id = this.value;

        // $.ajax(
        //     {
        //         url: "/panel/async/place/getinfo/",
        //         success: function(result){
        //
        //             var place = result.result;
        //
        //             // for(var i=0; i<places.length; i++) {
        //             //     (function () {
        //
        //             var place_marker = new google.maps.LatLng(place.place_lat, place.place_lan);
        //
        //             marker = new google.maps.Marker({
        //                 position: place_marker,
        //                 map: map,
        //                 animation: google.maps.Animation.DROP,
        //                 title: place.place_name
        //             });
        //
        //             marker._id = place._id;
        //
        //             marker.addListener('click', function() {
        //
        //                 var marker_lat = this.getPosition().lat();
        //                 var marker_lan = this.getPosition().lng();
        //                 var marker_title = this.title;
        //                 var marker_id = this._id;
        //
        //                 $('#place_lat').val(marker_lat);
        //                 $('#place_lan').val(marker_lan);
        //                 $('#place_name').val(marker_title);
        //                 $('#place_id').val(marker_id);
        //
        //             });
        //
        //             //     })();
        //             //
        //             // }
        //
        //         }
        //     }
        // );

    });

});

// google.maps.event.addListener(map, 'click', function( event ){
//     alert( "Latitude: "+event.latLng.lat()+" "+", longitude: "+event.latLng.lng() );
// });


// var new_position = new google.maps.LatLng(taxi_location_data.location_lat, taxi_location_data.location_lan);


