$(document).ready(function () {

    var socket = io();
    var track_taxi_loop_counter = 1000;

    $.notification.requestPermission(function () {
    });

    var map;

    $("#taxi-map-show-btn").click(function () {

        setTimeout(function () {
            map = new GMaps({
                div: '#taxi-map',
                lat: 31.33809669065171,
                lng: 48.6864280491136,
                zoom: 13
            });

            var taxi_id = $("#taxi-map-show-btn").attr("taxi_id");

            $.get("/panel/async/taxi", {taxi_id: taxi_id}, function (data, status) {

                var location_lat = data.location_lat;
                var location_lan = data.location_lan;

                map.addMarker({
                    lat: location_lat,
                    lng: location_lan,
                    title: 'تاکسی',
                    icon: '/icons/taxi_icon.png'
                });

            });

        }, 1000);

    });

    $("#travel-map-show-btn").click(function () {

        map = new GMaps({
            div: '#map',
            lat: 31.33809669065171,
            lng: 48.6864280491136,
            zoom: 13
        });

        var travel_id = $("#travel-page-travel-info").attr("travel_id");

        $.get("/panel/async/travel", {travel_id: travel_id}, function (data, status) {

            var destination_lat = data.travel.destination_lat;
            var destination_lan = data.travel.destination_lan;
            var source_lat = data.travel.source_lat;
            var source_lan = data.travel.source_lan;
            var customer_lat = data.travel.customer_lat;
            var customer_lan = data.travel.customer_lan;

            var source_position = new google.maps.LatLng(parseFloat(source_lat), parseFloat(source_lan));
            var destination_position = new google.maps.LatLng(parseFloat(destination_lat), parseFloat(destination_lan));

            map.fitLatLngBounds([source_position, destination_position]);

            map.addMarker({
                lat: source_lat,
                lng: source_lan,
                title: 'مبدا',
                icon: '/icons/source.9.png'
            });


            map.addMarker({
                lat: destination_lat,
                lng: destination_lan,
                title: 'مقصد',
                icon: '/icons/destination.9.png'
            });

            map.addMarker({
                lat: customer_lat,
                lng: customer_lan,
                title: 'مکان فعلی فرد',
                icon: '/icons/customer.png'
            });

            map.drawRoute({
                origin: [source_lat, source_lan],
                destination: [destination_lat, destination_lan],
                travelMode: 'driving',
                strokeColor: '#131540',
                strokeOpacity: 0.6,
                strokeWeight: 6
            });


        });


    });

    $("#taxis-map-show-btn").click(function () {

        map = new GMaps({
            div: '#taxis-map',
            lat: 31.33809669065171,
            lng: 48.6864280491136,
            zoom: 13
        });

        var filter = $("#taxis_list_filter_type").val();

        $.get("/panel/async/taxis/", {}, function (data, status) {

            for (var i = 0; i < data.length; i++) {

                var location_lan = data[i].location_lan;
                var location_lat = data[i].location_lat;
                var driver_name = data[i].driver_name;
                var is_online = data[i].is_online;
                var state = data[i].state;
                var driver_phone_number = data[i].driver_phone_number;
                var car_model = data[i].model + " " + data[i].car_color;
                var driver_data = "\n" + "نام راننده:‌ " + driver_name + " \n" + "شماره تماس: " + driver_phone_number + " \n" + "خودرو: " + car_model + "\n";
                // var driver_data = driver_name.concat(" تلفن تماس: ", driver_phone_number + "\n" + " sdasdsad");

                if(filter == "online") {

                    if (is_online == 1) {

                        if(state == "busy") {

                            map.addMarker({
                                lat: location_lat,
                                lng: location_lan,
                                title: driver_data,
                                icon: '/icons/taxi_icon_online.png'
                            });

                        } else {

                            map.addMarker({
                                lat: location_lat,
                                lng: location_lan,
                                title: driver_data,
                                icon: '/icons/taxi_icon_offline.png'
                            });

                        }


                    }

                } else if (filter == "offline") {

                    if (is_online == 0) {

                        map.addMarker({
                            lat: location_lat,
                            lng: location_lan,
                            title: driver_data,
                            icon: '/icons/taxi_icon_offline.png'
                        });
                    }

                } else {

                    map.addMarker({
                        lat: location_lat,
                        lng: location_lan,
                        title: driver_data,
                        icon: '/icons/taxi_icon_offline.png'
                    });

                }



                // } else {
                //
                //   map.addMarker({
                //     lat: location_lat,
                //     lng: location_lan,
                //     title: driver_data,
                //     icon: '/icons/taxi_icon_offline.png'
                //   });
                //
                // }


            }
        });


    });

    socket.on('connect', function () {

        socket.emit('update_socket_data', function (result) {


        });
    });

    socket.on('new-travel', function (data) {

        var notification = new window.Notification("آنتاکس", {
            title: 'سفر جدید',
            body: 'یک درخواست جدید برای آنتاکس'
        });

        notification.onclick = function (e) {
            window.focus();
            window.open("/panel/travel/" + data.travel_id);
        };

    });

    socket.on('travel_finished', function (travel_id) {

        var notification = new window.Notification("آنتاکس", {
            title: 'اتمام سفر',
            body: 'سفر توسط تاکسی اتمام شده اعلام شد'
        });

        notification.onclick = function (e) {
            window.focus();
            window.open("/panel/travel/" + travel_id);
        };

    });

    socket.on('taxi_canceled_travel', function (travel_id) {

        var notification = new window.Notification("آنتاکس", {
            title: 'لغو سفر',
            body: 'سفر از جانب تاکسی لغو شد'
        });

        notification.onclick = function (e) {
            window.focus();
            window.open("/panel/travel/" + travel_id);
        };

    });

    socket.on('taxi_confirmed', function (data) {

        var notification = new window.Notification("آنتاکس", {
            title: 'تایید سفر',
            body: 'تاکسی سفر را تایید کرد'
        });

        notification.onclick = function (e) {
            window.focus();
            window.open("/panel/travel/" + data.result._id);
        };

    });

    socket.on('customer_canceled_travel', function (travel_id) {

        var notification = new window.Notification("آنتاکس", {
            title: 'لغو سفر',
            body: 'سفر توسط مسافر لغو شد'
        });

        notification.onclick = function (e) {
            window.focus();
            window.open("/panel/travel/" + travel_id);
        };

    });

    $('.support_confirm_btn').click(function () {

        var data = {
            travel_id: $("#travel-page-travel-info").attr("travel_id"),
            taxi_id: $("#travel-page-travel-info").attr("taxi_id")
        };

        socket.emit('support_confirm_travel', data, function (result) {

            if (result) {

                $(this).removeClass("btn-danger");
                $(this).addClass("btn-success");
                $(this).val("تایید شد")
            }

        });

    });

    $('#support_cancel_btn').click(function () {

      var data = {
            travel_id: $("#travel-page-travel-info").attr("travel_id")
        };

        socket.emit('support_cancel_travel', data, function (result) {

            if (result) {

                location.reload();

            }

        });

    });

    $('.support_finish_btn').click(function () {

        var data = {
            travel_id: $("#travel-page-travel-info").attr("travel_id"),
            taxi_id: $("#travel-page-travel-info").attr("taxi_id")
        };


        socket.emit('support_finish_travel', data, function (result) {

            if (result) {

                location.reload();

            }

        });

    });

    $('.support_taxi_track_btn').click(function () {


        var taxi_id = $(this).attr('taxi_id');
        var data = {
            taxi_id: taxi_id
        };

        var taxi_marker = map.addMarker({
            lat: 31.33809669065171,
            lng: 48.6864280491136,
            title: 'تاکسی',
            icon: '/icons/taxi_icon_online.png'
        });

        myLoop(data, taxi_marker);

    });

    function myLoop(data, taxi_marker) {

        setTimeout(function () {

            socket.emit('support_track_taxi', data, function (taxi_location_data) {

                var new_position = new google.maps.LatLng(taxi_location_data.location_lat, taxi_location_data.location_lan);

                taxi_marker.setPosition(new_position);

            });

            track_taxi_loop_counter++;

            if (track_taxi_loop_counter < 500) {
                myLoop(data, taxi_marker);
            }

        }, 5000)
    }

    /*

     DISCOUNT PART

     */

    $('.remove_discount_place_btn').click(function () {

        var place_id = $(this).attr("place_id");
        var discount_id = $(this).attr("discount_id");
        var context = $(this);

        $.ajax(
            {
                url: "/panel/async/promotions/discount_code/removeplace/",
                type: "POST",
                data: {place_id: place_id, discount_id: discount_id},
                success: function (result) {

                    if (result) {

                        context.parent().remove();
                    }
                }
            }
        );

    });

    $('.remove_template_message_btn').click(function () {

        var template_message_id = $(this).attr("template_message_id");
        var context = $(this);

        $.ajax(
            {
                url: "/panel/support/message/removetemplatemessage/",
                type: "POST",
                data: {message_id: template_message_id},
                success: function (result) {

                    if (result) {

                        context.parent().remove();
                    }
                }
            }
        );

    });

    $('#add_discount_place_btn').click(function () {

        console.log("HERE AI AI");
        var place_id = $(this).prev().val();
        var discount_id = $(this).attr("discount_id");

        $.ajax(
            {
                url: "/panel/async/promotions/discount_code/addplace/",
                type: "POST",
                data: {place_id: place_id, discount_id: discount_id},
                success: function (result) {

                    console.log(result);

                    if (result.result.result) {

                        var place_data = result.result.place_data;

                        $("#discount_places_list").append('<li class="collection-item">' + place_data.place_name + '</li>');

                    }
                }
            }
        );

    });

    $(".start_date, .finish_date").pDatepicker({format: "YYYY/MM/DD"});

    $('#first_place_id').autocomplete({

        serviceUrl: '/panel/async/place/search/',

        onSelect: function (suggestion) {

            $(this).attr("place_id", suggestion.data);

        }

    });

    $('#second_place_id').autocomplete({

        serviceUrl: '/panel/async/place/search/',

        onSelect: function (suggestion) {

            $(this).attr("place_id", suggestion.data);

        }

    });

    $("#factor_register").click(function () {

        var first_place_id = $('#first_place_id').attr("place_id");
        var second_place_id = $('#second_place_id').attr("place_id");
        var factor_cost = $('#factor_cost').val();

        if (first_place_id && second_place_id && factor_cost) {

            $.ajax({

                    url: "/panel/async/factors/add/",
                    type: "POST",
                    data: {first_place_id: first_place_id, second_place_id: second_place_id, cost: factor_cost},
                    success: function (result) {

                        if (result.result == true) {

                            if (result.state == "new") {

                                Materialize.toast("با موفقیت ثبت شد.", 3000);

                                $("#first_place_id").val("");
                                $("#first_place_id").attr("place_id", "");
                                $("#second_place_id").attr("place_id", "");
                                $("#second_place_id").val("");
                                $("#factor_cost").val("");

                            } else if (result.state == "update") {

                                Materialize.toast("قبلا وارد شده بود، کرایه جدید جایگزین شد", 3000);

                                $("#first_place_id").val("");
                                $("#first_place_id").attr("place_id", "");
                                $("#second_place_id").attr("place_id", "");
                                $("#second_place_id").val("");
                                $("#factor_cost").val("");
                            }

                        } else {

                            Materialize.toast("خطا در ثبت کرایه، دوباره سعی کنید", 3000);

                            $("#first_place_id").val("");
                            $("#first_place_id").attr("place_id", "");
                            $("#second_place_id").attr("place_id", "");
                            $("#second_place_id").val("");
                            $("#factor_cost").val("");

                        }
                    }
                }
            );

        } else {

            Materialize.toast("لطفا اطلاعات را درست وارد کنید", 3000);

        }
    });

    $('#add-station-show-map-btn').click(function () {

        var map;
        var infowindow;

        setTimeout(function () {

            var mapCanvas = document.getElementById("add-station-map");

            var mapOptions = {

                // center: new google.maps.LatLng(32.669604, 51.667983), zoom: 12
                // lat: 31.33809669065171,
                // lng: 48.6864280491136,
                center: new google.maps.LatLng(31.33809669065171, 48.6864280491136), zoom: 13


            };

            map = new google.maps.Map(mapCanvas, mapOptions);
            infowindow = new google.maps.InfoWindow();

            google.maps.event.addListener(map, 'click', function (event) {

                $('#location_lat').val(event.latLng.lat());
                $('#location_lan').val(event.latLng.lng());


            });

        }, 1000)

    });

    $("#station-taxis-map-show-btn").click(function () {

        map = new GMaps({
            div: '#taxis-map',
            lat: 31.33809669065171,
            lng: 48.6864280491136,
            zoom: 13
        });

        $.get("/station/async/taxis/", {}, function (data, status) {

            for (var i = 0; i < data.length; i++) {

                var location_lan = data[i].location_lan;
                var location_lat = data[i].location_lat;
                var driver_name = data[i].driver_name;
                var is_online = data[i].is_online;
                var driver_phone_number = data[i].driver_phone_number;
                var driver_data = driver_name.concat(" تلفن تماس: ", driver_phone_number);

                if (is_online == 1) {

                    map.addMarker({
                        lat: location_lat,
                        lng: location_lan,
                        title: driver_data,
                        icon: '/icons/taxi_icon_offline.png'
                    });
                }

                // } else {
                //
                //   map.addMarker({
                //     lat: location_lat,
                //     lng: location_lan,
                //     title: driver_data,
                //     icon: '/icons/taxi_icon_offline.png'
                //   });
                //
                // }


            }
        });


    });


    $('#is_global').on('change', function () {

        var is_global = this.value;

        if (is_global == "true") {

            $('#place_id').prop('disabled', 'disabled');

        } else {

            $('#place_id').prop('disabled', false);

        }

    });

    $("#show_taxi_online_stat").click(function () {

        var taxi_id = $('#taxi_id').val();
        var date = $('#taxi_stat_date').val();

        $.ajax({

                url: "/panel/async/taxi/stat/",
                type: "POST",
                data: {taxi_id: taxi_id, date: date},
                success: function (result) {

                    console.log(result);

                    // var areaData = [[1, 1], [2, 1], [2, 0], [3, 0], [3, 1], [4, 1]];
                    var areaData = result.online_stats;

                    $.plot("#area-chart", [areaData], {
                        grid: {
                            borderWidth: 0
                        },
                        series: {
                            shadowSize: 0, // Drawing is faster without shadows
                            color: "#00c0ef"
                        },
                        lines: {
                            fill: false //Converts the line chart to area chart
                        },

                        yaxis: {
                            min: 0,
                            max: 1,
                            show: true
                        },

                        xaxis: {
                            min: 0,
                            max: 1440,
                            show: true
                        }
                    });


                }
            }
        );

    });


    $('#show-station-map-btn').click(function () {

        var map;
        var infowindow;

        var location_lat = $('#location_lat').val();
        var location_lan = $('#location_lan').val();

        setTimeout(function () {

            var mapCanvas = document.getElementById("show-station-map");

            var mapOptions = {

                center: new google.maps.LatLng(location_lat, location_lan), zoom: 17

            };

            map = new google.maps.Map(mapCanvas, mapOptions);

            infowindow = new google.maps.InfoWindow();

            var new_position = new google.maps.LatLng(location_lat, location_lan);

            var marker = new google.maps.Marker({

                position: new_position,
                map: map,
                animation: google.maps.Animation.DROP

            });

            // var source_position = new google.maps.LatLng(parseFloat(source_lat), parseFloat(source_lan));
            // var destination_position = new google.maps.LatLng(parseFloat(destination_lat), parseFloat(destination_lan));

            console.log(map);
            console.log(new_position);
            map.fitLatLngBounds([new_position]);

        }, 1000)

    });

  jQuery('#taxi_register_form').bind('submit',function(e){

    var is_image_loaded = $('#cropit-preview').hasClass('cropit-image-loaded');

    if(!is_image_loaded) {

      alert("عکس برای راننده انتخاب نشده است!!");

      e.preventDefault();

    }

  });


});

$(function () {
    $('.image-editor').cropit();
    $('form').submit(function () {

        var imageData = $('.image-editor').cropit('export');

        $('.hidden-image-data').val(imageData);

        var formValue = $(this).serialize();

        $("#form").submit(function (eventObj) {
            $('<input />').attr('type', 'file')
                .attr('name', "photo")
                .attr('value', formValue)
                .appendTo('#form');
            return true;
        });
    });
});

