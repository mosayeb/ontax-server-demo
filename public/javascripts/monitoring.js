$(document).ready(function () {

    var map;

    $("#live-taxi-monitor-btn").click(function () {

        setTimeout(function () {
            map = new GMaps({
                div: '#live-taxis-map',
                lat: 31.33809669065171,
                lng: 48.6864280491136,
                zoom: 13
            });

            var positions = [];

            var updateTaxi = setInterval(function(){


                var online_taxis_number = 0;
                var offline_taxis_number = 0;
                var free_taxis_number = 0;
                var busy_taxis_number = 0;
                var all_taxis_number = 0;


                $.get("/panel/async/taxis/", function (data, status) {

                    map.removeMarkers();

                    for (var i = 0; i < data.length; i++) {

                        all_taxis_number = data.length;


                        var is_online = data[i].is_online;

                        if (is_online == 1) {

                            var location_lan = data[i].location_lan;
                            var location_lat = data[i].location_lat;
                            var driver_name = data[i].driver_name;
                            var state = data[i].state;
                            var driver_phone_number = data[i].driver_phone_number;
                            var car_model = data[i].model + " " + data[i].car_color;
                            var driver_data = "\n" + "نام راننده:‌ " + driver_name + " \n" + "شماره تماس: " + driver_phone_number + " \n" + "خودرو: " + car_model + "\n";


                            online_taxis_number += 1;

                            if(state == "busy") {

                                busy_taxis_number += 1;

                                positions.push(new google.maps.LatLng(parseFloat(location_lat), parseFloat(location_lan)));

                                map.addMarker({
                                    lat: location_lat,
                                    lng: location_lan,
                                    title: driver_data,
                                    icon: '/icons/taxi_icon_online.png'
                                });

                            } else {

                                free_taxis_number += 1;

                                positions.push(new google.maps.LatLng(parseFloat(location_lat), parseFloat(location_lan)));


                                map.addMarker({
                                    lat: location_lat,
                                    lng: location_lan,
                                    title: driver_data,
                                    icon: '/icons/taxi_icon_offline.png'
                                });


                            }


                        } else {

                            offline_taxis_number += 1;

                        }

                    }

                    // map.fitLatLngBounds(positions);

                    $("#all_taxis").text(all_taxis_number);
                    $("#busy_taxis").text(busy_taxis_number);
                    $("#online_taxis").text(online_taxis_number);
                    $("#offline_taxis").text(offline_taxis_number);
                    $("#free_taxis").text(free_taxis_number);

                });

            }, 5000);


        }, 1000);

    });

});
