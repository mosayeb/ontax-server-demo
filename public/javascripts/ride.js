// function toTimeZone(iso) {
//   var dtstr= iso;
//   dtstr = dtstr.replace(/\D/g," ");
//   var dtcomps = dtstr.split(" ");
//   // modify month between 1 based ISO 8601 and zero based Date
//   dtcomps[1]--;
//   var convdt = new Date(Date.UTC(dtcomps[0],dtcomps[1],dtcomps[2],dtcomps[3],dtcomps[4],dtcomps[5]));
//   return moment(convdt).format('jYYYY/jM/jD')
// }
//
// function convertNumbersToPersian(str) {
//   let result = '';
//   if ( !str && str !== 0 ) {
//     return result;
//   }
//   str = str.toString();
//   for(let i = 0; i < str.toString().length; i++) {
//     result += str[i] === '1'? 'Û±' :
//       str[i] === '2'? 'Û²' :
//         str[i] === '3'? 'Û³' :
//           str[i] === '4'? 'Û´' :
//             str[i] === '5'? 'Ûµ' :
//               str[i] === '6'? 'Û¶' :
//                 str[i] === '7'? 'Û·' :
//                   str[i] === '8'? 'Û¸' :
//                     str[i] === '9'? 'Û¹' :
//                       str[i] === '0'? 'Û°' : str[i];
//   }
//   return result;
// }
//
//
//
// var runningCoords = [];
//
// // var str = "35.79327,51.42876|35.79467,51.43175|35.79420,51.43075|35.79700,51.43673|35.79746,51.43773|35.79816,51.43922|35.79886,51.44071|35.80096,51.44520|35.80143,51.44619|35.80244,51.44835|35.80244,51.44835|35.79704,51.45108|35.79409,51.45256|35.79605,51.45157|35.78820,51.45554|35.78869,51.45529|35.78624,51.45653|35.78378,51.45777|35.78084,51.45926|35.77936,51.46" + "|";
// var show_path = function(path) {
//   for (var i = 0; i < 100; i++) {
//     var kama = path.indexOf(",");
//     var firstPart = path.substring(0, kama);
//     var sep = path.indexOf("|");
//
//     if (sep == -1)
//       break;
//
//     var secondPart = path.substring(kama + 1, sep);
//     path = path.substring(sep + 1);
//     var obj = {
//       lat: parseFloat(firstPart),
//       lng: parseFloat(secondPart)
//     };
//     runningCoords.push(obj);
//   }
// };
//
// var path = $('#path').val() + "|";
//
// var date = $('#date').val();
// date = toTimeZone(date);
// $('#show-date').text(convertNumbersToPersian(date));
//
// var orgin_x = $('#orgin_x').val();
// var orgin_y = $('#orgin_y').val();
// var destination_x = $('#destination_x').val();
// var destination_y = $('#destination_y').val();
// var driver_x = $('#driver_x').val();
// var driver_y = $('#driver_y').val();
//
//
//
// var gmap = document.getElementById('map');
// var map;
// var settings = {
//   home: {
//     latitude: orgin_x,
//     longitude: orgin_y
//   },
//   icon_url: '',
//   zoom: 13
// };
//
// var coords = new google.maps.LatLng(settings.home.latitude, settings.home.longitude);
//
// var options = {
//   zoom: settings.zoom,
//   scrollwheel: false,
//   center: coords,
//   mapTypeId: google.maps.MapTypeId.ROADMAP,
//   mapTypeControl: true,
//   scaleControl: true,
//   streetViewControl: false,
//   zoomControlOptions: {
//     style: google.maps.ZoomControlStyle.DEFAULT
//   },
//   overviewMapControl: true
// };
//
// map = new google.maps.Map(gmap, options);
//
// if(destination_x && destination_y) {
//   var end = new google.maps.Marker({
//     position: new google.maps.LatLng(destination_x, destination_y),
//     map: map,
//     icon: {
//       url: 'https://my.tap30.taxi/assets/img/d2.png',
//       scaledSize: new google.maps.Size(25, 25),
//       anchor: new google.maps.Point(12.5, 12.5),
//       origin: new google.maps.Point(0, 0)
//     },
//     draggable: false
//   });
// }
//
// var start = new google.maps.Marker({
//   position: new google.maps.LatLng(orgin_x, orgin_y),
//   map: map,
//   icon: {
//     url: 'https://my.tap30.taxi/assets/img/o2.png',
//     scaledSize: new google.maps.Size(25, 25),
//     anchor: new google.maps.Point(12.5, 12.5),
//     origin: new google.maps.Point(0, 0)
//   },
//   draggable: false
// });
//
//
//
// if(driver_x && driver_y) {
//   var end = new google.maps.Marker({
//     position: new google.maps.LatLng(driver_x, driver_y),
//     map: map,
//     icon: {
//       url: 'https://my.tap30.taxi/assets/img/car.png',
//       scaledSize: new google.maps.Size(25, 25),
//       anchor: new google.maps.Point(12.5, 12.5),
//       origin: new google.maps.Point(0, 0)
//     },
//     draggable: false
//   });
// }
//
// var info = new google.maps.InfoWindow({
//   content: settings.text
// });
//
// var i = 0;
// function animPath() {
//
//   if (i > runningCoords.length-2) {
//     return false;
//   }
//
//   dept_lat = runningCoords[i].lat;
//   dept_lng = runningCoords[i].lng;
//   arr_lat = runningCoords[i + 1].lat;
//   arr_lng = runningCoords[i + 1].lng;
//
//   var departure = new google.maps.LatLng(dept_lat, dept_lng); //Set to whatever lat/lng you need for your departure location
//   var arrival = new google.maps.LatLng(arr_lat, arr_lng); //Set to whatever lat/lng you need for your arrival location
//   var line = new google.maps.Polyline({
//     path: [departure, departure],
//     strokeColor: "#58a7e2",
//     strokeOpacity: 1,
//     strokeWeight: 7,
//     geodesic: true, //set to false if you want straight line instead of arc
//     map: map,
//   });
//   var step = 0;
//   var numSteps = 50; //Change this to set animation resolution
//   var timePerStep = 2; //Change this to alter animation speed
//   var interval = setInterval(function() {
//     step += 1;
//     if (step > numSteps) {
//       clearInterval(interval);
//       i++;
//       animPath();
//     } else {
//       var are_we_there_yet = google.maps.geometry.spherical.interpolate(departure, arrival, step / numSteps);
//       line.setPath([departure, are_we_there_yet]);
//     }
//   }, timePerStep);
// }
//
// google.maps.event.addListenerOnce(map, 'idle', function() {
//   setTimeout(init, 300);
// });
//
//
//
// function init(){
//   show_path(path);
//   animPath();
//   /* Number animation */
//   $('.info-box mark span').each(function() {
//     $(this).prop('Counter', 0).animate({
//       Counter: parseFloat($(this).text()).toFixed(1)
//     }, {
//       duration: 3000,
//       easing: 'easeInOutQuart',
//       step: function(now) {
//         $(this).text(Math.round(now * 10) / 10);
//       }
//     });
//   });
// }
//
//
// $('.reload').click(function(){
//   location.href = location.href;
// });
$(document).ready(function () {
  var socket = io();
  var track_taxi_loop_counter = 1;

  map = new GMaps({
    div: '#map',
    scrollwheel: false,
    scaleControl: false,
    mapTypeControl: false,
    lat: 0,
    lng: 0,
    zoom: 13
  });

  var travel_id = $("#travel-page-travel-info").attr("travel_id");
  var taxi_id = $("#travel-page-travel-info").attr("taxi_id");
  var travel_state = $("#travel-page-travel-info").attr("travel_state");

  $.get("/panel/async/travel", {travel_id: travel_id}, function (data, status) {

    var destination_lat = data.travel.destination_lat;
    var destination_lan = data.travel.destination_lan;
    var source_lat = data.travel.source_lat;
    var source_lan = data.travel.source_lan;
    var customer_lat = data.travel.customer_lat;
    var customer_lan = data.travel.customer_lan;

    console.log(data);

    var source_position = new google.maps.LatLng(parseFloat(source_lat), parseFloat(source_lan));
    var destination_position = new google.maps.LatLng(parseFloat(destination_lat), parseFloat(destination_lan));

    map.fitLatLngBounds([source_position, destination_position]);

    map.addMarker({
      lat: source_lat,
      lng: source_lan,
      title: 'مبدا',
      icon: '/icons/source.9.png'
    });

    map.addMarker({
      lat: destination_lat,
      lng: destination_lan,
      title: 'مقصد',
      icon: '/icons/destination.9.png'
    });

    // map.addMarker({
    //   lat: customer_lat,
    //   lng: customer_lan,
    //   title: 'مکان فعلی فرد',
    //   icon: '/icons/customer.png'
    // });

    map.addMarker({
      lat: data.taxi.location_lat,
      lng: data.taxi.location_lan,
      title: 'تاکسی',
      icon: '/icons/taxi_icon_online.png'
    });

    map.drawRoute({
      origin: [source_lat, source_lan],
      destination: [destination_lat, destination_lan],
      travelMode: 'driving',
      strokeColor: '#131540',
      strokeOpacity: 0.6,
      strokeWeight: 6
    });

  });

  // var taxi_id = $(this).attr('taxi_id');
  var data = {
    taxi_id: taxi_id
  };

  var taxi_marker = map.addMarker({
    lat: 32.669604,
    lng: 51.667983,
    title: 'تاکسی',
    icon: '/icons/taxi_icon_online.png'
  });

  if(travel_state === "travel_started") {

    myLoop(data, taxi_marker);

    function myLoop(data, taxi_marker) {

      setTimeout(function () {

        console.log(data);

        socket.emit('support_track_taxi', data, function (taxi_location_data) {

          console.log(taxi_location_data);

          var new_position = new google.maps.LatLng(taxi_location_data.location_lat, taxi_location_data.location_lan);

          taxi_marker.setPosition(new_position);

        });

        track_taxi_loop_counter++;

        if (track_taxi_loop_counter < 500) {
          myLoop(data, taxi_marker);
        }

      }, 5000)
    }

  }



});

// myLoop(data, taxi_marker);
//
// function myLoop(data, taxi_marker) {
//
//   setTimeout(function () {
//
//     console.log(data);
//
//     socket.emit('support_track_taxi', data, function (taxi_location_data) {
//
//       console.log(taxi_location_data);
//
//       var new_position = new google.maps.LatLng(taxi_location_data.location_lat, taxi_location_data.location_lan);
//
//       taxi_marker.setPosition(new_position);
//
//     });
//
//     track_taxi_loop_counter++;
//
//     if (track_taxi_loop_counter < 500) {
//       myLoop(data, taxi_marker);
//     }
//
//   }, 5000)
// }