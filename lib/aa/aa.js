var jwt_simple = require('jwt-simple');
var password_hash = require('password-hash');

var config = require('./../config/config');
var customer_dao = require('./../dao/customer_dao');
var organization_dao = require('./../dao/organization_dao');
var aa_dao = require('./../dao/aa_dao');
var travel_dao = require('./../dao/travel_dao');
var taxi_dao = require('./../dao/taxi_dao');
var station_dao = require('./../dao/station_dao');

// generate token

module.exports.createNewToken = function (payload) {
    console.log("in Token.........."+payload);
    var token = jwt_simple.encode(payload, config.secret);
    return(token);

};

// token verification

module.exports.verifyToken = function (payload, token) {

    var decoded = jwt_simple.decode(token, config.secret);

    if(decoded == payload)

        return(true);

    else
        return(false);

};

// authentication by token | customers
module.exports.stationAuthenticate = function(req) {
    console.log('auth1');
    if(req.body) {
        console.log('auth2');
        var station_id = req.body.station_id;
        var token = req.body.token;

    } else {

        var station_id = req.station_id;
        var token = req.token;

    }

    var decoded = jwt_simple.decode(token, config.secret);
    if(decoded == station_id) {
        console.log('auth3');
        return (true);

    } else {

        return (false);

    }
}
module.exports.organizationAuthenticate = function(req) {
    if(req.body) {

        var organization_id = req.body.organization_id;
        var token = req.body.token;

    } else {

        var organization_id = req.organization_id;
        var token = req.token;

    }

    var decoded = jwt_simple.decode(token, config.secret);

    if(decoded == organization_id) {

        return (true);

    } else {

        return (false);

    }
};

module.exports.customerAuthenticate = function (req) {

    if(req.body) {

        var customer_id = req.body.customer_id;
        var token = req.body.token;

    } else {

        var customer_id = req.customer_id;
        var token = req.token;

    }

    var decoded = jwt_simple.decode(token, config.secret,true);

    if(decoded == customer_id) {

        return (true);

    } else {

        return (false);

    }
};

// authentication by token | taxis

module.exports.taxiAuthenticate = function (req) {

    if(req.body) {

        var taxi_id = req.body.taxi_id;
        var token = req.body.token;

    } else {

        var taxi_id = req.taxi_id;
        var token = req.token;

    }

    if(token && taxi_id) {

        var decoded = jwt_simple.decode(token, config.secret,true);

        if(decoded == taxi_id) {

            return (true);

        } else {

            return (false);

        }

    }


};

// check customer validation

module.exports.isCustomerValid = function (customer_id, callback) {

    customer_dao.isCustomerValid(customer_id, function (is_customer_valid) {

        if(is_customer_valid) {

            callback(true);

        } else {
            callback(false);
        }

    })

};

module.exports.isOrganizationValid = function (organization_id, callback) {

    organization_dao.isOrganizationValid(organization_id, function (is_organization_valid) {

        if(is_organization_valid) {

            callback(true);

        } else {
            callback(false);
        }

    })

};

module.exports.isStationValid = function (station_id, callback) {

    station_dao.isStationValid(station_id, function (is_station_valid) {

        if(is_station_valid) {

            callback(true);

        } else {
            callback(false);
        }

    })

};
// block customer

module.exports.blockCustomer = function (customer_id, callback) {

    customer_dao.blockCustomer(customer_id, function(block_result) {

        if(block_result) {
            callback(block_result);
        }

    });

};

// unblock customer

module.exports.unblockCustomer = function (customer_id, callback) {

    customer_dao.unblockCustomer(customer_id, function(unblock_result) {

        if(unblock_result) {
            callback(unblock_result);
        }

    });

};

// block taxi

module.exports.blockTaxi = function (taxi_id, callback) {

    taxi_dao.blockTaxi(taxi_id, function(block_result) {

        if(block_result) {
            callback(block_result);
        }

    });

};

// unblock taxi

module.exports.unblockTaxi = function (taxi_id, callback) {

    taxi_dao.unblockTaxi(taxi_id, function(unblock_result) {

        if(unblock_result) {
            callback(unblock_result);
        }

    });

};