var mongoose = require('mongoose');
var moment = require('moment-jalaali');
moment.loadPersian();
var config = require('./../config/config');

var db = mongoose.createConnection(config.database);

var ObjectId = mongoose.Schema.Types.ObjectId;

var commentSchema = mongoose.Schema({

    text: String,
    star: Number,
    date: String,
    time: String

});

var rateTravelSchema = mongoose.Schema({

    travel_id: ObjectId,
    taxi_id: ObjectId,
    customer_id: ObjectId,
    date: String,
    rate: Number,
    time: String

});

var Comment = db.model('Comment', commentSchema);

var RateTravel = db.model('RateTravel', rateTravelSchema);


// define new comment

module.exports.defineComment = function (text, star, callback) {

    var comment = new Comment({

        text: text,
        star: star,
        date: moment().format('jYYYY/jM/jD'),
        time: moment().format('HH:mm')

    });

    comment.save(function (err, result) {

        if(!err) {

            callback(true);

        } else callback (false);

    });

};

// save opinion about travel

module.exports.saveRateTravel = function (customer_id, travel_id, taxi_id, rate, callback) {

    var rateTravel = new RateTravel({

        travel_id: travel_id,
        taxi_id: taxi_id,
        customer_id: customer_id,
        rate: rate,
        date: moment().format('jYYYY/jM/jD'),
        time: moment().format('HH:mm')

    });

    rateTravel.save(function (err, result) {

        if(!err) {

            callback(true);

        } else callback (false);

    });

};

// get comments by category

module.exports.getCommentsByStar = function (star, callback) {

    Comment.find({star: star}, function (err, comments) {
        
        callback(comments);
    });

};

// get comment data

module.exports.getCommentData = function (comment_id, callback) {

    Comment.findOne({_id: comment_id}, function (err, result) {

        if(!err) {

            callback(result);

        }

    });

};

// get all comments

module.exports.getAllComments = function (callback) {

    Comment.find({}, function (err, comments) {

        callback(comments);
    });

};

// remove comment

module.exports.removeComment = function (comment_id, callback) {

    Comment.remove({_id: comment_id}, function (err, result) {

        if(!err) {

            callback(true);

        } else {

            callback(false);

        }

    });

};

// get taxi comments

module.exports.getTaxiComments = function (taxi_id, page, per_page, callback) {

    var page_number = parseInt(page);
    var comment_in_page = parseInt(per_page);
    var start = (page_number - 1) * comment_in_page;

    // get the data and return them

    RateTravel.find({taxi_id: taxi_id})
        .sort({'_id': -1})
        .skip(start)
        .limit(comment_in_page)
        .exec(function (err, result) {

            if (err) {

                callback(false);

            } else {

                callback(result);
            }
        });

};

// get comment data of a travel

module.exports.getTravelComment = function (travel_id, callback) {

    RateTravel.findOne({travel_id: travel_id}, function (err, comment_data) {

        if(!err) {

            callback(comment_data);

        }

    });

};

module.exports.getTaxiAvgRate = function (taxi_id, callback) {

    var taxi_id = new mongoose.Types.ObjectId(taxi_id);

    RateTravel.aggregate([
        { $match: { taxi_id: taxi_id } },
        { $group: {
            _id: '$taxi_id',
            rate: { $avg: '$rate'}
        }}
    ], function (err, results) {

        if (err) {

            console.error(err);

        } else {

            callback(results[0].rate);

        }
    });

}