var mongoose = require('mongoose');
var moment = require('moment-jalaali');
var customer_dao = require('./customer_dao');
var organization_dao = require('./organization_dao');
var turnover_dao = require('./turnover_dao');
var stationController = require('./../controller/station_controller');
var config = require('./../config/config');
var consts = require('./../config/consts');
var mongoosastic = require('mongoosastic');

var db = mongoose.createConnection(config.database);

var ObjectId = mongoose.Schema.Types.ObjectId;

var travelSchema = mongoose.Schema({

  organization_id: ObjectId,
  customer_name: String,
  customer_id: ObjectId,
  customer_phone_number: String,
  source_address: String,
  travel_code: {type: String, es_indexed: true},
  destination_address: String,
  source_place: String,
  destination_place: String,
  source_place_id: ObjectId,
  destination_place_id: ObjectId,
  source_lat: Number,
  source_lan: Number,
  destination_lat: Number,
  destination_lan: Number,
  customer_lat: Number,
  customer_lan: Number,
  state: String,
  cost: Number,
  payment_type: String,
  is_paid: {type: Boolean, default: false},
  discount_code: {type: String, default: ""},
  discount_amount: {type: Number, default: 0},
  date: String,
  taxi_id: ObjectId,
  time_travel: {type: String, default: 0},
  is_active: {type: Number, default: 0},
  refused_taxis: [ObjectId],
  stand_by_taxis: [ObjectId],
  is_taxi_near: Number,
  full_date: {type: String, default: ""},
  readable_date: {type: String, default: ""},
  time: {type: String, default: ""},
  taxi_timer_time: {type: Number, default: 0},
  taxi_found_date: {type: String, default: ""},
  rate_id: {type: ObjectId},
  travel_rate: {type: Number, default: 5},
  stations: [{
    station_id: {type: ObjectId, default: null},
    station_title: {type: String, default: ""}
  }],
  driver_id: {type: String, default: ""},
  distance: {type: Number, default: 0},
  duration: {type: Number, default: 0},
  service_type: {type: String, default: "normal"},
  share_link: {type: String, default: ""},
  driver_rate: {type: Number, default: 5},
  second_destination_place: {type: String, default: ""},
  second_destination_place_id: {type: ObjectId, default: null},
  second_destination_lat: {type: Number, default: 0},
  second_destination_lan: {type: Number, default: 0},
  is_two_way: {type: Boolean, default: false},
  stop_time: {type: String, default: ""},
  stop_time_value: {type: Number, default: 0},
  is_get_travel: {type: Boolean, default: false}

});

Array.prototype.unique = function () {

  var a = this.concat();
  for (var i = 0; i < a.length; ++i) {
    for (var j = i + 1; j < a.length; ++j) {

      if (a[i] === a[j])
        a.splice(j--, 1);

    }
  }

  return a;

};

travelSchema.plugin(mongoosastic, {
  hosts: [
    '127.0.0.1:9200'
  ]
});


var travelNumberSchema = mongoose.Schema({

  order_number: {type: Number, default: 1}

});

var TravelNumber = db.model('TravelNumber', travelNumberSchema);

var Travel = db.model('Travel', travelSchema);

// var stream = Travel.synchronize(function(err){
//     console.log(err);
// })
//     , count = 0;
// stream.on('data', function (err, doc) {
//     count++;
// });
// stream.on('close', function () {
//     console.log('indexed ' + count + ' documents from LeadSearch!');
// });
// stream.on('error', function (err) {
//     console.log(err);
// });

module.exports.createStationTravel = function (station_id, customer_name, customer_id, phone_number, source_lat, source_lan, destination_lat, destination_lan, customer_lat, customer_lan, date, cost, payment_type, is_paid, discount_code, discount_amount, full_date, source_place_id, destination_place_id, source_place, destination_place, driver_id, callback) {

  source_lat = parseFloat(source_lat);
  source_lan = parseFloat(source_lan);
  destination_lat = parseFloat(destination_lat);
  destination_lan = parseFloat(destination_lan);
  customer_lat = parseFloat(customer_lat);
  customer_lan = parseFloat(customer_lan);
  // create a new code for order

  var time_now = moment();

  // get today_order_number

  TravelNumber.update({}, {$inc: {order_number: 1}}, {upsert: true}, function (err, result) {

    TravelNumber.findOne({}, function (err, travel_number) {

      var today_travel_number = travel_number.order_number;
      var travel_code = "P" + time_now.jYear().toString().substr(2, 3) + time_now.jDayOfYear().toString() + today_travel_number;
      console.log(station_id);
      console.log(customer_name);
      console.log(phone_number);
      console.log(driver_id);

      stationController.getStationData(station_id, function (station_data) {

        if (station_data) {

          var travel = new Travel({
            stations: [{
              station_id: station_id,
              station_title: station_data.station_title
            }],
            customer_name: customer_name,
            travel_code: travel_code,
            customer_id: customer_id,
            customer_phone_number: phone_number,
            source_lat: source_lat,
            source_lan: source_lan,
            destination_lat: destination_lat,
            destination_lan: destination_lan,
            customer_lat: customer_lat,
            customer_lan: customer_lan,
            state: "none",
            payment_type: payment_type,
            is_paid: is_paid,
            discount_code: discount_code,
            discount_amount: discount_amount,
            cost: cost,
            is_active: 0,
            date: date,
            time_travel: 0,
            is_taxi_near: 0,
            full_date: "",
            destination_place: destination_place,
            source_place: source_place,
            source_place_id: source_place_id,
            destination_place_id: destination_place_id,
            refused_taxis: [],
            stand_by_taxis: [],
            driver_id: driver_id
          });
          console.log('our travel------------->' + travel);
          travel.save(function (err, result) {

            if (!err) {
              console.log(result);
              callback(result._id);
            } else {
              console.log(err);
              callback(false);
            }
          });

        }

      });


    });
  });
};

module.exports.createOrganizationTravel = function (organization_id, customer_name, customer_id, phone_number, source_lat, source_lan, destination_lat, destination_lan, customer_lat, customer_lan, date, cost, payment_type, is_paid, discount_code, discount_amount, full_date, source_place_id, destination_place_id, source_place, destination_place, driver_id, callback) {

  source_lat = parseFloat(source_lat);
  source_lan = parseFloat(source_lan);
  destination_lat = parseFloat(destination_lat);
  destination_lan = parseFloat(destination_lan);
  customer_lat = parseFloat(customer_lat);
  customer_lan = parseFloat(customer_lan);
  // create a new code for order

  var time_now = moment();

  // get today_order_number

  TravelNumber.update({}, {$inc: {order_number: 1}}, {upsert: true}, function (err, result) {

    TravelNumber.findOne({}, function (err, travel_number) {

      var today_travel_number = travel_number.order_number;

      var travel_code = "P" + time_now.jYear().toString().substr(2, 3) + time_now.jDayOfYear().toString() + today_travel_number;
      console.log(organization_id);
      console.log(customer_name);
      console.log(phone_number);
      console.log(driver_id);
      var travel = new Travel({
        organization_id: organization_id,
        customer_name: customer_name,
        travel_code: travel_code,
        customer_id: customer_id,
        customer_phone_number: phone_number,
        source_lat: source_lat,
        source_lan: source_lan,
        destination_lat: destination_lat,
        destination_lan: destination_lan,
        customer_lat: customer_lat,
        customer_lan: customer_lan,
        state: "none",
        payment_type: payment_type,
        is_paid: is_paid,
        discount_code: discount_code,
        discount_amount: discount_amount,
        cost: cost,
        is_active: 0,
        date: date,
        time_travel: 0,
        is_taxi_near: 0,
        full_date: "",
        destination_place: destination_place,
        source_place: source_place,
        source_place_id: source_place_id,
        destination_place_id: destination_place_id,
        refused_taxis: [],
        stand_by_taxis: [],
        driver_id: driver_id
      });
      console.log('our travel------------->' + travel);
      travel.save(function (err, result) {

        if (!err) {
          console.log(result);
          callback(result._id);
        } else {
          console.log(err);
          callback(false);
        }
      });
    });
  });
};

module.exports.getTravelsStatusByDate = function (start_date, finish_date, callback) {

    Travel.aggregate([
        {
            $match: {
                date: {"$gte": start_date, "$lte": finish_date}
            }
        },

        {
            $group: {
                _id: "$state",
                count: {$sum: 1}
            }
        }

    ], function (err, result) {
        if (err) {
            console.log(err);
        } else {
            callback(result);
        }
    });

}


module.exports.updateTaxiTimerTimeTimeTravelGetTravelByTaxi = function(travel_id, time_travel, taxi_found_date, taxi_timer_time, is_get_travel, callback) {

    Travel.update({_id: travel_id}, {$set: {

        time_travel: time_travel,
        taxi_found_date: taxi_found_date,
        taxi_timer_time: taxi_timer_time,
        is_get_travel: is_get_travel

    }}, function (err, result) {

        if(!err) {

            callback(true);

        }

    })

};

module.exports.updateTravelStatusRateActivate = function (travel_id, state, rate, activate_state, callback) {

    Travel.update({_id: travel_id}, {$set: {state: state, driver_rate: rate, is_active: activate_state}}, function (err, result) {

        if(!err) {

            callback(true);

        }

    });

}


module.exports.createTravel = function (customer_id,
                                        source_lat,
                                        source_lan,
                                        destination_lat,
                                        destination_lan,
                                        customer_lat,
                                        customer_lan,
                                        date,
                                        cost,
                                        payment_type,
                                        is_paid,
                                        discount_code,
                                        discount_amount,
                                        full_date,
                                        readable_date,
                                        time,
                                        source_place_id,
                                        destination_place_id,
                                        source_place,
                                        destination_place,
                                        service_type,
                                        second_destination_place,
                                        second_destination_place_id,
                                        second_destination_lat,
                                        second_destination_lan,
                                        is_two_way,
                                        stop_time,
                                        stop_time_value,
                                        callback) {

  source_lat = parseFloat(source_lat);
  source_lan = parseFloat(source_lan);
  destination_lat = parseFloat(destination_lat);
  destination_lan = parseFloat(destination_lan);
  customer_lat = parseFloat(customer_lat);
  customer_lan = parseFloat(customer_lan);
  second_destination_lat = parseFloat(second_destination_lat);
  second_destination_lan = parseFloat(second_destination_lan);

  console.log("asdasdaksjdksa");
  console.log(stop_time);
  console.log(stop_time_value);
  console.log("aasdkasdl");

  // create a new code for order

  var time_now = moment();

  // get today_order_number

  TravelNumber.update({}, {$inc: {order_number: 1}}, {upsert: true}, function (err, result) {

    TravelNumber.findOne({}, function (err, travel_number) {

      var today_travel_number = travel_number.order_number;

      var travel_code = "P" + time_now.jYear().toString().substr(2, 3) + time_now.jDayOfYear().toString() + today_travel_number;

      customer_dao.getCustomerInfoById(customer_id, function (customer_data) {
        var travel = new Travel({
          travel_code: travel_code,
          customer_id: customer_id,
          customer_phone_number: customer_data.phone_number,
          source_lat: source_lat,
          source_lan: source_lan,
          destination_lat: destination_lat,
          destination_lan: destination_lan,
          customer_lat: customer_lat,
          customer_lan: customer_lan,
          state: "customer_confirmed",
          payment_type: payment_type,
          is_paid: is_paid,
          discount_code: discount_code,
          discount_amount: discount_amount,
          cost: cost,
          is_active: 0,
          date: date,
          time: time,
          time_travel: 0,
          is_taxi_near: 0,
          full_date: full_date,
          readable_date: readable_date,
          destination_place: destination_place,
          source_place: source_place,
          source_place_id: source_place_id,
          destination_place_id: destination_place_id,
          refused_taxis: [],
          stand_by_taxis: [],
          service_type: service_type,
          share_link: consts.SHARE_TRAVEL_ADDRESS + travel_code,
          second_destination_place: second_destination_place,
          second_destination_place_id: second_destination_place_id,
          second_destination_lat: second_destination_lat,
          second_destination_lan: second_destination_lan,
          is_two_way: is_two_way,
          stop_time: stop_time,
          stop_time_value: stop_time_value
        });
        travel.save(function (err, result) {
          if (!err) {
            callback(result._id);
          }
          else {

            console.log("Hereeeeeeeeeeeeeeeeee");
            console.log(err);
            callback(false);
          }
        });

      });
    });
  });
};

module.exports.updateTravelStatus = function (travel_id, state, callback) {

  Travel.update({_id: travel_id}, {$set: {state: state}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {
      callback(false);
    }

  });

};

module.exports.getTravelInfo = function (travel_id, callback) {

  Travel.findOne({_id: travel_id}, function (err, result) {

    if (result && !err) {

      callback(result);

    } else {

      callback(false);

    }

  });

};

module.exports.updateTimeTravel = function (travel_id, time_travel, callback) {

  Travel.update({_id: travel_id}, {$set: {time_travel: time_travel}}, function (err, result) {

    if (!err) {

      callback(true);
    }

  });
};

module.exports.getTravelsList = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var travel_per_page = parseInt(per_page);
  var start = (page_number - 1) * travel_per_page;

  // get the data and return them

  Travel.find({})
    .sort({'_id': -1})
    .skip(start)
    .limit(travel_per_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });

};

module.exports.getOrganizationTravels = function (organization_id, page, per_page, callback) {

  var page_number = parseInt(page);
  var travel_per_page = parseInt(per_page);
  var start = (page_number - 1) * travel_per_page;

  // get the data and return them

  Travel.find({$and: [{$or: [{state: "finished"}, {state: "support_canceled"}, {state: "customer_canceled"}, {state: "taxi_canceled"}, {state: "finish_commented"}, {state: "taxi_confirmed"}, {state: "travel_started"}, {state: "searching"}, {state: "customer_confirmed"}]}, {organization_id: organization_id}]})
    .sort({'_id': -1})
    .skip(start)
    .limit(travel_per_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {
        console.log('travels--------->' + result);
        callback(result);
      }
    });
};

module.exports.getCustomerTravels = function (customer_id, page, per_page, callback) {

  var page_number = parseInt(page);
  var travel_per_page = parseInt(per_page);
  var start = (page_number - 1) * travel_per_page;

  // get the data and return them

  Travel.find({$and: [{$or: [{state: "finished"}, {state: "support_canceled"}, {state: "customer_canceled"}, {state: "taxi_canceled"}, {state: "finish_commented"}, {state: "taxi_confirmed"}, {state: "travel_started"}]}, {customer_id: customer_id}]})
    .sort({'_id': -1})
    .skip(start)
    .limit(travel_per_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });
};

module.exports.setTravelCost = function (travel_id, travel_cost, callback) {

  Travel.update({_id: travel_id}, {$set: {cost: travel_cost}}, function (err, result) {

    if (!err) {

      callback(true);

    }

  });

};

module.exports.getTaxiTravels = function (taxi_id, page, per_page, callback) {

  var page_number = parseInt(page);
  var travel_per_page = parseInt(per_page);
  var start = (page_number - 1) * travel_per_page;

  Travel.find({taxi_id: taxi_id})
    .sort({'_id': -1})
    .skip(start)
    .limit(travel_per_page)
    .exec(function (err, result) {

      if (err) {

        fn(false);

      } else {

        callback(result);
      }
    });

};

module.exports.getActiveTravels = function (callback) {

  Travel.find({is_active: 1}, function (err, travels) {

    if (!err && travels) {

      callback(travels)

    }

  });

};

module.exports.setTravelTaxi = function (travel_id, taxi_id, station_id, station_title, callback) {

  Travel.update({_id: travel_id}, {
    $set: {taxi_id: taxi_id},
    $push: {stations: {station_id: station_id, station_title: station_title}}
  }, function (err, result) {

    if (!err) {

      callback(true);

    }

  });

};

module.exports.getTravelsByFilter = function (travel_state, page, per_page, callback) {

  var page_number = parseInt(page);
  var travel_per_page = parseInt(per_page);
  var start = (page_number - 1) * travel_per_page;

  // get the data and return them

  Travel.find({state: travel_state})
    .sort({'_id': -1})
    .skip(start)
    .limit(travel_per_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });

};

module.exports.getStationTravelsByFilter = function (station_id, travel_state, page, per_page, callback) {

  var page_number = parseInt(page);
  var travel_per_page = parseInt(per_page);
  var start = (page_number - 1) * travel_per_page;

  // get the data and return them

  Travel.find({state: travel_state, 'stations.station_id': station_id})
    .sort({'_id': -1})
    .skip(start)
    .limit(travel_per_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });

};

module.exports.activateTravel = function (travel_id, callback) {

  Travel.update({_id: travel_id}, {$set: {is_active: 1}}, function (err, result) {

    if (!err) {
      callback(true);
    } else {
      callback(false);
    }

  });

};

module.exports.deactivateTravel = function (travel_id, callback) {

  Travel.update({_id: travel_id}, {$set: {is_active: 0}}, function (err, result) {

    if (!err) {
      callback(true);
    } else {
      callback(false);
    }

  });

};

module.exports.isTravelActive = function (travel_id, callback) {

  Travel.findOne({_id: travel_id}, function (err, result) {

    if (!err && result) {

      if (result.is_active) {
        callback(true);
      } else {
        callback(false);
      }

    } else {

      callback(false);
    }

  });
};

module.exports.getTaxiActiveTravels = function (taxi_id, callback) {

    Travel.find({taxi_id: taxi_id, is_active: 1, state: "taxi_founded"})
        .sort({'_id': -1})
        .limit(1)
        .exec(function (err, result) {

            if (err) {

                callback(false);

            } else {

                callback(result[0]);
            }
        });

};

module.exports.getFoundedTravel = function (taxi_id, callback) {

  Travel.findOne({taxi_id: taxi_id, state: "taxi_founded"}, function (err, result) {

    if (!err) {
      callback(result);
    }

  });

};

module.exports.setTaxiNear = function (travel_id, callback) {

  Travel.findOne({_id: travel_id}, function (err, travel_data) {

    if (!err) {

      var is_taxi_near = travel_data.is_taxi_near;

      if (is_taxi_near == 0) {

        Travel.update({_id: travel_id}, {$set: {is_taxi_near: 1}}, function (err, result) {

          if (!err) {

            callback(true);

          }

        });

      } else {

        callback(false);

      }

    }

  });

};

module.exports.setTravelFullDate = function (travel_id, callback) {

  var full_date = moment().format("jYYYY/jM/jD HH:mm:ss");

  Travel.update({_id: travel_id}, {$set: {full_date: full_date}}, function (err, result) {

    if (!err) {
      callback(true);
    }

  });

};

module.exports.updateTravelRate = function (travel_id, rate, callback) {

  Travel.update({_id: travel_id}, {$set: {travel_rate: rate}}, function (err, result) {

    if (!err) {

      callback(true);
    }

  });
};

module.exports.addStandByTaxi = function (travel_id, taxi_id, callback) {

  Travel.update({_id: travel_id}, {$push: {stand_by_taxis: taxi_id}}, function (err, result) {

    if (!err) {

      callback(true);

    }

  });

};

module.exports.addRefusedTaxi = function (travel_id, taxi_id, callback) {

  Travel.update({_id: travel_id}, {$push: {refused_taxis: taxi_id}}, function (err, result) {

    if (!err) {

      callback(true);

    }

  });

};

module.exports.getRefusedStandByTaxis = function (travel_id, callback) {

  Travel.findOne({_id: travel_id}, {
    stand_by_taxis: 1,
    refused_taxis: 1
  }, function (err, travel_data) {

    var refused_taxis = travel_data.refused_taxis;
    var stand_by_taxis = travel_data.stand_by_taxis;
    var final_list;

    if (refused_taxis.length > 0 && stand_by_taxis.length > 0)
      final_list = refused_taxis.concat(stand_by_taxis).unique();
    else if (refused_taxis.length > 0 && stand_by_taxis.length == 0)
      final_list = refused_taxis;
    else if (refused_taxis.length == 0 && stand_by_taxis.length > 0)
      final_list = stand_by_taxis;
    else if (refused_taxis.length == 0 && stand_by_taxis.length > 0)
      final_list = false;
    callback(final_list);

  });

};

module.exports.updateTaxiTimerTime = function (travel_id, taxi_found_date, taxi_timer_time, callback) {

  Travel.update({_id: travel_id}, {
    $set: {
      taxi_found_date: taxi_found_date,
      taxi_timer_time: taxi_timer_time
    }
  }, function (err, result) {

    if (!err) {

      callback(true);
    }

  });

};

module.exports.hasCustomerAnyTravelUntilNow = function (customer_id, callback) {

  Travel.find({
    customer_id: customer_id,
    state: 'finish_commented'
  }, function (err, travel_data) {

    if (!err) {
      callback(travel_data);
    } else callback([]);
  });
};

module.exports.updateIsPaid = function (travel_id, is_paid, callback) {

  Travel.update({_id: travel_id}, {$set: {is_paid: is_paid}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {
      callback(false);
    }

  });

};

module.exports.updateTravelCost = function (travel_id, cost, callback) {

  Travel.update({_id: travel_id}, {$set: {cost: cost}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {
      callback(false);
    }

  });

};

module.exports.getAllTravels = function (callback) {

  Travel.find({}, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

module.exports.getTravelsListByDate = function (start_date, finish_date, callback) {

  Travel.find({date: {"$gte": start_date, "$lte": finish_date}}, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};


module.exports.getStationTravelsListByDate = function (station_id, start_date, finish_date, callback) {

  Travel.find({
    date: {"$gte": start_date, "$lte": finish_date},
    'stations.station_id': station_id
  }, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

module.exports.getStationTravelsList = function (station_id, page, per_page, callback) {

  var page_number = parseInt(page);
  var travel_per_page = parseInt(per_page);
  var start = (page_number - 1) * travel_per_page;

  // get the data and return them
  Travel.aggregate([
   { $project: { id: 1,state: 1,cost: 1,date:1,travel_code: 1,time:1 , state: 1,payment_type:1,is_active:1,stations: { $slice: [ "$stations", -1 ] } } },
   { $match: {"stations.station_id": station_id} },
   { $sort : {'_id': -1} },
   { $skip : start },
   { $limit : travel_per_page }
  ], function (err, result) {
    if (err) {
      callback(false);
    } else {
      callback(result);
    }
  });
//   Travel.find({"stations.station_id": station_id})
//     .sort({'_id': -1})
//     .skip(start)
//     .limit(travel_per_page)
//     .exec(function (err, result) {
//
//       if (err) {
//
//         callback(false);
//
//       } else {
//
//         callback(result);
//       }
//     });
//
};

module.exports.search = function (query, callback) {

  Travel.search({
    query_string: {
      query: query
    }
  }, function (err, results) {

    callback(results.hits.hits);

  });

};

module.exports.getDriverTravelsListByDate = function (taxi_id, start_date, finish_date, callback) {

  Travel.find({
    date: {"$gte": start_date, "$lte": finish_date},
    taxi_id: taxi_id,
    state: {$ne: "taxi_not_found"}
  }, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

module.exports.getDriverRefusedTravelsListByDate = function (taxi_id, start_date, finish_date, callback) {

  Travel.find({
    date: {"$gte": start_date, "$lte": finish_date},
    refused_taxis: taxi_id
  }, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

module.exports.getDriverStandByTravelsListByDate = function (taxi_id, start_date, finish_date, callback) {

  Travel.find({
    date: {"$gte": start_date, "$lte": finish_date},
    stand_by_taxis: taxi_id
  }, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

module.exports.getTravelsDateStat = function (time, callback) {

  Travel.aggregate([
    {
      $match: {
        date: {$gte: time}
      }
    },
    {
      $group: {
        _id: "$date",
        count: {$sum: 1},
      }
    },
    {
      $sort: {
        "_id": 1
      }
    }
  ], function (err, result) {
    if (err) {
      next(err);
    } else {
      callback(result);
    }
  });

};

module.exports.getLatestTravels = function (limit, callback) {

  Travel.find({}).limit(limit).exec(function (err, result) {

    if (!err) {

      callback(result);

    }

  });

}

module.exports.getCustomerTravelsByFilter = function (customer_id, filter, callback) {

  Travel.find({customer_id: customer_id, state: filter}, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

module.exports.setTravelDistanceDuration = function (travel_id, distance, duration, callback) {

  Travel.update({_id: travel_id}, {
    $set: {
      distance: distance,
      duration: duration
    }
  }, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  })

};

module.exports.setTaxiTravelRate = function (travel_id, rate, callback) {

  Travel.update({_id: travel_id}, {$set: {driver_rate: rate}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  })

};

module.exports.getTravelInfoByCode = function (travel_code, callback) {

  Travel.findOne({travel_code: travel_code}, function (err, travel_data) {

    if (!err) {

      callback(travel_data);

    } else {

      callback(false);

    }

  })

};

module.exports.getTaxiTravelsStat = function (taxi_id, callback) {

  Travel.find({taxi_id: taxi_id}, function (err, result) {

    if (!err) {

      callback(result);

    }

  })

};

module.exports.updateTravelPaymentType = function (travel_id, payment_type, callback) {

  Travel.update({_id: travel_id}, {$set: {payment_type: payment_type}}, function (err, result) {

    if (!err) {

      callback(true);

    }

  })

}

module.exports.getCustomerDiscountTravels = function (customer_id, discount_code, callback) {

  Travel.find({
    customer_id: customer_id,
    discount_code: discount_code,
    $or: [{state: "finished"}, {state: "finish_commented"}]
  }, function (err, travels) {

    if (!err) {

      callback(travels);

    }

  })

}

// set travel paid

module.exports.setTravelPaid = function (travel_id, callback) {

  Travel.update({_id: travel_id}, {$set: {is_paid: true}}, function (err, result) {

    if (!err) {

      callback(result);

    }

  })

};


module.exports.setGetTravel = function (travel_id, is_get, callback) {

  Travel.update({_id: travel_id}, {$set: {is_get_travel: is_get}}, function (err, result) {

    if (!err) {

      callback(result);

    }

  })

};

module.exports.updateTravelOption = function (travel_id, second_destination_lan, second_destination_lat, second_destination_place_name, second_destination_place_id, is_two_way, stop_time, stop_time_value, discount_code, discount_amount, travel_cost, callback) {

  Travel.update({_id: travel_id}, {
    $set: {
      second_destination_lan: second_destination_lan,
      second_destination_lat: second_destination_lat,
      second_destination_place: second_destination_place_name,
      second_destination_place_id: second_destination_place_id,
      is_two_way: is_two_way,
      stop_time: stop_time,
      stop_time_value: stop_time_value,
      discount_amount: discount_amount,
      discount_code: discount_code,
      cost: travel_cost
    }
  }, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  })

}

module.exports.getFinishedTaxiTravels = function (taxi_id, page, per_page, callback) {

  var page_number = parseInt(page);
  var travel_per_page = parseInt(per_page);
  var start = (page_number - 1) * travel_per_page;

  Travel.find({taxi_id: taxi_id, $or: [{state: "finished"}, {state: "finish_commented"}]})
    .sort({'_id': -1})
    .skip(start)
    .limit(travel_per_page)
    .exec(function (err, result) {

      if (err) {

        fn(false);

      } else {

        callback(result);
      }
    });

};

module.exports.getStartDateFinishTravels = function (callback) {

  Travel.find({$or: [{state: "finished"}, {state: "finish_commented"}], date: {"$gt": "1396/03/31"}}, function (err, result) {

    if(!err) {

      callback(result);

    }

  })

}

module.exports.getTaxiStartedFinishedTravels = function (callback) {

  Travel.find({$or: [{state: "finished"}, {state: "finish_commented"}, {state: "travel_started"}], date: {"$gt": "1396/03/31"}}, function (err, result) {

    if(!err) {

      callback(result);

    }

  })

}

module.exports.getFinishedCommentedTravelsListByDate = function (start_date, finish_date, callback) {

  Travel.find({date: {"$gte": start_date, "$lte": finish_date}, $or: [{state: "finished"}, {state: "finish_commented"}]}, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

module.exports.ontaxSync = function (callback) {

  Travel.find({}, function (err, travels) {

    for (var i = 0; i < travels.length; i++) {

      (function (cntr) {

          Travel.findOne({_id: travels[cntr]._id}, function(err, travel_data){

              var readable_date = travel_data.date + " " + travel_data.time;

              Travel.update({_id: travels[cntr]._id}, {$set: {

                  distance: 0,
                  duration: 0,
                  service_type: "normal",
                  share_link: "",
                  driver_rate: 5,
                  second_destination_place: "",
                  second_destination_place_id: null,
                  second_destination_lat: 0,
                  second_destination_lan: 0,
                  is_two_way: false,
                  stop_time: "",
                  stop_time_value: 0,
                  is_get_travel: false,
                  readable_date: readable_date,
                  travel_rate: 5

              }}, {upsert: true}, function(err, result){

                  console.log(result);

              });

          });



      })(i);
    }

  });

}



module.exports.ontaxSyncDate = function (callback) {

    Travel.find({}, function (err, travels) {

        for (var i = 0; i < travels.length; i++) {

            (function (cntr) {

                Travel.findOne({_id: travels[cntr]._id}, function(err, travel_data){

                    var readable_date = travel_data.date + " " + travel_data.time;

                    Travel.update({_id: travels[cntr]._id}, {$set: {

                        readable_date: readable_date

                    }}, function(err, result){

                        console.log(result);

                    });

                });

            })(i);
        }

    });

}


module.exports.getCanceledTravelList = function(start_date , end_date , callback)
{

    Travel.aggregate([
        {$match : {state : "customer_canceled" , date : {$gte : start_date , $lte : end_date}}} ,
        {$group : {_id : "$customer_id" , sum : {$sum : 1}}} ,
        {$sort : {sum : -1}} ,
        {$lookup : {from : "customers" , localField : "_id" , foreignField : "_id" , as : "customer"}} ,
        {$unwind : "$customer"} ,
        {$limit : 50}
    ] , function (error , result) {

        if(error)
        {
            callback(error);
            return;
        }


        callback(undefined , result);

    });

};

module.exports.getTopTaxisRefusedTravels = function(start_date , end_date , callback) {

    Travel.aggregate([
        {$match : {date : {$gte : start_date , $lte : end_date}}} ,
        {$unwind : "$refused_taxis"},
        {$group : {_id:"$refused_taxis", sum:{$sum:1}, refused_taxis:{$first:"$refused_taxis"}}},
        {$lookup : {from:"taxis", localField:"refused_taxis", foreignField:"_id" , as:"taxi"}},
        {$sort : {sum : -1}},
        {$unwind : "$taxi"}

    ], function (error , result) {

        if(error)
        {
            callback(error);
            return;
        }

        callback(undefined , result);

    });

};

module.exports.getTopTaxisStandByTravels = function(start_date , end_date , callback) {

    Travel.aggregate([

        {$match : {date : {$gte : start_date , $lte : end_date}}},
        {$unwind : "$stand_by_taxis"} ,
        {$group : {_id : "$stand_by_taxis" , sum : {$sum : 1} , stand_by_taxis : {$first : "$stand_by_taxis"}}} ,
        {$lookup : {from : "taxis" , localField : "stand_by_taxis" , foreignField : "_id" , as : "taxi"}} ,
        {$sort : {sum : -1}},
        {$unwind : "$taxi"}

    ], function (error , result) {

        if(error)
        {
            callback(error);
            return;
        }


        callback(undefined , result);

    });

};
