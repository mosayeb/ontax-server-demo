var mongoose = require('mongoose');
var moment = require('moment-jalaali');
moment.loadPersian();
var config = require('./../config/config');
var moment_persian = require('moment-jalaali');

var db = mongoose.createConnection(config.database);

moment_persian.loadPersian();

var ObjectId = mongoose.Schema.Types.ObjectId;

var transactionSchema = mongoose.Schema({
    organization_id : ObjectId,
    customer_id: ObjectId,
    wallet_id: ObjectId,
    authority: String,
    ref_id: String,
    amount: String,
    description: String,
    is_paid: Boolean,
    date: String,
    readable_date: {type: String, default: ""},
    time: String,
    time_stamp: String

});

var walletSchema = mongoose.Schema({
    organization_id: ObjectId,
    customer_id: ObjectId,
    money: Number

});

var samanBankTransactionSchema = mongoose.Schema({

  customer_id: ObjectId,
  wallet_id: ObjectId,
  payment_link: {type: String, default: ""},
  amount: {type: String, default: '0'},
  description: {type: String, default: ""},
  is_paid: {type: Boolean, default: false},
  date: {type: String, default: ""},
  readable_date: {type: String, default: ""},
  time: {type: String, default: ""},
  time_stamp: {type: String, default: ""},
  ref_num: {type: String, default: ""},
  res_num: {type: String, default: ""},
  secure_pan: {type: String, default: ""},
  state: {type: String, default: ""}

});

var Transaction = db.model('Transaction', transactionSchema);

var Wallet = db.model('Wallet', walletSchema);

var SamanTransaction = db.model('SamanTransaction', samanBankTransactionSchema);

// save transaction

module.exports.saveTransaction = function (customer_id, wallet_id, authority, ref_id, amount, description, is_paid, callback) {

    var transaction = new Transaction({

        customer_id: customer_id,
        wallet_id: wallet_id,
        authority: authority,
        ref_id: ref_id,
        amount: amount,
        description: description,
        is_paid: is_paid,
        date: moment().format('jYYYY/jM/jD'),
        time: moment().format('HH:mm'),
        readable_date: moment_persian().format('dddd jD jMMMM'),
        time_stamp: Date.now()

    });

    transaction.save(function (err, transaction_data) {

        if(!err) {

            callback(transaction_data);

        } else callback (false);

    });

};

// find transaction by authority

module.exports.getTransactionByAuthority = function (authority, callback) {

    Transaction.findOne({authority: authority}, function (err, transaction_info) {

        if(!err && transaction_info) {

            callback(transaction_info);

        } else callback(false);
    });

};

// find transaction by id

module.exports.getTransactionById= function (transaction_id, callback) {

    Transaction.findOne({_id: transaction_id}, function (err, transaction_info) {

        if(!err && transaction_info) {

            callback(transaction_info);

        } else callback(false);
    });

};

// find all organization transaction by id

module.exports.getOrganizationTransactionsById = function (wallet_id, page, per_page, callback) {

    var page_number = parseInt(page);
    var transaction_per_page = parseInt(per_page);
    var start = (page_number - 1) * transaction_per_page;

    Transaction.find({wallet_id: wallet_id})
        .sort({time_stamp: -1})
        .skip(start)
        .limit(transaction_per_page)
        .exec(function (err, result) {

            if (err) {

                callback([]);

            } else {

                callback(result);
            }

        });

};

// find all customer transaction by id

module.exports.getCustomerTransactionsById= function (wallet_id, page, per_page, callback) {

    var page_number = parseInt(page);
    var transaction_per_page = parseInt(per_page);
    var start = (page_number - 1) * transaction_per_page;

    Transaction.find({wallet_id: wallet_id})
        .sort({time_stamp: -1})
        .skip(start)
        .limit(transaction_per_page)
        .exec(function (err, result) {

            if (err) {

                callback([]);

            } else {

                callback(result);
            }

        });

};

module.exports.getCustomerSamanTransactionsByWalletId= function (wallet_id, page, per_page, callback) {

    var page_number = parseInt(page);
    var transaction_per_page = parseInt(per_page);
    var start = (page_number - 1) * transaction_per_page;

    SamanTransaction.find({wallet_id: wallet_id})
        .sort({time_stamp: -1})
        .skip(start)
        .limit(transaction_per_page)
        .exec(function (err, result) {

            if (err) {

                callback([]);

            } else {

                callback(result);
            }

        });

};

// update transaction. ref_id and is_paid

module.exports.updateRefIdAndIsPaid = function (transaction_id, ref_id, is_paid, callback) {

    Transaction.update({_id: transaction_id}, {$set: {ref_id: ref_id, is_paid: is_paid}}, function (err, update_transaction) {

        if(!err) {

            callback(true);

        } else callback(false);
    });

};

// create organization wallet

module.exports.createOrganizationWallet = function (organization_id, money, callback) {

    var wallet = new Wallet({

        organization_id: organization_id,
        money: money

    });

    wallet.save(function (err, result) {

        if(!err) {

            callback(result);


        } else callback (false);

    });

};

// create wallet

module.exports.createWallet = function (customer_id, money, callback) {

    var wallet = new Wallet({

        customer_id: customer_id,
        money: money

    });

    wallet.save(function (err, result) {

        if(!err) {

            callback(result);

        } else callback (false);

    });

};

// find wallet of customer by customer_id

module.exports.getWalletInfoByCustomerId = function (customer_id, callback) {

    Wallet.findOne({customer_id: customer_id}, function (err, wallet_data) {

        if(!err) {

            callback(wallet_data);

        }

    });

};

module.exports.getWalletInfoByOrganizationId = function (organization_id, callback) {

    Wallet.findOne({organization_id: organization_id}, function (err, wallet_data) {

        if(!err) {

            callback(wallet_data);

        }

    });

};

// find wallet of customer by wallet_id

module.exports.getWalletInfoByWalletId = function (wallet_id, callback) {

    Wallet.findOne({_id: wallet_id}, function (err, wallet_info) {

        if(!err && wallet_info) {

            callback(wallet_info);

        } else callback(false);
    });

};

// update wallet money

module.exports.updateWalletMoney = function (wallet_id, money, callback) {

    Wallet.update({_id: wallet_id}, {$set: {money: money}}, function (err, update_wallet_data) {

        if(!err) {

            callback(true);

        } else callback(false);

    });

};

// increment wallet money

module.exports.incrementWalletMoney = function (wallet_id, money, callback) {

    Wallet.update({_id: wallet_id}, {$inc: {money: money}}, function (err, update_wallet_data) {

        if(!err) {

            callback(true);

        } else callback(false);
    });

};

// decrement wallet money

module.exports.decrementWalletMoney = function (wallet_id, money, callback) {

    Wallet.update({_id: wallet_id}, {$inc: {money: -money}}, function (err, update_wallet_data) {

      console.log(update_wallet_data);

        if(!err) {

            callback(true);

        } else callback(false);
    });

};

// save new saman transaction

module.exports.saveSamanTransaction = function (customer_id, wallet_id, payment_link, res_num, amount, description, state, callback) {

  var transaction = new SamanTransaction({

    customer_id: customer_id,
    wallet_id: wallet_id,
    payment_link: payment_link,
    res_num: res_num,
    amount: amount,
    description: description,
    state: state,
    is_paid: false,
    date: moment().format('jYYYY/jM/jD'),
    time: moment().format('HH:mm'),
    readable_date: moment_persian().format('dddd jD jMMMM'),
    time_stamp: Date.now()

  });

  transaction.save(function (err, transaction_data) {

    if(!err) {

      callback(transaction_data);

    } else {
      console.log(err);
      callback (false);

    }

  });

};

// find transaction by authority

module.exports.getSamanTransactionByPaymentLink = function (payment_link, callback) {

  SamanTransaction.findOne({payment_link: payment_link}, function (err, transaction_info) {

    if(!err && transaction_info) {

      callback(transaction_info);

    } else callback(false);
  });

};

// find transaction by id

module.exports.getSamanTransactionById= function (transaction_id, callback) {

  SamanTransaction.findOne({_id: transaction_id}, function (err, transaction_info) {

    if(!err && transaction_info) {

      callback(transaction_info);

    } else callback(false);
  });

};

// find transaction by res id

module.exports.getSamanTransactionByResNum= function (res_num, callback) {

  SamanTransaction.findOne({res_num: res_num}, function (err, transaction_info) {

    if(!err && transaction_info) {

      callback(transaction_info);

    } else callback(false);
  });

};

// find transaction by ref number

module.exports.getSamanTransactionByRefNum= function (ref_num, callback) {

  SamanTransaction.findOne({ref_num: ref_num}, function (err, transaction_info) {

    if(!err && transaction_info) {

      callback(transaction_info);

    } else callback(false);

  });

};

// update saman transaction

module.exports.updateTransactionPaymentData = function (transaction_id, state, ref_num, secure_pan, is_paid, callback) {

  SamanTransaction.update({_id: transaction_id}, {$set: {

    state: state,
    ref_num: ref_num,
    secure_pan: secure_pan,
    is_paid: is_paid

  }}, function (err, update_transaction) {

    if(!err) {

      callback(true);

    } else callback(false);
  });

};