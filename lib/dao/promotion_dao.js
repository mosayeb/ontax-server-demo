var mongoose = require('mongoose');
var moment = require('moment-jalaali');
moment.loadPersian();
var config = require('./../config/config');

var db = mongoose.createConnection(config.database);

var ObjectId = mongoose.Schema.Types.ObjectId;

var discountSchema = mongoose.Schema({

  amount: Number, // in percent
  code: String,
  description: String,
  is_active: Boolean,
  place_ids: {type: [ObjectId], default: []},
  max_cost: Number,
  is_global: {type: Boolean, default: true},
  days: Number,
  count: {type: Number, default: 1},
  date: String,
  time: String,
  time_stamp: Number

});

var Discount = db.model('Discount', discountSchema);


module.exports.defineNewDiscount = function (amount, code, description, is_active, place_id, days, max_cost, is_global, count, callback) {

  var place = [];
  place.push(place_id);

  console.log(amount);
  console.log(code);
  console.log(description);
  console.log(is_active);
  console.log(place_id);
  console.log(days);
  console.log(max_cost);
  console.log(is_global);
  console.log(count);

  var discount = new Discount({

    amount: amount,
    code: code,
    description: description,
    is_active: is_active,
    place_ids: place,
    days: days,
    date: moment().format('jYYYY/jM/jD'),
    time: moment().format('HH:mm'),
    time_stamp: Date.now(),
    max_cost: max_cost,
    is_global: is_global,
    count: count

  });

  discount.save(function (err, discount_data) {

    if (err) {
      throw err;
      callback(false);
    } else callback(discount_data);
  });
};

module.exports.getDiscountInfoById = function (discount_id, callback) {

  Discount.findOne({_id: discount_id}, function (err, discount_info) {

    if (!err) {
      if (discount_info) callback(discount_info);
      else callback(false);
    } else callback(false);

  });
};

module.exports.getDiscountInfoByCode = function (discount_code, callback) {

  Discount.findOne({code: discount_code}, function (err, discount_info) {

    if (!err) {
      if (discount_info) callback(discount_info);
      else callback(false);
    } else callback(false);

  });
};

module.exports.getDiscountsList = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var discount_per_page = parseInt(per_page);
  var start = (page_number - 1) * discount_per_page;

  // get the data and return them

  Discount.find({})
    .sort({'_id': -1})
    .skip(start)
    .limit(discount_per_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });

};

module.exports.editDiscount = function (discount_id, days, description, is_active, max_cost, callback) {


  Discount.update({_id: discount_id}, {
    $set: {
      days: days,
      description: description,
      is_active: is_active,
      max_cost: max_cost
    }
  }, function (err, result) {

    if (!err) {

      callback(false);

    } else {

      throw err;

    }

  });

};

module.exports.addDiscountPlace = function (discount_id, place_id, callback) {

  Discount.update({_id: discount_id}, {$addToSet: {place_ids: place_id}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.removeDiscountPlace = function (discount_id, place_id, callback) {

  Discount.update({_id: discount_id}, {$pull: {place_ids: place_id}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

