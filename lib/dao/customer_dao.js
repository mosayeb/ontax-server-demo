var mongoose = require('mongoose');
var mongoosastic = require('mongoosastic');
var wallet_dao = require('./wallet_dao');
var config = require('./../config/config');

var db = mongoose.createConnection(config.database);

var ObjectId = mongoose.Schema.Types.ObjectId;

var customerSchema = mongoose.Schema({

  phone_number: {type: String, es_indexed: true},
  verification_code: String,
  is_verified: Number,
  avatar: {type: String, default: ""},
  name: {type: String, es_indexed: true, default: ""},
  family: {type: String, es_indexed: true, default: ""},
  address: {type: String, es_indexed: true, default: ""},
  email: {type: String, es_indexed: true, default: ""},
  is_block: {type: Boolean, default: false},
  caller_number: String,
  sex: {type: String, default: "male"},
  blood_group: {type: String, default: ""},
  emergency_phone_number: {type: String, default: ""},
  birthday: {type: String, default: ""},
  subscription_number: Number,
  station_id: ObjectId,
  address_loc: {type: [Number], index: '2dsphere'},
  fav_address: {type: [ObjectId]},
  is_apple: {type: Boolean, default: false},
  device_id: {type: String, default: ""},
  invite_code: {type: String, default: ""},
  national_code: {type: String, default: ""}
});

customerSchema.plugin(mongoosastic, {
  hosts: [
    '127.0.0.1:9200'
  ]
});

var Customer = db.model('Customer', customerSchema);

// var stream = Customer.synchronize(function(err){
//     console.log(err);
// })
//     , count = 0;
// stream.on('data', function (err, doc) {
//     count++;
// });
// stream.on('close', function () {
//     console.log('indexed ' + count + ' documents from LeadSearch!');
// });
// stream.on('error', function (err) {
//     console.log(err);
// });

module.exports.registerStationCustomer = function (station_id, phone_number, caller_number, customer_name, customer_family, address, address_lat, address_lan, subscription_number, verification_code, callback) {
  var customer = new Customer({
    station_id: station_id,
    phone_number: phone_number,
    verification_code: verification_code,
    is_verified: 0,
    name: customer_name,
    family: customer_family,
    address: address,
    avatar: '',
    email: '',
    is_block: false,
    caller_number: caller_number,
    address_loc: [address_lan, address_lat],
    subscription_number: subscription_number,
    fav_address: [],
  });

  customer.save(function (err, customer_data) {

    if (err) {

      throw err;
      callback(false);

    } else {

      // create wallet

      wallet_dao.createWallet(customer_data._id, 0, function (wallet_data) {

        if (!err) callback(customer_data);
        else callback(false);

      });
    }

  });

};

module.exports.saveOrganizationCustomerData = function (phone_number, caller_number, customer_name, verification_code, callback) {

  Customer.findOne({phone_number: phone_number}, function (err, result) {

    if (result) {
      console.log("************custoemr for update************");
      callback(result);

    } else {
      console.log("************custoemr for save*************");
      var customer = new Customer({

        phone_number: phone_number,
        verification_code: verification_code,
        is_verified: 0,
        name: customer_name,
        address: '',
        avatar: '',
        email: '',
        is_block: false,
        caller_number: caller_number

      });

      customer.save(function (err, customer_data) {

        if (err) {
          throw err;
          callback(false);
        } else {

          // create wallet

          wallet_dao.createWallet(customer_data._id, 0, function (wallet_data) {

            if (!err) callback(customer_data);
            else callback(false);

          });
        }

      });

    }

  });


};

module.exports.saveCustomerData = function (phone_number, name, email, caller_number, verification_code, is_apple, device_id, invite_code, national_code, callback) {

  if (!(is_apple && device_id)) {

    is_apple = false;
    device_id = "";

  }

  if (!email) {

    email = "";

  }

  Customer.findOne({phone_number: phone_number}, function (err, result) {

    if (result) {

      if(email !== "" && caller_number !== "") {

        Customer.update({phone_number: phone_number}, {
          $set: {
            verification_code: verification_code,
            is_apple: is_apple,
            device_id: device_id,
            name: name,
            caller_number: caller_number,
            email: email
          }
        }, function (err, result) {

          if (!err) {

            callback(true);

          }

        });

      }

      if(email !== "" && caller_number == "") {

        Customer.update({phone_number: phone_number}, {
          $set: {
            verification_code: verification_code,
            is_apple: is_apple,
            device_id: device_id,
            name: name,
            email: email
          }
        }, function (err, result) {

          if (!err) {

            callback(true);

          }

        });

      }

      if(email == "" && caller_number == "" ) {

        Customer.update({phone_number: phone_number}, {
          $set: {
            verification_code: verification_code,
            is_apple: is_apple,
            device_id: device_id,
            name: name
          }
        }, function (err, result) {

          if (!err) {

            callback(true);

          }

        });

      }

      if(email == "" && caller_number !== "" ) {

        Customer.update({phone_number: phone_number}, {
          $set: {
            verification_code: verification_code,
            is_apple: is_apple,
            device_id: device_id,
            name: name,
            caller_number: caller_number
          }
        }, function (err, result) {

          if (!err) {

            callback(true);

          }

        });

      }

    } else {

      var customer = new Customer({

        phone_number: phone_number,
        verification_code: verification_code,
        is_verified: 0,
        name: name,
        email: email,
        address: '',
        avatar: '',
        family: '',
        is_block: false,
        caller_number: caller_number,
        is_apple: is_apple,
        device_id: device_id,
        national_code: national_code,
        invite_code: invite_code

      });

      customer.save(function (err, customer_data) {

        if (err) {
          throw err;
          callback(false);
        } else {

          // create wallet

          wallet_dao.createWallet(customer_data._id, 0, function (wallet_data) {

            if (!err) callback(true);

          });
        }

      });

    }

  });


};

module.exports.verifyCode = function (phone_number, verification_code, callback) {

  Customer.findOne({phone_number: phone_number}, function (err, result) {

    if (!err && result) {

      if (result.verification_code === verification_code) {

        Customer.update({phone_number: phone_number}, {$set: {is_verified: 1}}, function (err, result) {

          if (!err) {

            callback(true);

          }

        });

      } else {

        callback(false);

      }

    } else {

      callback(false);

    }

  });

};

module.exports.updateCode = function (phone_number, verification_code, callback) {

  Customer.update({phone_number: phone_number}, {$set: {verification_code: verification_code}}, function (err, result) {

    if (err) throw err;

    callback(true);

  });

};

module.exports.getCustomerInfo = function (phone_number, callback) {

  Customer.findOne({phone_number: phone_number}, function (err, result) {

    if (!err && result) {

      callback(result);

    }

  });

};

module.exports.getCustomerInfoByInviteCode = function (invite_code, callback) {

  Customer.findOne({invite_code: invite_code}, function (err, customer_data) {

    if(!err) {

      callback(customer_data);

    }

  })

}

module.exports.getCustomerInfoById = function (customer_id, callback) {

  Customer.findOne({_id: customer_id}, function (err, result) {

    if (!err && result) {

      callback(result);

    }
    else {
      callback(false);
    }

  });

};

module.exports.getCustomerInfoBySubscriberNum = function (station_id, subscription_number, callback) {

  Customer.findOne({
    station_id: station_id,
    subscription_number: subscription_number
  }, function (err, result) {

    if (!err && result) {

      callback(result);

    }
    else {
      callback(false);
    }

  });

};

module.exports.updateCustomerInfo = function (customer_id, email, address, name, family, sex, blood_group, emergency_phone_number, birthday, callback) {

  Customer.update({_id: customer_id},
    {
      $set: {
        email: email,
        address: address,
        name: name,
        family: family,
        sex: sex,
        blood_group: blood_group,
        birthday: birthday,
        emergency_phone_number: emergency_phone_number

      }
    }, function (err, result) {

      if (!err) {

        callback(true);

      }

    });

};

module.exports.getCustomersList = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var customer_in_page = parseInt(per_page);
  var start = (page_number - 1) * customer_in_page;

  // get the data and return them

  Customer.find({})
    .sort({'_id': -1})
    .skip(start)
    .limit(customer_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });
};

module.exports.search = function (query, callback) {

  Customer.search({
    query_string: {
      query: query
    }
  }, function (err, results) {

    console.log(err);
    console.log(results);

    callback(results.hits.hits);

  });

};

module.exports.isCustomerValid = function (customer_id, callback) {

  Customer.findOne({_id: customer_id}, function (err, customer_data) {

    if (!err && customer_data) {

      if (!customer_data.is_block) {

        callback(true);

      } else {

        callback(false);

      }

    }

  })

};

module.exports.blockCustomer = function (customer_id, callback) {

  Customer.update({_id: customer_id}, {$set: {is_block: true}}, function (err, result) {

    if (!err) {
      callback(true);
    }

  });

};

module.exports.unblockCustomer = function (customer_id, callback) {

  Customer.update({_id: customer_id}, {$set: {is_block: false}}, function (err, result) {

    if (!err) {
      callback(true);
    }

  });

};

module.exports.updateAvatar = function (customer_id, avatar_path, callback) {

  Customer.update({_id: customer_id}, {$set: {avatar: avatar_path}}, function (err, result) {

    if (!err) {

      callback(avatar_path);

    } else {

      console.log(err);

      callback(false);

    }

  });

};

module.exports.getCustomersCount = function (callback) {

  Customer.find({}).count().exec(function (err, result) {

    if (!err) {

      callback(result);

    } else {

      callback(false);

    }

  });

};

module.exports.getCustomersPhoneNumbers = function (callback) {

  Customer.find({}, {phone_number: 1, name: 1, _id: 0}, function (err, result) {

    if (!err) {

      callback(result);

    }

  })

}

module.exports.getStationSubscriberList = function (station_id, page, per_page, callback) {

  var page_number = parseInt(page);
  var taxis_in_page = parseInt(per_page);
  var start = (page_number - 1) * taxis_in_page;

  // get the data and return them

  Customer.find({station_id: station_id})
    .sort({'_id': -1})
    .skip(start)
    .limit(taxis_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });


};

module.exports.phoneSearch = function (phone_number, callback) {

  Customer
    .find({phone_number: new RegExp(phone_number, 'i')})
    .exec(function (err, result) {
      if (err) {
        console.log(err);
      } else {
        callback(result);
      }
    });

};

module.exports.addFavoritePlace = function (customer_id, place_id, callback) {

  Customer.update({_id: customer_id}, {$push: {fav_address: place_id}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false)

    }

  })

}

module.exports.removeFavoritePlace = function (customer_id, place_id, callback) {

  Customer.update({_id: customer_id}, {$pull: {fav_address: place_id}}, function (err, result) {

    if (!err) {

      callback(true);

    } else {

      callback(false)

    }

  })

}

module.exports.getCustomerFavoritePlaces = function (customer_id, callback) {

  Customer.findOne({_id: customer_id}, {fav_address: 1}, function (err, result) {

    if (!err) {

      console.log(result);

      callback(result.fav_address);

    } else {

      callback(false)

    }

  })

}

module.exports.setInviteCode = function (callback) {

  Customer.find({invite_code: {$exists: false}}, function (err, result) {

    console.log(err);
    console.log(result);

    for (var i = 0; i < result.length; i++) {
      (function (cntr) {

        var invite_code = makeid(6);

        Customer.update({_id: result[cntr]._id.toString()}, {$set: {invite_code: invite_code}}, {upsert: true}, function (err, result) {

          console.log(err);
          console.log(result);

          if (cntr == result.length - 1) {

            callback(true);

          }

        });

      })(i);
    }

  })

};

module.exports.updateCallerNumbers = function (callback) {

  Customer.find({caller_number: {$ne: ""}}, function (err, result) {

    console.log(err);
    // console.log(result);

    for (var i = 0; i < result.length; i++) {

      (function (cntr) {

        if(result[cntr].caller_number) {

          var caller_number = result[cntr].caller_number;

          if(caller_number.length <= 6) {

            caller_number = result[cntr].caller_number.toUpperCase();

            Customer.update({_id: result[cntr]._id.toString()}, {$set: {caller_number: caller_number}}, {upsert: true}, function (err, update_result) {

              console.log(err);

              console.log(update_result);

              if (cntr == result.length - 1) {

                callback(true);

              }

            });

          } else if(caller_number.length == 12) {

            console.log(result[cntr].caller_number);

            caller_number = result[cntr].caller_number;

            Customer.findOne({phone_number: caller_number}, function (err, customer_data) {

              if(customer_data) {

                Customer.update({_id: result[cntr]._id.toString()}, {$set: {caller_number: customer_data.invite_code}}, {upsert: true}, function (err, update_result) {

                  console.log(err);

                  console.log(update_result);

                  if (cntr == result.length - 1) {

                    callback(true);

                  }

                });

              }



            })

          }

        }


      })(i);
    }

  })

};

module.exports.updateCallerNumbersTwelveDigits = function (callback) {

  Customer.find({caller_number: {$ne: ""}}, function (err, result) {

    console.log(err);
    // console.log(result);

    for (var i = 0; i < result.length; i++) {

      (function (cntr) {

        if(result[cntr].caller_number) {

          var caller_number = result[cntr].caller_number;

          if(caller_number.length <= 6) {

            caller_number = result[cntr].caller_number.toUpperCase();

            Customer.update({_id: result[cntr]._id.toString()}, {$set: {caller_number: caller_number}}, {upsert: true}, function (err, update_result) {

              console.log(err);

              console.log(update_result);

              if (cntr == result.length - 1) {

                callback(true);

              }

            });

          } else if(caller_number.length == 12) {

            console.log(result[cntr].caller_number);

            caller_number = result[cntr].caller_number;

            Customer.findOne({phone_number: caller_number}, function (err, customer_data) {

              if(customer_data) {

                Customer.update({_id: result[cntr]._id.toString()}, {$set: {caller_number: customer_data.invite_code}}, {upsert: true}, function (err, update_result) {

                  console.log(err);

                  console.log(update_result);

                  if (cntr == result.length - 1) {

                    callback(true);

                  }

                });

              }



            })

          }

        }


      })(i);
    }

  })

};

module.exports.getIosCustomers = function (callback) {

  Customer.find({is_apple: true}, function (err, result) {

    if(!err) {

      callback(result);

    }

  })

}

module.exports.ontaxSync = function (callback) {

  // update Customer Invite Code

  Customer.find({}, function (err, customers) {

    for (var i = 0; i < customers.length; i++) {

      (function (cntr) {

        var invite_code = makeid(6);

        Customer.update({_id: customers[cntr]._id}, {$set: {invite_code: invite_code, national_code: ""}}, {upsert: true}, function(err, result){

          console.log(result);

        });

      })(i);
    }


  })

}

function makeid(length) {

  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  for( var i=0; i < length; i++ )
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;

};