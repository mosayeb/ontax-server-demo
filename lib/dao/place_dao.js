var mongoose = require('mongoose');

var config = require('./../config/config');
var consts = require('./../config/consts');

var db = mongoose.createConnection(config.database);

var ObjectId = mongoose.Schema.Types.ObjectId;

// var placeSchema = mongoose.Schema({
//
//   place_name: {
//     type: String,
//     default: ""
//   },
//   place_lat: {type: Number, default: 0},
//   place_lan: {type: Number, default: 0}
//
//
//
// });

var placeSchema = mongoose.Schema({

    place_name: {
        type: String,
        default: ""
    },
    loc: {type: [Number], index: '2dsphere'}

});

placeSchema.index({place_name: 'text'});

var travelFactorSchema = mongoose.Schema({

  first_place_id: ObjectId,
  second_place_id: ObjectId,
  cost: Number

});

var costBaseFactorSchema = mongoose.Schema({

  factor: Number

});

var costTimeFactorSchema = mongoose.Schema({

  start_time: Number,
  factor: Number

});

var costDistanceFactorSchema = mongoose.Schema({

  start_distance: Number,
  end_distance: Number,
  factor: Number

});

var inputFactorSchema = mongoose.Schema({


});

var serviceFactorSchema = mongoose.Schema({

  normal_factor: {type: Number, default: 1},
  vip_factor: {type: Number, default: 1},
  lady_factor: {type: Number, default: 1},
  stop_factor: {type: Number, default: 200},
  input_factor: {type: Number, default: 2500},
  two_way_factor: {type: Number, default: 2}

});

var Place = db.model('Place', placeSchema);

var Factor = db.model('Factor', travelFactorSchema);

var BaseFactor = db.model('BaseFactor', costBaseFactorSchema);

var TimeFactor = db.model('TimeFactor', costTimeFactorSchema);

var DistanceFactor = db.model('DistanceFactor', costDistanceFactorSchema);

var ServiceFactor = db.model('InputFactor', serviceFactorSchema);

module.exports.addNewFactor = function (first_place_id, second_place_id, cost, callback) {

  Factor.findOne({
    $or: [{
      first_place_id: first_place_id,
      second_place_id: second_place_id
    }, {first_place_id: second_place_id, second_place_id: first_place_id}]
  }, function (err, factor_data) {

    if(!err) {

      if (!factor_data) {

        var new_factor = new Factor({
          first_place_id: first_place_id,
          second_place_id: second_place_id,
          cost: cost
        });

        new_factor.save(function (err, result) {

          if (!err) {

            callback(true, "new");

          } else {

            callback(false, "");

          }

        });

      } else {

        Factor.update({
          $or: [{
            first_place_id: first_place_id,
            second_place_id: second_place_id
          }, {first_place_id: second_place_id, second_place_id: first_place_id}]
        }, {$set: {cost: cost}}, function (err, result) {

          if(!err) {

            callback(true, "update");

          } else {

            callback(false, "");

          }

        });

      }

    } else {

      callback(false, "");

    }

  });

};

module.exports.addNewPlace = function (place_name, place_lan, place_lat, callback) {

  place_lat = parseFloat(place_lat);
  place_lan = parseFloat(place_lan);

  var new_place = new Place({

      place_name: place_name,
      loc: [place_lan, place_lat]

  });

  new_place.save(function (err, result) {

      if (!err) {

          callback(result._id);

      } else {

          callback(false);

      }

  });

  // place_lan = parseFloat(place_lan);
  // place_lat = parseFloat(place_lat);
  //
  // var new_place = new Place({
  //
  //   place_name: place_name,
  //   place_lan: place_lan,
  //   place_lat: place_lat
  //
  // });
  //
  // new_place.save(function (err, result) {
  //
  //   if (!err) {
  //
  //     callback(true);
  //
  //   } else {
  //
  //     callback(false);
  //
  //   }
  //
  // });

};

module.exports.updatePlaceInfo = function (place_id, place_name, place_lat, place_lan, callback) {

  place_lan = parseFloat(place_lan);
  place_lat = parseFloat(place_lat);

  Place.update({_id: place_id}, {
    $set: {
      place_name: place_name,
      loc: [place_lan, place_lat]
    }
  }, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

  // place_lan = parseFloat(place_lan);
  // place_lat = parseFloat(place_lat);
  //
  // Place.update({_id: place_id}, {
  //   $set: {
  //     place_name: place_name,
  //     place_lat: place_lat,
  //     place_lan: place_lan
  //   }
  // }, function (err, result) {
  //
  //   if (!err) {
  //
  //     callback(result);
  //
  //   }
  //
  // });

};

module.exports.removePlace = function (place_id, callback) {

  Place.remove({_id: place_id}, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

module.exports.getPlacesList = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var places_in_page = parseInt(per_page);
  var start = (page_number - 1) * places_in_page;

  // get the data and return them

  Place.find({})
    .sort({'_id': -1})
    .skip(start)
    .limit(places_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });

};

module.exports.getFactorsList = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var factors_in_page = parseInt(per_page);
  var start = (page_number - 1) * factors_in_page;

  // get the data and return them

  Factor.find({})
    .sort({'_id': -1})
    .skip(start)
    .limit(factors_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);

      }
    });

};

module.exports.getAllPlaces = function (callback) {

  Place.find({}, function (err, places) {

    if (!err && places) {

      callback(places);

    }

  });

};

module.exports.getPlaceBoundsLocation = function (min_lat, max_lat, min_lan, max_lan, callback) {

  Place.find({
      place_lat: {"$gt": min_lat, "$lt": max_lat},
      place_lan: {"$gt": min_lan, "$lt": max_lan}
    },
    function (err, places_list) {

      if (places_list) {

        callback(places_list);

      } else {
        callback(false);
      }
    });

};

module.exports.getPlaceInfo = function (place_id, callback) {

  Place.findOne({_id: place_id}, function (err, place_data) {

    if (!err && place_data) {

      callback(place_data);

    } else {

      callback(false);

    }

  });

};

module.exports.getFactorData = function (first_place_id, second_place_id, callback) {

  Factor.findOne({
    $or: [{
      first_place_id: first_place_id,
      second_place_id: second_place_id
    }, {first_place_id: second_place_id, second_place_id: first_place_id}]
  }, function (err, factor_data) {

    if (!err && factor_data) {

      callback(factor_data);

    } else {

      callback([]);

    }

  });

};

module.exports.getFactorDataById = function (factor_id, callback) {

  Factor.findOne({_id: factor_id}, function (err, factor_data) {

    if (!err && factor_data) {

      callback(factor_data);

    }

  });

};

module.exports.updateFactorCost = function (factor_id, factor_cost, callback) {

  Factor.update({_id: factor_id}, {$set: {cost: factor_cost}}, function (err, update_result) {

    if (!err) {

      callback(true);

    }

  });

};

module.exports.removePlaceFactors = function (place_id, callback) {

  Factor.remove({$or: [{first_place_id: place_id}, {second_place_id: place_id}]}, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

module.exports.searchPlaceName = function (search_text, callback) {

  Place
    .find({place_name: new RegExp(search_text, 'i')})
    .sort('_id')
    .limit(6)
    .exec(function (err, result) {
      if (err) {
        console.log(err);
      } else {
        callback(result);
      }
    });
};

module.exports.getLocationPlaceName = function (lat, lan, callback) {

    Place
        .findOne({
            loc: {
                $nearSphere: {
                    $geometry: {
                        type: "Point",
                        coordinates: [lan, lat]
                    }, $maxDistance: consts.PLACE_SEARCH_RADIUS
                }
            }
        })
        .limit(1)
        .exec(function (err, result) {
            if (err) {
                callback(false);
            } else {
                callback(result);
            }
        });

};

module.exports.findTimeFactor = function (time, callback) {

  TimeFactor.findOne({start_time: time}, function (err, factor_data) {

    if(!err) {

      callback(factor_data.factor);

    } else {

      callback(false);

    }

  });

};

module.exports.getAllTimeFactors = function (callback) {

  TimeFactor.find({}, function (err, result) {

    if(!err) {

      callback(result);

    } else {

      callback(false);

    }

  });

};

module.exports.updateBaseFactor = function (factor, callback) {

  BaseFactor.findOne({}, function (req, base_factor) {

    if(base_factor !== null ) {

      if( base_factor.length == 0) {


        var new_base_factor = new BaseFactor({
          factor: factor
        });

        new_base_factor.save(function (err, result) {

          if (!err) {

            callback(true);

          } else {

            callback(false);

          }

        });

      } else {

        BaseFactor.update({_id: base_factor._id}, {$set: {
          factor: factor
        }}, function (err, result) {

          if(!err) {

            callback(true);

          }


        });

      }

    } else {

      var new_base_factor = new BaseFactor({

        factor: factor

      });

      new_base_factor.save(function (err, result) {

        if (!err) {

          callback(true);

        } else {

          callback(false);

        }

      });

    }

  });

};

module.exports.getBaseFactor = function (callback) {

  BaseFactor.findOne({}, function (err, base_factor) {

    if(!err) {

      callback(base_factor);

    } else {

      callback(false);

    }

  });

};

module.exports.setTimeFactor = function (factor_time, factor_value, callback) {

  TimeFactor.update({start_time: factor_time}, {$set: {start_time: factor_time, factor: factor_value}}, {upsert: true, setDefaultsOnInsert: true}, function (err, result) {

      console.log(result);

      if(!err) {

          callback(result);

      } else {

          callback(false);

      }

  });

};

module.exports.getTimeFactor = function (time, callback) {

    TimeFactor.findOne({start_time: time}, function (err, result) {

        if(!err) {

            callback(result);

        } else {

            callback(false);

        }

    });

};

module.exports.setDistanceFactor = function (distance_start, distance_end, factor_value, callback) {

  DistanceFactor.update({start_distance: distance_start, end_distance: distance_end}, {$set: {start_distance: distance_start, end_distance:  distance_end, factor: factor_value}}, {upsert: true, setDefaultsOnInsert: true}, function (err, result) {

    if(!err) {

      callback(result);

    } else {

      callback(false);

    }

  });

};

module.exports.getDistanceFactor = function (distance, callback) {

  var distance_in_km = (distance/1000).toFixed(0);

  console.log('DISTANCE IN KILOMETER: ');
  console.log(distance_in_km);

  DistanceFactor.findOne({start_distance: {$lte: distance_in_km}, end_distance: {$gt: distance_in_km}}, function (err, result) {

    if(!err) {

      console.log('DISTANCE FACTOR: ');
      console.log(result);

      callback(result);

    } else {

      callback(false);

    }

  });

};

module.exports.getAllDistanceFactors = function (callback) {

  DistanceFactor.find({}, function (err, result) {

    if(!err) {

      callback(result);

    } else {

      callback(false);

    }

  });

};

module.exports.updateServiceFactor = function (input_factor, stop_factor, normal_factor, vip_factor, lady_factor, two_way_factor, callback) {

  ServiceFactor.findOne({}, function (req, service_factor) {

    if (service_factor !== null) {

      if (service_factor.length == 0) {

       var new_service_factor = new ServiceFactor({
          input_factor: input_factor,
          stop_factor: stop_factor,
          normal_factor: normal_factor,
          vip_factor: vip_factor,
          lady_factor: lady_factor,
         two_way_factor: two_way_factor
        });

        new_service_factor.save(function (err, result) {

          if (!err) {

            callback(true);

          } else {

            callback(false);

          }

        });
      } else {

        ServiceFactor.update({_id: service_factor._id}, {
          $set: {
            input_factor: input_factor,
            stop_factor: stop_factor,
            normal_factor: normal_factor,
            vip_factor: vip_factor,
            lady_factor: lady_factor,
            two_way_factor: two_way_factor
          }
        }, function (err, result) {

          if (!err) {

            callback(true);

          } else {

            callback(false);

          }

        });

      }

    } else {

      var new_service_factor = new ServiceFactor({
        input_factor: input_factor,
        stop_factor: stop_factor,
        normal_factor: normal_factor,
        vip_factor: vip_factor,
        lady_factor: lady_factor,
        two_way_factor: two_way_factor
      });

      new_service_factor.save(function (err, result) {

        if (!err) {

          callback(true);

        } else {

          callback(false);

        }

      });

    }

  });

};

module.exports.getServiceFactor= function (callback) {

  ServiceFactor.findOne({}, function (err, input_factor) {

    if (!err) {

      callback(input_factor);

    } else {

      callback(false);

    }

  });

};

