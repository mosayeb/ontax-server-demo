/**
 * Created by amir on 10/8/16.
 */

var mongoose = require('mongoose');
var moment = require('moment-jalaali');
var config = require('./../config/config');

var db = mongoose.createConnection(config.database);


var ObjectId = mongoose.Schema.Types.ObjectId;

var commentSchema = mongoose.Schema({

    title: String,
    description: String,
    category: String,
    date: String,
    time: String
});

var opinionsAboutDriverSchema = mongoose.Schema({

    driver_id: ObjectId,
    customer_id: ObjectId,
    comment_id: ObjectId,
    rate: String,
    date: String,
    time: String

});

var Comment = db.model('Comment', commentSchema);
var OpinionsAboutDriver = db.model('OpinionsAboutDriver', opinionsAboutDriverSchema);


// define new comment

module.exports.defineComment = function (title, description, category, callback) {

    var comment = new Comment({

        title: title,
        description: description,
        category: category,
        date: moment().format('jYYYY/jM/jD'),
        time: moment().format('HH:mm')

    });

    comment.save(function (err, result) {

        if(!err) {

            callback(true);

        } else callback (false);

    });

};

// save opinion about driver

module.exports.saveOpinionsAboutDriver = function (customer_id, driver_id, comment_id, rate, callback) {

    var opinionAboutDriver = new OpinionsAboutDriver({

        driver_id: driver_id,
        customer_id: customer_id,
        comment_id: comment_id,
        rate: rate,
        date: moment().format('jYYYY/jM/jD'),
        time: moment().format('HH:mm')

    });

    opinionAboutDriver.save(function (err, result) {

        if(!err) {

            callback(true);

        } else callback (false);

    });

};

