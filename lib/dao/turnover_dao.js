var mongoose = require('mongoose');
var mongoosastic = require('mongoosastic');
var wallet_dao = require('./wallet_dao');
var travel_dao = require('./travel_dao');
var taxi_dao = require('./taxi_dao');
var config = require('./../config/config');
var moment = require('moment-jalaali');
var moment_persian = require('moment-jalaali');
moment_persian.loadPersian();


var db = mongoose.createConnection(config.database);

var ObjectId = mongoose.Schema.Types.ObjectId;

var customerTurnOverSchema = mongoose.Schema({

  date: {type: String},
  full_date: {type: String},
  time: {type: String},
  amount: {type: Number},
  explanation: {type: String, default: ""},
  turnover_type: {type: String, default: ""}, // debtor, creditor
  customer_id: {type: ObjectId}

});

var taxiTurnOverSchema = mongoose.Schema({

  travel_id: {type: ObjectId, default: null},
  date: {type: String},
  full_date: {type: String, default: ""},
  readable_date: {type: String, default: ""},
  time: {type: String, default: ""},
  amount: {type: Number},
  explanation: {type: String, default: ""},
  turnover_type: {type: String, default: ""}, // debtor, creditor, income
  taxi_id: {type: ObjectId}

});

var taxiPonySchema = mongoose.Schema({

  date: {type: String},
  full_date: {type: String, default: ""},
  readable_date: {type: String, default: ""},
  time: {type: String, default: ""},
  pony_type: {type: String, default: ""}, // debt, credit
  amount: {type: Number},
  explanation: {type: String, default: ""},
  taxi_id: {type: ObjectId}

});

var CustomerTurnOver = db.model('CustomerTurnOver', customerTurnOverSchema);

var TaxiTurnOver = db.model('TaxiTurnOver', taxiTurnOverSchema);

var TaxiPony = db.model('TaxiPony', taxiPonySchema);

// var stream = Customer.synchronize(function(err){
//     console.log(err);
// })
//     , count = 0;
// stream.on('data', function (err, doc) {
//     count++;
// });
// stream.on('close', function () {
//     console.log('indexed ' + count + ' documents from LeadSearch!');
// });
// stream.on('error', function (err) {
//     console.log(err);
// });

module.exports.createCustomerTurnover = function (customer_id, amount, turnover_type, explanation, date, time, callback) {

  var customer_turn_over = new CustomerTurnOver({
    customer_id: customer_id,
    amount: amount,
    turnover_type: turnover_type,
    explanation: explanation,
    date: date,
    time: time
  });

  customer_turn_over.save(function (err, customer_data) {

    if (err) {

      throw err;
      callback(false);

    } else {

      callback(true);
    }

  });

};

module.exports.createTaxiTurnover = function (taxi_id, travel_id, amount, turnover_type, explanation, date, readable_date, time, callback) {

  var taxi_turn_over = new TaxiTurnOver({
    taxi_id: taxi_id,
    travel_id: travel_id,
    amount: amount,
    readable_date: readable_date,
    turnover_type: turnover_type,
    explanation: explanation,
    full_date: moment().format("jYYYY/jMM/jDD HH:mm"),
    date: date,
    time: time
  });

  taxi_turn_over.save(function (err, turnover_data) {

    if (err) {

      callback(false);

    } else {

      callback(true);
    }

  });

};

module.exports.getTaxiTransactions = function (taxi_id, page, per_page, callback) {

  var page_number = parseInt(page);
  var transaction_in_page = parseInt(per_page);
  var start = (page_number - 1) * transaction_in_page;

  // get the data and return them

  TaxiTurnOver.find({taxi_id: taxi_id})
    .sort({'full_date': -1})
    .skip(start)
    .limit(transaction_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });
};

module.exports.getTurnOverList = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var transaction_in_page = parseInt(per_page);
  var start = (page_number - 1) * transaction_in_page;

  // get the data and return them

  TaxiTurnOver.find()
    .sort({'_id': -1})
    .skip(start)
    .limit(transaction_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });
};

module.exports.getDateTaxiTurnOverList = function (taxi_id, start_date, finish_date, callback) {

  TaxiTurnOver.find({
    taxi_id: taxi_id,
    date: {"$gte": start_date, "$lte": finish_date}
  }, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};
module.exports.getTaxiTurnOverListFullDate = function (taxi_id, start_date, finish_date, callback) {

  console.log(start_date);
  console.log(finish_date);

  TaxiTurnOver.find({
    taxi_id: taxi_id,
    full_date: {"$gte": start_date, "$lte": finish_date}
  }, function (err, result) {

    if (!err) {

      callback(result);

    }

  });

};

module.exports.getTaxiTurnOverList = function (taxi_id, page, per_page, callback) {

  var page_number = parseInt(page);
  var transaction_in_page = parseInt(per_page);
  var start = (page_number - 1) * transaction_in_page;

  // get the data and return them

  TaxiTurnOver.find({taxi_id: taxi_id})
    .sort({'_id': -1})
    .skip(start)
    .limit(transaction_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }

    });

};

module.exports.createTaxiPony = function (taxi_id, amount, explanation, pony_type, callback) {

  var taxi_pony = new TaxiPony({
    taxi_id: taxi_id,
    amount: amount,
    pony_type: pony_type,
    readable_date: moment_persian().format('dddd jD jMMMM'),
    explanation: explanation,
    full_date: moment().format("jYYYY/jMM/jDD HH:mm"),
    date: moment().format("jYYYY/jMM/jDD"),
    time: moment().format("HH:mm")
  });

  taxi_pony.save(function (err, taxi_pony_data) {

    if (err) {

      callback(false);

    } else {

      callback(true);
    }

  });

};

module.exports.getTaxiPonies = function (taxi_id, page, per_page, callback) {

  var page_number = parseInt(page);
  var pony_in_page = parseInt(per_page);
  var start = (page_number - 1) * pony_in_page;

  // get the data and return them

  TaxiPony.find({taxi_id: taxi_id})
    .sort({'_id': -1})
    .skip(start)
    .limit(pony_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });
};

module.exports.getPonyList = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var pony_in_page = parseInt(per_page);
  var start = (page_number - 1) * pony_in_page;

  // get the data and return them

  TaxiPony.find()
    .sort({'_id': -1})
    .skip(start)
    .limit(pony_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });
};

module.exports.getAllTurnOvers = function (callback) {

  TaxiTurnOver.find({}, function (err, result) {

    if (!err) {

      callback(result);

    }

  })

};

module.exports.getTaxisListTurnOvers = function (taxi_ids, callback) {

  TaxiTurnOver.find({taxi_id: {$in: taxi_ids}}, function (err, result) {

    if (!err) {

      callback(result);

    } else {

      callback(err);

    }

  });

};

module.exports.getLastTaxiPony = function (taxi_id, callback) {

  TaxiPony.find({taxi_id: taxi_id}).
    sort({full_date: -1}).
    limit(1).
    exec(function (err, result) {

      if(!err) {

        callback(result);

      }

  })

}

module.exports.fixTemp = function (callback) {

  TaxiTurnOver.find({turnover_type: "income"}, function (err, result) {

    for (var i = 0; i < result.length; i++) {

      (function (cntr) {

        var turn_over = result[cntr].toObject();

        // var travel_cost = turn_over.amount + 15;
        //
        // var commission_amount = travel_cost * (15 / 100);
        //
        // var final_amount = travel_cost - commission_amount;
        //
        // // update turn over
        //
        // TaxiTurnOver.update({_id: turn_over._id}, {$set: {amount: final_amount}}, function (err, result) {
        //
        //   if (cntr == result.length - 1) {
        //     callback(true);
        //   }
        //
        // })

        travel_dao.getTravelInfo(result[cntr].travel_id, function (travel_data) {

          taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

            taxi_dao.getServiceTypeData(taxi_data.service_type_id, function (taxi_service_data) {

              var taxi_id = travel_data.taxi_id;
              var travel_id = travels[cntr]._id;
              var commission_rate = parseInt(taxi_service_data.commission_rate);
              var commission_amount = travel_data.cost * (commission_rate / 100);
              var income = travel_data.cost - commission_amount;
              var income_explanation = "درآمد سفر " + travel_data.travel_code;
              var cash_deptor_explanation = "کسر کمیسون سفر  " + travel_data.travel_code;
              var cash_creditor_explanation = "افزایش اعتبار سفر  " + travel_data.travel_code;
              // var date = turnovers[0].date;
              // var time = turnovers[0].time;
              // var full_date = turnovers[0].full_date;
              // var readable_date = turnovers[0].readable_date;

              // create turn over income

              TaxiTurnOver.update({_id: turn_over._id}, {$set: {amount: income}}, function (err, result) {

                if (cntr == result.length - 1) {
                  callback(true);
                }

              })
              // createCustomerTaxiTurnover(taxi_id, travel_id, income, "income", income_explanation, date, readable_date, time, full_date, function (result) {});

            });

          });

        });

      })(i);

    }

  })

};

module.exports.removeRedundantTurnOvers = function (callback) {

  // get all new travels travels

  travel_dao.getStartDateFinishTravels(function (travels) {

    for (var i = 0; i < travels.length; i++) {

      (function (cntr) {

        // get transactions list of a travel

        var redundant_turnovers_list = [];

        TaxiTurnOver.find({travel_id: travels[cntr]._id})
          .sort({'_id': 1})
          .skip(2)
          .exec(function (err, turnovers) {

            if (err) {

              callback(false);

            } else {


              for (var j = 0; j < turnovers.length; j++) {

                redundant_turnovers_list.push(turnovers[j]._id);

              }

              TaxiTurnOver.remove({_id: {$in: redundant_turnovers_list}}, function (err, remove_result) {

                console.log(remove_result);

              })

            }

          });

      })(i);

    }

  })

}

module.exports.getTravelTurnOvers = function (travel_id, callback) {

  TaxiTurnOver.find({travel_id: travel_id}, function (err, result) {

      if (!err) {

        callback(result);
      }
    }
  )

};

module.exports.getTopDriversIncomeByDate = function (start_date, finish_date, callback) {

  TaxiTurnOver.aggregate([
    {
      $match: {
        "turnover_type": "income",
        "date": {"$gte": start_date, "$lte": finish_date}
      }
    },
    {
      $group: {
        _id: "$taxi_id",
        income: {
          $sum: "$amount"
        }
      }
    },
    {
      $sort: {
        income: -1
      }
    },
    {
      $limit: 10
    }

  ], function (err, result) {

    if(!err) {

      console.log("\n\n");
      console.log(result);
      console.log("\n\n");

      callback(result);

    }

  });

}

module.exports.removeRedundantTurnOversWithIds = function (turnoverIds, callback) {

  TaxiTurnOver.remove({_id: {$in: turnoverIds}}, function (err, remove_result) {

    console.log(remove_result);

  })

}

module.exports.syncTransactions = function (callback) {

  var now = moment().format('jYYYY/jMM/jDD');
  var start = '1395/05/01';

  travel_dao.getFinishedCommentedTravelsListByDate(start, now, function (travels) {

    for (var i = 0; i < travels.length; i++) {

      (function (cntr) {

        // get transactions list of a travel

        var redundant_turnovers_list = [];

        TaxiTurnOver.find({travel_id: travels[cntr]._id})
          .exec(function (err, turnovers) {

            if (err) {

              callback(false);

            } else {

              if (turnovers.length === 1) {

                // if transaction is debtor

                if (turnovers[0].turnover_type == "debtor") {

                  // create income transaction

                  travel_dao.getTravelInfo(travels[cntr]._id, function (travel_data) {

                    taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

                      taxi_dao.getServiceTypeData(taxi_data.service_type_id, function (taxi_service_data) {

                        var taxi_id = travel_data.taxi_id;
                        var travel_id = travels[cntr]._id;
                        var commission_rate = parseInt(taxi_service_data.commission_rate);
                        var commission_amount = travel_data.cost * (commission_rate / 100);
                        var income = travel_data.cost - commission_rate;
                        var income_explanation = "درآمد سفر " + travel_data.travel_code;
                        var cash_deptor_explanation = "کسر کمیسون سفر  " + travel_data.travel_code;
                        var cash_creditor_explanation = "افزایش اعتبار سفر  " + travel_data.travel_code;
                        var date = turnovers[0].date;
                        var time = turnovers[0].time;
                        var full_date = turnovers[0].full_date;
                        var readable_date = turnovers[0].readable_date;

                        // create turn over income

                        createCustomerTaxiTurnover(taxi_id, travel_id, income, "income", income_explanation, date, readable_date, time, full_date, function (result) {});

                      });

                    });

                  });

                }

                // if transaction is creditor

                if (turnovers[0].turnover_type == "creditor") {

                  // create income transaction

                  travel_dao.getTravelInfo(travels[cntr]._id, function (travel_data) {

                    taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

                      taxi_dao.getServiceTypeData(taxi_data.service_type_id, function (taxi_service_data) {

                        var taxi_id = travel_data.taxi_id;
                        var travel_id = travels[cntr]._id;
                        var commission_rate = parseInt(taxi_service_data.commission_rate);
                        var commission_amount = travel_data.cost * (commission_rate / 100);
                        var income = travel_data.cost - commission_rate;
                        var income_explanation = "درآمد سفر " + travel_data.travel_code;
                        var cash_deptor_explanation = "کسر کمیسون سفر  " + travel_data.travel_code;
                        var cash_creditor_explanation = "افزایش اعتبار سفر  " + travel_data.travel_code;
                        var date = turnovers[0].date;
                        var time = turnovers[0].time;
                        var full_date = turnovers[0].full_date;
                        var readable_date = turnovers[0].readable_date;

                        // create turn over income

                        createCustomerTaxiTurnover(taxi_id, travel_id, income, "income", income_explanation, date, readable_date, time, full_date, function (result) {});

                      });

                    });

                  });

                }

                // if its just income

                if (turnovers[0].turnover_type == "income") {

                  // create debtor or creditor transaction

                  travel_dao.getTravelInfo(travels[cntr]._id, function (travel_data) {

                    taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

                      taxi_dao.getServiceTypeData(taxi_data.service_type_id, function (taxi_service_data) {

                        var taxi_id = travel_data.taxi_id;
                        var travel_id = travels[cntr]._id;
                        var commission_rate = parseInt(taxi_service_data.commission_rate);
                        var commission_amount = travel_data.cost * (commission_rate / 100);
                        var income = travel_data.cost - commission_rate;
                        var income_explanation = "درآمد سفر " + travel_data.travel_code;
                        var cash_deptor_explanation = "کسر کمیسون سفر  " + travel_data.travel_code;
                        var cash_creditor_explanation = "افزایش اعتبار سفر  " + travel_data.travel_code;
                        var date = turnovers[0].date;
                        var time = turnovers[0].time;
                        var full_date = turnovers[0].full_date;
                        var readable_date = turnovers[0].readable_date;

                        console.log(travel_data);

                        // create turn over income

                        // createCustomerTaxiTurnover(taxi_id, travel_id, income, "income", income_explanation, date, readable_date, time, full_date, function (result) {});

                        if (travel_data.payment_type == "cash") {

                          createCustomerTaxiTurnover(taxi_id, travel_id, commission_amount, "debtor", cash_deptor_explanation, date, readable_date, time, full_date, function (result) {
                          });

                          // decrease taxi credit

                          // taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function(result){});

                          // set travel paid

                          // travel_dao.setTravelPaid(travel_id, function (result) {
                          // });

                        } else if (travel_data.payment_type == "wallet" && travel_data.is_paid) {

                          // add a turn over transaction

                          createCustomerTaxiTurnover(taxi_id, travel_id, income, "creditor", cash_creditor_explanation, date, readable_date, time, full_date, function (result) {
                          });

                          // add credit to taxi

                          // taxi_dao.increaseTaxiCredit(taxi_id, income, function(result){});


                        } else if (travel_data.payment_type == "wallet" && !travel_data.is_paid) {

                          createCustomerTaxiTurnover(taxi_id, travel_id, commission_amount, "debtor", cash_deptor_explanation, date, readable_date, time, full_date, function (result) {});

                          // decrease taxi credit

                          // taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function(result){});

                          // change travel payment type

                          // travel_dao.updateTravelPaymentType(travel_id, "cash", function (result) {});

                          // travel_dao.setTravelPaid(travel_id, function (result) {
                          // });

                        }

                      });

                    });

                  });


                }

              } else if (turnovers.length === 0) {

                console.log("this is 0 trans " + travels[cntr].travel_code);
                // console.log(turnovers);

              } else {

                console.log("this is more than 1 trans " + travels[cntr].travel_code);

              }


              // for(var j=0; j<turnovers.length; j++) {
              //
              //   redundant_turnovers_list.push(turnovers[j]._id);
              //
              // }
              //
              // TaxiTurnOver.remove({_id: {$in: redundant_turnovers_list}}, function (err, remove_result) {
              //
              //   console.log(remove_result);
              //
              // })

            }

          });

      })(i);

    }


  });

}

function createCustomerTaxiTurnover(taxi_id, travel_id, amount, turnover_type, explanation, date, readable_date, time, full_date, callback) {

  var taxi_turn_over = new TaxiTurnOver({
    taxi_id: taxi_id,
    travel_id: travel_id,
    amount: amount,
    readable_date: readable_date,
    turnover_type: turnover_type,
    explanation: explanation,
    full_date: full_date,
    date: date,
    time: time
  });

  taxi_turn_over.save(function (err, turnover_data) {

    if (err) {

      callback(false);

    } else {

      callback(true);
    }

  });

};

