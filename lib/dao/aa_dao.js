var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');
var config = require('./../config/config');

var db = mongoose.createConnection(config.database);

var ObjectId = mongoose.Schema.Types.ObjectId;

var AccountSchema = new Schema({
  username: String,
  password: String,
  station_id: {type: ObjectId, default: null},
  account_type: {default: "station", type: String} // [admin, operator, station]
});

AccountSchema.plugin(passportLocalMongoose);

Account = db.model('Account', AccountSchema);

module.exports.Account = db.model('Account', AccountSchema);


module.exports.changePassword = function (username, password, callback) {

    Account.findByUsername(username).then(function(sanitizedUser){
        if (sanitizedUser){
            sanitizedUser.setPassword(password, function(){
                sanitizedUser.save();
                callback(true);
            });
        } else {
            callback(false);
        }
    },function(err){
        console.error(err);
    })

};


module.exports.getAccountByUsername = function(username, callback) {

    Account.findOne({username: username}, function(err, result) {

        if(!err && result) {
            callback(result);
        }

    });

};