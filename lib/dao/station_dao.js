var mongoose = require('mongoose');
var mongoosastic = require('mongoosastic');
//var uniqueValidator = require('mongoose-unique-validator');
var config = require('./../config/config');

var db = mongoose.createConnection(config.database);

var ObjectId = mongoose.Schema.Types.ObjectId;

var stationSchema = mongoose.Schema({

  title: {type: String, required: true, lowercase: true, trim: true},
  username: {type: String, default: ""},
  loc: {type: [Number], index: '2dsphere'},
  phone_number: {type: String, es_indexed: true, default: ""},
  mobile_number: {type: String, default: ""},
  address: {type: String, default: ""}

});


var stationSocketScheme = mongoose.Schema({

  socket_id: String,
  type: String,
  station_id: ObjectId

});

var StationSocket = db.model('StationSocket', stationSocketScheme);

var Station = db.model('Station', stationSchema);


module.exports.registerStation = function(title, location_lat, location_lan, phone_number, mobile_number, address, username, callback){

  var new_station = new Station({

    title: title,
    loc: [location_lan, location_lat],
    phone_number: phone_number,
    mobile_number: mobile_number,
    address: address,
    username: username

  });

  new_station.save(function (err, result) {

    if(!err) {

      callback(true, result);

    } else {

      callback(false, null);

    }

  });

};

module.exports.getStationData = function (station_id, callback) {

  Station.findOne({_id: station_id}, function (err, station_data) {
    if(!err) {

      callback(station_data);

    } else {

      callback(false)

    }

  })

};

module.exports.getAllStations = function (callback) {

  Station.find({}, function (err, stations) {

    if(!err) {

      callback(stations);

    }

  });

}

module.exports.getStationsList = function (page, per_page, callback) {

  var page_number = parseInt(page);
  var stations_in_page = parseInt(per_page);
  var start = (page_number - 1) * stations_in_page;

  // get the data and return them

  Station.find({})
    .sort({'_id': -1})
    .skip(start)
    .limit(stations_in_page)
    .exec(function (err, result) {

      if (err) {

        callback(false);

      } else {

        callback(result);
      }
    });

};

module.exports.editStation = function (station_id, title, phone_number, mobile_number, address, callback) {

  Station.update({_id: station_id}, {$set: {title: title, phone_number: phone_number, mobile_number: mobile_number, address: address}}, function (err, result) {

    if(!err) {

      callback(true);

    } else {

      callback(false);

    }

  })

};


/*          start borna      */

module.exports.getStationByUsername = function(username, callback) {

    Station.findOne({username: username}, function(err, result) {

        if(!err && result) {
            callback(result);
        }

    });

};

module.exports.isStationValid = function (station_id, callback) {

    Station.findOne({_id: station_id}, function (err, station_data) {

        if (!err && station_data) {

            if (!station_data.is_block) {

                callback(true);

            } else {

                callback(false);

            }

        }

    })

};

module.exports.getStationInfoById = function (station_id, callback) {

    Station.findOne({_id: station_id}, function (err, result) {

        if (!err && result) {

            callback(result);

        }
        else{
            callback(false);
        }

    });

};

