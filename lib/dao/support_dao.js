var customer_dao = require('./customer_dao');
var mongoose = require('mongoose');
var moment = require('moment-jalaali');
moment.loadPersian();

var config = require('./../config/config');

var db = mongoose.createConnection(config.database);

var ObjectId = mongoose.Schema.Types.ObjectId;

var customerMessageSchema = mongoose.Schema({

    customer_id: ObjectId,
    customer_name: String,
    customer_phone_number: String,
    title: String,
    body: String

});

var nofitySchema = mongoose.Schema({

    current_id: {type: Number, default: 1},
    url: String,
    title: String,
    ticker: String,
    body: String

});

var socketScheme = mongoose.Schema({
    socket_id: String,
    type: String
});

var appVersionScheme = mongoose.Schema({

    version_code: Number,
    url: {type: String, default: ""},
    is_force_update: Number,
    last_force_version: Number,
    is_deprecated: {type: Boolean, default: false},
    package_name: {type: String, default: ""}

});

var IosVersionScheme = mongoose.Schema({
    version_code: Number,
    url: {type: String, default: ""},
    is_force_update: Number,
    last_force_version: Number
});

var DriverVersionScheme = mongoose.Schema({
    version_code: Number,
    url: {type: String, default: ""},
    is_force_update: Number,
    last_force_version: Number
});

var DriverIosVersionScheme = mongoose.Schema({
    version_code: Number,
    url: {type: String, default: ""},
    is_force_update: Number,
    last_force_version: Number
});

var templateMessageSchema = mongoose.Schema({

    title: String,
    explanation: {type: String, default: ""},
    date: String,
    time: String

});

var serviceCommissionRate = mongoose.Schema({

    title: String,
    rate: {type: Number, default: 13},
    explanation: {type: String, default: ""},

});

var customerTemplateMessageSchema = mongoose.Schema({

    customer_id: ObjectId,
    customer_phone_number: {type: String, default: ""},
    template_message_title: {type: String, default: ""},
    explanation: {type: String, default: ""},
    template_message_id: ObjectId,
    date: String,
    time: String

});

var activeSystemSchema = mongoose.Schema({

    is_active: {type: Boolean, default: true},
    message: String

});

var driverBannerSchema = mongoose.Schema({

    first_title: {type: String, default: ""},
    first_body: {type: String, default: ""},
    second_title: {type: String, default: ""},
    second_body: {type: String, default: ""}

});

var supportPhoneSchema = mongoose.Schema({

    driver_support: {type: String, default: ""},
    passenger_support: {type: String, default: ""},

});

var driverTelegramLinkSchema = mongoose.Schema({

  telegram_link: {type: String, default: ""}

});

var TemplateMessage = db.model('TemplateMessage', templateMessageSchema);

var CustomerTemplateMessage = db.model('CustomerTemplateMessage', customerTemplateMessageSchema);

var CustomerMessage = db.model('CustomerMessage', customerMessageSchema);

var PanelSocket = db.model('PanelSocket', socketScheme);

var AppVersion = db.model('AppVersion', appVersionScheme);

var IosVersion = db.model('IosVersion', IosVersionScheme);

var DriverVersion = db.model('DriverVersion', DriverVersionScheme);

var DriverIosVersion = db.model('DriverIosVersion', DriverIosVersionScheme);

var Notify = db.model('Notify', nofitySchema);

var ActiveSystem = db.model('ActiveSystem', activeSystemSchema);

var ServiceCommissionRate = db.model('ServiceCommissionRate', serviceCommissionRate);

var DriverBanner = db.model('DriverBanner', driverBannerSchema);

var SupportPhone = db.model('SupportPhone', supportPhoneSchema);

var DriverTelegramLink = db.model('DriverTelegramLink', driverTelegramLinkSchema);

// define new comment

module.exports.createTemplateMessage = function (title, explanation, callback) {

    var templateMessage = new TemplateMessage({

        title: title,
        explanation: explanation,
        date: moment().format('jYYYY/jM/jD'),
        time: moment().format('HH:mm')

    });

    templateMessage.save(function (err, result) {

        if (!err) {

            callback(true);

        } else callback(false);

    });

};

// save opinion about travel

module.exports.saveCustomerTemplateMessage = function (customer_id, template_message_id, explanation, customer_phone_number, template_message_title, callback) {

    var customerTemplateMessage = new CustomerTemplateMessage({

        customer_id: customer_id,
        customer_phone_number: customer_phone_number,
        template_message_id: template_message_id,
        template_message_title: template_message_title,
        explanation: explanation,
        date: moment().format('jYYYY/jM/jD'),
        time: moment().format('HH:mm')

    });

    customerTemplateMessage.save(function (err, result) {

        if (!err) {

            callback(true);

        } else callback(false);

    });

};

// get comment data

module.exports.getTemplateMessageData = function (message_id, callback) {

    TemplateMessage.findOne({_id: message_id}, function (err, result) {

        if (!err) {

            callback(result);

        }

    });

};

// get all template messages

module.exports.getTemplateMessages = function (callback) {

    TemplateMessage.find({}, function (err, result) {

        if (!err) {

            callback(result);

        }

    });

};

// remove template message

module.exports.removeTemplateMessage = function (message_id, callback) {

    TemplateMessage.remove({_id: message_id}, function (err, result) {

        if (!err) {

            callback(true);

        } else {

            callback(false);

        }

    })

}

module.exports.saveMessage = function (customer_id, title, body, callback) {

    customer_dao.getCustomerInfoById({_id: customer_id}, function (customer_data) {

        if (customer_data) {

            var message = new CustomerMessage({

                customer_id: customer_id,
                customer_phone_number: customer_data.phone_number,
                customer_name: customer_data.name,
                body: body,
                title: title
            });

            message.save(function (err, result) {

                if (!err) {

                    callback(true);

                }

            });

        }

    });

};

module.exports.updateSupportInfo = function (socket_id, type, callback) {

    PanelSocket.update({type: type}, {socket_id: socket_id}, {multi: true}, function (err, result) {

        if (!err) {
            callback(true);
        }

    });

};

module.exports.getSupportSocketInfo = function (callback) {

    PanelSocket.findOne({type: "support"}, function (err, socket_data) {

        if (!err) {
            callback(socket_data);
        }

    });

};

module.exports.getCustomerTemplateMessagesList = function (page, per_page, callback) {

    var page_number = parseInt(page);
    var messages_in_page = parseInt(per_page);
    var start = (page_number - 1) * messages_in_page;

    // get the data and return them

    CustomerTemplateMessage.find({})
        .sort({'_id': -1})
        .skip(start)
        .limit(messages_in_page)
        .exec(function (err, result) {

            if (err) {

                callback(false);

            } else {

                callback(result);
            }
        });
};

module.exports.updateAppVersion = function (version_code, is_force_update, url, callback) {

    AppVersion.findOne({}, function (req, app_versions) {

        if (app_versions !== null) {

            if (app_versions.length == 0) {


                var new_app_version = new AppVersion({
                    version_code: version_code,
                    url: url,
                    is_force_update: is_force_update,
                    last_force_version: version_code
                });

                new_app_version.save(function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });
            } else {

                if (is_force_update == 1) {

                    AppVersion.update({_id: app_versions._id}, {
                        $set: {
                            version_code: version_code,
                            is_force_update: is_force_update,
                            last_force_version: version_code,
                            url: url
                        }
                    }, function (err, result) {

                        if (!err) {

                            callback(true);

                        } else {

                            callback(false);

                        }

                    });

                } else {

                    AppVersion.update({_id: app_versions._id}, {
                        $set: {
                            version_code: version_code,
                            is_force_update: is_force_update,
                            url: url
                        }
                    }, function (err, result) {

                        if (!err) {

                            callback(true);

                        } else {

                            callback(false);

                        }

                    });

                }

            }

        } else {

            var new_app_version = new AppVersion({
                version_code: version_code,
                is_force_update: is_force_update,
                last_force_version: version_code,
                url: url
            });

            new_app_version.save(function (err, result) {

                if (!err) {

                    callback(true);

                } else {

                    callback(false);

                }

            });

        }

    });

};

module.exports.setAppDeprecated = function (is_deprecate, package_name, url, callback) {

    AppVersion.findOne({}, function (err, app_versions) {

        if (app_versions == null) {

            // var new_app_version = new AppVersion({
            //   version_code: version_code,
            //   url: url,
            //   is_force_update: is_force_update,
            //   last_force_version: version_code
            // });
            //
            // new_app_version.save(function (err, result) {
            //
            //   if (!err) {
            //
            //     callback(true);
            //
            //   } else {
            //
            //     callback(false);
            //
            //   }
            //
            // });

        } else {

            // if (is_force_update == 1) {

            AppVersion.update({_id: app_versions._id}, {
                $set: {
                    is_deprecated: is_deprecate,
                    package_name: package_name,
                    url: url
                }
            }, function (err, result) {

                if (!err) {

                    callback(true);

                } else {

                    callback(false);

                }

            });

            // } else {
            //
            //   AppVersion.update({_id: app_versions._id}, {
            //     $set: {
            //       version_code: version_code,
            //       is_force_update: is_force_update,
            //       url: url
            //     }
            //   }, function (err, result) {
            //
            //     if (!err) {
            //
            //       callback(true);
            //
            //     } else {
            //
            //       callback(false);
            //
            //     }
            //
            //   });
            //
            // }

        }
    });

}

module.exports.getAppVersion = function (callback) {

    AppVersion.findOne({}, function (err, app_current_version) {

        if (!err) {

            callback(app_current_version);

        } else {

            callback(false);

        }

    });

};

module.exports.updateIosVersion = function (version_code, is_force_update, url, callback) {

    IosVersion.findOne({}, function (req, ios_versions) {

        if (ios_versions !== null) {

            if (ios_versions.length == 0) {

                var new_ios_version = new IosVersion({
                    version_code: version_code,
                    url: url,
                    is_force_update: is_force_update,
                    last_force_version: version_code
                });

                new_ios_version.save(function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });
            } else {

                if (is_force_update == 1) {

                    IosVersion.update({_id: ios_versions._id}, {
                        $set: {
                            version_code: version_code,
                            is_force_update: is_force_update,
                            last_force_version: version_code,
                            url: url
                        }
                    }, function (err, result) {

                        if (!err) {

                            callback(true);

                        } else {

                            callback(false);

                        }

                    });

                } else {

                    IosVersion.update({_id: ios_versions._id}, {
                        $set: {
                            version_code: version_code,
                            is_force_update: is_force_update,
                            url: url
                        }
                    }, function (err, result) {

                        if (!err) {

                            callback(true);

                        } else {

                            callback(false);

                        }

                    });

                }

            }

        } else {

            var new_ios_version = new IosVersion({
                version_code: version_code,
                is_force_update: is_force_update,
                last_force_version: version_code,
                url: url
            });

            new_ios_version.save(function (err, result) {

                if (!err) {

                    callback(true);

                } else {

                    callback(false);

                }

            });

        }

    });

};

module.exports.getIosVersion = function (callback) {

    IosVersion.findOne({}, function (err, ios_current_version) {

        if (!err) {

            callback(ios_current_version);

        } else {

            callback(false);

        }

    });

};

module.exports.updateDriverVersion = function (version_code, is_force_update, url, callback) {

    DriverVersion.findOne({}, function (req, driver_versions) {

        if (driver_versions !== null) {

            if (driver_versions.length == 0) {

                var new_driver_version = new DriverVersion({
                    version_code: version_code,
                    url: url,
                    is_force_update: is_force_update,
                    last_force_version: version_code
                });

                new_driver_version.save(function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });
            } else {

                if (is_force_update == 1) {

                    DriverVersion.update({_id: driver_versions._id}, {
                        $set: {
                            version_code: version_code,
                            is_force_update: is_force_update,
                            last_force_version: version_code,
                            url: url
                        }
                    }, function (err, result) {

                        if (!err) {

                            callback(true);

                        } else {

                            callback(false);

                        }

                    });

                } else {

                    DriverVersion.update({_id: driver_versions._id}, {
                        $set: {
                            version_code: version_code,
                            is_force_update: is_force_update,
                            url: url
                        }
                    }, function (err, result) {

                        if (!err) {

                            callback(true);

                        } else {

                            callback(false);

                        }

                    });

                }

            }

        } else {

            var new_driver_version = new DriverVersion({
                version_code: version_code,
                is_force_update: is_force_update,
                last_force_version: version_code,
                url: url
            });

            new_driver_version.save(function (err, result) {

                if (!err) {

                    callback(true);

                } else {

                    callback(false);

                }

            });

        }

    });

};

module.exports.updateDriverIosVersion = function (version_code, is_force_update, url, callback) {

    DriverIosVersion.findOne({}, function (req, driver_versions) {

        if (driver_versions !== null) {

            if (driver_versions.length == 0) {

                var new_driver_version = new DriverVersion({
                    version_code: version_code,
                    url: url,
                    is_force_update: is_force_update,
                    last_force_version: version_code
                });

                new_driver_version.save(function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });
            } else {

                if (is_force_update == 1) {

                    DriverIosVersion.update({_id: driver_versions._id}, {
                        $set: {
                            version_code: version_code,
                            is_force_update: is_force_update,
                            last_force_version: version_code,
                            url: url
                        }
                    }, function (err, result) {

                        if (!err) {

                            callback(true);

                        } else {

                            callback(false);

                        }

                    });

                } else {

                    DriverIosVersion.update({_id: driver_versions._id}, {
                        $set: {
                            version_code: version_code,
                            is_force_update: is_force_update,
                            url: url
                        }
                    }, function (err, result) {

                        if (!err) {

                            callback(true);

                        } else {

                            callback(false);

                        }

                    });

                }

            }

        } else {

            var new_driver_version = new DriverIosVersion({
                version_code: version_code,
                is_force_update: is_force_update,
                last_force_version: version_code,
                url: url
            });

            new_driver_version.save(function (err, result) {

                if (!err) {

                    callback(true);

                } else {

                    callback(false);

                }

            });

        }

    });

};

module.exports.getDriverVersion = function (callback) {

    DriverVersion.findOne({}, function (err, driver_current_version) {

        if (!err) {

            callback(driver_current_version);

        } else {

            callback(false);

        }

    });

};

module.exports.getIosDriverCurrentVersion = function (callback) {

    DriverIosVersion.findOne({}, function (err, driver_current_version) {

        if (!err) {

            callback(driver_current_version);

        } else {

            callback(false);

        }

    });

};

module.exports.getCurrentNotify = function (callback) {

    Notify.findOne({}, function (err, result) {

        if (!err) {

            callback(result);

        }

    });

};

module.exports.updateNotify = function (current_id, url, title, ticker, body, callback) {

    Notify.findOne({}, function (req, current_notify) {

        if (current_notify !== null) {

            if (current_notify.length == 0) {


                var new_notify = new Notify({
                    current_id: current_id,
                    url: url,
                    title: title,
                    ticker: ticker,
                    body: body
                });

                new_notify.save(function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });

            } else {

                Notify.update({_id: current_notify._id}, {
                    $set: {
                        current_id: current_id,
                        url: url,
                        title: title,
                        ticker: ticker,
                        body: body
                    }
                }, function (err, result) {

                    if (!err) {

                        callback(true);

                    }


                });

            }

        } else {

            var new_notify = new Notify({

                current_id: current_id,
                url: url,
                title: title,
                ticker: ticker,
                body: body

            });

            new_notify.save(function (err, result) {

                if (!err) {

                    callback(true);

                } else {

                    callback(false);

                }

            });

        }

    });

};

module.exports.setSystemStatus = function (is_active, message, callback) {

    ActiveSystem.find({}, function (req, active_data) {

        if (active_data !== null) {

            if (active_data.length == 0) {


                var new_active_system = new ActiveSystem({
                    is_active: is_active,
                    message: message
                });

                new_active_system.save(function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });

            } else {

                ActiveSystem.update({_id: active_data[0]._id}, {
                    $set: {
                        is_active: is_active,
                        message: message
                    }
                }, function (err, result) {

                    if (!err) {

                        callback(true);

                    }

                });

            }

        } else {

            var new_active_system = new ActiveSystem({

                is_active: is_active,
                message: message

            });

            new_active_system.save(function (err, result) {

                if (!err) {

                    callback(true);

                } else {

                    callback(false);

                }

            });

        }

    });
};

module.exports.getSystemActivated = function (callback) {

    ActiveSystem.findOne({}, function (err, result) {

        if (!err) {

            callback(result);

        }

    });

};


module.exports.updateDriverBanner = function (first_title, first_body, second_title, second_body, callback) {

    DriverBanner.findOne({}, function (req, driver_banner) {

        if (driver_banner !== null) {

            if (driver_banner.length == 0) {


                var new_driver_banner = new DriverBanner({
                    first_title: first_title,
                    first_body: first_body,
                    second_title: second_title,
                    second_body: second_body
                });

                new_driver_banner.save(function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });

            } else {

                DriverBanner.update({_id: driver_banner._id}, {
                    $set: {
                        first_title: first_title,
                        first_body: first_body,
                        second_title: second_title,
                        second_body: second_body
                    }
                }, function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });

            }

        } else {

            var new_driver_banner = new DriverBanner({
                first_title: first_title,
                first_body: first_body,
                second_title: second_title,
                second_body: second_body
            });

            new_driver_banner.save(function (err, result) {

                if (!err) {

                    callback(true);

                } else {

                    callback(false);

                }

            });

        }

    });

};

module.exports.getDriverBanner = function (callback) {

  DriverBanner.findOne({}, function (err, driver_banner) {

    if (!err) {

      callback(driver_banner);

    } else {

      callback(false);

    }

  });

};

module.exports.updateDriverTelegramLink = function (telegram_link,  callback) {

   DriverTelegramLink.findOne({}, function (req, driver_telegram_link) {

        if (driver_telegram_link !== null) {

            if (driver_telegram_link.length == 0) {


                var new_driver_telegram_link = new DriverTelegramLink({
                  telegram_link: telegram_link
                });

              new_driver_telegram_link.save(function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });
            } else {

              DriverTelegramLink.update({_id: driver_telegram_link._id}, {
                    $set: {
                      telegram_link: telegram_link
                    }
                }, function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });

            }

        } else {

          var new_driver_telegram_link = new DriverTelegramLink({
            telegram_link: telegram_link
          });

          new_driver_telegram_link.save(function (err, result) {

                if (!err) {

                    callback(true);

                } else {

                    callback(false);

                }

            });

        }

    });

};


module.exports.getDriverTelegramLink = function (callback) {

    DriverTelegramLink.findOne({}, function (err, driver_telegram_link) {

        if (!err) {

            callback(driver_telegram_link);

        } else {

            callback(false);

        }

    });

};


module.exports.updateSupportPhone = function (driver_support, passenger_support, callback) {

    SupportPhone.findOne({}, function (req, support_phone) {

        if (support_phone !== null) {

            if (support_phone.length == 0) {


                var new_support_phone = new SupportPhone({
                    driver_support: driver_support,
                    passenger_support: passenger_support
                });

                new_support_phone.save(function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });
            } else {

                SupportPhone.update({_id: support_phone._id}, {
                    $set: {
                        driver_support: driver_support,
                        passenger_support: passenger_support
                    }
                }, function (err, result) {

                    if (!err) {

                        callback(true);

                    } else {

                        callback(false);

                    }

                });

            }

        } else {

            var new_support_phone = new SupportPhone({
                driver_support: driver_support,
                passenger_support: passenger_support
            });

            new_support_phone.save(function (err, result) {

                if (!err) {

                    callback(true);

                } else {

                    callback(false);

                }

            });

        }

    });

};


module.exports.getSupportPhone = function (callback) {

    SupportPhone.findOne({}, function (err, support_phone) {

        if (!err) {

            callback(support_phone);

        } else {

            callback(false);

        }

    });

};


