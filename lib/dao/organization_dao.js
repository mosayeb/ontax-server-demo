var mongoose = require('mongoose');
var mongoosastic = require('mongoosastic');
var wallet_dao = require('./wallet_dao');
var moment = require('moment-jalaali');
var geolib = require('geolib');

var config = require('./../config/config');

var db = mongoose.createConnection(config.database);

var ObjectId = mongoose.Schema.Types.ObjectId;

var organizationSchema = mongoose.Schema({

    username: {type: String, es_indexed: true},
    password: String,
    organization_name: {type: String, es_indexed: true},
    avatar: {type: String, default: ""},
    address: {type: String, es_indexed: true, default: ""},
    email: {type: String, es_indexed: true},
    is_block: {type: Boolean, default: false},
    caller_number: {type: String, default: ""},
    phone_number: {type:String , default:""},
    emergency_phone_number: {type: String, default: ""},

});

organizationSchema.plugin(mongoosastic, {
    hosts: [
        '127.0.0.1:9200'
    ]
});

var Organization = db.model('Organization', organizationSchema);

module.exports.registerOrganization = function(username, password,organization_name,email,address,phone_number, callback) {

    var organization = new Organization({

        username: username,
        password: password,
        organization_name : organization_name,
        email:email,
        address:address,
        phone_number:phone_number
    });

    organization.save(function(err, result){

        if(!err && result) {

            wallet_dao.createOrganizationWallet(result._id, 0, function (wallet_data) {

                if (!err) callback(true);

            });

        } else {
            callback(false);
        }

    });

};

module.exports.getOrganizationInfo = function(organization_id, callback) {

    Organization.findOne({_id: organization_id}, function(err, result) {

        if(!err && result) {
            callback(result);
        } else {
        }

    });

};

module.exports.getOrganizationByUsername = function(username, callback) {

    Organization.findOne({username: username}, function(err, result) {

        if(!err && result) {
            callback(result);
        }

    });

};

module.exports.isOrganizationValid = function (organization_id, callback) {

    Organization.findOne({_id: organization_id}, function (err, organization_data) {

        if (!err && organization_data) {

            if (!organization_data.is_block) {

                callback(true);

            } else {

                callback(false);

            }

        }

    })

};

module.exports.getOrganizationInfoById = function (organization_id, callback) {

    Organization.findOne({_id: organization_id}, function (err, result) {

        if (!err && result) {

            callback(result);

        }
        else{
            callback(false);
        }

    });

};

module.exports.updateOrganizationInfo = function (organization_id, email, address, organization_name,  emergency_phone_number,phone_number, callback) {

    Organization.update({_id: organization_id},
        {$set:
            {
                email: email,
                address: address,
                organization_name: organization_name,
                emergency_phone_number: emergency_phone_number,
                phone_number:phone_number

            }
        }, function (err, result) {

            if (!err) {

                callback(true);

            }

        });

};