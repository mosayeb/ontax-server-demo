
module.exports = {
  PAYMENT_CALL_BACK_URL: 'http://185.105.239.20:8005/customer/paymentresult/ipg/',
  ZARINPAL_URL: 'https://www.zarinpal.com/pg/StartPay/',
  ONE_DAY: 1000 * 60 * 60 * 24, // one day in milli second
  CALLER_PROMOTION_GIFT: 5000, // tomans
  CALLER_PROMOTION_DESCRIPTION: 'افزایش اعتبار هدیه به مبلغ',
  IPG_PAYMENT_DESCRIPTION: 'افزایش اعتبار به مبلغ',
  TAXI_SEARCH_RADIUS: 30000,
  PLACE_SEARCH_RADIUS: 500000,
  APPLE_TEAM_ID: "44TA83447Z",
  APPLE_KEY_ID: "QB89A4P8GU",
  APPLE_DRIVER_KEY_ID: "QB89A4P8GU",
  APPLE_PASSENGER_TOPIC: "ir.ontax.passenger",
  APPLE_DRIVER_TOPIC: "ir.ontax.driver",
  BASE_TRAVEL_COST: 2500,
  SHARE_TRAVEL_ADDRESS: "http://94.130.68.80:9660/ftsd12W00q/",
  PAYMENT_MERCHANT_ID: "fd49a9ec-cb41-11e6-9f26-005056a205be",
  APN_SERVER_URL: "http://94.130.68.80:9660",
  ADD_CUSTOMER_IPG_MONEY_EXP: "افزایش مبلغ اعتبار توسط پرداخت اینترنتی",
  ADD_CUSTOMER_USSD_MONEY_EXP: "افزایش مبلغ اعتبار توسط ussd",
  TRAVEL_COST_DECREASE_EXP: "کاهش اعتبار بواسطه‌ی پرداخت هزینه سفر",
  SERVER_ADDRESS: 'http://94.130.68.80:9660',
  SAMAN_CALLBACK_URL: 'http://94.130.68.80:9660/payment/callback/',
  SAMAN_MERCHANT_ID: '10859888',
  SAMAN_TAXI_CALLBACK_URL: 'http://94.130.68.80:9660/payment/taxicallback/'
}
