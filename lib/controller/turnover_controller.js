var fs = require('fs');
var soap = require('soap');
var customer_dao = require('./../dao/customer_dao');
var organization_dao = require('./../dao/organization_dao');
var taxi_dao = require('./../dao/taxi_dao');
var travel_dao = require('./../dao/travel_dao');
var comment_dao = require('./../dao/comment_dao');
var station_dao = require('./../dao/station_dao');
var support_dao = require('./../dao/support_dao');
var turnover_dao = require('./../dao/turnover_dao');
var support_controller = require('./../controller/support_controller');
var map_tools = require('./../tools/map_tools');
var image_tools = require('./../tools/image_tools');
var aa = require('./../aa/aa');
var passwordHash = require("password-hash");
var moment = require('moment-jalaali');

module.exports.createCustomerTurnover = function (req, callback) {

  var customer_id = req.body.customer_id;
  var amount = req.body.amount;
  var turnover_type = req.body.turnover_type;
  var explanation = req.body.explanation;
  var date = moment().format('jYYYY/jMM/jDD');
  var time = moment().format('HH:mm');

  turnover_dao.createCustomerTurnover(customer_id, amount, turnover_type, explanation, date, time, function (result) {

    if (result) {

      callback(true);

    }

  });

};

module.exports.createTaxiTurnover = function (req, callback) {

  var taxi_id = req.body.taxi_id;
  var amount = req.body.amount;
  var turnover_type = req.body.turnover_type;
  var explanation = req.body.explanation;
  var date = moment().format('jYYYY/jMM/jDD');
  var full_date = moment().format('jYYYY/jMM/jDD HH:mm');
  var time = moment().format('HH:mm');

  turnover_dao.createTaxiTurnover(taxi_id, amount, turnover_type, explanation, full_date, date, time, function (result) {

    if (result) {

      callback(true);

    }

  });

};


module.exports.createTaxiTravelTurnOver = function (taxi_id, travel_id, callback) {

  taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

    travel_dao.getTravelInfo(travel_id, function (travel_data) {


    });

  });

};

module.exports.getTaxiTransactions = function (req, callback) {

  if (req.body.taxi_id) {

    var taxi_id = req.body.taxi_id;
    var page = req.body.page;
    var per_page = req.body.per_page;

  } else {

    var taxi_id = req.params.taxi_id;
    var page = req.params.page;
    var per_page = req.params.per_page;

  }


  turnover_dao.getTaxiTransactions(taxi_id, page, per_page, function (transactions) {

    callback(transactions);

  });

}

module.exports.getTurnOverList = function (req, callback) {

  var page = req.params.page;
  var per_page = req.params.per_page;
  var final_trans_list = [];

  turnover_dao.getTurnOverList(page, per_page, function (transactions) {

    if (transactions.length > 0) {

      for (var i = 0; i < transactions.length; i++) {

        (function (cntr) {

          var trans_obj = transactions[cntr];
          var taxi_id = trans_obj.taxi_id;

          taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

            if (taxi_data) {
              trans_obj.driver_name = taxi_data.driver_name;
              trans_obj.taxi_code = taxi_data.taxi_code;
              final_trans_list.push(trans_obj);
            }

            if (cntr === transactions.length - 1) {
              callback(final_trans_list);
            }

          });

        })(i);
      }

    } else {

      callback(final_trans_list);

    }


  })

};

module.exports.createTaxiPony = function (req, callback) {

  console.log(req.body);

  var taxi_id = req.body.taxi_id;
  var explanation = req.body.explanation;
  var amount = req.body.amount;
  var pony_type = req.body.pony_type;

  turnover_dao.createTaxiPony(taxi_id, amount, explanation, pony_type, function (result) {

    // set taxi credit to zero

    taxi_dao.setTaxiCredit(taxi_id, 0, function (result) {

      console.log("SET TAXI DATA TO ZERO");

      if (result) {

        callback(result);

      }

    });

  });

};

module.exports.getTaxiPonies = function (req, callback) {

  var taxi_id = req.params.taxi_id;
  var page = req.params.page;
  var per_page = req.params.per_page;

  turnover_dao.getTaxiPonies(taxi_id, page, per_page, function (ponies) {

    if (ponies) {

      callback(ponies);

    }

  })
};

module.exports.getTurnOverStat = function (req, callback) {

  var data = {
    total_ontax_income: 0, // system income
    total_credit: 0, // credits of drivers
    total_driver_debt: 0, // credits of drivers
    total_driver_income: 0, // total income on drivers
  };

  taxi_dao.getTaxisListAll(function (taxis) {

    turnover_dao.getAllTurnOvers(function (turnovers) {

      for (var i = 0; i < taxis.length; i++) {

        if (taxis[i].credit) {

          if (taxis[i].credit > 0) {

            data.total_credit += taxis[i].credit;

          } else {

            data.total_driver_debt += taxis[i].credit;

          }
        }


      }

      for (var j = 0; j < turnovers.length; j++) {

        if (turnovers[j].turnover_type == 'creditor' || turnovers[j].turnover_type == 'debtor') {

          data.total_ontax_income += turnovers[j].amount;

        } else {

          data.total_driver_income += turnovers[j].amount;

        }

      }

      callback(data);

    })

  })

}


module.exports.getPonyList = function (req, callback) {

  var page = req.params.page;
  var per_page = req.params.per_page;
  var final_pony_list = [];

  turnover_dao.getPonyList(page, per_page, function (ponies) {

    console.log(ponies);

    if (ponies.length > 0) {

      for (var i = 0; i < ponies.length; i++) {

        (function (cntr) {

          var pony_obj = ponies[cntr];
          var taxi_id = pony_obj.taxi_id;

          taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

            if (taxi_data) {
              pony_obj.driver_name = taxi_data.driver_name;
              pony_obj.taxi_code = taxi_data.taxi_code;
              final_pony_list.push(pony_obj);
            }

            if (cntr === ponies.length - 1) {

              console.log(final_pony_list);
              callback(final_pony_list);
            }

          });

        })(i);
      }

    } else {

      callback(final_pony_list);

    }


  })

};


module.exports.getStationFinanceStat = function (req, callback) {

  var station_id = req.params.station_id;

  taxi_dao.getStationTaxisListAll(station_id, function (taxis) {

    var data = {

      total_credit: 0, // credits of drivers
      total_driver_debt: 0, // credits of drivers

    };

    turnover_dao.getAllTurnOvers(function (turnovers) {

      for (var i = 0; i < taxis.length; i++) {

        if (taxis[i].credit) {

          if (taxis[i].credit > 0) {

            data.total_credit += taxis[i].credit;

          } else {

            data.total_driver_debt += taxis[i].credit;

          }
        }


      }

      callback(data);

    })


  })

}

module.exports.getStationTaxisTransactions = function (req, callback) {

  var station_id = req.params.station_id;
  var taxi_ids = [];
  var final_trans_list = [];

  taxi_dao.getStationTaxisListAll(station_id, function (taxis) {

    for(var i=0; i<taxis.length; i++) {

      taxi_ids.push(taxis[i]._id);

    }

    turnover_dao.getTaxisListTurnOvers(taxi_ids, function (turnovers) {

      if (turnovers.length > 0) {

        for (var i = 0; i < turnovers.length; i++) {

          (function (cntr) {

            var trans_obj = turnovers[cntr];
            var taxi_id = trans_obj.taxi_id;

            taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

              if (taxi_data) {
                trans_obj.driver_name = taxi_data.driver_name;
                trans_obj.taxi_code = taxi_data.taxi_code;
                final_trans_list.push(trans_obj);
              }

              if (cntr === turnovers.length - 1) {
                callback(final_trans_list);
              }

            });

          })(i);
        }

      } else {

        callback(final_trans_list);

      }

    });


  })

}