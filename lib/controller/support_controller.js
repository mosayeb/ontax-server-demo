var soap = require('soap');
var request = require("request");
var mongoose = require('mongoose');
var customer_dao = require('./../dao/customer_dao');
var station_dao = require('./../dao/station_dao');
var organization_dao = require('./../dao/organization_dao');
var travel_dao = require('./../dao/travel_dao');
var taxi_dao = require('./../dao/taxi_dao');
var support_dao = require('./../dao/support_dao');
var place_dao = require('./../dao/place_dao');
var comment_dao = require('./../dao/comment_dao');
var taxi_dao = require('./../dao/taxi_dao');
var map_tools = require('./../tools/map_tools');
var aa_dao = require('./../dao/aa_dao');
var apn_handler = require('./../handler/apn_handler');
var http_handler = require('./../handler/http_handler');
var utils = require('./../tools/utils');

module.exports.saveSupportMessage = function (req, callback) {

  var customer_id = req.body.customer_id;
  var message_body = req.body.body;
  var message_title = req.body.title;

  support_dao.saveMessage(customer_id, message_title, message_body, function (result) {

    callback(result);

  });
};

module.exports.getTravelCompleteInfo = function (req, callback) {

  var travel_id = (req.query.travel_id) ? req.query.travel_id : req.params.travel_id;
  travel_id = mongoose.Types.ObjectId(travel_id);
  travel_dao.getTravelInfo(travel_id, function (travel_data) {


    if (travel_data && travel_data.taxi_id) {

      taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

        if (taxi_data) {

          customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {


            var travelCompleteData = {
              travel: travel_data,
              taxi: taxi_data,
              customer: customer_data,
            };

            callback(travelCompleteData);


          });

        }

      });

    } else {

      customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

        var travelCompleteData = {
          travel: travel_data,
          taxi: false,
          customer: customer_data,
          comment: []
        };

        callback(travelCompleteData);

      });

    }

  });

};

module.exports.updateSupportInfo = function (socket, callback) {

  var socket_id = socket.id;
  var type = "support";

  support_dao.updateSupportInfo(socket_id, type, function (result) {

    if (result) {

      travel_dao.getActiveTravels(function (active_travels) {

        for (var i = 0; i < active_travels.length; i++) {

          socket.join(active_travels[i]._id);

        }

        callback(result);

      });

    }
  });
};

module.exports.getCustomerTemplateMessagesList = function (req, callback) {

  var page = req.params.page;
  var per_page = req.params.per_page;

  support_dao.getCustomerTemplateMessagesList(page, per_page, function (result) {

    if (result) {

      callback(result);

    }

  });
};

module.exports.updateAppNotify = function (req, callback) {

  var url = req.body.url;
  var current_id = req.body.current_id;
  var title = req.body.title;
  var ticker = req.body.ticker;
  var body = req.body.body;
  var devices_list = [];

  support_dao.updateNotify(current_id, url, title, ticker, body, function (update_notify_result) {

    // send notification fo ios users

    customer_dao.getIosCustomers(function (customers) {

      console.log(customers);

      for(var i=0; i<customers.length; i++) {

        // apn_handler.sendMessageNotification(customers[i].device_id, body);

        devices_list.push(customers[i].device_id);

      }

      http_handler.sendBulkNotification(devices_list, body, function (result) {

      });

    });

    callback(update_notify_result);

  });

};

module.exports.getAppCurrentNotify = function (callback) {

  support_dao.getCurrentNotify(function (app_notify_info) {

    callback(app_notify_info);

  });

};

module.exports.updateAppVersion = function (req, callback) {

  var version_code = req.body.version_code;
  var is_force_update = req.body.is_force_update;
  var url = req.body.url;

  support_dao.updateAppVersion(version_code, is_force_update, url, function (update_version_result) {

    callback(update_version_result);

  });

};

module.exports.getAppCurrentVersion = function (callback) {

  support_dao.getAppVersion(function (app_version_info) {

    callback(app_version_info);

  });

};

module.exports.updateIosVersion = function (req, callback) {

  var version_code = req.body.version_code;
  var is_force_update = req.body.is_force_update;
  var url = req.body.url;

  support_dao.updateIosVersion(version_code, is_force_update, url, function (update_version_result) {

    callback(update_version_result);

  });

};

module.exports.getIosCurrentVersion = function (callback) {

  support_dao.getIosVersion(function (app_version_info) {

    callback(app_version_info);

  });

};

module.exports.updateDriverVersion = function (req, callback) {

  var version_code = req.body.version_code;
  var is_force_update = req.body.is_force_update;
  var url = req.body.url;

  support_dao.updateDriverVersion(version_code, is_force_update, url, function (update_version_result) {

    callback(update_version_result);

  });

};

module.exports.updateIosDriverVersion = function (req, callback) {

  var version_code = req.body.version_code;
  var is_force_update = req.body.is_force_update;
  var url = req.body.url;

  support_dao.updateDriverIosVersion(version_code, is_force_update, url, function (update_version_result) {

    callback(update_version_result);

  });

};

module.exports.getDriverCurrentVersion = function (callback) {

  support_dao.getDriverVersion(function (app_version_info) {

    callback(app_version_info);

  });

};

module.exports.getIosDriverCurrentVersion = function (callback) {

  support_dao.getIosDriverCurrentVersion(function (app_version_info) {

    callback(app_version_info);

  });

};

module.exports.smsNewTravelNotify = function () {

  request({
    uri: "http://rgshop.ir/smshandler.php",
    method: "POST",
    form: {
      reciever: "989167125770",
      text: "سفر جدید از رهتاک. لطفا سریعا اقدام نمایید."
    }
  }, function (error, response, body) {

  });

  //var message = "سفر جدید درخواست داده شده است";
  //var url = 'http://www.linepayamak.ir/Post/Send.asmx?wsdl';
  //var args = {
  //    username: "rahsafar",
  //    password: "123456",
  //    from: "50001000000000",
  //    to: ["989167161676"],
  //    text: message
  //};
  //
  //var options = {
  //    ignoredNamespaces: {
  //        namespaces: [],
  //        override: true
  //    }
  //};
  //
  ////callback(true, verification_code);
  //
  //soap.createClient(url, options, function (err, client) {
  //
  //    client.SendSimpleSMS(args, function (err, result) {
  //
  //    });
  //});

};

module.exports.notifyHandler = function (data, io, socket) {

  var customer_notify_id = data.current_id;

  support_dao.getCurrentNotify(function (notify) {

    if (notify) {

      if (customer_notify_id < notify.current_id) {

        var data = {

          notify_id: notify.current_id,
          url: notify.url,
          title: notify.title,
          ticker: notify.ticker,
          body: notify.body

        };

        socket.emit('support-notify', data);

      }

    } else {


      var data = {

        notify_id: 1,
        url: "",
        title: "",
        ticker: "",
        body: ""

      };

      socket.emit('support-notify', data);

    }

  });

};

// define template message

module.exports.createTemplateMessage = function (req, callback) {

  if (req.body.title) {

    var title = req.body.title;
    var explanation = req.body.explanation;

    support_dao.createTemplateMessage(title, explanation, function (result) {

      if (result)
        callback(true);
      else
        callback(false);
    });


  } else callback(false);

};


module.exports.saveStationTemplateMessage = function (req, callback) {

  var station_id = req.body.station_id;
  var explanation = req.body.explanation;
  var message_id = req.body.message_id;

  support_dao.getTemplateMessageData(message_id, function (message_data) {

    station_dao.getStationInfoById(station_id, function (station_data) {

      support_dao.saveCustomerTemplateMessage(station_id, message_id, explanation, station_data.phone_number, message_data.title, function (result) {

        if (result) {

          callback(true);

        } else {

          callback(false);

        }

      });

    });

  });

};

module.exports.saveOrganizationTemplateMessage = function (req, callback) {

  var organization_id = req.body.organization_id;
  var explanation = req.body.explanation;
  var message_id = req.body.message_id;

  support_dao.getTemplateMessageData(message_id, function (message_data) {

    organization_dao.getOrganizationInfoById(organization_id, function (organization_data) {

      support_dao.saveCustomerTemplateMessage(organization_id, message_id, explanation, organization_data.phone_number, message_data.title, function (result) {

        if (result) {

          callback(true);

        } else {

          callback(false);

        }

      });

    });

  });

};

module.exports.saveCustomerTemplateMessage = function (req, callback) {

  var customer_id = req.body.customer_id;
  var explanation = req.body.explanation;
  var message_id = req.body.message_id;

  support_dao.getTemplateMessageData(message_id, function (message_data) {

    customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

      support_dao.saveCustomerTemplateMessage(customer_id, message_id, explanation, customer_data.phone_number, message_data.title, function (result) {

        if (result) {

          callback(true);

        } else {

          callback(false);

        }

      });

    });

  });

};

module.exports.getTemplateMessages = function (callback) {

  support_dao.getTemplateMessages(function (template_messages) {

    if (template_messages) {

      callback(template_messages);

    } else {

      callback(false);

    }

  });

};

module.exports.getCustomerTemplateMessages = function (callback) {

  support_dao.getTemplateMessages(function (template_messages) {

    if (template_messages) {

      support_dao.getSupportPhone(function (support_phone) {

        if (support_phone) {

          callback(template_messages, support_phone.passenger_support);

        }

      });

    } else {

      callback(false, "");

    }

  });

};

module.exports.removeTemplateMessage = function (req, callback) {

  var message_id = req.body.message_id;

  support_dao.removeTemplateMessage(message_id, function (remove_result) {

    if (remove_result) {

      callback(true);

    }

  });

};

// get all comments type

module.exports.getComments = function (req, callback) {

  comment_dao.getAllComments(function (comments) {

    callback({comments: comments, result: true});
  });

};


module.exports.getSystemActivated = function (callback) {

  support_dao.getSystemActivated(function (active_data) {

    callback(active_data);

  });

};

// remove comment

module.exports.removeComment = function (req, callback) {

  var comment_id = req.params.comment_id;

  comment_dao.removeComment(comment_id, function (result) {

    callback(result);

  });

};

module.exports.setSystemStatus = function (req, callback) {

  var is_active = req.body.is_active;
  var message = req.body.message;

  support_dao.setSystemStatus(is_active, message, function (result) {

    if (result == true) {

      callback(true);

    }

  });

};

// restore database and images

module.exports.restoreData = function (req, callback) {

  var backup_version = req.body.backup_version;

  utils.restore(backup_version, function (result) {

    callback(result);

  });

};

// get all backups

module.exports.backupLatestList = function (callback) {

  utils.getLatestBackups(function (backup_list) {

    callback(backup_list);

  })

};

// export phone numbers

module.exports.exportCustomerNumbers = function (callback) {

  customer_dao.getCustomersPhoneNumbers(function (customers) {

    utils.excelExportCustomerPhones(customers, function (exported_file) {

      callback(exported_file);

    });

  });

};

// export phone numbers

module.exports.exportTaxisNumbers = function (callback) {

  taxi_dao.getTaxiPhoneNumbers(function (taxis) {

    utils.exportTaxisNumbers(taxis, function (exported_file) {

      callback(exported_file);

    });

  });

};

module.exports.setAppDeprecated = function (req, callback) {

  var is_deprecated = req.body.is_deprecated;
  var package_name = req.body.package_name;
  var url = req.body.url;

  support_dao.setAppDeprecated(is_deprecated, package_name, url, function (result) {

    callback(result);

  })

}

module.exports.createNewServiceType = function (req, callback) {

  var service_title = req.body.service_title;
  var commission_rate = req.body.commission_rate;
  var explanation = req.body.explanation;

  taxi_dao.createNewServiceType(service_title, commission_rate, explanation, function (result) {

    if (result) {

      callback(true);

    } else {

      callback(false);

    }

  })

};

module.exports.updateServiceType = function (req, callback) {

  var service_id = req.body.service_id;
  var service_title = req.body.service_title;
  var commission_rate = req.body.commission_rate;
  var explanation = req.body.explanation;

  taxi_dao.updateServiceType(service_id, commission_rate, explanation, function (result) {

    if (result) {

      callback(true);

    } else {

      callback(false);

    }

  })

};

module.exports.getServiceTypeData = function (req, callback) {

  var service_id = req.params.service_id;

  taxi_dao.getServiceTypeData(service_id, function (service_data) {

    if (service_data) {

      callback(service_data);

    } else {

      callback(false);

    }

  })

};

module.exports.getAllServiceTypes = function (callback) {

  taxi_dao.getAllServiceTypes(function (services) {

    if (services) {

      callback(services);

    } else {

      callback(false);

    }

  })

};

module.exports.updateDriverBanner = function (req, callback) {

  var first_title = req.body.first_title;
  var first_body = req.body.first_body;
  var second_title = req.body.second_title;
  var second_body = req.body.second_body;

  support_dao.updateDriverBanner(first_title, first_body, second_title, second_body, function (update_driver_result) {

    callback(update_driver_result);

  });

};

module.exports.getDriverBanner = function (callback) {

  support_dao.getDriverBanner(function (app_driver_info) {

    callback(app_driver_info);

  });

};

module.exports.updateDriverTelegramLink = function (req, callback) {

  var telegram_link = req.body.telegram_link;

  support_dao.updateDriverTelegramLink(telegram_link, function (update_driver_result) {

    callback(update_driver_result);

  });

};

module.exports.getDriverTelegramLink = function (callback) {

  support_dao.getDriverTelegramLink(function (telegram_link) {

    callback(telegram_link);

  });

};

module.exports.updateSupportPhone = function (req, callback) {

  var driver_support = req.body.driver_support;
  var passenger_support = req.body.passenger_support;

  support_dao.updateSupportPhone(driver_support, passenger_support, function (update_driver_result) {

    callback(update_driver_result);

  });

};

module.exports.getSupportPhone = function (callback) {

  support_dao.getSupportPhone(function (support_info) {

    callback(support_info);

  });

};

module.exports.updateInputFactor = function (req, callback) {

  var factor = req.body.factor;

  place_dao.updateInputFactor(factor, function (update_input_factor) {

    callback(update_input_factor);

  });

};

module.exports.getBaseFactor = function (callback) {

  place_dao.getBaseFactor(function (base_factor) {

    if(base_factor) {

      callback(base_factor.factor);

    } else {

      callback(0);

    }

  });

};

module.exports.changePassword = function (req, callback) {

  var password = req.body.password;
  var password = req.body.password_retype;

  aa_dao.changePassword("admin", password, function (result) {

    callback(result);

  });

}