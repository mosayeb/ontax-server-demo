var soap = require('soap');
var request = require("request");
var customer_dao = require('./../dao/customer_dao');
var orgaization_dao = require('./../dao/organization_dao');
var turnover_dao = require('./../dao/turnover_dao');
var ipg_controller = require('./ipg_controller');
var wallet_dao = require('./../dao/wallet_dao');
var consts = require('./../config/consts');
var sms_handler = require('./../handler/sms_handler');
var utils = require('./../tools/utils');
var moment = require('moment-jalaali');
var util = require('util');
var uuid = require('uuid');
var taxi_dao = require('./../dao/taxi_dao');

// add money to the wallet

module.exports.addMoneyToWallet = function (req, callback) {

  if (req.body.customer_id && req.body.amount) {

    var customer_id = req.body.customer_id;
    var amount = req.body.amount;

    customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

      if (customer_data) {

        wallet_dao.getWalletInfoByCustomerId(customer_data._id, function (wallet_info) {

          if (wallet_info) {

            ipg_controller.paymentRequest(amount, consts.PAYMENT_CALL_BACK_URL, consts.IPG_PAYMENT_DESCRIPTION, '', customer_data.phone_number, function (payment_data) {

              if (!payment_data) callback(false, false);

              else {

                console.log("HERE 1");


                wallet_dao.saveTransaction(customer_id._id, wallet_info._id, payment_data, '', amount, consts.IPG_PAYMENT_DESCRIPTION, false, function (save_transaction_data) {

                  if (save_transaction_data) callback(payment_data, save_transaction_data._id);
                  else callback(false, false);
                });
              }
            });

          } else callback(false, false);
        });

      } else callback(false, false);

    });


  } else callback(false);

};

module.exports.addMoneyToOrganizationWallet = function (req, callback) {

  if (req.body.organization_id && req.body.amount) {

    var organization_id = req.body.organization_id;

    var amount = req.body.amount;

    orgaization_dao.getOrganizationInfoById(organization_id, function (organization_data) {

      if (organization_data) {

        wallet_dao.getWalletInfoByOrganizationId(organization_data._id, function (wallet_info) {

          if (wallet_info) {

            ipg_controller.paymentRequest(amount, consts.PAYMENT_CALL_BACK_URL, consts.IPG_PAYMENT_DESCRIPTION, '', organization_data.phone_number, function (payment_data) {

              if (!payment_data) callback(false, false);

              else {

                wallet_dao.saveTransaction(organization_id._id, wallet_info._id, payment_data, '', amount, consts.IPG_PAYMENT_DESCRIPTION, false, function (save_transaction_data) {

                  if (save_transaction_data) callback(payment_data, save_transaction_data._id);
                  else callback(false, false);
                });
              }
            });

          } else callback(false, false);
        });

      } else callback(false, false);

    });


  } else callback(false);

};

// get payment result from ipg

module.exports.getIpgPaymentResult = function (req, callback) {

  var status = req.query.Status;
  var authority = consts.ZARINPAL_URL + req.query.Authority;

  if (status == 'OK') {

    wallet_dao.getTransactionByAuthority(authority, function (transaction_data) {

      if (!transaction_data) {
        // transaction not found
      } else {

        if (!transaction_data.is_paid) {

          ipg_controller.paymentVerification(transaction_data.amount, req.query.Authority, function (ref_id) {

            if (!ref_id) {

              // ref_id not found

            } else {

              wallet_dao.updateRefIdAndIsPaid(transaction_data._id, ref_id, true, function (update_transaction_data) {

                if (update_transaction_data) {

                  wallet_dao.getWalletInfoByWalletId(transaction_data.wallet_id, function (wallet_info) {

                    if (wallet_info) {

                      wallet_dao.incrementWalletMoney(transaction_data.wallet_id, parseInt(transaction_data.amount), function (update_money_data) {

                        // send info to customer

                        // save to customer turn over

                        var date = moment().format('jYYYY/jMM/jDD');
                        var time = moment().format('HH:mm');

                        turnover_dao.createCustomerTurnover(transaction_data.customer_id, transaction_data.amount, "creditor", consts.ADD_CUSTOMER_IPG_MONEY_EXP, date, time, function (result) {
                        });

                        callback(true);

                      });
                    }
                  });
                }
              });
            }
          });
        }
      }
    });
  }

};

// get payment result from ussd

module.exports.getUssdPaymentResult = function (req, res) {

  var status = req.query.Status;
  var authority = consts.ZARINPAL_URL + req.query.Authority;
  var header = req.headers;
  var amount = header["x-pay-amount"];
  var customer_phone = header['x-pay-mobile'];
  customer_phone = "98" + customer_phone.slice(1, customer_phone.length);

  console.log("status " + status);
  console.log("authority " + authority);
  console.log("amount " + amount);
  console.log("customer_phone " + customer_phone);
  console.log("header  " + util.inspect(req.headers, false, null));

  if (status == 'OK') {

    wallet_dao.getTransactionByAuthority(authority, function (transaction_data) {

      if (!transaction_data) {

        ipg_controller.paymentVerification(amount, req.query.Authority, function (ref_id) {

          if (!ref_id) {
            // ref_id not found
          } else {
            +
              customer_dao.getCustomerInfo(customer_phone, function (customer_data) {

                if (!customer_data) {
                  // this customer not found.
                  // we have some dirty money :)
                } else {

                  wallet_dao.getWalletInfoByCustomerId(customer_data._id, function (wallet_data) {

                    if (!wallet_data) {
                      // customer has no wallet
                    } else {

                      wallet_dao.saveTransaction(customer_data._id, wallet_data._id, authority, ref_id, amount, consts.IPG_PAYMENT_DESCRIPTION, true, function (save_transaction_data) {

                        if (!save_transaction_data) {
                          // customer pay money but error in update data
                        } else {

                          wallet_dao.incrementWalletMoney(wallet_data._id, amount, function (update_money_data) {

                            // do nothing

                            // add turnover to customer

                            var date = moment().format('jYYYY/jMM/jDD');
                            var time = moment().format('HH:mm');

                            turnover_dao.createCustomerTurnover(transaction_data.customer_id, transaction_data.amount, "creditor", consts.ADD_CUSTOMER_USSD_MONEY_EXP, date, time, function (result) {
                            });

                            console.log("ussd payment is ok");
                          });
                        }
                      });
                    }
                  });
                }
              });
          }
        });

      } else {

        // duplicate request
      }
    });
  }

};

// verify payment result from client

module.exports.verifyPaymentResult = function (req, callback) {

  if (req.body.transaction_id && req.body.customer_id) {

    var transaction_id = req.body.transaction_id;
    var customer_id = req.body.customer_id;

    wallet_dao.getSamanTransactionById(transaction_id, function (transaction_data) {

      if (transaction_data) {

        if (transaction_data.is_paid) {

          wallet_dao.getWalletInfoByCustomerId(customer_id, function (wallet_data) {

            callback(wallet_data, transaction_id);
          });

        } else callback(false, false);

      } else callback(false, false);
    });

  } else callback(false, false);

};

// get organization wallet info

module.exports.getOrganizationWalletInfo = function (req, callback) {

  if (req.method == "POST") {
    var organization_id = req.body.organization_id;
  } else {
    var organization_id = req.params.organization_id;
  }

  if (organization_id) {

    wallet_dao.getWalletInfoByOrganizationId(organization_id, function (wallet_data) {
      console.log('wallet date...>' + wallet_data);
      callback(wallet_data);

      console.log("wallet data " + wallet_data.money);

    });


  } else callback(false);

};

// get wallet info

module.exports.getWalletInfo = function (req, callback) {

  if (req.method == "POST") {
    var customer_id = req.body.customer_id;
  } else {
    var customer_id = req.params.customer_id;
  }

  if (customer_id) {

    wallet_dao.getWalletInfoByCustomerId(customer_id, function (wallet_data) {

      callback(wallet_data);

    });


  } else callback(false);

};

// get organization transactions

module.exports.getOrganizationTransactions = function (req, callback) {

  if (req.body.organization_id) {

    var organization_id = req.body.organization_id;
    var page = req.body.page;
    var per_page = req.body.per_page;

    wallet_dao.getWalletInfoByOrganizationId(organization_id, function (wallet_data) {

      if (wallet_data) {

        wallet_dao.getOrganizationTransactionsById(wallet_data._id, page, per_page, function (transactions) {

          callback(transactions);

        });

      } else callback(false);
    });


  } else callback(false);

};

// get customer transactions 

module.exports.getCustomerTransactions = function (req, callback) {

  if (req.body.customer_id) {

    var customer_id = req.body.customer_id;
    var page = req.body.page;
    var per_page = req.body.per_page;

  } else {

    var customer_id = req.params.customer_id;
    var page = req.params.page;
    var per_page = req.params.per_page;

  }

  wallet_dao.getWalletInfoByCustomerId(customer_id, function (wallet_data) {

    if (wallet_data) {

      wallet_dao.getCustomerSamanTransactionsByWalletId(wallet_data._id, page, per_page, function (transactions) {

        callback(transactions);

      });

    } else callback(false);

  });

};


// get customer transactions

module.exports.getCustomerSamanTransactions = function (req, callback) {

  if (req.body.customer_id) {

    var customer_id = req.body.customer_id;
    var page = req.body.page;
    var per_page = req.body.per_page;

  } else {

    var customer_id = req.params.customer_id;
    var page = req.params.page;
    var per_page = req.params.per_page;

  }

  wallet_dao.getWalletInfoByCustomerId(customer_id, function (wallet_data) {

    if (wallet_data) {

      wallet_dao.getCustomerSamanTransactionsByWalletId(wallet_data._id, page, per_page, function (transactions) {

        callback(transactions);

      });

    } else callback(false);

  });

};

// add money to wallet by panel

module.exports.addMoneyToWalletBySupport = function (req, callback) {

  var customer_id = req.body.customer_id;
  var money = req.body.money;

  customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

    wallet_dao.getWalletInfoByCustomerId(customer_id, function (wallet_data) {

      var added_amount = money - wallet_data.money;

      wallet_dao.updateWalletMoney(wallet_data._id, money, function (result) {

        sms_handler.sendCustomerIncreaseMoney(customer_data.phone_number, added_amount, function (result) {
        });

        callback(result);

      })

    });

  });
};

// save new saman transaction

module.exports.saveNewSamanTransaction = function (req, callback) {


  if (req.body.customer_id && req.body.amount) {

    var customer_id = req.body.customer_id;
    var amount = req.body.amount;

    customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

      if (customer_data) {

        wallet_dao.getWalletInfoByCustomerId(customer_data._id, function (wallet_info) {

          if (wallet_info) {

            var description = "افزایش اعتبار به مبلغ " + amount + " تومان";
            var res_num = uuid.v1();
            var payment_link = consts.SERVER_ADDRESS + "/payment/confirmation/" + customer_id + "/" + amount + "/" + res_num + "/";

            wallet_dao.saveSamanTransaction(customer_id, wallet_info._id, payment_link, res_num, amount, description, 'initial', function(save_transaction_result){

              if(save_transaction_result) {

                callback(payment_link, save_transaction_result._id)

              } else {

                callback(false, false);

              }

            });

            // ipg_controller.paymentRequest(amount, consts.PAYMENT_CALL_BACK_URL, consts.IPG_PAYMENT_DESCRIPTION, '', customer_data.phone_number, function (payment_data) {
            //
            //   if (!payment_data) callback(false, false);
            //
            //   else {
            //
            //     wallet_dao.saveTransaction(customer_id._id, wallet_info._id, payment_data, '', amount, consts.IPG_PAYMENT_DESCRIPTION, false, function (save_transaction_data) {
            //
            //       if (save_transaction_data) callback(payment_data, save_transaction_data._id);
            //       else callback(false, false);
            //     });
            //   }
            // });

          } else callback(false, false);
        });

      } else callback(false, false);

    });


  } else callback(false);

};

module.exports.getSamanPaymentResult = function (req, callback) {

  var state = req.body.State;
  var res_num = req.body.ResNum;
  var mid = req.body.MID;
  var ref_num = req.body.RefNum;
  var secure_pan = req.body.SecurePan;


  if (state == 'OK') {

    wallet_dao.getSamanTransactionByRefNum(ref_num, function (trans_data_by_ref) {

      if(trans_data_by_ref.length > 0) {

        // this ref used before

        callback(false, false, false);

      } else {

        wallet_dao.getSamanTransactionByResNum(res_num, function (transaction_data) {

          if (!transaction_data) {

            // transaction not found

            callback(false, null, null);

          } else {

            if (!transaction_data.is_paid) {

              // payment verification

              soap.createClient("https://sep.shaparak.ir/payments/referencepayment.asmx?WSDL", function (err, client) {

                var args={
                  String_1:ref_num,
                  String_2:consts.SAMAN_MERCHANT_ID

                };

                client.verifyTransaction(args, function (err, result) {

                  console.log(result);

                  var bank_transaction_value = parseInt(result.result.$value) / 10;

                  console.log("BANK VALUE");
                  console.log(bank_transaction_value);


                  console.log("TRANS DATA");
                  console.log(transaction_data);


                  if(bank_transaction_value > 0) {

                    if(bank_transaction_value == parseInt(transaction_data.amount)) {

                      console.log("TRANS WE ARE HERE");

                      wallet_dao.updateTransactionPaymentData(transaction_data._id, state, ref_num, secure_pan, true, function (update_transaction_data) {

                        console.log("TRANS WE ARE HERE 2");

                        if (update_transaction_data) {

                          wallet_dao.getWalletInfoByWalletId(transaction_data.wallet_id, function (wallet_info) {

                            console.log("TRANS WE ARE HERE 3");

                            if (wallet_info) {

                              console.log("TRANS WE ARE HERE 4");


                              wallet_dao.incrementWalletMoney(transaction_data.wallet_id, parseInt(transaction_data.amount), function (update_money_data) {

                                // send info to customer

                                // save to customer turn over

                                var date = moment().format('jYYYY/jMM/jDD');
                                var time = moment().format('HH:mm');

                                turnover_dao.createCustomerTurnover(transaction_data.customer_id, transaction_data.amount, "creditor", consts.ADD_CUSTOMER_IPG_MONEY_EXP, date, time, function (result) {
                                });

                                customer_dao.getCustomerInfoById(transaction_data.customer_id, function (customer_data) {

                                  if(customer_data) {

                                    wallet_dao.getSamanTransactionById(transaction_data._id, function (transaction_data_final) {

                                      callback(true, transaction_data_final, customer_data);

                                    });

                                  } else {

                                    callback(false, null, null);

                                  }

                                });

                              });
                            } else {

                              callback(false, null, null);


                            }
                          });
                        }
                      });

                    }

                  } else {

                    callback(false, null, null);

                  }

                })

              });

              // ipg_controller.paymentVerification(transaction_data.amount, req.query.Authority, function (ref_id) {
              //
              //   if (!ref_id) {
              //
              //     // ref_id not found
              //
              //   } else {
              //
              //     wallet_dao.updateRefIdAndIsPaid(transaction_data._id, ref_id, true, function (update_transaction_data) {
              //
              //       if (update_transaction_data) {
              //
              //         wallet_dao.getWalletInfoByWalletId(transaction_data.wallet_id, function (wallet_info) {
              //
              //           if (wallet_info) {
              //
              //             wallet_dao.incrementWalletMoney(transaction_data.wallet_id, parseInt(transaction_data.amount), function (update_money_data) {
              //
              //               // send info to customer
              //
              //               // save to customer turn over
              //
              //               var date = moment().format('jYYYY/jMM/jDD');
              //               var time = moment().format('HH:mm');
              //
              //               turnover_dao.createCustomerTurnover(transaction_data.customer_id, transaction_data.amount, "creditor", consts.ADD_CUSTOMER_IPG_MONEY_EXP, date, time, function (result) {
              //               });
              //
              //               callback(true);
              //
              //             });
              //           }
              //         });
              //       }
              //     });
              //   }
              // });
            } else {
              callback(false, null, null);
            }
          }
        });

      }

    });

  } else {

    callback(false, null, null);


  }

};

//--------------------------------------------------------
//driver payment save 


module.exports.savetaxiNewSamanTransaction = function (req, callback) {
  
  
    if (req.body.taxi_id && req.body.amount) {
  
      var taxi_id = req.body.taxi_id;
      var amount = req.body.amount;
  
      taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {
  
        if (taxi_data) {
  
          // wallet_dao.getWalletInfoByCustomerId(customer_data._id, function (wallet_info) {
  
          //   if (wallet_info) {
  
              var description = "افزایش اعتبار به مبلغ " + amount + " تومان";
              var res_num = uuid.v1();
              var payment_link = consts.SERVER_ADDRESS + "/payment/taxiconfirmation/" + taxi_id + "/" + amount + "/" + res_num + "/";
  
              wallet_dao.saveSamanTransaction(taxi_id,taxi_id, payment_link, res_num, amount, description, 'initial', function(save_transaction_result){
  
                if(save_transaction_result) {
  
                  callback(payment_link, save_transaction_result._id)
  
                } else {
  
                  callback(false, false);
  
                }
  
              });
  
              // ipg_controller.paymentRequest(amount, consts.PAYMENT_CALL_BACK_URL, consts.IPG_PAYMENT_DESCRIPTION, '', customer_data.phone_number, function (payment_data) {
              //
              //   if (!payment_data) callback(false, false);
              //
              //   else {
              //
              //     wallet_dao.saveTransaction(customer_id._id, wallet_info._id, payment_data, '', amount, consts.IPG_PAYMENT_DESCRIPTION, false, function (save_transaction_data) {
              //
              //       if (save_transaction_data) callback(payment_data, save_transaction_data._id);
              //       else callback(false, false);
              //     });
              //   }
              // });
  
          //   } else callback(false, false);
          // });
  
        } else callback(false, false);
  
      });
  
  
    } else callback(false);
  
  };


//taxi payment verify
module.exports.verifytaxiPaymentResult = function (req, callback) {
  
    if (req.body.transaction_id && req.body.taxi_id) {
  
      var transaction_id = req.body.transaction_id;
      var taxi_id = req.body.taxi_id;
  
      wallet_dao.getSamanTransactionById(transaction_id, function (transaction_data) {
  
        if (transaction_data) {
  
          if (transaction_data.is_paid) {
  
            taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {
  
              callback(taxi_data, transaction_id);
            });
  
          } else callback(false, false);
  
        } else callback(false, false);
      });
  
    } else callback(false, false);
  
  };
  


module.exports.gettaxiSamanPaymentResult = function (req, callback) {

  var state = req.body.State;
  var res_num = req.body.ResNum;
  var mid = req.body.MID;
  var ref_num = req.body.RefNum;
  var secure_pan = req.body.SecurePan;


  if (state == 'OK') {

    wallet_dao.getSamanTransactionByRefNum(ref_num, function (trans_data_by_ref) {

      if(trans_data_by_ref.length > 0) {

        // this ref used before

        callback(false, false, false);

      } else {

        wallet_dao.getSamanTransactionByResNum(res_num, function (transaction_data) {

          if (!transaction_data) {

            // transaction not found

            callback(false, null, null);

          } else {

            if (!transaction_data.is_paid) {

              // payment verification

              soap.createClient("https://sep.shaparak.ir/payments/referencepayment.asmx?WSDL", function (err, client) {

                var args={
                  String_1:ref_num,
                  String_2:consts.SAMAN_MERCHANT_ID

                };

                client.verifyTransaction(args, function (err, result) {

                  console.log(result);

                  var bank_transaction_value = parseInt(result.result.$value) / 10;

                  console.log("BANK VALUE");
                  console.log(bank_transaction_value);


                  console.log("TRANS DATA");
                  console.log(transaction_data);


                  if(bank_transaction_value > 0) {

                    if(bank_transaction_value == parseInt(transaction_data.amount)) {

                      console.log("TRANS WE ARE HERE");

                      wallet_dao.updateTransactionPaymentData(transaction_data._id, state, ref_num, secure_pan, true, function (update_transaction_data) {

                        console.log("TRANS WE ARE HERE 2");

                        if (update_transaction_data) {

                          taxi_dao.getTaxiInfo(transaction_data.wallet_id, function (taxi_info) {

                            console.log("TRANS WE ARE HERE 3");

                            if (taxi_info) {

                              console.log("TRANS WE ARE HERE 4");


                              taxi_dao.increaseTaxiCredit(transaction_data.wallet_id, parseInt(transaction_data.amount), function (update_money_data) {

                                // send info to customer

                                // save to customer turn over
                                console.log("TRANS WE ARE HERE 5");
                                var date = moment().format('jYYYY/jMM/jDD');
                                var time = moment().format('HH:mm');
                                var readable_date= moment().format('dddd jD jMMMM');
                                turnover_dao.createTaxiTurnover (transaction_data.customer_id,transaction_data.wallet_id,transaction_data.amount, "creditor", consts.ADD_CUSTOMER_IPG_MONEY_EXP, date,readable_date, time, function (result) {
                                });

                                taxi_dao.getTaxiInfo(transaction_data.wallet_id, function (taxi_data) {
                                  console.log("TRANS WE ARE HERE 6");
                                  if(taxi_data) {

                                    wallet_dao.getSamanTransactionById(transaction_data._id, function (transaction_data_final) {
                                      console.log("TRANS WE ARE HERE 7");
                                      callback(true, transaction_data_final, taxi_data);

                                    });

                                  } else {

                                    callback(false, null, null);

                                  }

                                });

                              });
                            } else {

                              callback(false, null, null);


                            }
                          });
                        }
                      });

                    }

                  } else {

                    callback(false, null, null);

                  }

                })

              });

            } else {
              callback(false, null, null);
            }
          }
        });

      }

    });

  } else {

    callback(false, null, null);


  }

};