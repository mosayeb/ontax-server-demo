var soap = require('soap');
var request = require("request");
var customer_dao = require('./../dao/customer_dao');
var travel_dao = require('./../dao/travel_dao');
var taxi_dao = require('./../dao/taxi_dao');
var place_dao = require('./../dao/place_dao');
var wallet_dao = require('./../dao/wallet_dao');
var aa = require('./../aa/aa');
var support_dao = require('./../dao/support_dao');
var map_tools = require('./../tools/map_tools');
var image_tools = require('./../tools/image_tools');
var sms_handler = require('./../handler/sms_handler');
var supportController = require('./support_controller');
var travelController = require('./travel_controller');
var placeController = require('./place_controller');
var consts = require('./../config/consts');
var utils = require('./../tools/utils');
var moment = require('moment-jalaali');
var gm = require('gm');
var easyimg = require('easyimage');
var PNG = require('pngjs').PNG;
var fs = require('fs');
var crypto = require('crypto');
var async = require('async');

var imageMagick = gm.subClass({imageMagick: true});


module.exports.registerCustomerForOrganization = function (req, callback) {

    var phone_number = req.body.phone_number;
    var caller_number = req.body.caller_number.toUpperCase();
    var customer_name = req.body.customer_name;
    var customer_family = req.body.customer_family;
    customer_dao.saveOrganizationCustomerData(phone_number, caller_number,customer_name,(Math.floor(Math.random()*90000) + 10000).toString() , function (result) {

        if(result)
        {
            callback({result:true,customer_id:result._id})
        }
        else{
            callback({result:false,customer_id:null});
        }

    });

};

module.exports.registerCustomer = function (req, callback) {

  console.log("\n\n\n");
  console.log(req.body);
  console.log("\n\n\n");

  var phone_number = req.body.phone_number;
  var name = req.body.name;
  var email = req.body.email;
  var caller_number = req.body.caller_number.toUpperCase();
  var is_apple = req.body.is_apple;
  var device_id = req.body.device_id;
  var national_code = req.body.national_code;
  var invite_code = utils.makeid(6);

  sms_handler.sendVerifyCode(phone_number, function (result, verification_code) {

    if (result) {

      customer_dao.saveCustomerData(phone_number, name, email, caller_number, verification_code, is_apple, device_id, invite_code, national_code, function (result) {

        callback({result: true});

      });

    } else {

      callback({result: false});

    }

  });

};

module.exports.updateCode = function (req, callback) {

  var phone_number = req.body.phone_number;

  sms_handler.sendVerifyCode(phone_number, function (result, verification_code) {

    customer_dao.updateCode(phone_number, verification_code, function (result) {

      if(result) {

        callback({result: true});

      } else {

        callback({result: false});

      }


    });

  });

};

module.exports.verifyCode = function (req, callback) {

  var phone_number = req.body.phone_number;
  var verification_code = req.body.verification_code;

  console.log(req.body);

  customer_dao.verifyCode(phone_number, verification_code, function (is_code_verified) {

    if (is_code_verified) {

      customer_dao.updateCode(phone_number, verification_code, function (result) {

        if (result) {

          customer_dao.getCustomerInfo(phone_number, function (customer_data) {

            wallet_dao.getWalletInfoByCustomerId(customer_data._id, function (wallet_data) {

              if (customer_data) {

                support_dao.getCurrentNotify(function (notify) {

                  var token = aa.createNewToken(customer_data._id);

                  customer_data = customer_data.toObject();

                  customer_data.current_notify_id = notify.current_id;

                  customer_data.token = token;

                  customer_data.wallet = wallet_data.money;

                  console.log(customer_data);

                  callback({result: true, customer_data: customer_data});

                });
              }

            });

          });
        }

      });

    } else {

      callback({result: false, customer_data: {}});

    }

  });

};

module.exports.calculateTravelCost = function (req, callback) {

  var customer_id = req.body.customer_id;
  var source_lat = req.body.source_lat;
  var source_lan = req.body.source_lan;
  var destination_lat = req.body.destination_lat;
  var destination_lan = req.body.destination_lan;
  var second_destination_lat = parseFloat(req.body.second_destination_lat);
  var second_destination_lan = parseFloat(req.body.second_destination_lan);
  var is_two_way = req.body.is_two_way;
  var stop_time_value = parseInt(req.body.stop_time_value);
  var service_type = req.body.service_type;

  calculateCost(source_lan, source_lat, destination_lan, destination_lat, second_destination_lat, second_destination_lan, is_two_way, stop_time_value, service_type, function (travel_cost) {

    if (travel_cost) {

      callback({

        result: true,
        travel_cost: travel_cost

      });

    }

  });

};

module.exports.saveTravel = function (req, callback) {

  // create travel

  findNearestTaxi(travel_id, function (taxi_id) {

    if (taxi_id) {

      calculateTimeTravel(travel_id, taxi_id, function (time_travel) {

        // taxi with taxi_id founded

        travel_dao.updateTimeTravel(travel_id, time_travel, function (result) {

          if (result) {

            taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

              var r = taxi_data.toObject();
              r.time_travel = time_travel;

              var taxi_data = {
                taxi_data: r,
                result: true
              };

              callback(taxi_data);

            });
          }

        });


      });

    } else {

      // no taxi founded

      callback({result: false});

    }

  });

};

module.exports.travelsList = function (req, callback) {

  if (req.method == 'GET') {

    var customer_id = req.params.customer_id;
    var page = req.params.page;
    var per_page = req.params.per_page;

  } else {

    var customer_id = req.body.customer_id;
    var page = req.body.page;
    var per_page = req.body.per_page;

  }

  var final_travels_list = [];

  travel_dao.getCustomerTravels(customer_id, page, per_page, function (travels_list) {

    if (travels_list) {

      var len = travels_list.length;

      if (len > 0) {

        for (var i = 0; i < len; i++) {
          (function (cntr) {

            var travel_obj = travels_list[cntr].toObject();
            var taxi_id = travel_obj.taxi_id;

            taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

              if (taxi_data) {

                travel_obj.taxi_code = taxi_data.taxi_code;
                final_travels_list.push(travel_obj);

                if (cntr === len - 1) {
                  callback(final_travels_list);
                }

              }

            });

          })(i);
        }

      } else {

        callback(final_travels_list);

      }

    } else {

      callback(final_travels_list);

    }

  });

};

module.exports.customerTravelsList = function (req, callback) {

  if (req.method == 'GET') {

    var customer_id = req.params.customer_id;
    var page = req.params.page;
    var per_page = req.params.per_page;

  } else {

    var customer_id = req.body.customer_id;
    var page = req.body.page;
    var per_page = req.body.per_page;

  }

  var final_result = {
      final_travels_list: [],
      total_distance: 0,
      discount_amount: 0,
      travels_number: 0
  };

  travel_dao.getCustomerTravels(customer_id, page, per_page, function (travels_list) {

    if (travels_list) {

      var len = travels_list.length;

      if (len > 0) {

        async.eachSeries(travels_list,

          function (item, callback) {

            var travel_obj = item.toObject();
            var taxi_id = travel_obj.taxi_id;

            taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

              if (taxi_data) {

                travel_obj.taxi_code = taxi_data.taxi_code;
                travel_obj.driver_avatar = taxi_data.driver_avatar;
                travel_obj.driver_name = taxi_data.driver_name;
                final_result.final_travels_list.push(travel_obj);
                final_result.total_distance += travel_obj.distance;
                final_result.discount_amount += travel_obj.discount_amount;
                final_result.travels_number += 1;

              }

              callback(null);

            });
          },

          function (err, result) {

            if (err) {

              callback(false);

            } else {

              callback(final_result);

            }

            //No errors, move on with your code ...

          });

      } else {

        callback(final_result);

      }

    } else {

      callback(final_result);

    }

  });

};

module.exports.trackTravelTaxi = function (data, socket, callback) {

  var travel_id = data.travel_id;

  travel_dao.getTravelInfo(travel_id, function (travel_data) {

    if (travel_data) {

      taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

        if (travel_data.state == "taxi_confirmed") {

          var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

          var now = moment();

          var difference_time = now.diff(start_time, "seconds");

          var remaining_time = travel_data.time_travel - difference_time;

          if (remaining_time < 0) {

            remaining_time = 1;

          }

          var remaining_time = Math.floor(remaining_time / 60);

          var taxi_location = {

            _id: taxi_data._id,
            location_lat: taxi_data.location_lat,
            location_lan: taxi_data.location_lan,
            remaining_time: remaining_time
          };

          callback({result: true, taxi_location: taxi_location});

        } else {

          var taxi_location = {
            _id: taxi_data._id,
            location_lat: taxi_data.location_lat,
            location_lan: taxi_data.location_lan,
            remaining_time: 1
          };

          callback({result: false, taxi_location: taxi_location});

        }

      });

    }

  });

};

module.exports.updateInfo = function (req, callback) {

  var customer_id = req.body.customer_id;
  var email = req.body.email;
  var address = req.body.address;
  var name = req.body.name;
  var family = req.body.family;
  var sex = req.body.sex;
  var blood_group = req.body.blood_group;
  var emergency_phone_number = req.body.emergency_phone_number;
  var birthday = req.body.birthday;

  customer_dao.updateCustomerInfo(customer_id, email, address, name, family, sex, blood_group, emergency_phone_number, birthday, function (result) {

    if (result) {

      callback(true);

    }

  });

};

module.exports.getCustomerInfo = function (req, callback) {

  if (req.method == "POST") {
    var customer_id = req.body.customer_id;
  } else {
    var customer_id = req.params.customer_id;
  }
  customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

    if (customer_data) {

      callback(customer_data);

    }

  });

};

module.exports.getCustomerInfoBySubscriberNum = function (req, callback) {

    if (req.method == "POST") {
        var subscription_number = req.body.subscription_number;
        var station_id = req.body.station_id;
    } else {
        var subscription_number = req.params.subscription_number;
        var station_id = req.params.station_id;
    }
    customer_dao.getCustomerInfoBySubscriberNum(station_id,subscription_number, function (customer_data) {

        if (customer_data) {

            callback(customer_data);

        }
        else callback(false);

    });

};

module.exports.getCustomersList = function (req, callback) {

  var page = req.params.page;
  var per_page = req.params.per_page;

  customer_dao.getCustomersList(page, per_page, function (result) {

    if (result) {

      callback(result);

    }

  });

};

module.exports.editCustomer = function (req, callback) {

  var customer_id = req.body.customer_id;
  var name = req.body.name;
  var address = req.body.address;
  var email = req.body.email;

  customer_dao.updateCustomerInfo(customer_id, email, address, name, function (edit_result) {

    if (edit_result) {
      callback(edit_result);
    }

  });

};

module.exports.search = function (req, callback) {

  var search_query = req.body.search_query;

  if(search_query.length == 10) {

      search_query = "98" + search_query;

  }

  customer_dao.search(search_query, function (search_result) {

    if (search_result) {

      var customers_list = [];

      var len = search_result.length;

      if (len > 0) {
        for (var i = 0; i < len; i++) {
          (function (cntr) {

            var customer_obj = search_result[cntr];
            var customer_id = customer_obj._id;

            customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

              if (customer_data) {
                customers_list.push(customer_data);
              }

              if (cntr === len - 1) {
                callback(customers_list);
              }
            });


          })(i);
        }
      } else {
        callback(customers_list);
      }
    }

  });

};

module.exports.customerOpenedReconnect = function (data, io, socket, callback) {

  var travel_id = data.travel_id;

  travel_dao.getTravelInfo(travel_id, function (travel_data) {

    console.log(travel_data);

    if (travel_data.state == "taxi_not_found" || travel_data.state == "none") {

      console.log("HERE WE ARE");

      callback({travel_data: travel_data, taxi_data: {}, result: true});

    } else {

        console.log("HERE WE ARE 2");

        taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

          if(taxi_data) {

              var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

              var now = moment();
              var difference_time = now.diff(start_time, "seconds");

              var remaining_time = travel_data.time_travel - difference_time;

              if (remaining_time < 0) {

                  remaining_time = 0;

              }

              travel_dao.isTravelActive(travel_id, function (travel_is_active) {


                  if (travel_is_active) {
                      socket.join(travel_id);
                  }

                  var final_travel_data = travel_data.toObject();
                  final_travel_data.remaning_time = remaining_time;

                  callback({travel_data: final_travel_data, taxi_data: taxi_data, result: true});

              });

          } else {

              callback({travel_data: null, taxi_data: null, result: false});


          }

      });
    }

  });

};

module.exports.customerAppOpened = function (data, io, socket, callback) {

  var travel_id = data.travel_id;

  travel_dao.getTravelInfo(travel_id, function (travel_data) {

    if (travel_data.state == "taxi_not_found" || travel_data.state == "none" || travel_data.state == "support_canceled") {

      callback({travel_data: travel_data, taxi_data: {}, result: true});

    } else {

      taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

        var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

        var now = moment();

        var difference_time = now.diff(start_time, "seconds");

        var remaining_time = travel_data.time_travel - difference_time;

        if (remaining_time < 0) {

          remaining_time = 0;

        }

          if(typeof travel_data.toObject == 'function') {

              var final_travel_data = travel_data.toObject();

              final_travel_data.remaning_time = remaining_time;

          } else {

              var final_travel_data = travel_data;

              final_travel_data.remaning_time = remaining_time;

          }


          callback({travel_data: final_travel_data, taxi_data: taxi_data, result: true});

      });

    }

  });

};

module.exports.customerClosedReconnect = function (data, io, socket) {

  var travel_id = data.travel_id;
  var customer_travel_state = data.travel_state;

  travel_dao.getTravelInfo(travel_id, function (travel_data) {

    var current_travel_state = travel_data.state;
    var taxi_id = travel_data.taxi_id;

    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

      travel_dao.isTravelActive(travel_id, function (is_travel_active) {

        if (is_travel_active) {

          socket.join(travel_id);

        }

        if (current_travel_state !== customer_travel_state) {

          if (current_travel_state == "taxi_confirmed") {

            io.sockets.in(travel_id).emit('taxi_confirmed', {result: travel_data});

          } else if (current_travel_state == "taxi_canceled") {

            socket.emit('taxi_canceled_travel', travel_id);

          } else if (current_travel_state == "finished") {

            socket.emit('travel_finished', travel_id);

          } else if (current_travel_state == "support_canceled") {

            socket.emit('support_canceled_travel', travel_id);

          } else if (current_travel_state == "taxi_not_found") {

            socket.emit('taxi_not_found', {result: {travel_id: travel_id}});

          } else if (current_travel_state == "travel_started") {

            var data = {
              travel_data: travel_data,
              taxi_data: taxi_data
            };

            socket.emit('travel_started', {result: data});

          } else if (current_travel_state == "put_off_searching") {

            socket.emit('taxi_put_off_travel', travel_id);

          }
        }

      });

    });

  });

};

module.exports.checkAppVersion = function (req, callback) {

  var customer_current_version = parseInt(req.body.current_version);

      // if(customer_current_version == 3) {
      //
      //     callback({result: "not_active", message: "کاربر عزیز، طی چند روز آینده، نسخه‌ی جدید با امکانات فوق‌العاده منتشر می‌شود."});
      //
      // } else {

          support_dao.getSystemActivated(function (active_data) {

              if (active_data.is_active == false) {

                  callback({result: "not_active", message: active_data.message});

              } else {

                  supportController.getAppCurrentVersion(function (app_version) {

                      if(!app_version.is_deprecated) {

                          if (app_version.version_code == customer_current_version) {

                              callback({result: "no_update", message: "", url: "", package_name: ""});

                          } else if (app_version.version_code > customer_current_version) {

                              if (app_version.is_force_update) {

                                  callback({result: "force_update", message: "", url: app_version.url, package_name: ""});

                              } else if (app_version.last_force_version > customer_current_version) {

                                  callback({result: "force_update", message: "", url: app_version.url, package_name: ""});

                              } else {

                                  callback({result: "normal_update", message: "", url: app_version.url, package_name: ""});

                              }

                          } else if (app_version.version_code < customer_current_version) {

                              callback({result: "no_update", message: "", url: "", package_name: ""});

                          }

                      } else {

                          callback({result: "deprecated", message: "", url: app_version.url, package_name: app_version.package_name});

                      }

                  });

              }

          });

      // }

};

module.exports.checkIosVersion = function (req, callback) {

  var customer_current_version = parseInt(req.body.current_version);

  support_dao.getSystemActivated(function (active_data) {

    if (active_data.is_active == false) {

      callback({result: "not_active", message: active_data.message});

    } else {

      supportController.getIosCurrentVersion(function (app_version) {

        if (app_version.version_code == customer_current_version) {

          callback({result: "no_update", message: ""});

        } else if (app_version.version_code > customer_current_version) {

          if (app_version.is_force_update) {

            callback({result: "force_update", message: "", url: app_version.url});

          } else if (app_version.last_force_version > customer_current_version) {

            callback({result: "force_update", message: "", url: app_version.url});

          } else {

            callback({result: "normal_update", message: "", url: app_version.url});

          }

        } else if (app_version.version_code < customer_current_version) {

        callback({result: "no_update", message: "", url: "", package_name: ""});

        }

      });

    }

  });

};

// get taxis around man

module.exports.getAroundTaxis = function (req, callback) {

  var location_lat = req.body.lat;
  var location_lan = req.body.lan;
  var service_type = req.body.service_type;

  // var around_taxis = [];

  // var bound = map_tools.getSourceBound(location_lat, location_lan, consts.TAXI_SEARCH_RADIUS);

  // taxi_dao.getTaxisBoundsLocation(parseFloat(bound.min_lat), parseFloat(bound.max_lat), parseFloat(bound.min_lan), parseFloat(bound.max_lan), function (taxis_list) {

  taxi_dao.getAroundTaxis(service_type, parseFloat(location_lat), parseFloat(location_lan), function (taxis_list) {

    if (taxis_list.length >= 1) {

      callback({result: true, taxis_list: taxis_list});

    } else {

      callback({result: false, taxis_list: false});

    }

  });

  // console.log(req.body);
  //
  // taxi_dao.getTaxisLocation(function (taxis_list) {
  //
  //     console.log(taxis_list.length);
  //
  //     if(taxis_list.length >= 1) {
  //
  //         for (var i = 0; i < taxis_list.length; i++) {
  //
  //             var taxi_distance = map_tools.findDistance(location_lat, location_lan, taxis_list[i].location_lan, taxis_list[i].location_lat);
  //
  //             if(taxi_distance < 1000000) {
  //
  //                 around_taxis.push(taxis_list[i]);
  //
  //             }
  //
  //         }
  //
  //         callback({result: true, taxis_list: taxis_list});
  //
  //     } else {
  //
  //         callback({result: false, taxis_list: false});
  //
  //     }
  //
  // });


};

function calculateTwoPlaceCost(source_lan, source_lat, destination_lan, destination_lat, callback) {

  map_tools.findGoogleDistance(source_lan, source_lat, destination_lan, destination_lat, function (distance_in_meters) {


    // if( 21 <= moment().hour() <= 7) {
    //
    //     var cost = distance_in_meters * 1.2;
    //     cost = 500 * Math.round(cost/500);
    //
    // } else if( 16 <= moment().hour() <= 19 ) {
    //
    //     var cost = distance_in_meters * 1.2;
    //     cost = 500 * Math.round(cost/500);
    //
    // } else {
    //
    //     var cost = distance_in_meters * 1;
    //     cost = 500 * Math.round(cost/500);
    //
    // }

    // var cost = distance_in_meters * 1;
    // cost = 500 * Math.round(cost / 500);
    //
    // var final_cost = cost + consts.BASE_TRAVEL_COST;
    //
    // if (distance_in_meters > 8000) {
    //
    //   final_cost = final_cost * 0.8;
    //
    // }

    var current_hour = moment().hour();

    place_dao.getServiceFactor(function (service_factor) {

      // get input factor cost

      var input_factor = service_factor.input_factor;

      place_dao.getTimeFactor(current_hour, function (time_factor) {

        place_dao.getDistanceFactor(distance_in_meters, function (distance_factor) {

          if(distance_factor) {

            if(time_factor) {

              var cost = ( distance_in_meters / 1000 ) * time_factor.factor * distance_factor.factor;

              cost = 500 * Math.round(cost / 500);

              var final_cost = cost + input_factor;

              callback(final_cost);

            } else {


              place_dao.getBaseFactor(function (base_factor) {


                var cost = ( distance_in_meters / 1000 ) * base_factor.factor * distance_factor.factor;

                cost = 500 * Math.round(cost / 500);

                var final_cost = cost + input_factor;

                callback(final_cost);

              })

            }

          } else {

            if(time_factor) {

              var cost = ( distance_in_meters / 1000 ) * time_factor.factor;

              cost = 500 * Math.round(cost / 500);

              var final_cost = cost + input_factor;

              callback(final_cost);

            } else {


              place_dao.getBaseFactor(function (base_factor) {


                var cost = ( distance_in_meters / 1000 ) * base_factor.factor;

                cost = 500 * Math.round(cost / 500);

                var final_cost = cost + input_factor;

                callback(final_cost);

              })

            }

          }

        });


      });

    })


  });

}

// function calculateCost(source_lan, source_lat, destination_lan, destination_lat, second_destination_lat, second_destination_lan, is_two_way, stop_time_value, callback) {
//
//   calculateTwoPlaceCost(source_lan, source_lat, destination_lan, destination_lat, function(first_cost) {
//
//     place_dao.getServiceFactor(function (service_factors) {
//
//
//
//     })
//
//     if(is_two_way !== "false" || is_two_way == true) {
//
//       first_cost = first_cost + (first_cost * 0.8);
//
//     }
//
//     if(stop_time_value > 0) {
//
//       first_cost = first_cost + (stop_time_value * 200);
//
//     }
//
//     if(second_destination_lan !== 0) {
//
//       calculateTwoPlaceCost(destination_lan, destination_lat, second_destination_lan, second_destination_lat, function(second_cost) {
//
//         console.log(second_cost);
//
//         first_cost = first_cost + second_cost;
//
//         callback(first_cost);
//
//       });
//
//     } else {
//
//       callback(first_cost);
//
//     }
//
//
//
//   })
//
//   // callback(3000);
//
//   // placeController.getLocationPlaceData(source_lat, source_lan, function (source_place_data) {
//
//     // placeController.getLocationPlaceData(destination_lat, destination_lan, function (destination_place_data) {
//
//       // place_dao.getFactorData(source_place_data._id, destination_place_data._id, function (factor_cost) {
//       //
//       //   if (factor_cost.cost) {
//       //
//       //     callback(factor_cost.cost);
//
//         // } else {
//
//       // map_tools.findGoogleDistance(source_lan, source_lat, destination_lan, destination_lat, function (distance_in_meters) {
//       //
//       //       // if( 21 <= moment().hour() <= 7) {
//       //       //
//       //       //     var cost = distance_in_meters * 1.2;
//       //       //     cost = 500 * Math.round(cost/500);
//       //       //
//       //       // } else if( 16 <= moment().hour() <= 19 ) {
//       //       //
//       //       //     var cost = distance_in_meters * 1.2;
//       //       //     cost = 500 * Math.round(cost/500);
//       //       //
//       //       // } else {
//       //       //
//       //       //     var cost = distance_in_meters * 1;
//       //       //     cost = 500 * Math.round(cost/500);
//       //       //
//       //       // }
//       //
//       //       // var cost = distance_in_meters * 1;
//       //       // cost = 500 * Math.round(cost / 500);
//       //       //
//       //       // var final_cost = cost + consts.BASE_TRAVEL_COST;
//       //       //
//       //       // if (distance_in_meters > 8000) {
//       //       //
//       //       //   final_cost = final_cost * 0.8;
//       //       //
//       //       // }
//       //
//       //       var current_hour = moment().hour();
//       //
//       //       place_dao.getInputFactor(function (input_factor_data) {
//       //
//       //         // get input factor cost
//       //
//       //         var input_factor = input_factor_data.factor;
//       //
//       //         place_dao.getTimeFactor(current_hour, function (time_factor) {
//       //
//       //           place_dao.getDistanceFactor(distance_in_meters, function (distance_factor) {
//       //
//       //             if(distance_factor) {
//       //
//       //               if(time_factor) {
//       //
//       //                 var cost = ( distance_in_meters / 1000 ) * time_factor.factor * distance_factor.factor;
//       //
//       //                 cost = 500 * Math.round(cost / 500);
//       //
//       //                 var final_cost = cost + input_factor;
//       //
//       //                 callback(final_cost);
//       //
//       //               } else {
//       //
//       //
//       //                 place_dao.getBaseFactor(function (base_factor) {
//       //
//       //
//       //                   var cost = ( distance_in_meters / 1000 ) * base_factor.factor * distance_factor.factor;
//       //
//       //                   cost = 500 * Math.round(cost / 500);
//       //
//       //                   var final_cost = cost + input_factor;
//       //
//       //                   callback(final_cost);
//       //
//       //                 })
//       //
//       //               }
//       //
//       //             } else {
//       //
//       //               if(time_factor) {
//       //
//       //                 var cost = ( distance_in_meters / 1000 ) * time_factor.factor;
//       //
//       //                 cost = 500 * Math.round(cost / 500);
//       //
//       //                 var final_cost = cost + input_factor;
//       //
//       //                 callback(final_cost);
//       //
//       //               } else {
//       //
//       //
//       //                 place_dao.getBaseFactor(function (base_factor) {
//       //
//       //
//       //                   var cost = ( distance_in_meters / 1000 ) * base_factor.factor;
//       //
//       //                   cost = 500 * Math.round(cost / 500);
//       //
//       //                   var final_cost = cost + input_factor;
//       //
//       //                   callback(final_cost);
//       //
//       //                 })
//       //
//       //               }
//       //
//       //             }
//       //
//       //           });
//       //
//       //           // if(time_factor) {
//       //           //
//       //           //   var cost = ( distance_in_meters / 1000 ) * time_factor.factor;
//       //           //
//       //           //   cost = 500 * Math.round(cost / 500);
//       //           //
//       //           //   var final_cost = cost + consts.BASE_TRAVEL_COST;
//       //           //
//       //           //   callback(final_cost);
//       //           //
//       //           // } else {
//       //           //
//       //           //
//       //           //   place_dao.getBaseFactor(function (base_factor) {
//       //           //
//       //           //
//       //           //     var cost = ( distance_in_meters / 1000 ) * base_factor.factor;
//       //           //
//       //           //     cost = 500 * Math.round(cost / 500);
//       //           //
//       //           //     var final_cost = cost + consts.BASE_TRAVEL_COST;
//       //           //
//       //           //     callback(final_cost);
//       //           //
//       //           //   })
//       //           //
//       //           // }
//       //
//       //         });
//       //
//       //       })
//       //
//       //
//       //     });
//
//       //   }
//       //
//       // })
//
//     // });
//
//   // });
//
// };

function calculateCost(source_lan, source_lat, destination_lan, destination_lat, second_destination_lat, second_destination_lan, is_two_way, stop_time_value, service_type, callback) {

  calculateTwoPlaceCost(source_lan, source_lat, destination_lan, destination_lat, function(first_cost) {

    place_dao.getServiceFactor(function (service_factors) {

      if(is_two_way !== "false" || is_two_way == true) {

        first_cost = first_cost + (first_cost * service_factors.two_way_factor);

      }

      if(stop_time_value > 0) {

        first_cost = first_cost + (stop_time_value * service_factors.stop_factor);

      }

      if(second_destination_lan !== 0) {

        calculateTwoPlaceCost(destination_lan, destination_lat, second_destination_lan, second_destination_lat, function(second_cost) {

          first_cost = first_cost + second_cost;

          if(service_type) {

            if(service_type === "vip") {

              first_cost = first_cost * service_factors.vip_factor;

            }

            if(service_type === "lady") {

              first_cost = first_cost * service_factors.lady_factor;

            }

            if(service_type === "normal") {

              first_cost = first_cost * service_factors.normal_factor;

            }

            callback(first_cost);

          } else {

            callback(first_cost);

          }

        });

      } else {

        if(service_type) {

          if(service_type === "vip") {

            first_cost = first_cost * service_factors.vip_factor;

          }

          if(service_type === "lady") {

            first_cost = first_cost * service_factors.lady_factor;

          }

          if(service_type === "normal") {

            first_cost = first_cost * service_factors.normal_factor;

          }

          callback(first_cost);


        } else {

          callback(first_cost);

        }


      }

    });

  });

};

function findNearestTaxi(travel_id, callback) {

  travel_dao.getTravelInfo(travel_id, function (travel_data) {

    if (travel_data) {

      var around_taxis = [];

      var source_lat = travel_data.source_lat;
      var source_lan = travel_data.source_lan;

      taxi_dao.getTaxisLocation(function (taxis_list) {

        if (taxis_list.length >= 1) {

          for (var i = 0; i < taxis_list.length; i++) {

            var taxi_distance = map_tools.findDistance(source_lan, source_lat, taxis_list[i].location_lan, taxis_list[i].location_lat);

            if (taxi_distance < 6000) {

              around_taxis.push(taxis_list[i]);

            }

            //if (map_tools.findDistance(customer_lan, customer_lat, candidate_taxi.location_lan, candidate_taxi.location_lat) > map_tools.findDistance(customer_lan, customer_lat, taxis_list[i].location_lan, taxis_list[i].location_lat)) {
            //
            //    candidate_taxi = taxis_list[i];
            //
            //}
          }

          if (around_taxis.length > 0) {

            var candidate_taxi = around_taxis[0];

            taxi_dao.getTaxiWaitTime(candidate_taxi._id, function (first_taxi_wait_time) {

              var candidate_taxi_wait_time = first_taxi_wait_time;

              for (var j = 0; j < around_taxis.length; j++) {

                (function (cntr) {

                  taxi_dao.getTaxiWaitTime(around_taxis[cntr]._id, function (taxi_wait_time) {

                    if (taxi_wait_time > candidate_taxi_wait_time) {

                      var candidate_taxi_wait_time = taxi_wait_time;
                      candidate_taxi = around_taxis[cntr];

                    }

                    if (cntr === around_taxis.length - 1) {

                      callback(candidate_taxi._id);

                    }

                  });

                })(j);

              }

            });

          } else {

            callback(false);

          }

        } else {

          callback(false);

        }

      });
    }

  });

};

function calculateTimeTravel(travel_id, taxi_id, callback) {

  travel_dao.getTravelInfo(travel_id, function (travel_data) {

    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

      map_tools.timeTravelByGoogle(travel_data.source_lan, travel_data.source_lat, taxi_data.location_lan, taxi_data.location_lat, function (time_travel) {

        callback(time_travel);

      });

    });

  });
};

module.exports.updateAvatar = function (req, callback) {

  var customer_id = req.body.customer_id;
  var photo = req.body.image;
  var photo_path = "public/images/";

  var base64Data = photo.replace(/^data:image\/png;base64,/, "");

  var seed = crypto.randomBytes(20);
  var uniqueSHA1String = crypto
    .createHash('sha256')
    .update(seed)
    .digest('hex');

  fs.writeFile(photo_path + uniqueSHA1String + ".png", base64Data, 'base64', function (err) {

    photo_path = photo_path + uniqueSHA1String + ".png";

    imageMagick(photo_path).resize(300, 300).quality(100).write(photo_path, function (err) {

      if (!err) {

        fs.createReadStream(photo_path)
          .pipe(new PNG({
            filterType: 4
          }))
          .on('parsed', function () {
            for (var y = 0; y < this.height; y++) {
              for (var x = 0; x < this.width; x++) {
                var idx = (this.width * y + x) << 2;
                var radius = this.height / 2;
                if (y >= Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2)) + radius || y <= -(Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2))) + radius) {
                  this.data[idx + 3] = 0;
                }
              }
            }

            this.pack().pipe(fs.createWriteStream(photo_path));

            customer_dao.updateAvatar(customer_id, photo_path, function (result) {

              if (result) {

                callback({result: true, avatar: result});

              } else {

                callback({result: false, avatar: ""});

              }

            });

          });

      } else {

        callback({result: false, avatar: ""});

      }

    });

  });



}

module.exports.getCustomerAvatar = function (req, callback) {

  console.log(req.body);

  var customer_id = req.body.customer_id;

  customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

    console.log(customer_data);


    if (customer_data) {

      console.log(customer_data.avatar);

      callback({result: true, avatar: customer_data.avatar});

    } else {

      callback({result: false, avatar: ""});

    }

  })

}


module.exports.getStationSubscriberList = function (station_id, page, per_page, callback) {

    customer_dao.getStationSubscriberList(station_id, page, per_page, function (result) {

        if (result) {

            callback(result);

        }

    });

};

module.exports.phoneSearch = function (req, callback) {

  var phone_number = req.body.search_query;

  customer_dao.phoneSearch(phone_number, function (result) {

    callback(result);

  });

};

module.exports.travelsListByFilter = function (req, callback) {

  var customer_id = req.body.customer_id;
  var filter = req.body.filter;

  travel_dao.getCustomerTravelsByFilter(customer_id, filter, function (travels) {

    callback(travels);

  })

};

module.exports.addFavoritePlace = function (req, callback) {

  var place_id = req.body.place_id;
  var customer_id = req.body.customer_id;

  customer_dao.addFavoritePlace(customer_id, place_id, function (result) {

    if(result) {

      callback({result: true});

    } else {

      callback({result: false});

    }

  });

};

module.exports.removeFavoritePlace = function (req, callback) {

  var place_id = req.body.place_id;
  var customer_id = req.body.customer_id;

  customer_dao.removeFavoritePlace(customer_id, place_id, function (result) {

      if(result) {

          callback({result: true});

      } else {

          callback({result: false});

      }

  })

};

module.exports.getCustomerFavoritePlaces = function (req, callback) {

  var customer_id = req.body.customer_id;
  var final_places = [];

  customer_dao.getCustomerFavoritePlaces(customer_id, function (places) {

    if(places.length > 0) {

        var len = places.length;

        if (len > 0) {

          async.eachSeries(places,

            function (item, callback) {

              place_dao.getPlaceInfo(item, function (place_info) {

                if (place_info) {

                  final_places.push(place_info);

                }

                callback(null);

              });
            },

            function (err, result) {

              if (err) {

                callback(false);

              } else {

                callback({result: final_places});

              }

              //No errors, move on with your code ...

            });

        } else {

            callback({result: final_places});

        }

    } else {

        callback({result: final_places});

    }

  })

};

module.exports.getTravelInfoForCustomer = function (req, callback) {

  var travel_id = req.body.travel_id;
  var result = {};

  travel_dao.getTravelInfo(travel_id, function (travel_data) {

    taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

      result.travel_data = travel_data;
      result.taxi_data = taxi_data;
      result.result = true;

      callback(result);

    })

  })

}


module.exports.getTravelInfo = function (req, callback) {

  var travel_id = req.body.travel_id;
  var result = {};

  travel_dao.getTravelInfo(travel_id, function (travel_data) {

    taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

      result.travel = travel_data;
      result.taxi = taxi_data;

      callback(result);

    })

  })

}
