var fs = require('fs');
var soap = require('soap');
var customer_dao = require('./../dao/customer_dao');
var organization_dao = require('./../dao/organization_dao');
var taxi_dao = require('./../dao/taxi_dao');
var travel_dao = require('./../dao/travel_dao');
var comment_dao = require('./../dao/comment_dao');
var station_dao = require('./../dao/station_dao');
var support_dao = require('./../dao/support_dao');
var turnover_dao = require('./../dao/turnover_dao');
var support_controller = require('./../controller/support_controller');
var map_tools = require('./../tools/map_tools');
var image_tools = require('./../tools/image_tools');
var utils = require('./../tools/utils');
var aa = require('./../aa/aa');
var passwordHash = require("password-hash");
var moment = require('moment-jalaali');
var gm = require('gm');
var easyimg = require('easyimage');
var crypto = require('crypto');
var base64 = require('../tools/base64');
var smsHandler = require('../handler/sms_handler');
var PNG = require('pngjs').PNG;
var async = require('async');

var imageMagick = gm.subClass({imageMagick: true});

module.exports.getorganizationtaxis = function (req, callback) {
    var organization_id = req.body.organization_id;
    var page = req.params.page;
    var per_page = req.params.per_page;

    taxi_dao.getOrganizationTaxisList(page, per_page, organization_id, function (result) {

        if (result) {

            callback(result);

        }

    });

};

module.exports.registerStationTaxi = function (req, callback) {

    var station_id = req.body.station_id;
    var station_title = req.body.station_title;
    var username = req.body.username;
    var password = req.body.password;
    var driver_name = req.body.driver_name;
    var driver_phone_number = req.body.driver_phone_number;
    var model = req.body.model;
    var year = req.body.year;
    var car_code = req.body.car_code;
    var car_code_base = req.body.car_code_base;
    var car_color = req.body.car_color;
    var photo = req.body.image_data;

    var hashed_password = passwordHash.generate(password);

    station_dao.getStationInfoById(station_id, function (station_data) {

        image_tools.uploadTaxiImage(photo, function (photo_path) {
            console.log("driver register for station2 ..........>" + photo_path)
            imageMagick(photo_path).resize(300, 300).quality(100).write(photo_path, function (err) {

                if (!err) {

                    fs.createReadStream(photo_path)
                        .pipe(new PNG({
                            filterType: 4
                        }))
                        .on('parsed', function () {
                            for (var y = 0; y < this.height; y++) {
                                for (var x = 0; x < this.width; x++) {
                                    var idx = (this.width * y + x) << 2;
                                    var radius = this.height / 2;
                                    if (y >= Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2)) + radius || y <= -(Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2))) + radius) {
                                        this.data[idx + 3] = 0;
                                    }
                                }
                            }
                            this.pack().pipe(fs.createWriteStream(photo_path));

                            taxi_dao.registerStationTaxi(station_id, station_data.title, username, hashed_password, model, year, driver_name, driver_phone_number, car_code, car_code_base, photo_path, car_color, function (result) {
                                console.log("driver register for organization2 ..........>" + result)
                                callback(result);

                            });

                        });

                } else {

                    callback(false);

                }

            });

        });

    });

};
module.exports.registerOrganizationTaxi = function (req, callback) {


    var organization_id = req.body.organization_id;
    var organization_title = req.body.organization_title;
    var username = req.body.username;
    var password = req.body.password;
    var driver_name = req.body.driver_name;
    var driver_phone_number = req.body.driver_phone_number;
    var model = req.body.model;
    var year = req.body.year;
    var car_code = req.body.car_code;
    var car_code_base = req.body.car_code_base;
    var car_color = req.body.car_color;
    var photo = req.body.image_data;
    var email = req.body.email;
    var account_number = req.body.account_number;
    var account_name = req.body.account_name;
    var account_bank = req.body.account_bank;
    var hashed_password = passwordHash.generate(password);
    var passenger_invite_code = utils.makeid(5);
    var driver_invite_code = utils.makeid(5);

    organization_dao.getOrganizationInfoById(organization_id, function (organization_data) {

        image_tools.uploadTaxiImage(photo, function (photo_path) {
            console.log("driver register for organization2 ..........>" + photo_path)
            imageMagick(photo_path).resize(300, 300).quality(100).write(photo_path, function (err) {

                if (!err) {

                    fs.createReadStream(photo_path)
                        .pipe(new PNG({
                            filterType: 4
                        }))
                        .on('parsed', function () {
                            for (var y = 0; y < this.height; y++) {
                                for (var x = 0; x < this.width; x++) {
                                    var idx = (this.width * y + x) << 2;
                                    var radius = this.height / 2;
                                    if (y >= Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2)) + radius || y <= -(Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2))) + radius) {
                                        this.data[idx + 3] = 0;
                                    }
                                }
                            }
                            this.pack().pipe(fs.createWriteStream(photo_path));

                            taxi_dao.registerOrganizationTaxi(organization_id, organization_data.organization_name, username, hashed_password, model, year, driver_name, driver_phone_number, car_code, car_code_base, photo_path, car_color, function (result) {
                                console.log("driver register for organization2 ..........>" + result)
                                callback(result);

                            });

                        });

                } else {

                    callback(false);

                }

            });

        });

    });

};

module.exports.registerTaxi = function (req, callback) {

    var username = req.body.username;
    var password = req.body.password;
    var driver_name = req.body.driver_name;
    var driver_phone_number = req.body.driver_phone_number;
    var model = req.body.model;
    var year = req.body.year;
    var car_code = req.body.car_code;
    var car_code_base = req.body.car_code_base;
    var car_color = req.body.car_color;
    var station_id = req.body.station_id;
    var photo = req.body.image_data;
    var service_type = req.body.service_type;
    var service_id = req.body.service_type;
    var email = req.body.email;
    var account_number = req.body.account_number;
    var account_name = req.body.account_name;
    var account_bank = req.body.account_bank;
    var hashed_password = passwordHash.generate(password);
    var passenger_invite_code = utils.makeid(5);
    var driver_invite_code = utils.makeid(5);

    taxi_dao.getServiceTypeData(service_id, function (service_data) {

        station_dao.getStationData(station_id, function (station_data) {

            image_tools.uploadTaxiImage(photo, function (photo_path) {

                imageMagick(photo_path).resize(300, 300).quality(100).write(photo_path, function (err) {

                    if (!err) {

                        fs.createReadStream(photo_path)
                            .pipe(new PNG({
                                filterType: 4
                            }))
                            .on('parsed', function () {
                                for (var y = 0; y < this.height; y++) {
                                    for (var x = 0; x < this.width; x++) {
                                        var idx = (this.width * y + x) << 2;
                                        var radius = this.height / 2;
                                        if (y >= Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2)) + radius || y <= -(Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2))) + radius) {
                                            this.data[idx + 3] = 0;
                                        }
                                    }
                                }

                                this.pack().pipe(fs.createWriteStream(photo_path));

                                taxi_dao.registerTaxi(username, email, hashed_password, model, year, driver_name, driver_phone_number, car_code, car_code_base, photo_path, car_color, station_data._id, station_data.title, service_data.service_title, service_id, driver_invite_code, passenger_invite_code, account_number, account_name, account_bank, function (result) {

                                    smsHandler.sendDriverWelcomeMessage(driver_phone_number, driver_name, username, password, function (result) {
                                    });

                                    callback(result);

                                });

                            });

                    } else {

                        callback(false);

                    }

                });

            });

        });

    })


};

module.exports.loginTaxi = function (req, callback) {

    console.log(req.body);

    var username = req.body.username;
    var password = req.body.password;
    var is_apple = req.body.is_apple;
    var device_id = req.body.device_id;

    taxi_dao.getTaxiByUsername(username, function (taxi_data) {

        if (taxi_data) {

            console.log(taxi_data);

            if (passwordHash.verify(password, taxi_data.password)) {

                var token = aa.createNewToken(taxi_data._id);

                taxi_data = taxi_data.toObject();

                taxi_data.token = token;

                taxi_data.is_online = 0;

                if ((is_apple && device_id)) {

                    taxi_dao.updateTaxiAppleInfo(taxi_data._id, is_apple, device_id, function (result) {

                        if (result) {

                            taxi_dao.setTaxiOffline(taxi_data._id, function (result) {
                            });

                            callback({result: true, taxi_data: taxi_data});

                        }

                    });

                } else {

                    taxi_dao.setTaxiOffline(taxi_data._id, function (result) {
                    });

                    callback({result: true, taxi_data: taxi_data});

                }

            } else {

                taxi_dao.setTaxiOffline(taxi_data._id, function (result) {
                });

                callback({result: false, taxi_data: null});

            }

        } else {

            // taxi_dao.setTaxiOffline(taxi_data._id, function (result) {});

            callback({result: false, taxi_data: null});

        }


    });

};

module.exports.getTaxisList = function (req, callback) {

    var page = req.params.page;
    var per_page = req.params.per_page;

    taxi_dao.getTaxisList(page, per_page, function (result) {

        if (result) {

            callback(result);

        }

    });

};

module.exports.getStationTaxisList = function (station_id, page, per_page, callback) {

    taxi_dao.getStationTaxisList(station_id, page, per_page, function (result) {

        if (result) {

            callback(result);

        }

    });

};

module.exports.getTaxisListAll = function (req, callback) {

    taxi_dao.getTaxisListAll(function (result) {

        if (result) {

            callback(result);

        }

    });

};

module.exports.allTaxiTravels = function (req, callback) {

    console.log(req.body);

    if (req.method == 'GET') {
        var taxi_id = req.params.taxi_id;
        var page = req.params.page;
        var per_page = req.params.per_page;
    } else {
        var taxi_id = req.body.taxi_id;
        var page = req.body.page;
        var per_page = req.body.per_page;
    }

    travel_dao.getTaxiTravels(taxi_id, page, per_page, function (travels_list) {

        var final_travels_list = [];

        var len = travels_list.length;

        if (len > 0) {

            async.eachSeries(travels_list,

                function (item, callback) {

                    var travel_obj = item.toObject();
                    var customer_id = travel_obj.customer_id;

                    customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

                        if (customer_data) {
                            travel_obj.customer_name = customer_data.name;
                            final_travels_list.push(travel_obj);
                        }

                        callback(null);

                    });
                },

                function (err, result) {

                    if (err) {

                        callback(false);

                    } else {

                        callback(final_travels_list);
                    }

                    //No errors, move on with your code..
                });

            // for (var i = 0; i < len; i++) {
            //     (function (cntr) {
            //
            //         var travel_obj = travels_list[cntr].toObject();
            //         var customer_id = travel_obj.customer_id;
            //
            //         customer_dao.getCustomerInfoById(customer_id, function (customer_data) {
            //
            //             if (customer_data) {
            //                 travel_obj.customer_name = customer_data.name;
            //                 final_travels_list.push(travel_obj);
            //
            //                 if (cntr === len - 1) {
            //                     callback(final_travels_list);
            //                 }
            //             }
            //
            //         });
            //
            //
            //     })(i);
            // }

        } else {

            callback(final_travels_list);
        }

    });

};

module.exports.travelsList = function (req, callback) {

    console.log(req.body);

    if (req.method == 'GET') {
        var taxi_id = req.params.taxi_id;
        var page = req.params.page;
        var per_page = req.params.per_page;
    } else {
        var taxi_id = req.body.taxi_id;
        var page = req.body.page;
        var per_page = req.body.per_page;
    }

    travel_dao.getFinishedTaxiTravels(taxi_id, page, per_page, function (travels_list) {

        var final_travels_list = [];

        var len = travels_list.length;

        if (len > 0) {

          async.eachSeries(travels_list,

            function (item, callback) {

              var travel_obj = item.toObject();
              var taxi_id = travel_obj.taxi_id;

              var travel_obj = item.toObject();
              var customer_id = travel_obj.customer_id;

              customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

                if (customer_data) {

                  travel_obj.customer_name = customer_data.name;
                  final_travels_list.push(travel_obj);

                }

                callback(null);

              });
            },

            function (err, result) {

              if (err) {

                callback(false);

              } else {

                callback(final_travels_list);

              }

              //No errors, move on with your code ...

            });

        } else {

            callback(final_travels_list);
        }

    });

};

module.exports.setTaxiOffline = function (req, callback) {

    if (req.body) {
        var taxi_id = req.body.taxi_id;
    } else {
        var taxi_id = req.taxi_id;
    }
    taxi_dao.setTaxiOffline(taxi_id, function (result) {

        callback(result);

    });


};

module.exports.setTaxiOnline = function (req, callback) {

    var taxi_id = req.body.taxi_id;

    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

        if (taxi_data.is_online == 0) {

            var mmt = moment();
            var mmtMidnight = mmt.clone().startOf('day');
            var start_time = mmt.diff(mmtMidnight, 'seconds');

            var today = moment().format("jYYYY/jM/jD");

            taxi_dao.setTaxiOnlineStartTime(taxi_id, start_time, today, function (result) {
            });

        }

        taxi_dao.setTaxiOnline(taxi_id, function (result) {

            callback(result);

        });

    });


};

module.exports.intervalUpdateTaxiInfo = function (data, io, socket, callback) {

    taxi_dao.getTaxiInfo(data.taxi_id, function (taxi_data) {

        if (taxi_data.is_online == 0) {

            var mmt = moment();
            var mmtMidnight = mmt.clone().startOf('day');
            var start_time = mmt.diff(mmtMidnight, 'seconds');

            var today = moment().format("jYYYY/jM/jD");

            taxi_dao.setTaxiOnlineStartTime(data.taxi_id, start_time, today, function (result) {
            });

        }

        taxi_dao.setSocketId(data.taxi_id, socket.id, function (result) {

            taxi_dao.setTaxiOnline(data.taxi_id, function (result) {

                taxi_dao.setTaxiLocation(data.taxi_id, data.location_lat, data.location_lan, function (result) {

                    if (result) {

                        callback(result);

                    }

                });

            });

        });

    });

};

module.exports.getTaxiInfo = function (req, callback) {

    var taxi_id = req.query.taxi_id ? req.query.taxi_id : req.params.taxi_id;

    if (!taxi_id)
        taxi_id = req.body.taxi_id;

    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

        callback(taxi_data);

    });

};

module.exports.editTaxi = function (req, callback) {

    var taxi_id = req.body.taxi_id;
    var username = req.body.username;
    var color = req.body.color;
    var model = req.body.model;
    var driver_phone_number = req.body.driver_phone_number;
    var driver_name = req.body.driver_name;
    var year = req.body.year;
    var car_code = req.body.car_code;
    var car_code_base = req.body.car_code_base;
    var car_color = req.body.car_color;
    var station_id = req.body.station_id;
    var service_type = req.body.service_type;
    var account_number = req.body.account_number;
    var account_name = req.body.account_name;
    var account_bank = req.body.account_bank;

    station_dao.getStationData(station_id, function (station_data) {

        taxi_dao.getServiceTypeData(service_type, function (service_data) {

            taxi_dao.editTaxiInfo(taxi_id, username, color, model, driver_name, driver_phone_number, year, car_code, car_code_base, car_color, station_data._id, station_data.title, service_data.service_title, service_type, account_number, account_name, account_bank, function (edit_result) {

                if (edit_result) {
                    callback(edit_result);
                }

            });

        });
    });

};

module.exports.updateTaxiAvatar = function (req, callback) {

    var taxi_id = req.body.taxi_id;
    var photo = req.body.image_data;

    image_tools.uploadTaxiImage(photo, function (photo_path) {

        imageMagick(photo_path).resize(300, 300).quality(100).write(photo_path, function (err) {

            if (!err) {

                fs.createReadStream(photo_path)
                    .pipe(new PNG({
                        filterType: 4
                    }))
                    .on('parsed', function () {
                        for (var y = 0; y < this.height; y++) {
                            for (var x = 0; x < this.width; x++) {
                                var idx = (this.width * y + x) << 2;
                                var radius = this.height / 2;
                                if (y >= Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2)) + radius || y <= -(Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2))) + radius) {
                                    this.data[idx + 3] = 0;
                                }
                            }
                        }
                        this.pack().pipe(fs.createWriteStream(photo_path));

                        taxi_dao.updateTaxiAvatar(taxi_id, photo_path, function (result) {

                            callback(result);

                        });

                    });

            } else {

                callback(false);

            }

        });

    });


}

module.exports.search = function (req, callback) {

    var search_query = req.body.search_query;

    taxi_dao.search(search_query, function (search_result) {

        if (search_result) {

            var taxis_list = [];

            var len = search_result.length;

            if (len > 0) {
                for (var i = 0; i < len; i++) {
                    (function (cntr) {

                        var taxi_obj = search_result[cntr];
                        var taxi_id = taxi_obj._id;

                        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                            if (taxi_data) {
                                taxis_list.push(taxi_data);
                            }

                            if (cntr === len - 1) {
                                callback(taxis_list);
                            }
                        });


                    })(i);
                }
            } else {
                callback(taxis_list);
            }
        }

    });

};

module.exports.getTaxiLocation = function (data, socket, callback) {

    var taxi_id = data.taxi_id;

    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

        var result = {
            location_lat: taxi_data.location_lat,
            location_lan: taxi_data.location_lan
        };

        callback(result);
    });
};

function trackingActivateCheck(data, io, socket) {

    var taxi_id = data.taxi_id;

    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

        travel_dao.getTaxiActiveTravels(taxi_id, function (travel) {

            if (travel) {

                var travel_id = travel._id;

                travel_dao.getTravelInfo(travel_id, function (travel_data) {

                    if (
                        travel_data.state == 'taxi_confirmed' &&
                        travel_data.is_active &&
                        (map_tools.findDistance(travel_data.source_lan, travel_data.source_lat, taxi_data.location_lan, taxi_data.location_lat)) < 2000) {

                        travel_dao.setTaxiNear(travel_id, function (is_near_set) {

                            if (is_near_set) {

                                // emit to client that taxi arrived

                                io.sockets.in(travel_id).emit('taxi_tracking_ activate', travel_id);

                            } else {
                                // don't emit that

                            }

                        });

                    }

                });

            }

        });

    });


};

module.exports.taxiAppOpened = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var taxi_id = data.taxi_id;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if(travel_data.taxi_id.toString() === taxi_id.toString()) {

          taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

            if (taxi_data) {

              if (travel_data.state == "taxi_founded") {

                // calculate timer time

                var taxi_found_date = moment(travel_data.taxi_found_date, "jYYYY/jM/jD HH:mm:ss");

                var now = moment();

                var timer_difference_time = now.diff(taxi_found_date, "seconds");

                var timer_time = travel_data.taxi_timer_time - timer_difference_time;

                if (timer_time < 0) {

                  timer_time = 0;

                }

                // calculate timer time

                var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                var difference_time = now.diff(start_time, "seconds");

                var remaining_time = travel_data.time_travel - difference_time;

                if (remaining_time < 0) {

                  remaining_time = 0;

                }

                var final_travel_data = travel_data.toObject();

                final_travel_data.taxi_timer_time = timer_time;

                final_travel_data.remaning_time = remaining_time;

                final_travel_data.taxi_location_lat = taxi_data.location_lat;
                final_travel_data.taxi_location_lan = taxi_data.location_lan;

                callback({travel_data: final_travel_data, result: true});

              } else if (travel_data.state == "taxi_not_found" || travel_data.full_date == "") {

                console.log("\n\n\n");
                console.log("FARIIIIIIIIIID");
                console.log("\n\n\n");

                var final_travel_data = travel_data.toObject();

                final_travel_data.taxi_timer_time = 0;

                final_travel_data.remaning_time = 0;

                final_travel_data.taxi_location_lat = taxi_data.location_lat;
                final_travel_data.taxi_location_lan = taxi_data.location_lan;

                console.log("\n\n\n");
                console.log(final_travel_data);
                console.log("\n\n\n");

                callback({travel_data: final_travel_data, result: true});


              } else {

                // calculate timer time

                var now = moment();

                var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                var difference_time = now.diff(start_time, "seconds");

                var remaining_time = travel_data.time_travel - difference_time;

                if (remaining_time < 0) {

                  remaining_time = 0;

                }

                var final_travel_data = travel_data.toObject();

                final_travel_data.taxi_timer_time = 0;

                final_travel_data.remaning_time = remaining_time;

                final_travel_data.taxi_location_lat = taxi_data.location_lat;
                final_travel_data.taxi_location_lan = taxi_data.location_lan;

                callback({travel_data: final_travel_data, result: true});


              }

            }

          });

        } else {

          callback({travel_data: null, result: false});

        }

    });

};


module.exports.taxiOpenedReconnect = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var taxi_id = data.taxi_id;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

          if(travel_data.taxi_id.toString() === taxi_id.toString()) {

            if (taxi_data) {

              if (travel_data.state == "taxi_founded") {

                // calculate timer time

                var taxi_found_date = moment(travel_data.taxi_found_date, "jYYYY/jM/jD HH:mm:ss");

                var now = moment();

                var timer_difference_time = now.diff(taxi_found_date, "seconds");

                var timer_time = travel_data.taxi_timer_time - timer_difference_time;

                if (timer_time < 0) {

                  timer_time = 0;

                }

                // calculate timer time

                var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                var difference_time = now.diff(start_time, "seconds");

                var remaining_time = travel_data.time_travel - difference_time;

                if (remaining_time < 0) {

                  remaining_time = 0;

                }

                var final_travel_data = travel_data.toObject();

                final_travel_data.taxi_timer_time = timer_time;

                final_travel_data.remaning_time = remaining_time;

                final_travel_data.taxi_location_lat = taxi_data.location_lat;
                final_travel_data.taxi_location_lan = taxi_data.location_lan;

                callback({travel_data: final_travel_data, result: true});

              } else {

                // calculate timer time

                var now = moment();

                var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                var difference_time = now.diff(start_time, "seconds");

                var remaining_time = travel_data.time_travel - difference_time;

                if (remaining_time < 0) {

                  remaining_time = 0;

                }

                var final_travel_data = travel_data.toObject();

                final_travel_data.taxi_timer_time = 0;

                final_travel_data.remaning_time = remaining_time;

                final_travel_data.taxi_location_lat = taxi_data.location_lat;
                final_travel_data.taxi_location_lan = taxi_data.location_lan;

                callback({travel_data: final_travel_data, result: true});


              }

            }

          } else {

            callback({travel_data: null, result: false})

          }

            // if (taxi_data) {
            //
            //   var start_time = moment(travel_data.full_date, "jYYYY/jM/jD");
            //   var now = moment();
            //   var remaining_time = now.diff(start_time, "seconds");
            //
            //   if (remaining_time < 0) {
            //
            //     remaining_time = 0;
            //
            //   }
            //
            //   travel_dao.isTravelActive(travel_id, function (travel_is_active) {
            //
            //
            //     if (travel_is_active) {
            //       socket.join(travel_id);
            //     }
            //
            //     var final_travel_data = travel_data.toObject();
            //     final_travel_data.remaning_time = remaining_time;
            //     final_travel_data.taxi_location_lat = taxi_data.location_lat;
            //     final_travel_data.taxi_location_lan = taxi_data.location_lan;
            //
            //     callback({travel_data: final_travel_data});
            //
            //   });
            //
            // }


        });

    });

};

module.exports.HasTaxiNewTravel = function (req, callback) {

    console.log("FREE RECONNECT INIT");

    var taxi_id = req.body.taxi_id;

    // get all active travels ot this taxi

    travel_dao.getTaxiActiveTravels(taxi_id, function (active_travel) {



        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

            if (active_travel) {

                if (active_travel.state == "taxi_founded") {

                    console.log("\n\n");
                    console.log(active_travel);
                    console.log("\n\n");

                    // calculate timer time

                    var taxi_found_date = moment(active_travel.taxi_found_date, "jYYYY/jM/jD HH:mm:ss");

                    var now = moment();

                    var timer_difference_time = now.diff(taxi_found_date, "seconds");

                    var timer_time = active_travel.taxi_timer_time - timer_difference_time;

                    if (timer_time < 0) {

                        timer_time = 0;

                    }

                    // calculate timer time

                    var start_time = moment(active_travel.full_date, "jYYYY/jM/jD HH:mm:ss");

                    var difference_time = now.diff(start_time, "seconds");

                    var remaining_time = active_travel.time_travel - difference_time;

                    if (remaining_time < 0) {

                        remaining_time = 0;

                    }

                    var final_travel_data = active_travel.toObject();

                    final_travel_data.taxi_timer_time = timer_time;

                    final_travel_data.remaning_time = remaining_time;

                    final_travel_data.taxi_location_lat = taxi_data.location_lat;
                    final_travel_data.taxi_location_lan = taxi_data.location_lan;

                    console.log("\n\nHERE WE ARE");
                    console.log(active_travel);
                    console.log("\n\n");

                    // callback({travel_data: final_travel_data});

                    if (timer_time == 0) {

                        callback(false);

                    } else {

                        callback(final_travel_data);

                    }

                } else {

                    callback(false);

                }

            } else {

                callback(false);

            }

        });

    });

};


module.exports.taxiClosedReconnect = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var taxi_travel_state = data.travel_state;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

            if(taxi_data) {

                var current_travel_state = travel_data.state;

                travel_dao.isTravelActive(travel_id, function (is_travel_active) {

                    if (is_travel_active) {

                        socket.join(travel_id);
                    }

                    if (current_travel_state !== taxi_travel_state) {

                        if (current_travel_state == "taxi_confirmed") {

                            io.sockets.in(travel_id).emit('taxi_confirmed', {result: travel_data});

                        } else if (current_travel_state == "customer_canceled") {

                            socket.emit('customer_canceled_travel', travel_id, travel_data.taxi_id);

                        } else if (current_travel_state == "finished") {

                            socket.emit('travel_finished', travel_id, travel_data.taxi_id);

                        } else if (current_travel_state == "support_canceled") {

                            socket.emit('support_canceled_travel', travel_id, travel_data.taxi_id);

                        }
                    }

                    var now = moment();

                    var start_time = moment(travel_data.full_date, "jYYYY/jMM/jDD HH:mm:ss");

                    var taxi_found_date = moment(travel_data.taxi_found_date, "jYYYY/jMM/jDD HH:mm:ss");

                    var timer_difference_time = now.diff(taxi_found_date, "seconds");

                    var timer_time = travel_data.taxi_timer_time - timer_difference_time;

                    if (timer_time < 0) {

                        timer_time = 0;

                    }

                    var difference_time = now.diff(start_time, "seconds");

                    var remaining_time = travel_data.time_travel - difference_time;

                    if (remaining_time < 0) {

                        remaining_time = 0;

                    }

                    var final_travel_data = travel_data.toObject();

                    final_travel_data.taxi_timer_time = timer_time;

                    final_travel_data.remaning_time = remaining_time;
                    final_travel_data.taxi_location_lat = taxi_data.location_lat;
                    final_travel_data.taxi_location_lan = taxi_data.location_lan;

                    // callback({result: true, travel_data: final_travel_data});

                });


            }

        });

    });
};


module.exports.taxiSocketReconnect = function (data, socket) {

    var taxi_id = data.taxi_id;
    var socket_id = socket.id.toString();

    taxi_dao.setSocketId(taxi_id, socket_id, function (result) {
    });


};

module.exports.taxiSocketDisconnected = function (socket) {

    var socket_id = socket.id.toString();

    taxi_dao.setTaxiOfflineBySocket(socket_id);


    // taxi_dao.getTaxiInfoBySocketId(socket_id, function (taxi_data) {
    //
    //     if (taxi_data) {
    //
    //         var taxi_id = taxi_data._id;
    //
    //         taxi_dao.setTaxiOffline(taxi_id, function (result) {});
    //
    //     }
    //
    // });

};

module.exports.taxiFreeReconnect = function (data, io, socket) {

    var taxi_id = data.taxi_id;

    // get all active travels ot this taxi

    travel_dao.getFoundedTravel(taxi_id, function (active_travel) {

        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

            if (active_travel) {

              var travel_id = active_travel._id;
              var taxi_socket_id = taxi_data.socket_id;

              if (active_travel.state == "taxi_founded") {

                    // calculate timer time

                    var taxi_found_date = moment(active_travel.taxi_found_date, "jYYYY/jM/jD HH:mm:ss");

                    var now = moment();

                    var timer_difference_time = now.diff(taxi_found_date, "seconds");

                    var timer_time = active_travel.taxi_timer_time - timer_difference_time;

                    if (timer_time < 0) {

                        timer_time = 0;

                    }

                    // calculate timer time

                    var start_time = moment(active_travel.full_date, "jYYYY/jM/jD HH:mm:ss");

                    var difference_time = now.diff(start_time, "seconds");

                    var remaining_time = active_travel.time_travel - difference_time;

                    if (remaining_time < 0) {

                        remaining_time = 0;

                    }

                    var final_travel_data = active_travel.toObject();

                    final_travel_data.taxi_timer_time = timer_time;

                    final_travel_data.remaning_time = remaining_time;

                    final_travel_data.taxi_location_lat = taxi_data.location_lat;
                    final_travel_data.taxi_location_lan = taxi_data.location_lan;

                    // callback({travel_data: final_travel_data});

                    if (timer_time == 0) {


                    } else {

                        // socket.emit('new-travel', final_travel_data);

                      travel_dao.setGetTravel(travel_id, false, function (set_get_travel_result) {

                        if(io.of('/').connected[taxi_socket_id.toString()]) {

                          io.of('/').connected[taxi_socket_id.toString()].emit('new-travel', final_travel_data, function (is_taxi_get_travel) {

                            // io.to(taxi_socket_id.toString()).emit('new-travel', travel_data_final, function (is_taxi_get_travel) {

                            if (is_taxi_get_travel) {

                              console.log("FREE RECONNECT");

                              console.log("\nTAXI GETS TRAVEL\n");

                              travel_dao.setGetTravel(travel_id, true, function (result) {});

                            }

                          });

                        }


                      });

                      // new travel after 3 second

                      var count = 0;

                      var newTravelInterval = setInterval((function () {

                        count = count + 4;

                        console.log("COUNT IS: " + count);

                        travel_dao.getTravelInfo(travel_id, function (travel_data) {

                          var state = travel_data.state;
                          var taxi_id = travel_data.taxi_id;

                          if (state == "taxi_founded" && !travel_data.is_get_travel && count <= 50) {

                            console.log("\nNOT ACKED YET!\n ");

                            taxi_dao.getTaxiInfo(taxi_id, function (current_taxi_data) {

                              final_travel_data.taxi_timer_time = final_travel_data.taxi_timer_time - count;

                              console.log("\nTHIS IS HERE(COUNT):\n " + count);

                              if(io.of('/').connected[taxi_socket_id.toString()]) {


                                io.of('/').connected[current_taxi_data.socket_id.toString()].emit('new-travel', final_travel_data, function (is_taxi_get_travel) {

                                  // io.to(current_taxi_data.socket_id.toString()).emit('new-travel', travel_data_final, function (is_taxi_get_travel) {

                                  if (is_taxi_get_travel) {

                                    console.log("\nTAXI ACK IS COME :)!\n ");

                                    console.log("FREE RECONNECT")

                                    travel_dao.setGetTravel(travel_id, true, function (result) {
                                    });

                                  }


                                });

                              }

                            });

                          } else {

                            console.log("\nWE STOP INTERVAL\n");

                            console.log("FREE RECONNECT");

                            console.log("\nTAXI GETS TRAVEL\n");

                            clearInterval(newTravelInterval);

                          }


                        })

                      }), 4000);

                    }

                }

            }

        });

    });

};

module.exports.setTaxiFree = function (taxi_id, callback) {

    taxi_dao.setTaxiFree(taxi_id, function (result) {

        callback(result);

    });

};

// upload blog image

function uploadTaxiImage(image, cb) {

    // Regular expression for image type:
    // This regular image extracts the "jpeg" from "image/jpeg"

    var imageTypeRegularExpression = /\/(.*?)$/;

    // Generate random string

    var seed = crypto.randomBytes(20);
    var uniqueSHA1String = crypto
        .createHash('sha256')
        .update(seed)
        .digest('hex');

    var ImageEncodedString = image;
    var imageBuffer = base64.decodeBase64Image(ImageEncodedString);
    var userUploadedLocation = 'public/images/';

    var uniqueRandomImageName = 'image-' + uniqueSHA1String;

    // This variable is actually an array which has 5 values,
    // The [1] value is the real image extension

    var imageTypeDetected = imageBuffer.type.match(imageTypeRegularExpression);

    var userUploadedImagePath = userUploadedLocation + uniqueRandomImageName + '.' + imageTypeDetected[1];
    var returnUserAddress = 'public/images/' + uniqueRandomImageName + '.' + imageTypeDetected[1];

    fs.writeFile(userUploadedImagePath, imageBuffer.data, function (err) {
        if (err)

            cb(0);

        else {
            console.log('File saved.');
            cb(returnUserAddress);
        }

    });
};

module.exports.changeTaxiPassword = function (req, callback) {

    var taxi_id = req.body.taxi_id;
    var password = req.body.password;
    var re_password = req.body.re_password;

    if (password == re_password) {

        var hashed_password = passwordHash.generate(password);

        taxi_dao.changeTaxiPassword(taxi_id, hashed_password, function (result) {

            taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                smsHandler.sendDriverChangePasswordMessage(taxi_data.driver_phone_number, taxi_data.driver_name, taxi_data.username, password, function (result) {
                });

                callback(result);

            });

        });

    } else {

        callback(false);

    }

};

// get list of taxi comments

module.exports.getTaxiComments = function (req, callback) {

    var taxi_id = req.params.taxi_id;
    var page = req.params.page;
    var per_page = req.params.per_page;

    comment_dao.getTaxiComments(taxi_id, page, per_page, function (comments) {

        if (comments) {

            // console.log("We Are Here 2");
            // console.log(comments);

            // if (comments.length > 0) {

            // console.log("We Are Here 1");

            callback(comments);

            // for (var i = 0; i < comments.length; i++) {
            //
            //   (function (cntr) {
            //
            //     // get comment data
            //
            //     comments[cntr] = comments[i].toObject();
            //     comments[cntr].comments_text = [];
            //     comments[cntr].star = 0;
            //     var current_id = i;
            //
            //     // console.log("We Are Here");
            //     // console.log(comments[i]);
            //
            //     for (var j = 0; j < comments[i].comment_ids.length; j++) {
            //
            //       (function (cntr_j) {
            //
            //         comment_dao.getCommentData(comments[cntr].comment_ids[cntr_j], function (comment_data) {
            //
            //           if (comment_data) {
            //
            //             comments[cntr].star = comment_data.star;
            //
            //             comments[cntr].comments_text.push(comment_data);
            //
            //             if(comments[cntr].comment_ids) {
            //
            //                 if (cntr_j == comments[cntr].comment_ids.length - 1 && cntr == comments.length - 1) {
            //
            //                     callback(comments);
            //
            //                 }
            //             }
            //
            //               if (cntr == comments.length - 1) {
            //
            //                   callback(comments);
            //
            //               }
            //
            //
            //
            //           }
            //
            //         });
            //
            //       })(j);
            //
            //     }
            //
            //   })(i);
            //
            // }

            // } else {

            // callback(comments);

            // }

        }

    });

};


// getting driver avatar path

module.exports.getTaxiDriverAvatar = function (req, callback) {

    var taxi_id = req.body.taxi_id;

    taxi_dao.getTaxiDriverAvatar(taxi_id, function (driver_avatar) {

        if (driver_avatar) {

            callback({result: true, avatar: driver_avatar});

        } else {

            callback({result: false, avatar: ""});

        }

    })

};

module.exports.getTaxiOnlineStats = function (req, callback) {

    var taxi_id = req.params.taxi_id;
    if (!taxi_id)
        taxi_id = req.body.taxi_id;
    var result = {
        online_dates: [],
        online_stats: []
    };

    taxi_dao.getTaxiOnlineDates(taxi_id, function (online_dates) {

        if (online_dates.length > 0) {

            for (var i = 0; i < online_dates.length; i++) {

                if (result.online_dates.indexOf(online_dates[i].date) == -1)
                    result.online_dates.push(online_dates[i].date)

            }


            if (online_dates.length > 0) {

                taxi_dao.getDateTaxiOnlineStat(taxi_id, online_dates[0].date, function (online_stats) {

                    if (online_stats.length > 0) {

                        for (var i = 0; i < online_stats.length; i++) {

                            var start_time_in_minute = Math.floor(online_stats[i].start_time / 60);
                            var finish_time_in_minute = Math.floor(online_stats[i].finish_time / 60);

                            if (i == 0) {

                                if (start_time_in_minute == 0) {

                                    result.online_stats.push([start_time_in_minute, 1]);
                                    result.online_stats.push([finish_time_in_minute, 1]);

                                } else {

                                    result.online_stats.push([0, 0]);
                                    result.online_stats.push([start_time_in_minute, 0]);

                                }

                            } else {

                                var prev_finish_time_in_minute = Math.floor(online_stats[i - 1].finish_time / 60);

                                if (prev_finish_time_in_minute <= start_time_in_minute) {

                                    result.online_stats.push([prev_finish_time_in_minute, 0]);
                                    result.online_stats.push([start_time_in_minute, 0]);

                                } else {

                                    result.online_stats.push([start_time_in_minute, 1]);
                                    result.online_stats.push([finish_time_in_minute, 1]);

                                }

                            }

                        }

                    }

                    callback(result);


                });

            } else {

                callback(result);

            }

        } else {

            callback(result);

        }

    });

};

module.exports.getDateTaxiOnlineStats = function (req, callback) {

    var taxi_id = req.body.taxi_id;
    var date = req.body.date;

    var result = {
        first_quarter: 0,
        second_quarter: 0,
        third_quarter: 0,
        fourth_quarter: 0,
    };

    taxi_dao.getDateTaxiOnlineStat(taxi_id, date, function (online_stats) {

        console.log(online_stats);

        if (online_stats) {

            if (online_stats.length > 0) {

                for (var i = 0; i < online_stats.length; i++) {

                    var start_time_in_minute = Math.floor(online_stats[i].start_time / 60);
                    var finish_time_in_minute = Math.floor(online_stats[i].finish_time / 60);

                    if (0 < finish_time_in_minute <= 360) {

                        result.first_quarter = result.first_quarter + (finish_time_in_minute - start_time_in_minute);

                    } else if (360 < finish_time_in_minute <= 720) {

                        result.second_quarter = result.second_quarter + (finish_time_in_minute - start_time_in_minute);

                    } else if (720 < finish_time_in_minute <= 1080) {

                        result.third_quarter = result.third_quarter + (finish_time_in_minute - start_time_in_minute);

                    } else if (1080 < finish_time_in_minute <= 1440) {

                        result.fourth_quarter = result.fourth_quarter + (finish_time_in_minute - start_time_in_minute);

                    }

                    var final_result = [["0-6", result.first_quarter], ["6-12", result.second_quarter], ["12-18", result.third_quarter], ["18-24", result.fourth_quarter]];

                    // if (i == 0) {
                    //
                    //     if (start_time_in_minute == 0) {
                    //
                    //         result.online_stats.push([start_time_in_minute, 1]);
                    //         result.online_stats.push([finish_time_in_minute, 1]);
                    //
                    //     } else {
                    //
                    //         result.online_stats.push([0, 0]);
                    //         result.online_stats.push([start_time_in_minute, 0]);
                    //
                    //     }
                    //
                    // } else {
                    //
                    //     var prev_finish_time_in_minute = Math.floor(online_stats[i - 1].finish_time / 60);
                    //
                    //     if (prev_finish_time_in_minute < start_time_in_minute) {
                    //
                    //         result.online_stats.push([prev_finish_time_in_minute, 0]);
                    //         result.online_stats.push([start_time_in_minute, 0]);
                    //
                    //     } else {
                    //
                    //         result.online_stats.push([start_time_in_minute, 1]);
                    //         result.online_stats.push([finish_time_in_minute, 1]);
                    //
                    //     }
                    //
                    // }

                }

                callback(final_result);

            }

        } else {

            callback(result);

        }

    });


};

module.exports.checkAppVersion = function (req, callback) {

    var customer_current_version = parseInt(req.body.current_version);

    support_dao.getSystemActivated(function (active_data) {

        if (active_data.is_active == false) {

            callback({result: "not_active", message: active_data.message});

        } else {

            support_controller.getDriverCurrentVersion(function (app_version) {

                if (app_version.version_code == customer_current_version) {

                    callback({result: "no_update", message: ""});

                } else if (app_version.version_code > customer_current_version) {

                    if (app_version.is_force_update) {

                        callback({result: "force_update", message: "", url: app_version.url});

                    } else if (app_version.last_force_version > customer_current_version) {

                        callback({result: "force_update", message: "", url: app_version.url});

                    } else {

                        callback({result: "normal_update", message: "", url: app_version.url});

                    }

                } else if (app_version.version_code < customer_current_version) {

                    callback({result: "no_update", message: "", url: "", package_name: ""});

                }

            });

        }

    });


};

module.exports.getStationTaxisListAll = function (station_id, callback) {

    taxi_dao.getStationTaxisListAll(station_id, function (taxis) {

        callback(taxis);

    });

};

module.exports.getTaxiStatByDate = function (req, callback) {

    var taxi_id = req.body.taxi_id;
    var start_date = req.body.start_date;
    var finish_date = req.body.finish_date;
    var final_result = {
        refused_travels_number: 0,
        stand_by_travels_number: 0
    };

    travel_dao.getDriverTravelsListByDate(taxi_id, start_date, finish_date, function (taxi_travels) {

        travel_dao.getDriverRefusedTravelsListByDate(taxi_id, start_date, finish_date, function (refused_travels) {

            travel_dao.getDriverStandByTravelsListByDate(taxi_id, start_date, finish_date, function (stand_by_travels) {

                // get travels number that he refused

                final_result.refused_travels_number = refused_travels.length;

                // get travels number that stand_by

                final_result.stand_by_travels_number = stand_by_travels.length;

                final_result.taxi_travels_number = taxi_travels.length;

                var travels_stat = {};
                travels_stat.all_cost = 0;
                travels_stat.all_discount = 0;
                travels_stat.support_canceled_number = 0;
                travels_stat.taxi_canceled_number = 0;
                travels_stat.customer_canceled_number = 0;
                travels_stat.finished_number = 0;
                travels_stat.finish_commented_number = 0;
                travels_stat.travel_started_number = 0;

                // calculate travels number


                // travels by state

                for (var i = 0; i < taxi_travels.length; i++) {

                    if (taxi_travels[i].state == "customer_canceled") {

                        travels_stat.customer_canceled_number = travels_stat.customer_canceled_number + 1;

                    } else if (taxi_travels[i].state == "support_canceled") {

                        travels_stat.support_canceled_number = travels_stat.support_canceled_number + 1;

                    } else if (taxi_travels[i].state == "travel_started") {

                        travels_stat.travel_started_number = travels_stat.travel_started_number + 1;

                    } else if ((taxi_travels[i].state == "finished") || (taxi_travels[i].state == "finish_commented")) {

                        travels_stat.all_cost = travels_stat.all_cost + parseInt(taxi_travels[i].cost);
                        travels_stat.all_discount = travels_stat.all_discount + parseInt(taxi_travels[i].discount_amount);

                        if (taxi_travels[i].state == "finished")
                            travels_stat.finished_number = travels_stat.finished_number + 1;

                        if (taxi_travels[i].state == "finish_commented")
                            travels_stat.finish_commented_number = travels_stat.finish_commented_number + 1;

                    } else if (taxi_travels[i].state == "taxi_canceled") {

                        travels_stat.taxi_canceled_number = travels_stat.taxi_canceled_number + 1;

                    }

                }

                final_result.travels_stat = travels_stat;

                callback(final_result);

            });


        });

    });

}

module.exports.setTaxiInfo = function (req, callback) {

    var taxi_id = req.body.taxi_id;
    var account_number = req.body.account_number;
    var driver_name = req.body.driver_name;

    taxi_dao.setTaxiInfo(taxi_id, account_number, driver_name, function (result) {

        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

            callback(taxi_data);

        });

    })

}

module.exports.checkIosAppVersion = function (req, callback) {

    var customer_current_version = parseInt(req.body.current_version);

    support_dao.getSystemActivated(function (active_data) {

        if (active_data.is_active == false) {

            callback({result: "not_active", message: active_data.message});

        } else {

            support_controller.getIosDriverCurrentVersion(function (app_version) {

                if (app_version.version_code == customer_current_version) {

                    callback({result: "no_update", message: ""});

                } else if (app_version.version_code > customer_current_version) {

                    if (app_version.is_force_update) {

                        callback({result: "force_update", message: "", url: app_version.url});

                    } else if (app_version.last_force_version > customer_current_version) {

                        callback({result: "force_update", message: "", url: app_version.url});

                    } else {

                        callback({result: "normal_update", message: "", url: app_version.url});

                    }

                } else if (app_version.version_code < customer_current_version) {

                    callback({result: "no_update", message: ""});

                }

            });

        }

    });


};

module.exports.getOpenAppInfo = function (req, callback) {

    var taxi_id = req.body.taxi_id;

    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

        support_controller.getSupportPhone(function (support_phone) {

            // support_controller.getDriverBanner(function (banners) {

                // support_controller.getDriverTelegramLink(function (telegram_link) {

                    var full_date_now = moment().format('jYYYY/jMM/jDD HH:mm');

                    var data = {
                        credit: 0
                    };

                    turnover_dao.getLastTaxiPony(taxi_id, function (latest_pony) {

                        if (latest_pony.length === 1) {

                            var latest_pony_time = latest_pony[0].full_date;

                            turnover_dao.getTaxiTurnOverListFullDate(taxi_id, latest_pony_time, full_date_now, function (all_turnovers) {

                                for (var i = 0; i < all_turnovers.length; i++) {

                                    if (all_turnovers[i].turnover_type == "debtor") {

                                        data.credit = data.credit - all_turnovers[i].amount;

                                    } else if (all_turnovers[i].turnover_type == "creditor") {

                                        data.credit = data.credit + all_turnovers[i].amount;

                                    }

                                }

                                var taxi_data_final = taxi_data.toObject();
                                taxi_data_final.credit = data.credit;

                                console.log("TACXI OPEN APP");
                                console.log(taxi_data_final);
                                console.log(data);

                                var final_data = {
                                    taxi_data: taxi_data_final,
                                    support_phone: support_phone,
                                    // banners: banners,
                                    // telegram_link: telegram_link
                                };

                                callback(final_data);


                            });

                        } else {

                            var latest_pony_time = "0000/00/00 00:00";

                            turnover_dao.getTaxiTurnOverListFullDate(taxi_id, latest_pony_time, full_date_now, function (all_turnovers) {

                                for (var i = 0; i < all_turnovers.length; i++) {

                                    if (all_turnovers[i].turnover_type == "debtor") {

                                        data.credit = data.credit - all_turnovers[i].amount;

                                    } else if (all_turnovers[i].turnover_type == "creditor") {

                                        data.credit = data.credit + all_turnovers[i].amount;

                                    }

                                }

                                var taxi_data_final = taxi_data.toObject();
                                taxi_data_final.credit = data.credit;

                                var final_data = {
                                    taxi_data: taxi_data_final,
                                    support_phone: support_phone
                                    // banners: banners,
                                    // telegram_link: telegram_link
                                };

                                callback(final_data);

                            });

                        }

                    });


                // });

            // });

        })

    })

};


module.exports.updateAccountInfo = function (req, callback) {

    var taxi_id = req.body.taxi_id;
    var account_number = req.body.account_number;
    var account_name = req.body.account_name;
    var account_bank = req.body.account_bank;

    taxi_dao.updateAccountInfo(taxi_id, account_number, account_name, account_bank, function (result) {

        callback(result);

    })

}

module.exports.getTaxiIncomeInfo = function (req, callback) {


    if (req.body.taxi_id) {

        var taxi_id = req.body.taxi_id;

    } else {

        var taxi_id = req.params.taxi_id;

    }
    var now = moment().format('jYYYY/jMM/jDD');
    var full_date_now = moment().format('jYYYY/jMM/jDD HH:mm');
    var start_week = moment().startOf('week').format('jYYYY/jMM/jDD');
    var start_day = moment().startOf('day').format('jYYYY/jMM/jDD');
    var start_month = moment().startOf('jMonth').format('jYYYY/jMM/jDD');

    var data = {
        today_income: 0,
        week_income: 0,
        month_week: 0,
        credit: 0,
        travels_number: 0,
        travels_requested: 0,
        cash_work: 0,
        credit_work: 0,
        commission_amount: 0
    };

    turnover_dao.getLastTaxiPony(taxi_id, function (latest_pony) {

        if (latest_pony.length === 1) {

            // console.log(all_turnovers.length);

            var latest_pony_time = latest_pony[0].full_date;

            turnover_dao.getTaxiTurnOverListFullDate(taxi_id, latest_pony_time, full_date_now, function (all_turnovers) {

                console.log(all_turnovers.length);

                for (var i = 0; i < all_turnovers.length; i++) {

                    if (all_turnovers[i].turnover_type == "debtor") {

                        data.credit = data.credit - all_turnovers[i].amount;
                        data.cash_work = parseInt(data.cash_work + ((all_turnovers[i].amount * 100) / 13));
                        data.commission_amount = parseInt(data.commission_amount + all_turnovers[i].amount);

                    } else if (all_turnovers[i].turnover_type == "creditor") {

                        data.credit = data.credit + all_turnovers[i].amount;
                        data.credit_work = parseInt(data.credit_work + ((all_turnovers[i].amount * 100) / 87));
                        data.commission_amount = parseInt(data.commission_amount + ((((all_turnovers[i].amount * 100) / 87) * 13) / 100));

                    }

                }

                travel_dao.getTaxiTravelsStat(taxi_id, function (travels) {

                    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                        turnover_dao.getDateTaxiTurnOverList(taxi_id, start_day, now, function (day_transactions) {

                            turnover_dao.getDateTaxiTurnOverList(taxi_id, start_week, now, function (week_transactions) {

                                turnover_dao.getDateTaxiTurnOverList(taxi_id, start_month, now, function (month_transactions) {

                                    for (var i = 0; i < day_transactions.length; i++) {

                                        if (day_transactions[i].turnover_type == 'income') {

                                            data.today_income += day_transactions[i].amount;

                                        }

                                    }

                                    for (var j = 0; j < week_transactions.length; j++) {

                                        if (week_transactions[j].turnover_type == 'income') {

                                            data.week_income += week_transactions[j].amount;

                                        }

                                    }

                                    for (var k = 0; k < month_transactions.length; k++) {

                                        if (month_transactions[k].turnover_type == 'income') {

                                            data.month_week += month_transactions[k].amount;

                                        }

                                    }

                                    for (var l = 0; l < travels.length; l++) {

                                        if (travels[l].state == 'finished' || travels[l].state == 'finish_commented') {

                                            data.travels_number += 1;
                                            data.travels_requested += 1;

                                        }

                                        if (travels[l].refused_taxis.indexOf(taxi_id) > -1 || travels[l].stand_by_taxis.indexOf(taxi_id) > -1) {

                                            data.travels_requested += 1;

                                        }

                                    }

                                    data.today_income = parseInt(data.today_income);
                                    data.week_income = parseInt(data.week_income);
                                    data.month_week = parseInt(data.month_week);
                                    data.credit = parseInt(data.credit);
                                    data.travels_number = parseInt(data.travels_number);
                                    data.travels_requested = parseInt(data.travels_requested);

                                    callback(data);

                                });

                            });

                        });


                    })


                });


            });

        } else {

            var latest_pony_time = "0000/00/00 00:00";

            turnover_dao.getTaxiTurnOverListFullDate(taxi_id, latest_pony_time, full_date_now, function (all_turnovers) {

                console.log(all_turnovers.length);
                console.log("ELSE PART");

                for (var i = 0; i < all_turnovers.length; i++) {

                    if (all_turnovers[i].turnover_type == "debtor") {

                        data.credit = data.credit - all_turnovers[i].amount;
                        data.cash_work = parseInt(data.cash_work + ((all_turnovers[i].amount * 100) / 13));
                        data.commission_amount = parseInt(data.commission_amount + all_turnovers[i].amount);


                    } else if (all_turnovers[i].turnover_type == "creditor") {

                        data.credit = data.credit + all_turnovers[i].amount;
                        data.credit_work = parseInt(data.credit_work + ((all_turnovers[i].amount * 100) / 87));
                        data.commission_amount = parseInt(data.commission_amount + ((((all_turnovers[i].amount * 100) / 87) * 13) / 100));

                    }

                }

                travel_dao.getTaxiTravelsStat(taxi_id, function (travels) {

                    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                        turnover_dao.getDateTaxiTurnOverList(taxi_id, start_day, now, function (day_transactions) {

                            turnover_dao.getDateTaxiTurnOverList(taxi_id, start_week, now, function (week_transactions) {

                                turnover_dao.getDateTaxiTurnOverList(taxi_id, start_month, now, function (month_transactions) {

                                    for (var i = 0; i < day_transactions.length; i++) {

                                        if (day_transactions[i].turnover_type == 'income') {

                                            data.today_income += day_transactions[i].amount;

                                        }

                                    }

                                    for (var j = 0; j < week_transactions.length; j++) {

                                        if (week_transactions[j].turnover_type == 'income') {

                                            data.week_income += week_transactions[j].amount;

                                        }

                                    }

                                    for (var k = 0; k < month_transactions.length; k++) {

                                        if (month_transactions[k].turnover_type == 'income') {

                                            data.month_week += month_transactions[k].amount;

                                        }

                                    }

                                    data.credit = taxi_data.credit;

                                    for (var l = 0; l < travels.length; l++) {

                                        if (travels[l].state == 'finished' || travels[l].state == 'finish_commented') {

                                            data.travels_number += 1;
                                            data.travels_requested += 1;

                                        }

                                        if (travels[l].refused_taxis.indexOf(taxi_id) > -1 || travels[l].stand_by_taxis.indexOf(taxi_id) > -1) {

                                            data.travels_requested += 1;

                                        }

                                    }

                                    data.today_income = parseInt(data.today_income);
                                    data.week_income = parseInt(data.week_income);
                                    data.month_week = parseInt(data.month_week);
                                    data.credit = parseInt(data.credit);
                                    data.travels_number = parseInt(data.travels_number);
                                    data.travels_requested = parseInt(data.travels_requested);

                                    callback(data);

                                });

                            });

                        });


                    })


                });


            });

        }

    });

}


module.exports.getTaxiTurnOverReport = function (req, callback) {

    var taxi_id = req.body.taxi_id;
    var now = moment().format('jYYYY/jMM/jDD HH:mm');
    var start_week = moment().startOf('week').format('jYYYY/jMM/jDD HH:mm');
    var start_day = moment().startOf('day').format('jYYYY/jMM/jDD HH:mm');
    var start_month = moment().startOf('jMonth').format('jYYYY/jMM/jDD HH:mm');

    var data = {
        today_income: 0,
        week_income: 0,
        month_week: 0,
        credit: 0,
        travels_number: 0,
        travels_requested: 0
    };

    callback(data);

    turnover_dao.getDateTaxiTurnOverList(taxi_id, start_day, now, function (day_transactions) {

        turnover_dao.getDateTaxiTurnOverList(taxi_id, start_week, now, function (week_transactions) {

            turnover_dao.getDateTaxiTurnOverList(taxi_id, start_month, now, function (month_transactions) {

                for (var i = 0; i < day_transactions.length; i++) {

                    if (day_transactions[i].turnover_type == 'income') {

                        data.today_income = +day_transactions[i].amount;

                    }

                }

                for (var j = 0; j < week_transactions.length; j++) {

                    if (week_transactions[j].turnover_type == 'income') {

                        data.week_income = +week_transactions[i].amount;

                    }

                }

                for (var k = 0; k < month_transactions.length; k++) {


                    if (month_transactions[i].turnover_type == 'income') {

                        data.month_week = +month_transactions[i].amount;

                    }

                }

                callback(data);

            });

        });

    });


};

// module.exports.getOpenAppInfo = function (req, callback) {
//
//   var taxi_id = req.body.taxi_id;
//
//   taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {
//
//     support_controller.getSupportPhone(function (support_phone) {
//
//       support_controller.getDriverBanner(function (banners) {
//
//         support_controller.getDriverTelegramLink(function (telegram_link) {
//
//           var full_date_now = moment().format('jYYYY/jMM/jDD HH:mm');
//
//           var data = {
//             credit: 0
//           };
//
//           turnover_dao.getLastTaxiPony(taxi_id, function (latest_pony) {
//
//             if(latest_pony.length === 1) {
//
//               var latest_pony_time = latest_pony[0].full_date;
//
//               turnover_dao.getTaxiTurnOverListFullDate(taxi_id, latest_pony_time, full_date_now, function (all_turnovers) {
//
//                 for(var i=0; i<all_turnovers.length; i++) {
//
//                   if(all_turnovers[i].turnover_type == "debtor") {
//
//                     data.credit = data.credit - all_turnovers[i].amount;
//
//                   } else if (all_turnovers[i].turnover_type == "creditor") {
//
//                     data.credit = data.credit + all_turnovers[i].amount;
//
//                   }
//
//                 }
//
//                 var taxi_data_final = taxi_data.toObject();
//                 taxi_data_final.credit = data.credit;
//
//                 var data = {
//                   taxi_data: taxi_data_final,
//                   support_phone: support_phone,
//                   banners: banners,
//                   telegram_link: telegram_link
//                 };
//
//                 callback(data);
//
//
//               });
//
//             } else {
//
//               var latest_pony_time = "0000/00/00 00:00";
//
//               turnover_dao.getTaxiTurnOverListFullDate(taxi_id, latest_pony_time, full_date_now, function (all_turnovers) {
//
//                 for(var i=0; i<all_turnovers.length; i++) {
//
//                   if(all_turnovers[i].turnover_type == "debtor") {
//
//                     data.credit = data.credit - all_turnovers[i].amount;
//
//                   } else if (all_turnovers[i].turnover_type == "creditor") {
//
//                     data.credit = data.credit + all_turnovers[i].amount;
//
//                   }
//
//                 }
//
//                 var taxi_data_final = taxi_data.toObject();
//                 taxi_data_final.credit = data.credit;
//
//                 var data = {
//                   taxi_data: taxi_data_final,
//                   support_phone: support_phone,
//                   banners: banners,
//                   telegram_link: telegram_link
//                 };
//
//                 callback(data);
//
//               });
//
//             }
//
//           });
//
//
//
//         });
//
//       });
//
//     })
//
//   })
//
// };

module.exports.getTaxiCredits = function (callback) {

    var final_taxis_list = [];

    taxi_dao.getTaxisListAll(function (taxis) {

        for (var j = 0; j < taxis.length; j++) {

            (function (cntr) {

                var data = {
                    credit: 0
                };
                var taxi_obj = taxis[cntr];
                var full_date_now = moment().format('jYYYY/jMM/jDD HH:mm');

                turnover_dao.getLastTaxiPony(taxi_obj._id, function (latest_pony) {

                    if (latest_pony.length === 1) {

                        var latest_pony_time = latest_pony[0].full_date;

                        turnover_dao.getTaxiTurnOverListFullDate(taxi_obj._id, latest_pony_time, full_date_now, function (all_turnovers) {

                            for (var i = 0; i < all_turnovers.length; i++) {

                                if (all_turnovers[i].turnover_type == "debtor") {

                                    data.credit = data.credit - all_turnovers[i].amount;

                                } else if (all_turnovers[i].turnover_type == "creditor") {

                                    data.credit = data.credit + all_turnovers[i].amount;

                                }

                            }

                            taxi_obj.credit = data.credit;

                            if (taxi_obj.credit < 0) {

                                taxi_obj.credit = Math.abs(taxi_obj.credit);
                                taxi_obj.is_debt = true;

                            } else {

                                taxi_obj.is_debt = false;

                            }

                            if (taxi_obj.credit !== 0)
                                final_taxis_list.push(taxi_obj);

                            if (cntr === taxis.length - 1) {
                                callback(final_taxis_list);
                            }

                        });

                    } else {

                        var latest_pony_time = "0000/00/00 00:00";

                        turnover_dao.getTaxiTurnOverListFullDate(taxi_obj._id, latest_pony_time, full_date_now, function (all_turnovers) {

                            for (var i = 0; i < all_turnovers.length; i++) {

                                if (all_turnovers[i].turnover_type == "debtor") {

                                    data.credit = data.credit - all_turnovers[i].amount;

                                } else if (all_turnovers[i].turnover_type == "creditor") {

                                    data.credit = data.credit + all_turnovers[i].amount;

                                }

                            }

                            taxi_obj.credit = data.credit;

                            if (taxi_obj.credit < 0) {

                                taxi_obj.credit = Math.abs(taxi_obj.credit);
                                taxi_obj.is_debt = true;

                            } else {

                                taxi_obj.is_debt = false;

                            }

                            if (taxi_obj.credit !== 0)
                                final_taxis_list.push(taxi_obj);

                            if (cntr === taxis.length - 1) {
                                callback(final_taxis_list);
                            }


                        });
                    }
                });

            })(j);
        }

    });

}

module.exports.getStationTaxisCredit = function (req, callback) {

    var station_id = req.params.station_id;
    var final_taxis_list = [];

    taxi_dao.getStationTaxisListAll(station_id, function (taxis) {

        for (var j = 0; j < taxis.length; j++) {

            (function (cntr) {

                var data = {
                    credit: 0
                };
                var taxi_obj = taxis[cntr];
                var full_date_now = moment().format('jYYYY/jMM/jDD HH:mm');

                turnover_dao.getLastTaxiPony(taxi_obj._id, function (latest_pony) {

                    if (latest_pony.length === 1) {

                        var latest_pony_time = latest_pony[0].full_date;

                        turnover_dao.getTaxiTurnOverListFullDate(taxi_obj._id, latest_pony_time, full_date_now, function (all_turnovers) {

                            for (var i = 0; i < all_turnovers.length; i++) {

                                if (all_turnovers[i].turnover_type == "debtor") {

                                    data.credit = data.credit - all_turnovers[i].amount;

                                } else if (all_turnovers[i].turnover_type == "creditor") {

                                    data.credit = data.credit + all_turnovers[i].amount;

                                }

                            }

                            taxi_obj.credit = data.credit;

                            if (taxi_obj.credit < 0) {

                                taxi_obj.credit = Math.abs(taxi_obj.credit);
                                taxi_obj.is_debt = true;

                            } else {

                                taxi_obj.is_debt = false;

                            }

                            if (taxi_obj.credit !== 0)
                                final_taxis_list.push(taxi_obj);

                            if (cntr === taxis.length - 1) {
                                callback(final_taxis_list);
                            }

                        });

                    } else {

                        var latest_pony_time = "0000/00/00 00:00";

                        turnover_dao.getTaxiTurnOverListFullDate(taxi_obj._id, latest_pony_time, full_date_now, function (all_turnovers) {

                            for (var i = 0; i < all_turnovers.length; i++) {

                                if (all_turnovers[i].turnover_type == "debtor") {

                                    data.credit = data.credit - all_turnovers[i].amount;

                                } else if (all_turnovers[i].turnover_type == "creditor") {

                                    data.credit = data.credit + all_turnovers[i].amount;

                                }

                            }

                            taxi_obj.credit = data.credit;

                            if (taxi_obj.credit < 0) {

                                taxi_obj.credit = Math.abs(taxi_obj.credit);
                                taxi_obj.is_debt = true;

                            } else {

                                taxi_obj.is_debt = false;

                            }

                            if (taxi_obj.credit !== 0)
                                final_taxis_list.push(taxi_obj);

                            if (cntr === taxis.length - 1) {
                                callback(final_taxis_list);
                            }


                        });
                    }
                });

            })(j);
        }

    });

}

