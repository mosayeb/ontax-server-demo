var soap = require('soap');
var moment = require('moment-jalaali');
var moment_persian = require('moment-jalaali');
var customer_controller = require('./customer_controller');
var customer_dao = require('./../dao/customer_dao');
var taxi_dao = require('./../dao/taxi_dao');
var comment_dao = require('./../dao/comment_dao');
var map_tools = require('./../tools/map_tools');
var travel_dao = require('./../dao/travel_dao');
var support_dao = require('./../dao/support_dao');
var socketController = require('./socket_controller');
var supportController = require('./support_controller');
var turnOverController = require('./turnover_controller');
var placeController = require('./place_controller');
var promotionController = require('./promotion_controller');
var promotion_dao = require('./../dao/promotion_dao');
var wallet_dao = require('./../dao/wallet_dao');
var turnover_dao = require('./../dao/turnover_dao');
var consts = require('./../config/consts');
var utils = require('./../tools/utils');
var smsHandler = require('./../handler/sms_handler');
var apnHandler = require('./../handler/apn_handler');

moment_persian.loadPersian();

module.exports.getTravelsList = function (req, callback) {

    var page = req.params.page;
    var per_page = req.params.per_page;

    travel_dao.getTravelsList(page, per_page, function (result) {

        if (result) {

            callback(result);

        }

    });

};

module.exports.taxiArrived = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var taxi_id = data.taxi_id;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data.state == "taxi_confirmed" && travel_data.taxi_id.toString() == taxi_id.toString()) {

            // smsHandler.sendTaxiArrivedSms(travel_data.customer_phone_number, function (result) {});

            customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                if (customer_data.is_apple) {

                    apnHandler.sendNotification(customer_data.device_id, "taxi_arrived");

                }

            });

            io.sockets.in(travel_id).emit('taxi_arrived', travel_id);

            callback(true);

        } else {

            callback(false);

        }
    });

};


// rate travel by customer

module.exports.travelCommented = function (req, callback) {

    if (req.body.customer_id && req.body.travel_id) {

        var customer_id = req.body.customer_id;
        var travel_id = req.body.travel_id;
        var rate = req.body.rate;

        customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

            if (customer_data) {

                travel_dao.getTravelInfo(travel_id, function (travel_data) {

                    if (travel_data && !travel_data.rate_id) {

                        comment_dao.saveRateTravel(customer_id, travel_id, travel_data.taxi_id, rate, function (rate_data) {

                            travel_dao.updateTravelRate(travel_id, rate, function (update_rate_data) {

                                if (update_rate_data) {

                                    travel_dao.updateTravelStatus(travel_id, "finish_commented", function (result) {

                                        if (result) {

                                            if (update_rate_data) {

                                                travel_dao.getTravelInfo(travel_id, function (travel_data) {

                                                    if (travel_data) {

                                                        promotionController.addMoneyToCallerAndCustomerAfterFirstTravel(travel_data, function (promotion_data) {

                                                            if (promotion_data) {

                                                                // io.sockets.in(travel_id).emit('promotion_caller_gift', consts.CALLER_PROMOTION_GIFT);

                                                            }

                                                        });

                                                    }

                                                });

                                                map_tools.findGoogleDistanceDuration(travel_data.source_lan, travel_data.source_lat, travel_data.destination_lan, travel_data.destination_lat, function (distance, duration) {

                                                    travel_dao.setTravelDistanceDuration(travel_id, distance, duration, function (result) {
                                                    });

                                                });

                                                // rate

                                                // restaurant_dao.rateToRestaurant(restaurant_id, customer_id, rate, comment, sender, date, function (result) {
                                                //
                                                //     if(result) {

                                                comment_dao.getTaxiAvgRate(travel_data.taxi_id, function (taxi_rate) {

                                                    if (taxi_rate) {

                                                        taxi_rate = Math.round(taxi_rate * 100) / 100;

                                                        taxi_dao.updateTaxiRate(travel_data.taxi_id, taxi_rate, function (update_result) {

                                                        });

                                                    } else {


                                                    }

                                                });

                                                //     } else {
                                                //
                                                //         callback(false);
                                                //
                                                //     }
                                                //
                                                // });

                                                callback(true);
                                            }

                                        }

                                    });

                                } else {

                                    callback(false);

                                }

                            });
                        });

                    } else callback(false);
                });

            } else callback(false);

        });


    } else callback(false);

};

module.exports.travelCommentedOlder = function (req, callback) {

    if (req.body.customer_id && req.body.travel_id) {

        var customer_id = req.body.customer_id;
        var travel_id = req.body.travel_id;
        var comment_ids = req.body.comment_ids;
        var rate = req.body.rate;

        customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

            if (customer_data) {

                travel_dao.getTravelInfo(travel_id, function (travel_data) {

                    if (travel_data && !travel_data.rate_id) {

                        comment_dao.saveRateTravel(customer_id, travel_id, travel_data.taxi_id, comment_ids, function (rate_data) {

                            travel_dao.updateTravelRate(travel_id, rate_data._id, function (update_rate_data) {

                                if (update_rate_data) {

                                    travel_dao.updateTravelStatus(travel_id, "finish_commented", function (result) {

                                        if (result) {

                                            if (update_rate_data) {

                                                travel_dao.getTravelInfo(travel_id, function (travel_data) {

                                                    if (travel_data) {

                                                        promotionController.addMoneyToCallerAndCustomerAfterFirstTravel(travel_data, function (promotion_data) {

                                                            if (promotion_data) {

                                                                // io.sockets.in(travel_id).emit('promotion_caller_gift', consts.CALLER_PROMOTION_GIFT);

                                                            }

                                                        });

                                                    }

                                                });

                                                callback(true);
                                            }

                                        }

                                    });

                                } else {

                                    callback(false);

                                }

                            });
                        });

                    } else callback(false);
                });

            } else callback(false);

        });


    } else callback(false);

    if (req.body.customer_id && req.body.travel_id) {

        var customer_id = req.body.customer_id;
        var travel_id = req.body.travel_id;
        var rate = req.body.rate;

        customer_dao.getCustomerInfoById(customer_id, function (customer_data) {

            if (customer_data) {

                travel_dao.getTravelInfo(travel_id, function (travel_data) {

                    if (travel_data && !travel_data.rate_id) {

                        comment_dao.saveRateTravel(customer_id, travel_id, travel_data.taxi_id, rate, function (rate_data) {

                            travel_dao.updateTravelRate(travel_id, rate, function (update_rate_data) {

                                if (update_rate_data) {

                                    travel_dao.updateTravelStatus(travel_id, "finish_commented", function (result) {

                                        if (result) {

                                            if (update_rate_data) {

                                                travel_dao.getTravelInfo(travel_id, function (travel_data) {

                                                    if (travel_data) {

                                                        promotionController.addMoneyToCallerAndCustomerAfterFirstTravel(travel_data, function (promotion_data) {

                                                            if (promotion_data) {

                                                                // io.sockets.in(travel_id).emit('promotion_caller_gift', consts.CALLER_PROMOTION_GIFT);

                                                            }

                                                        });

                                                    }

                                                });

                                                map_tools.findGoogleDistanceDuration(travel_data.source_lan, travel_data.source_lat, travel_data.destination_lan, travel_data.destination_lat, function (distance, duration) {

                                                    travel_dao.setTravelDistanceDuration(travel_id, distance, duration, function (result) {
                                                    });

                                                });

                                                // rate

                                                // restaurant_dao.rateToRestaurant(restaurant_id, customer_id, rate, comment, sender, date, function (result) {
                                                //
                                                //     if(result) {

                                                comment_dao.getTaxiAvgRate(travel_data.taxi_id, function (taxi_rate) {

                                                    if (taxi_rate) {

                                                        taxi_rate = Math.round(taxi_rate * 100) / 100;

                                                        taxi_dao.updateTaxiRate(travel_data.taxi_id, taxi_rate, function (update_result) {

                                                        });

                                                    } else {


                                                    }

                                                });

                                                //     } else {
                                                //
                                                //         callback(false);
                                                //
                                                //     }
                                                //
                                                // });

                                                callback(true);
                                            }

                                        }

                                    });

                                } else {

                                    callback(false);

                                }

                            });
                        });

                    } else callback(false);
                });

            } else callback(false);

        });


    } else callback(false);

};

module.exports.travelFinished = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var taxi_id = data.taxi_id;
    var rate = data.rate;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data.state == "travel_started" && travel_data.taxi_id.toString() == taxi_id.toString()) {

            travel_dao.updateTravelStatus(travel_id, "finished", function (result) {

                if (result) {

                    travel_dao.deactivateTravel(travel_id, function (deactivateTravel_result) {

                        if (result) {

                            taxi_dao.setTaxiFree(taxi_id, function (result) {

                                taxi_dao.setLastFreeTime(taxi_id, function (set_free_time_result) {

                                    travel_dao.setTaxiTravelRate(travel_id, rate, function (rate_travel_result) {
                                    });

                                    if (set_free_time_result) {

                                        customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                                            if (customer_data.is_apple) {

                                                apnHandler.sendNotification(customer_data.device_id, "travel_finished");

                                            }

                                        });

                                        io.sockets.in(travel_id).emit('travel_finished', travel_id);


                                        // create turnover things

                                        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                                            taxi_dao.getServiceTypeData(taxi_data.service_type_id, function (taxi_service_data) {

                                                var commission_rate = parseInt(taxi_service_data.commission_rate);
                                                var commission_amount = travel_data.cost * (commission_rate / 100);
                                                var income = travel_data.cost - commission_amount;
                                                var income_explanation = "درآمد سفر " + travel_data.travel_code;
                                                var cash_deptor_explanation = "کسر کمیسون سفر  " + travel_data.travel_code;
                                                var cash_creditor_explanation = "افزایش اعتبار سفر  " + travel_data.travel_code;
                                                var date = moment().format('jYYYY/jMM/jDD');
                                                var time = moment().format('HH:mm');
                                                var readable_date = moment_persian().format('dddd jD jMMMM');

                                                // create turn over income

                                                turnover_dao.createTaxiTurnover(taxi_id, travel_id, income, "income", income_explanation, date, readable_date, time, function (result) {});


                                                if (travel_data.payment_type == "cash") {

                                                    turnover_dao.createTaxiTurnover(taxi_id, travel_id, commission_amount, "debtor", cash_deptor_explanation, date, readable_date, time, function (result) {
                                                    });

                                                    // decrease taxi credit

                                                    // taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function(result){});

                                                    // set travel paid

                                                    travel_dao.setTravelPaid(travel_id, function (result) {
                                                    });

                                                } else if (travel_data.payment_type == "wallet" && travel_data.is_paid) {

                                                    // add a turn over transaction

                                                    turnover_dao.createTaxiTurnover(taxi_id, travel_id, income, "creditor", cash_creditor_explanation, date, readable_date, time, function (result) {
                                                    });

                                                    // add credit to taxi

                                                    // taxi_dao.increaseTaxiCredit(taxi_id, income, function(result){});


                                                } else if (travel_data.payment_type == "wallet" && !travel_data.is_paid) {

                                                    turnover_dao.createTaxiTurnover(taxi_id, travel_id, commission_amount, "debtor", cash_deptor_explanation, date, readable_date, time, function (result) {
                                                    });

                                                    // decrease taxi credit

                                                    // taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function(result){});

                                                    // change travel payment type

                                                    travel_dao.updateTravelPaymentType(travel_id, "cash", function (result) {
                                                    });

                                                    travel_dao.setTravelPaid(travel_id, function (result) {
                                                    });

                                                }

                                                setTimeout(
                                                    function() {

                                                        handleTravelTurnOvers(travel_id, taxi_id, income, commission_amount, function (result) {});

                                                    }, 3000);


                                            });

                                        });

                                        // callback(true);

                                        // socketController.emptyRoom(io, travel_id);
                                        //
                                        // travel_dao.getTravelInfo(travel_id, function (travel_data) {
                                        //
                                        //   if (travel_data) {
                                        //
                                        //     promotionController.addMoneyToCallerAndCustomerAfterFirstTravel(travel_data, function (promotion_data) {
                                        //
                                        //       if (promotion_data) {
                                        //
                                        //         io.sockets.in(travel_id).emit('promotion_caller_gift', consts.CALLER_PROMOTION_GIFT);
                                        //
                                        //       }
                                        //
                                        //     });
                                        //
                                        //   }

                                        // });

                                        callback(true);

                                    }

                                });

                            });

                        }

                    });

                }

            });

        } else {

            callback(false);

        }

    });
};

module.exports.cancelTravelByTaxi = function (data, io, socket, callback) {

    var taxi_id = data.taxi_id;
    var travel_id = data.travel_id;
    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data.state == "taxi_confirmed" && travel_data.taxi_id.toString() == taxi_id.toString()) {

            travel_dao.updateTravelStatus(travel_id, "taxi_canceled", function (result) {

                if (result) {

                    travel_dao.deactivateTravel(travel_id, function (deactivateTravel_result) {

                        if (deactivateTravel_result) {

                            smsHandler.sendTravelCanceledByTaxiSms(travel_data.customer_phone_number, travel_data.travel_code, function (result) {
                            });

                            customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                                if (customer_data.is_apple) {

                                    apnHandler.sendNotification(customer_data.device_id, "taxi_canceled_travel");

                                }

                            });

                            console.log("WE ARE BEFORE EMIT");

                            io.sockets.in(travel_id).emit('taxi_canceled_travel', travel_id);

                            taxi_dao.setTaxiFree(taxi_id, function (result) {

                                //socketController.emptyRoom(io, travel_id);

                                callback(result);

                            });

                        }

                    });

                }

            });

        } else {

            callback(false);

        }
    });

};


// taxi put off travel

module.exports.taxiPutOffTravel = function (data, io, socket, callback) {

    var taxi_id = data.taxi_id;
    var travel_id = data.travel_id;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data.state == "taxi_confirmed" && travel_data.taxi_id == taxi_id) {

            // add taxi to refused list of travel taxis

            travel_dao.addRefusedTaxi(travel_id, taxi_id, function (add_refused_taxi_result) {

                travel_dao.getTravelInfo(travel_id, function (travel_data) {

                    var travel_state = travel_data.state;

                    if (travel_state !== "travel_started" || travel_state == "taxi_confirmed") {

                        travel_dao.updateTravelStatus(travel_id, "put_off_searching", function (result) {

                            if (result) {

                                taxi_dao.setTaxiFree(taxi_id, function (result) {

                                    // customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {
                                    //
                                    //     if (customer_data.is_apple) {
                                    //
                                    //         // apnHandler.sendNotification(customer_data.device_id, "taxi_put_off_travel");
                                    //
                                    //     }
                                    //
                                    // });

                                    io.sockets.in(travel_id).emit('taxi_put_off_travel', travel_id);

                                    socket.leave(travel_id);

                                    travel_dao.updateTravelStatus(travel_id, "customer_confirmed", function (result) {

                                        if(result) {

                                            find_taxi_and_notify_two(travel_id, io, function () {});

                                        }

                                    });

                                    // socketController.emptyRoom(io, travel_id);

                                    callback(result);

                                });

                            }

                        });

                    } else {

                        travel_dao.updateTravelStatus(travel_id, "taxi_canceled", function (result) {

                            if (result) {

                                travel_dao.deactivateTravel(travel_id, function (deactivateTravel_result) {

                                    if (deactivateTravel_result) {

                                        taxi_dao.setTaxiFree(taxi_id, function (result) {

                                            customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                                                if (customer_data.is_apple) {

                                                    apnHandler.sendNotification(customer_data.device_id, "taxi_canceled_travel");

                                                }

                                            });

                                            io.sockets.in(travel_id).emit('taxi_canceled_travel', travel_id);

                                            callback(result);

                                        });

                                    }

                                });

                            }

                        });

                    }

                });

            });

        } else {

            callback(false);

        }

    });

};


module.exports.cancelTravelByCustomer = function (data, io, socket, callback) {

    var customer_id = data.customer_id;
    var travel_id = data.travel_id;

    // travel_dao.isTravelActive(travel_id, function (is_active) {
    //
    //   if (is_active) {

    travel_dao.getTravelInfo(travel_id, function (travel_data_primary) {

        var latest_state = travel_data_primary.state;

        if (latest_state == "taxi_founded" || latest_state == "searching" || latest_state == "put_off_searching" || latest_state == "taxi_confirmed") {

            if (travel_data_primary.state == "taxi_founded") {

                taxi_dao.getTaxiInfo(travel_data_primary.taxi_id, function (taxi_data) {

                    travel_dao.deactivateTravel(travel_id, function (deactivateTravel_result) {

                        if (deactivateTravel_result) {

                            travel_dao.getTravelInfo(travel_id, function (travel_data) {

                                if (travel_data) {

                                    if (travel_data.taxi_id) {

                                        taxi_dao.setTaxiFree(travel_data.taxi_id, function (result) {
                                        });

                                        taxi_dao.setLastFreeTime(travel_data.taxi_id, function (set_free_time_result) {
                                        });


                                        travel_dao.updateTravelStatus(travel_id, "customer_canceled", function (result) {

                                            if (result) {

                                                if (taxi_data.is_apple) {

                                                    apnHandler.sendTaxiNotification(taxi_data.device_id, "customer_canceled_travel");

                                                }

                                                io.to(taxi_data.socket_id.toString()).emit('customer_canceled_travel', travel_id, travel_data_primary.taxi_id);

                                                smsHandler.sendCustomerCancelTravel(taxi_data.driver_phone_number, travel_data.travel_code, function (result) {
                                                })

                                                callback(true);

                                            }
                                        });

                                    }

                                }

                            });
                        }
                    });

                })

            } else if (travel_data_primary.state == "taxi_not_found") {

                callback(true);

            } else {

                taxi_dao.getTaxiInfo(travel_data_primary.taxi_id, function (taxi_data) {

                    console.log("WE ARE HERE 1");

                    travel_dao.updateTravelStatus(travel_id, "customer_canceled", function (result) {

                        console.log("WE ARE HERE 2");

                        if (result) {

                            travel_dao.deactivateTravel(travel_id, function (deactivateTravel_result) {

                                console.log("WE ARE HERE 3");

                                if (deactivateTravel_result) {

                                    travel_dao.getTravelInfo(travel_id, function (travel_data) {

                                        if (travel_data) {

                                            if (travel_data.taxi_id) {

                                                taxi_dao.setTaxiFree(travel_data.taxi_id, function (result) {
                                                });

                                                taxi_dao.setLastFreeTime(travel_data.taxi_id, function (set_free_time_result) {
                                                });

                                                io.sockets.in(travel_id).emit('customer_canceled_travel', travel_id, travel_data_primary.taxi_id);

                                                io.to(taxi_data.socket_id.toString()).emit('customer_canceled_travel', travel_id, travel_data_primary.taxi_id);

                                                smsHandler.sendCustomerCancelTravel(taxi_data.driver_phone_number, travel_data.travel_code, function (result) {
                                                })

                                                callback(true);

                                            }

                                        }

                                    });
                                }
                            });
                        }

                    });

                })

            }

        } else {

            callback(false);

        }

    });

    //   }
    // });

};


module.exports.cancelTravelBySupport = function (data, io, socket, callback) {

    var travel_id = data.travel_id;

    // this is prev

    travel_dao.getTravelInfo(travel_id, function(travel_data){

        var travel_state = travel_data.state;

        if(

            travel_state === "travel_started" ||
            travel_state === "taxi_confirmed" ||
            travel_state === "searching" ||
            travel_state === "put_off_searching" ||
            travel_state === "taxi_founded"

        ) {

            if (travel_data.taxi_id) {

                taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

                    taxi_dao.setTaxiFree(travel_data.taxi_id, function (result) {});

                    smsHandler.sendTravelCanceledBySupportSms(travel_data.customer_phone_number, travel_data.travel_code, function (result) {});

                    if (taxi_data.is_apple) {

                        apnHandler.sendTaxiNotification(taxi_data.device_id, "support_canceled_travel");

                    }

                    io.sockets.in(travel_id).emit('support_canceled_travel', travel_id, travel_data.taxi_id);

                    if (travel_data.state == 'taxi_founded')
                        io.to(taxi_data.socket_id.toString()).emit('support_canceled_travel', travel_id, travel_data.taxi_id);

                    travel_dao.updateTravelStatus(travel_id, "support_canceled", function (result) {

                        if (result) {

                            // deactivate travel

                            travel_dao.deactivateTravel(travel_id, function (deactivateTravel_result) {

                                callback(true);

                            });

                        }

                    });

                });

            }


        } else {

        }

    });

    // travel_dao.isTravelActive(travel_id, function (is_active) {
    //
    //   if (is_active) {
    //
    //     travel_dao.deactivateTravel(travel_id, function (deactivateTravel_result) {
    //
    //       if (deactivateTravel_result) {
    //
    //         travel_dao.getTravelInfo(travel_id, function (travel_data) {
    //
    //           if (travel_data) {
    //
    //             if (travel_data.taxi_id) {
    //
    //               taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {
    //
    //                 taxi_dao.setTaxiFree(travel_data.taxi_id, function (result) {});
    //
    //                 smsHandler.sendTravelCanceledBySupportSms(travel_data.customer_phone_number, travel_data.travel_code, function (result) {
    //                 });
    //
    //                 if (taxi_data.is_apple) {
    //
    //                   apnHandler.sendTaxiNotification(taxi_data.device_id, "support_canceled_travel");
    //
    //                 }
    //
    //                 io.sockets.in(travel_id).emit('support_canceled_travel', travel_id);
    //
    //                 if (travel_data.state == 'taxi_founded')
    //                   io.to(taxi_data.socket_id.toString()).emit('support_canceled_travel', travel_id);
    //
    //                 travel_dao.updateTravelStatus(travel_id, "support_canceled", function (result) {
    //
    //                   if (result) {
    //
    //                     //socketController.emptyRoom(io, travel_id);
    //
    //
    //
    //                     callback(true);
    //                   }
    //
    //                 });
    //
    //               });
    //
    //             }
    //
    //           }
    //
    //         });
    //       }
    //     });
    //   }
    //
    // });

};

module.exports.customerConfirmedTravel = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var driver_id = data.driver_id;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        travel_dao.updateTravelStatus(travel_id, "customer_confirmed", function (result) {

            if (result) {

                travel_dao.activateTravel(travel_id, function (activateTravel_result) {

                    if (activateTravel_result) {

                        socket.join(travel_id);

                        support_dao.getSupportSocketInfo(function (socket_data) {

                            if (socket_data) {

                                io.to(socket_data.socket_id.toString()).emit('new-travel', {travel_id: travel_id});

                            }

                        });

                        // find taxi and notify to it

                        if (driver_id) {

                            notify_taxi_travel(travel_id, driver_id, io, function (result) {

                                callback({result: {result: result}});

                            });
                        }
                        else {

                            find_taxi_and_notify(travel_id, io, function (result) {});

                        }

                        callback({result: {result: true}});

                    } else {

                        callback({result: {result: false}});

                    }
                });

            } else {

                callback({result: {result: false}});

            }

        });

    });

};

// module.exports.supportConfirmedTravel = function (data, io, socket, callback) {
//
//   var travel_id = data.travel_id;
//   var taxi_id = data.taxi_id;
//
//   travel_dao.updateTravelStatus(travel_id, "support_confirmed", function (result) {
//
//     if (result) {
//
//       socket.join(travel_id);
//
//       taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {
//
//         //if(taxi_data.is_online) {
//
//         travel_dao.setTravelTaxi(travel_id, taxi_id, function (result) {
//
//           taxi_dao.setTaxiBusy(taxi_id, function (set_taxi_busy_result) {
//
//             if (set_taxi_busy_result) {
//
//               if (result) {
//
//                 travel_dao.getTravelInfo(travel_id, function (travel_data) {
//
//                   io.to(taxi_data.socket_id.toString()).emit('new-travel', travel_data);
//
//                   io.sockets.in(travel_id).emit('support_confirmed', {result: travel_data});
//
//                   callback(true);
//
//                 });
//
//               }
//
//             }
//
//           });
//
//
//         });
//
//         //} else {
//         //
//         //    io.sockets.in(travel_id).emit('support_canceled_travel', travel_id);
//         //
//         //}
//
//       });
//
//     } else {
//
//       callback(false);
//
//     }
//
//   });
// };



module.exports.taxiRefusedTravel = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var taxi_id = data.taxi_id;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data.state == "taxi_founded" && travel_data.taxi_id.toString() == taxi_id.toString()) {

            travel_dao.updateTravelStatus(travel_id, "taxi_refused", function (taxi_refused_result) {

                // add taxi to refused list of travel taxis

                travel_dao.addRefusedTaxi(travel_id, taxi_id, function (add_refused_taxi_result) {

                    if (add_refused_taxi_result) {

                        // set taxi free

                        taxi_dao.setTaxiFree(taxi_id, function (set_taxi_free_result) {

                            if (set_taxi_free_result) {

                                socket.leave(travel_id);

                                travel_dao.updateTravelStatus(travel_id, "customer_confirmed", function (result) {

                                    find_taxi_and_notify_two(travel_id, io, function (result) {});


                                });

                                callback(true);

                            }

                        });

                    }

                });

            });
        } else {

            callback(false);

        }

    });

};


module.exports.taxiConfirmedTravel = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var taxi_id = data.taxi_id;

    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

        travel_dao.getTravelInfo(travel_id, function(travel_data) {

            if(travel_data.state == "taxi_founded" && travel_data.taxi_id.toString() == taxi_id.toString()) {

                socket.join(travel_id);

                travel_dao.setTravelTaxi(travel_id, taxi_id, taxi_data.station_id, taxi_data.station_title, function (set_taxi_result) {

                    if (set_taxi_result) {

                        // set taxi busy

                        taxi_dao.setTaxiBusy(taxi_id, function (set_taxi_busy_result) {

                            if (set_taxi_result) {

                                travel_dao.updateTravelStatus(travel_id, "taxi_confirmed", function (result) {

                                    if (result) {

                                        travel_dao.setTravelFullDate(travel_id, function (update_full_date_result) {

                                            if (update_full_date_result) {

                                                taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                                                    // emit to user that taxi is coming

                                                    travel_dao.getTravelInfo(travel_id, function (travel_data) {

                                                        if (travel_data) {

                                                            var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                                            var now = moment();

                                                            var difference_time = now.diff(start_time, "seconds");

                                                            var remaining_time = travel_data.time_travel - difference_time;

                                                            if (remaining_time < 0) {

                                                                remaining_time = 0;

                                                            }

                                                            var final_travel_data = travel_data.toObject();
                                                            final_travel_data.remaning_time = remaining_time;

                                                            var data = {
                                                                travel_data: final_travel_data,
                                                                taxi_data: taxi_data
                                                            };

                                                            smsHandler.sendTaxiFoundedSms(travel_data.customer_phone_number, travel_data.travel_code, taxi_data.driver_name, taxi_data.driver_phone_number, taxi_data.model, function (result) {
                                                            });

                                                            customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                                                                if (customer_data.is_apple) {

                                                                    apnHandler.sendNotification(customer_data.device_id, "taxi_confirmed");

                                                                }

                                                            });

                                                            io.sockets.in(travel_id).emit('taxi_confirmed', {result: data});

                                                            callback(true);

                                                        }

                                                    });

                                                });

                                            }

                                        });

                                    }

                                });

                            }


                        });

                    }


                });

            } else {

                callback(false);

            }

        });

    });
};

// taxi start travel

module.exports.taxiStartTravel = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var taxi_id = data.taxi_id;

    travel_dao.getTravelInfo(travel_id, function(travel_data){

        if(travel_data.state == "taxi_confirmed" && travel_data.taxi_id.toString() === taxi_id.toString()) {

            socket.join(travel_id);

            // update travel state

            travel_dao.updateTravelStatus(travel_id, "travel_started", function (travel_started_result) {

                if (travel_started_result) {

                    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                        // emit to user that travel is started

                        travel_dao.getTravelInfo(travel_id, function (travel_data) {

                            if (travel_data) {

                                wallet_dao.getWalletInfoByCustomerId(travel_data.customer_id, function (wallet_data) {

                                    if ((travel_data.payment_type == 'wallet') && !travel_data.is_paid && wallet_data) {

                                        if (travel_data.discount_amount != 0) {

                                            promotion_dao.getDiscountInfoByCode(travel_data.discount_code, function (discount_data) {

                                                // calculate discount and decrease money

                                                var discount_amount = travel_data.cost * (discount_data.amount / 100);

                                                if (discount_amount >= discount_data.max_cost)
                                                    discount_amount = discount_data.max_cost;

                                                discount_amount = Math.ceil(discount_amount / 100) * 100;

                                                var final_cost = travel_data.cost - discount_amount;

                                                if ((wallet_data.money >= final_cost)) {

                                                    wallet_dao.decrementWalletMoney(wallet_data._id, final_cost, function (update_wallet_data) {

                                                        travel_dao.updateIsPaid(travel_id, true, function (update_is_paid) {

                                                            var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                                            var now = moment();

                                                            var difference_time = now.diff(start_time, "seconds");

                                                            var remaining_time = travel_data.time_travel - difference_time;

                                                            if (remaining_time < 0) {

                                                                remaining_time = 0;

                                                            }

                                                            var final_travel_data = travel_data.toObject();
                                                            final_travel_data.remaning_time = remaining_time;
                                                            final_travel_data.is_paid = true;

                                                            var data = {
                                                                travel_data: final_travel_data,
                                                                taxi_data: taxi_data
                                                            };

                                                            customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                                                                if (customer_data.is_apple) {

                                                                    apnHandler.sendNotification(customer_data.device_id, "travel_started");

                                                                }

                                                            });

                                                            // save turn over customer

                                                            var date = moment().format('jYYYY/jMM/jDD');
                                                            var time = moment().format('HH:mm');

                                                            turnover_dao.createCustomerTurnover(travel_data.customer_id, travel_data.cost, "debtor", consts.TRAVEL_COST_DECREASE_EXP, date, time, function (result) {
                                                            });

                                                            // add money to driver

                                                            io.sockets.in(travel_id).emit('travel_started', {result: data});

                                                            smsHandler.sendDriverTravelPaidSms(taxi_data.driver_phone_number, travel_data.travel_code, travel_data.cost, function(result){});

                                                            callback(true);

                                                        });
                                                    });

                                                } else {

                                                    var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                                    var now = moment();

                                                    var difference_time = now.diff(start_time, "seconds");

                                                    var remaining_time = travel_data.time_travel - difference_time;

                                                    if (remaining_time < 0) {

                                                        remaining_time = 0;

                                                    }

                                                    var final_travel_data = travel_data.toObject();
                                                    final_travel_data.remaning_time = remaining_time;

                                                    var data = {
                                                        travel_data: final_travel_data,
                                                        taxi_data: taxi_data
                                                    };

                                                    customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                                                        if (customer_data.is_apple) {

                                                            apnHandler.sendNotification(customer_data.device_id, "travel_started");

                                                        }

                                                    });

                                                    io.sockets.in(travel_id).emit('travel_started', {result: data});

                                                    callback(true);
                                                }

                                            });

                                        } else {

                                            // decrease money without any discount

                                            if (wallet_data.money >= travel_data.cost) {

                                                wallet_dao.decrementWalletMoney(wallet_data._id, travel_data.cost, function (update_wallet_data) {

                                                    travel_dao.updateIsPaid(travel_id, true, function (update_is_paid) {

                                                        var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                                        var now = moment();

                                                        var difference_time = now.diff(start_time, "seconds");

                                                        var remaining_time = travel_data.time_travel - difference_time;

                                                        if (remaining_time < 0) {

                                                            remaining_time = 0;

                                                        }

                                                        var final_travel_data = travel_data.toObject();
                                                        final_travel_data.remaning_time = remaining_time;
                                                        final_travel_data.is_paid = true;

                                                        var data = {
                                                            travel_data: final_travel_data,
                                                            taxi_data: taxi_data
                                                        };

                                                        customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                                                            if (customer_data.is_apple) {

                                                                apnHandler.sendNotification(customer_data.device_id, "travel_started");

                                                            }

                                                        });

                                                        var date = moment().format('jYYYY/jMM/jDD');
                                                        var time = moment().format('HH:mm');

                                                        turnover_dao.createCustomerTurnover(travel_data.customer_id, travel_data.cost, "debtor", consts.TRAVEL_COST_DECREASE_EXP, date, time, function (result) {
                                                        });

                                                        smsHandler.sendDriverTravelPaidSms(taxi_data.driver_phone_number, travel_data.travel_code, travel_data.cost, function(result){});

                                                        io.sockets.in(travel_id).emit('travel_started', {result: data});

                                                        callback(true);
                                                    });
                                                });

                                            } else {

                                                // can not pay

                                                var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                                var now = moment();

                                                var difference_time = now.diff(start_time, "seconds");

                                                var remaining_time = travel_data.time_travel - difference_time;

                                                if (remaining_time < 0) {

                                                    remaining_time = 0;

                                                }

                                                var final_travel_data = travel_data.toObject();
                                                final_travel_data.remaning_time = remaining_time;

                                                var data = {
                                                    travel_data: final_travel_data,
                                                    taxi_data: taxi_data
                                                };

                                                customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                                                    if (customer_data.is_apple) {

                                                        apnHandler.sendNotification(customer_data.device_id, "travel_started");

                                                    }

                                                });

                                                io.sockets.in(travel_id).emit('travel_started', {result: data});

                                                callback(true);
                                            }
                                        }

                                    } else {

                                        var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                        var now = moment();

                                        var difference_time = now.diff(start_time, "seconds");

                                        var remaining_time = travel_data.time_travel - difference_time;

                                        if (remaining_time < 0) {

                                            remaining_time = 0;

                                        }

                                        var final_travel_data = travel_data.toObject();
                                        final_travel_data.remaning_time = remaining_time;

                                        var data = {
                                            travel_data: final_travel_data,
                                            taxi_data: taxi_data
                                        };

                                        customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                                            if (customer_data.is_apple) {

                                                apnHandler.sendNotification(customer_data.device_id, "travel_started");

                                            }

                                        });

                                        io.sockets.in(travel_id).emit('travel_started', {result: data});

                                        callback(true);

                                    }
                                });

                            }

                        });

                    });

                }
            });

        } else {

            callback(false);

        }

    });

};

module.exports.setTravelTaxi = function (req, callback) {

    var travel_id = req.body.travel_id;
    var taxi_id = req.body.taxi_id;

    taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

        travel_dao.setTravelTaxi(travel_id, taxi_id, taxi_data.station_id, taxi_data.station_title, function (result) {

            if (result) {

                callback(true);

            }

        });

    })


};

module.exports.getTravelsListByFilter = function (req, callback) {

    if (req.body.travel_state) {

        var travel_state = req.body.travel_state;
        var page = 1;
        var per_page = 10;

    } else {

        var travel_state = req.params.travel_state;
        var page = req.params.page;
        var per_page = req.params.per_page;


    }

    travel_dao.getTravelsByFilter(travel_state, page, per_page, function (travels_list) {

        if (travels_list) {
            callback(travels_list);
        }

    });

};

module.exports.getStationTravelsListByFilter = function (req, callback) {

    var travel_state = req.body.travel_state;
    var station_id = req.body.station_id;
    var page = req.body.page;
    var per_page = req.body.per_page;

    travel_dao.getStationTravelsByFilter(station_id, travel_state, page, per_page, function (travels_list) {

        if (travels_list) {
            callback(travels_list);
        }

    });

};

module.exports.getTravelsListByDate = function (req, callback) {

    var start_date = req.params.start_date;
    var finish_date = req.params.finish_date;
    start_date = utils.replaceDate(start_date, "_", "/");
    finish_date = utils.replaceDate(finish_date, "_", "/");

    travel_dao.getTravelsListByDate(start_date, finish_date, function (travels) {

        callback(travels);

    });

};

module.exports.getStationTravelsListByDate = function (req, callback) {

    var start_date = req.params.start_date;
    var finish_date = req.params.finish_date;
    var station_id = req.user.station_id;
    start_date = utils.replaceDate(start_date, "_", "/");
    finish_date = utils.replaceDate(finish_date, "_", "/");

    travel_dao.getStationTravelsListByDate(station_id, start_date, finish_date, function (travels) {

        callback(travels);

    });

};

module.exports.createStationTravel = function (req, callback) {
    var station_id = req.body.station_id;
    var customer_name = req.body.customer_name;
    var customer_id = req.body.customer_id;
    var phone_number = req.body.phone_number;
    var caller_number = req.body.caller_number;
    var source_lat = req.body.source_lat;
    var source_lan = req.body.source_lan;
    var destination_lat = req.body.destination_lat;
    var destination_lan = req.body.destination_lan;
    var customer_lat = req.body.customer_lat;
    var customer_lan = req.body.customer_lan;
    var cost = parseInt(req.body.cost);
    var payment_type = req.body.payment_type;
    var discount_code = req.body.discount_code;
    var date = moment().format('jYYYY/jMM/jDD');
    var full_date = moment().format('jYYYY/jMM/jDD HH:mm:ss');
    var driver_id = req.body.driver_id;
    console.log('driver id ------>' + driver_id);
    customer_controller.registerCustomerForOrganization(req, function (result) {
        if (result.result) {
            customer_id = result.customer_id;
            placeController.getLocationPlaceData(source_lat, source_lan, function (source_place_data) {

                placeController.getLocationPlaceData(destination_lat, destination_lan, function (destination_place_data) {

                    req.body.place_id = source_place_data._id;

                    if (discount_code) {

                        promotionController.checkDiscount(req, function (discount_amount) {

                            if (discount_amount) {

                                travel_dao.createStationTravel(station_id, customer_name, customer_id, phone_number, source_lat, source_lan, destination_lat, destination_lan, customer_lat, customer_lan, date, cost, payment_type, false, discount_code, discount_amount, full_date, source_place_data._id, destination_place_data._id, source_place_data.place_name, destination_place_data.place_name, driver_id, function (result) {

                                    if (result)
                                        callback({result: true, message: "", travel_id: result});
                                    else
                                        callback({
                                            result: false,
                                            message: "مشکلی پیش آمد، لطفا مجددا تلاش فرمایید",
                                            travel_id: ""
                                        });
                                });

                            } else {

                                travel_dao.createStationTravel(station_id, customer_name, customer_id, phone_number, source_lat, source_lan, destination_lat, destination_lan, customer_lat, customer_lan, date, cost, payment_type, false, '', 0, full_date, source_place_data._id, destination_place_data._id, source_place_data.place_name, destination_place_data.place_name, driver_id, function (result) {

                                    if (result)
                                        callback({result: true, message: "", travel_id: result});
                                    else
                                        callback({
                                            result: false,
                                            message: "کد تخفیف به اشتباه وارد شده است",
                                            travel_id: ""
                                        });
                                });
                            }
                        });

                    }
                    else {

                        travel_dao.createStationTravel(station_id, customer_name, customer_id, phone_number, source_lat, source_lan, destination_lat, destination_lan, customer_lat, customer_lan, date, cost, payment_type, false, '', 0, full_date, source_place_data._id, destination_place_data._id, source_place_data.place_name, destination_place_data.place_name, driver_id, function (result) {

                            if (result)
                                callback({result: true, message: "", travel_id: result});
                            else
                                callback({
                                    result: false,
                                    message: "مشکلی پیش آمد، لطفا مجددا تلاش فرمایید",
                                    travel_id: ""
                                });
                        });
                    }

                });

            });
        }
        else {
            callback({
                result: false,
                message: "مشکلی پیش آمد، لطفا مجددا تلاش فرمایید",
                travel_id: ""
            });
        }
    });

};

module.exports.customerConfirmedTravelTwo = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    // var driver_id = data.driver_id;

    // travel_dao.getTravelInfo(travel_id, function (travel_data) {

    travel_dao.updateTravelStatus(travel_id, "customer_confirmed", function (result) {

        if (result) {

            travel_dao.activateTravel(travel_id, function (activateTravel_result) {

                if (activateTravel_result) {

                    socket.join(travel_id);

                    find_taxi_and_notify_two(travel_id, io, function (result) {});

                    callback({result: {result: true}});

                } else {

                    callback({result: {result: false}});

                }
            });

        } else {

            callback({result: {result: false}});

        }

    });

    // });

};

function findTaxiTwo(travel_id, callback) {

    travel_dao.updateTravelStatus(travel_id, "searching", function (result) {});

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data) {

            var source_lat = travel_data.source_lat;
            var source_lan = travel_data.source_lan;
            var service_type = travel_data.service_type;

            travel_dao.getRefusedStandByTaxis(travel_id, function (refused_list) {

                taxi_dao.getAroundTaxisTwo(service_type, parseFloat(source_lat), parseFloat(source_lan), refused_list, function (nearest_taxi) {

                    if (nearest_taxi.length > 0) {

                        travel_dao.setTravelTaxi(travel_id, nearest_taxi[0]._id, nearest_taxi[0].station_id, nearest_taxi[0].station_title, function (set_travel_taxi_result) {

                            travel_dao.updateTravelStatus(travel_id, "taxi_founded", function (result) {

                                callback(nearest_taxi[0]._id);

                            });

                        });


                    } else {

                        callback(false);

                    }

                });

            });
        }

    });

}

function notify_taxi_travel_two(travel_id, taxi_id, io, callback) {

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

            var date = moment().format("jYYYY/jMM/jDD HH:mm:ss");

            map_tools.timeTravelByGoogle(travel_data.source_lan, travel_data.source_lat, taxi_data.location_lan, taxi_data.location_lat, function (time_travel) {

                if(time_travel) {

                    travel_dao.updateTaxiTimerTimeTimeTravelGetTravelByTaxi(travel_id, time_travel, date, 50, false, function (update_travel_result) {

                        var taxi_socket_id = taxi_data.socket_id;
                        var travel_data_final = travel_data.toObject();
                        travel_data_final.taxi_location_lat = taxi_data.location_lat;
                        travel_data_final.taxi_location_lan = taxi_data.location_lan;
                        travel_data_final.taxi_timer_time = 50;
                        travel_data_final.time_travel = time_travel;

                        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                            if(taxi_data.state == "standby" && travel_data.taxi_id.toString() === taxi_id.toString()) {

                                if (io.of('/').connected[taxi_socket_id.toString()]) {

                                    smsHandler.sendNewTravelSms(taxi_data.driver_phone_number, travel_data.travel_code, function (result) {
                                    });

                                    io.of('/').connected[taxi_socket_id.toString()].emit('new-travel', travel_data_final, function (is_taxi_get_travel) {

                                        if (is_taxi_get_travel) {

                                            travel_dao.setGetTravel(travel_id, true, function (result) {
                                            });

                                        }

                                    });

                                }

                                // new travel after 4 second

                                var count = 0;

                                var newTravelInterval = setInterval((function () {

                                    count = count + 4;

                                    console.log("COUNT IS: " + count);

                                    travel_dao.getTravelInfo(travel_id, function (travel_data) {

                                        var state = travel_data.state;
                                        var taxi_id = travel_data.taxi_id;

                                        if (state == "taxi_founded" && !travel_data.is_get_travel && count < 48) {

                                            console.log("\nNOT ACKED YET!\n ");

                                            taxi_dao.getTaxiInfo(taxi_id, function (current_taxi_data) {

                                                travel_data_final.taxi_timer_time = travel_data_final.taxi_timer_time - count;

                                                console.log("\nTHIS IS HERE(COUNT):\n " + count);

                                                if (io.of('/').connected[current_taxi_data.socket_id.toString()]) {

                                                    io.of('/').connected[current_taxi_data.socket_id.toString()].emit('new-travel', travel_data_final, function (is_taxi_get_travel) {

                                                        if (is_taxi_get_travel) {

                                                            console.log("\nTAXI ACK IS COME :)!\n ");

                                                            travel_dao.setGetTravel(travel_id, true, function (result) {
                                                            });

                                                        }


                                                    });

                                                }

                                            });

                                        } else {

                                            console.log("\nWE STOP INTERVAL\n");

                                            console.log("\nTAXI GETS TRAVEL\n");

                                            clearInterval(newTravelInterval);

                                        }


                                    })

                                }), 4000);

                                setTimeout((function () {

                                    console.log("INTERVAL CHECK");

                                    travel_dao.getTravelInfo(travel_id, function (travel_data) {

                                        if (travel_data.state == "taxi_founded" && travel_data.taxi_id.toString() == taxi_id.toString()) {

                                            console.log("INTERVAL CHECK IN TAXI FOUNDED");

                                            taxi_dao.setTaxiFreeOnStandBy(taxi_id, function (set_taxi_busy_result) {

                                                console.log("FREE TAXI");

                                                if (set_taxi_busy_result) {

                                                    travel_dao.addStandByTaxi(travel_id, taxi_id, function (add_stand_by_taxi_result) {

                                                        if (add_stand_by_taxi_result) {

                                                            travel_dao.updateTravelStatus(travel_id, "customer_confirmed", function (result) {

                                                                find_taxi_and_notify_two(travel_id, io, function (find_and_notify_result) {});

                                                            });
                                                        }

                                                    });

                                                } else {

                                                    callback(true);

                                                }

                                            });

                                        } else if (travel_data.state == "taxi_founded" && !(travel_data.taxi_id.toString() == taxi_id.toString())) {

                                            callback(true);

                                        }

                                    })

                                }), 50000)

                            } else {

                                travel_dao.updateTravelStatus(travel_id, "customer_confirmed", function (result) {

                                    find_taxi_and_notify_two(travel_id, io, function (find_and_notify_result) {});

                                });

                            }

                        });

                    });

                }


            });
        });

    });

}

function find_taxi_and_notify_two(travel_id, io, callback) {

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if(travel_data.state == "customer_confirmed") {

            findTaxiTwo(travel_id, function (taxi_id) {

                if (taxi_id) {

                    console.log("WE FOUND NEW TAXI");

                    taxi_dao.setTaxiStandBy(taxi_id, function (set_taxi_standby_result) {

                        travel_dao.activateTravel(travel_id, function (result) {

                            notify_taxi_travel_two(travel_id, taxi_id, io, function (result) {

                                callback(result);

                            });

                        });

                    });

                } else {

                    console.log("WE NOT FOUND NEW TAXI");

                    io.sockets.in(travel_id).emit('taxi_not_found', {result: {travel_id: travel_id}});

                    travel_dao.deactivateTravel(travel_id, function (result) {

                        travel_dao.updateTravelStatus(travel_id, 'taxi_not_found', function (result) {

                            callback(false);

                        });

                    });
                }

            });

        }

    })


}



module.exports.createOrganizationTravel = function (req, callback) {
    var organization_id = req.body.organization_id;
    var customer_name = req.body.customer_name;
    var customer_id = req.body.customer_id;
    var phone_number = req.body.phone_number;
    var caller_number = req.body.caller_number;
    var source_lat = req.body.source_lat;
    var source_lan = req.body.source_lan;
    var destination_lat = req.body.destination_lat;
    var destination_lan = req.body.destination_lan;
    var customer_lat = req.body.customer_lat;
    var customer_lan = req.body.customer_lan;
    var cost = parseInt(req.body.cost);
    var payment_type = req.body.payment_type;
    var discount_code = req.body.discount_code;
    var date = moment().format('jYYYY/jMM/jDD');
    var full_date = moment().format('jYYYY/jMM/jDD HH:mm:ss');
    var driver_id = req.body.driver_id;
    console.log('driver id ------>' + driver_id);
    customer_controller.registerCustomerForOrganization(req, function (result) {
        if (result.result) {
            customer_id = result.customer_id;
            placeController.getLocationPlaceData(source_lat, source_lan, function (source_place_data) {

                placeController.getLocationPlaceData(destination_lat, destination_lan, function (destination_place_data) {

                    req.body.place_id = source_place_data._id;

                    if (discount_code) {

                        promotionController.checkDiscount(req, function (discount_amount) {

                            if (discount_amount) {

                                travel_dao.createOrganizationTravel(organization_id, customer_name, customer_id, phone_number, source_lat, source_lan, destination_lat, destination_lan, customer_lat, customer_lan, date, cost, payment_type, false, discount_code, discount_amount, full_date, source_place_data._id, destination_place_data._id, source_place_data.place_name, destination_place_data.place_name, driver_id, function (result) {

                                    if (result)
                                        callback({result: true, message: "", travel_id: result});
                                    else
                                        callback({
                                            result: false,
                                            message: "مشکلی پیش آمد، لطفا مجددا تلاش فرمایید",
                                            travel_id: ""
                                        });
                                });

                            } else {

                                travel_dao.createOrganizationTravel(organization_id, customer_name, customer_id, phone_number, source_lat, source_lan, destination_lat, destination_lan, customer_lat, customer_lan, date, cost, payment_type, false, '', 0, full_date, source_place_data._id, destination_place_data._id, source_place_data.place_name, destination_place_data.place_name, driver_id, function (result) {

                                    if (result)
                                        callback({result: true, message: "", travel_id: result});
                                    else
                                        callback({
                                            result: false,
                                            message: "کد تخفیف به اشتباه وارد شده است",
                                            travel_id: ""
                                        });
                                });
                            }
                        });

                    }
                    else {

                        travel_dao.createOrganizationTravel(organization_id, customer_name, customer_id, phone_number, source_lat, source_lan, destination_lat, destination_lan, customer_lat, customer_lan, date, cost, payment_type, false, '', 0, full_date, source_place_data._id, destination_place_data._id, source_place_data.place_name, destination_place_data.place_name, driver_id, function (result) {

                            if (result)
                                callback({result: true, message: "", travel_id: result});
                            else
                                callback({
                                    result: false,
                                    message: "مشکلی پیش آمد، لطفا مجددا تلاش فرمایید",
                                    travel_id: ""
                                });
                        });
                    }

                });

            });
        }
        else {
            callback({
                result: false,
                message: "مشکلی پیش آمد، لطفا مجددا تلاش فرمایید",
                travel_id: ""
            });
        }
    });

};

module.exports.createTravel = function (req, callback) {

    var customer_id = req.body.customer_id;
    var phone_number = req.body.phone_number;
    var caller_number = req.body.caller_number;
    var source_lat = req.body.source_lat;
    var source_lan = req.body.source_lan;
    var destination_lat = req.body.destination_lat;
    var destination_lan = req.body.destination_lan;
    var customer_lat = req.body.customer_lat;
    var customer_lan = req.body.customer_lan;
    var cost = parseInt(req.body.cost);
    var payment_type = req.body.payment_type;
    var discount_code = req.body.discount_code;
    var service_type = req.body.service_type;
    var date = moment().format('jYYYY/jMM/jDD');
    var full_date = moment().format('jYYYY/jMM/jDD HH:mm:ss');
    var readable_date = moment_persian().format('dddd jD jMMMM');
    var time = moment().format('HH:mm');
    var second_destination_lat = req.body.second_destination_lat;
    var second_destination_lan = req.body.second_destination_lan;
    var is_two_way = req.body.is_two_way;
    var stop_time = req.body.stop_time;
    var stop_time_value = parseInt(req.body.stop_time_value);

    console.log(req.body);


    placeController.getLocationPlaceData(source_lat, source_lan, function (source_place_data) {

        placeController.getLocationPlaceData(destination_lat, destination_lan, function (destination_place_data) {

            placeController.getLocationPlaceData(second_destination_lat, second_destination_lan, function (second_destination_place_data) {

                console.log("/HHHHHHHHHHHHHHHA");
                console.log(second_destination_place_data);

                req.body.place_id = source_place_data._id;

                if (!second_destination_place_data) {

                    var second_destination_place_name = "";
                    var second_destination_place_id = null;

                } else {

                    var second_destination_place_name = second_destination_place_data.place_name;
                    var second_destination_place_id = second_destination_place_data._id;

                }

                if (discount_code) {

                    promotionController.checkDiscount(req, function (discount_amount) {

                        if (discount_amount) {

                            travel_dao.createTravel(
                                customer_id,
                                source_lat,
                                source_lan,
                                destination_lat,
                                destination_lan,
                                customer_lat,
                                customer_lan,
                                date,
                                cost,
                                payment_type,
                                false,
                                discount_code,
                                discount_amount,
                                full_date,
                                readable_date,
                                time,
                                source_place_data._id,
                                destination_place_data._id,
                                source_place_data.place_name,
                                destination_place_data.place_name,
                                service_type,
                                second_destination_place_name,
                                second_destination_place_id,
                                second_destination_lat,
                                second_destination_lan,
                                is_two_way,
                                stop_time,
                                stop_time_value,
                                function (result) {

                                    if (result)
                                        callback({result: true, message: "", travel_id: result});
                                    else
                                        callback({
                                            result: false,
                                            message: "مشکلی پیش آمد، لطفا مجددا تلاش فرمایید",
                                            travel_id: ""
                                        });
                                });

                        } else {

                            travel_dao.createTravel(customer_id, source_lat, source_lan, destination_lat, destination_lan, customer_lat, customer_lan, date, cost, payment_type, false, '', 0, full_date, readable_date, time, source_place_data._id, destination_place_data._id, source_place_data.place_name, destination_place_data.place_name, service_type, second_destination_place_name,
                                second_destination_place_id,
                                second_destination_lat,
                                second_destination_lan,
                                is_two_way,
                                stop_time,
                                stop_time_value, function (result) {

                                    if (result)
                                        callback({result: true, message: "", travel_id: result});
                                    else
                                        callback({
                                            result: false,
                                            message: "کد تخفیف به اشتباه وارد شده است",
                                            travel_id: ""
                                        });
                                });
                        }
                    });

                } else {

                    travel_dao.createTravel(customer_id, source_lat, source_lan, destination_lat, destination_lan, customer_lat, customer_lan, date, cost, payment_type, false, '', 0, full_date, readable_date, time, source_place_data._id, destination_place_data._id, source_place_data.place_name, destination_place_data.place_name, service_type, second_destination_place_name,
                        second_destination_place_id,
                        second_destination_lat,
                        second_destination_lan,
                        is_two_way,
                        stop_time,
                        stop_time_value, function (result) {

                            if (result)
                                callback({result: true, message: "", travel_id: result});
                            else
                                callback({
                                    result: false,
                                    message: "مشکلی پیش آمد، لطفا مجددا تلاش فرمایید",
                                    travel_id: ""
                                });
                        });
                }


            });

        });

    });

};

// taxi get cash from customer

module.exports.taxiGetCash = function (data, io, socket) {

    var travel_id = data.travel_id;

    socket.join(travel_id);

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data) {

            if ((travel_data.payment_type == 'cash') && !travel_data.is_paid) {

                travel_dao.updateIsPaid(travel_id, true, function (update_is_paid_data) {

                    if (update_is_paid_data) {

                        var final_data = travel_data.toObject();
                        final_data.is_paid = true;

                        customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                            if (customer_data.is_apple) {

                                apnHandler.sendNotification(customer_data.device_id, "travel_is_paid");

                            }

                        });

                        io.sockets.in(travel_id).emit('travel_is_paid', {result: final_data});
                    }
                });
            }
        }
    });

};

module.exports.changeTravelCost = function (req, callback) {

    var travel_id = req.body.travel_id;
    var cost = req.body.travel_cost;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data.is_active) {

            travel_dao.updateTravelCost(travel_id, cost, function (result) {

                smsHandler.sendTravelCostChange(travel_data.customer_phone_number, cost, function (result) {
                });

                callback(true);

            });

        }

    });

};

// change travel cost

// customer pay travel cost after increase money

module.exports.customerPayTravelCost = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    console.log(data);

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        console.log("SADDASD 7");

        taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

            console.log("SADDASD 9");

            var taxi_socket_id = taxi_data.socket_id;

            if (travel_data && (travel_data.state == 'travel_started')) {

                wallet_dao.getWalletInfoByCustomerId(travel_data.customer_id, function (wallet_data) {

                    console.log("SADDASD 21");

                    if ((travel_data.payment_type == 'wallet') && !travel_data.is_paid && wallet_data) {

                        console.log("SADDASD 2");

                        if (travel_data.discount_amount != 0) {

                            console.log("SADDASD 4");

                            promotion_dao.getDiscountInfoByCode(travel_data.discount_code, function (discount_data) {

                                // calculate discount and decrease money

                                var discount_amount = travel_data.cost * (discount_data.amount / 100);

                                if (discount_amount >= discount_data.max_cost)
                                    discount_amount = discount_data.max_cost;

                                discount_amount = Math.ceil(discount_amount / 100) * 100;

                                var final_cost = travel_data.cost - discount_amount;

                                // var final_cost = Math.ceil((travel_data.cost - (travel_data.cost * travel_data.discount_amount / 100))/100)*100;

                                if ((wallet_data.money >= final_cost)) {

                                    wallet_dao.decrementWalletMoney(wallet_data._id, final_cost, function (update_wallet_data) {

                                        travel_dao.updateIsPaid(travel_id, true, function (update_is_paid) {

                                            var final_travel_data = travel_data.toObject();

                                            final_travel_data.is_paid = true;

                                            socket.join(travel_id);

                                            var date = moment().format('jYYYY/jMM/jDD');
                                            var time = moment().format('HH:mm');

                                            turnover_dao.createCustomerTurnover(travel_data.customer_id, travel_data.cost, "debtor", consts.TRAVEL_COST_DECREASE_EXP, date, time, function (result) {
                                            });

                                            smsHandler.sendDriverTravelPaidSms(taxi_data.driver_phone_number, travel_data.travel_code, travel_data.cost, function(result){});

                                            io.to(taxi_socket_id.toString()).emit('travel_is_paid', {result: final_travel_data});

                                            io.sockets.in(travel_id).emit('travel_is_paid', {result: final_travel_data});

                                            callback({result: true});

                                        });

                                    });

                                }

                            })

                        } else {

                            console.log("SADDASD 3");

                            // decrease money without any discount

                            if (wallet_data.money >= travel_data.cost) {

                                wallet_dao.decrementWalletMoney(wallet_data._id, travel_data.cost, function (update_wallet_data) {

                                    travel_dao.updateIsPaid(travel_id, true, function (update_is_paid) {

                                        var final_travel_data = travel_data.toObject();
                                        final_travel_data.is_paid = true;

                                        socket.join(travel_id);

                                        io.sockets.in(travel_id).emit('travel_is_paid', {result: final_travel_data});

                                        var date = moment().format('jYYYY/jMM/jDD');
                                        var time = moment().format('HH:mm');

                                        turnover_dao.createCustomerTurnover(travel_data.customer_id, travel_data.cost, "debtor", consts.TRAVEL_COST_DECREASE_EXP, date, time, function (result) {
                                        });

                                        io.to(taxi_socket_id.toString()).emit('travel_is_paid', {result: final_travel_data});

                                        smsHandler.sendDriverTravelPaidSms(taxi_data.driver_phone_number, travel_data.travel_code, travel_data.cost, function(result){});

                                        callback({result: true});

                                    });
                                });

                            }
                        }

                    }
                });

            } else callback({result: false});

        });

    });

};

module.exports.search = function (req, callback) {

    var search_query = req.body.search_query;

    if (search_query.length == 8) {

        search_query = "P" + search_query;

    }

    travel_dao.search(search_query, function (search_result) {

        if (search_result) {

            var travels_list = [];

            var len = search_result.length;

            if (len > 0) {
                for (var i = 0; i < len; i++) {

                    (function (cntr) {

                        var travel_obj = search_result[cntr];
                        var travel_id = travel_obj._id;

                        travel_dao.getTravelInfo(travel_id, function (travel_data) {

                            if (travel_data) {
                                travels_list.push(travel_data);
                            }

                            if (cntr === len - 1) {
                                callback(travels_list);
                            }

                        });


                    })(i);
                }
            } else {
                callback(travels_list);
            }
        }

    });

};

module.exports.travelFinishedTwo = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var taxi_id = data.taxi_id;
    var rate = data.rate;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data.state == "travel_started" && travel_data.taxi_id.toString() == taxi_id.toString()) {

            taxi_dao.setTaxiFree(taxi_id, function (free_result) {

                travel_dao.updateTravelStatusRateActivate(travel_id, "finished", rate, 0, function (result) {

                    if (result) {


                        if (result) {

                            customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                                if (customer_data.is_apple) {

                                    apnHandler.sendNotification(customer_data.device_id, "travel_finished");

                                }

                            });

                            io.sockets.in(travel_id).emit('travel_finished', travel_id, taxi_id);

                            // create turnover things

                            taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                                taxi_dao.getServiceTypeData(taxi_data.service_type_id, function (taxi_service_data) {

                                    var commission_rate = parseInt(taxi_service_data.commission_rate);
                                    var commission_amount = travel_data.cost * (commission_rate / 100);
                                    var income = travel_data.cost - commission_amount;
                                    var income_explanation = "درآمد سفر " + travel_data.travel_code;
                                    var cash_deptor_explanation = "کسر کمیسون سفر  " + travel_data.travel_code;
                                    var cash_creditor_explanation = "افزایش اعتبار سفر  " + travel_data.travel_code;
                                    var date = moment().format('jYYYY/jMM/jDD');
                                    var time = moment().format('HH:mm');
                                    var readable_date = moment_persian().format('dddd jD jMMMM');

                                    // create turn over income

                                    turnover_dao.createTaxiTurnover(taxi_id, travel_id, income, "income", income_explanation, date, readable_date, time, function (result) {
                                    });


                                    if (travel_data.payment_type == "cash") {

                                        turnover_dao.createTaxiTurnover(taxi_id, travel_id, commission_amount, "debtor", cash_deptor_explanation, date, readable_date, time, function (result) {
                                        });

                                        // decrease taxi credit

                                        // taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function(result){});

                                        // set travel paid

                                        travel_dao.setTravelPaid(travel_id, function (result) {
                                        });

                                    } else if (travel_data.payment_type == "wallet" && travel_data.is_paid) {

                                        // add a turn over transaction

                                        turnover_dao.createTaxiTurnover(taxi_id, travel_id, income, "creditor", cash_creditor_explanation, date, readable_date, time, function (result) {
                                        });

                                        // add credit to taxi

                                        // taxi_dao.increaseTaxiCredit(taxi_id, income, function(result){});


                                    } else if (travel_data.payment_type == "wallet" && !travel_data.is_paid) {

                                        turnover_dao.createTaxiTurnover(taxi_id, travel_id, commission_amount, "debtor", cash_deptor_explanation, date, readable_date, time, function (result) {
                                        });

                                        // decrease taxi credit

                                        // taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function(result){});

                                        // change travel payment type

                                        travel_dao.updateTravelPaymentType(travel_id, "cash", function (result) {
                                        });

                                        travel_dao.setTravelPaid(travel_id, function (result) {});

                                    }

                                    setTimeout(
                                        function () {

                                            handleTravelTurnOvers(travel_id, taxi_id, income, commission_amount, function (result) {
                                            });

                                        }, 3000);


                                });

                            });

                            callback(true);

                        }


                    }

                });

            });

        } else {

            callback(false);

        }

    });
};


// find best taxi function | base function

// function findTaxi(travel_id, callback) {
//
//   travel_dao.updateTravelStatus(travel_id, "searching", function (result) {});
//
//   travel_dao.getTravelInfo(travel_id, function (travel_data) {
//
//     if (travel_data) {
//
//       var around_taxis = [];
//
//       var source_lat = travel_data.source_lat;
//       var source_lan = travel_data.source_lan;
//
//       // var bound = map_tools.getSourceBound(source_lat, source_lan, consts.TAXI_SEARCH_RADIUS);
//
//       // taxi_dao.getTaxisBoundsLocation(parseFloat(bound.min_lat), parseFloat(bound.max_lat), parseFloat(bound.min_lan), parseFloat(bound.max_lan), function (taxis_list) {
//
//       taxi_dao.getAroundTaxis(parseFloat(source_lat), parseFloat(source_lan), function (taxis_list) {
//
//         travel_dao.getRefusedStandByTaxis(travel_id, function (refused_standby_taxis_list) {
//
//           var taxis_list_final = [];
//           var refused_taxi_ids = [];
//
//           if (refused_standby_taxis_list) {
//
//             for (var i = 0; i < refused_standby_taxis_list.length; i++) {
//
//               refused_taxi_ids.push(refused_standby_taxis_list[i].toString());
//
//             }
//
//             for (var i = 0; i < taxis_list.length; i++) {
//
//               if (refused_taxi_ids.indexOf(taxis_list[i]._id.toString()) < 0) {
//
//                 taxis_list_final.push(taxis_list[i]);
//
//               }
//
//             }
//
//           } else {
//
//             taxis_list_final = taxis_list;
//
//           }
//
//           if (taxis_list_final.length >= 1) {
//
//             for (var i = 0; i < taxis_list_final.length; i++) {
//
//               var taxi_distance = map_tools.findDistance(source_lan, source_lat, taxis_list_final[i].location_lan, taxis_list_final[i].location_lat);
//
//               if (taxi_distance < consts.TAXI_SEARCH_RADIUS) {
//
//                 around_taxis.push(taxis_list_final[i]);
//
//               }
//
//             }
//
//             if (around_taxis.length > 0) {
//
//               // set travel state
//
//               var candidate_taxi = around_taxis[0];
//
//               taxi_dao.getTaxiWaitTime(candidate_taxi._id, function (first_taxi_wait_time) {
//
//                 var candidate_taxi_wait_time = first_taxi_wait_time;
//
//                 for (var j = 0; j < around_taxis.length; j++) {
//
//                   (function (cntr) {
//
//                     taxi_dao.getTaxiWaitTime(around_taxis[cntr]._id, function (taxi_wait_time) {
//
//                       if (taxi_wait_time > candidate_taxi_wait_time) {
//
//                         var candidate_taxi_wait_time = taxi_wait_time;
//                         candidate_taxi = around_taxis[cntr];
//
//                       }
//
//                       if (cntr === around_taxis.length - 1) {
//
//                         // set taxi for travel
//
//                         travel_dao.setTravelTaxi(travel_id, candidate_taxi._id, function (set_travel_taxi_result) {
//
//                           travel_dao.updateTravelStatus(travel_id, "taxi_founded", function (result) {});
//
//                           callback(candidate_taxi._id);
//
//                         });
//
//                       }
//
//                     });
//
//                   })(j);
//
//                 }
//
//               });
//
//             } else {
//
//               callback(false);
//
//             }
//
//           } else {
//
//             callback(false);
//
//           }
//
//         });
//
//       });
//     }
//
//   });
//
// }

// find taxi, enhanced function

function findTaxi(travel_id, callback) {

    travel_dao.updateTravelStatus(travel_id, "searching", function (result) {
    });

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data) {

            var around_taxis = [];

            var source_lat = travel_data.source_lat;
            var source_lan = travel_data.source_lan;
            var service_type = travel_data.service_type;

            // var bound = map_tools.getSourceBound(source_lat, source_lan, consts.TAXI_SEARCH_RADIUS);

            // taxi_dao.getTaxisBoundsLocation(parseFloat(bound.min_lat), parseFloat(bound.max_lat), parseFloat(bound.min_lan), parseFloat(bound.max_lan), function (taxis_list) {

            taxi_dao.getAroundTaxis(service_type, parseFloat(source_lat), parseFloat(source_lan), function (taxis_list) {

                if (taxis_list.length > 0) {

                    travel_dao.getRefusedStandByTaxis(travel_id, function (refused_standby_taxis_list) {

                        var taxis_list_final = [];
                        var refused_taxi_ids = [];

                        if (refused_standby_taxis_list) {

                            for (var i = 0; i < refused_standby_taxis_list.length; i++) {

                                refused_taxi_ids.push(refused_standby_taxis_list[i].toString());

                            }

                            for (var i = 0; i < taxis_list.length; i++) {

                                if (refused_taxi_ids.indexOf(taxis_list[i]._id.toString()) < 0) {

                                    taxis_list_final.push(taxis_list[i]);

                                }

                            }

                        } else {

                            taxis_list_final = taxis_list;

                        }

                        if (taxis_list_final.length >= 1) {

                            // set travel state

                            var candidate_taxi = taxis_list_final[0];

                            taxi_dao.getTaxiWaitTime(candidate_taxi._id, function (first_taxi_wait_time) {

                                var candidate_taxi_wait_time = first_taxi_wait_time;

                                for (var j = 0; j < taxis_list_final.length; j++) {

                                    (function (cntr) {

                                        taxi_dao.getTaxiWaitTime(taxis_list_final[cntr]._id, function (taxi_wait_time) {

                                            if (taxi_wait_time > candidate_taxi_wait_time) {

                                                candidate_taxi_wait_time = taxi_wait_time;
                                                candidate_taxi = taxis_list_final[cntr];

                                            }

                                            if (cntr === taxis_list_final.length - 1) {

                                                // set taxi for travel

                                                travel_dao.setTravelTaxi(travel_id, candidate_taxi._id, candidate_taxi.station_id, candidate_taxi.station_title, function (set_travel_taxi_result) {

                                                    travel_dao.updateTravelStatus(travel_id, "taxi_founded", function (result) {
                                                    });

                                                    callback(candidate_taxi._id);

                                                });

                                            }

                                        });

                                    })(j);

                                }

                            });

                            // travel_dao.setTravelTaxi(travel_id, taxis_list_final[0]._id, function (set_travel_taxi_result) {
                            //
                            //   travel_dao.updateTravelStatus(travel_id, "taxi_founded", function (result) {
                            //
                            //     callback(taxis_list_final[0]._id);
                            //
                            //   });
                            //
                            // });


                        } else {

                            callback(false);

                        }

                        // } else {
                        //
                        //   callback(false);
                        //
                        // }

                    });
                } else {

                    callback(false);

                }

            });
        }

    });

}

function notify_taxi_travel(travel_id, taxi_id, io, callback) {

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

            map_tools.timeTravelByGoogle(travel_data.source_lan, travel_data.source_lat, taxi_data.location_lan, taxi_data.location_lat, function (time_travel) {

                travel_dao.updateTimeTravel(travel_id, time_travel, function (err, result) {

                    var date = moment().format("jYYYY/jMM/jDD HH:mm:ss");

                    travel_dao.updateTaxiTimerTime(travel_id, date, 50, function (update_ttt_result) {

                        if (update_ttt_result) {

                            travel_dao.getTravelInfo(travel_id, function (travel_data_final) {

                                var taxi_socket_id = taxi_data.socket_id;

                                travel_data_final = travel_data_final.toObject();
                                travel_data_final.taxi_location_lat = taxi_data.location_lat;
                                travel_data_final.taxi_location_lan = taxi_data.location_lan;

                                smsHandler.sendNewTravelSms(taxi_data.driver_phone_number, travel_data.travel_code, function (result) {});

                                // set is_get_travel to true

                                travel_dao.setGetTravel(travel_id, false, function (set_get_travel_result) {

                                    if(io.of('/').connected[taxi_socket_id.toString()]) {

                                        io.of('/').connected[taxi_socket_id.toString()].emit('new-travel', travel_data_final, function (is_taxi_get_travel) {

                                            // io.to(taxi_socket_id.toString()).emit('new-travel', travel_data_final, function (is_taxi_get_travel) {

                                            if (is_taxi_get_travel) {

                                                console.log("\nTAXI GETS TRAVEL\n");

                                                travel_dao.setGetTravel(travel_id, true, function (result) {
                                                });

                                            }

                                        });

                                    }

                                    taxi_dao.setTaxiStandBy(taxi_id, function (set_taxi_standby_result) {});

                                });


                                // new travel after 3 second

                                var count = 0;

                                var newTravelInterval = setInterval((function () {

                                    count = count + 4;

                                    console.log("COUNT IS: " + count);

                                    travel_dao.getTravelInfo(travel_id, function (travel_data) {

                                        var state = travel_data.state;
                                        var taxi_id = travel_data.taxi_id;

                                        if(state == "taxi_founded" && !travel_data.is_get_travel && count <= 50) {

                                            console.log("\nNOT ACKED YET!\n ");

                                            taxi_dao.getTaxiInfo(taxi_id, function (current_taxi_data) {

                                                travel_data_final.taxi_timer_time = travel_data_final.taxi_timer_time - count;

                                                console.log("\nTHIS IS HERE(COUNT):\n " + count);

                                                if(io.of('/').connected[taxi_socket_id.toString()]) {

                                                    io.of('/').connected[current_taxi_data.socket_id.toString()].emit('new-travel', travel_data_final, function (is_taxi_get_travel) {

                                                        // io.to(current_taxi_data.socket_id.toString()).emit('new-travel', travel_data_final, function (is_taxi_get_travel) {

                                                        if (is_taxi_get_travel) {

                                                            console.log("\nTAXI ACK IS COME :)!\n ");

                                                            travel_dao.setGetTravel(travel_id, true, function (result) {
                                                            });

                                                        }


                                                    });

                                                }

                                            });

                                        } else {

                                            console.log("\nWE STOP INTERVAL\n");

                                            console.log("\nTAXI GETS TRAVEL\n");

                                            clearInterval(newTravelInterval);

                                        }


                                    })

                                }), 4000);

                                setTimeout((function () {

                                    taxi_dao.setTaxiFreeOnStandBy(taxi_id, function (set_taxi_busy_result) {

                                        if (set_taxi_busy_result) {

                                            travel_dao.addStandByTaxi(travel_id, taxi_id, function (add_stand_by_taxi_result) {

                                                if (add_stand_by_taxi_result) {

                                                    travel_dao.getTravelInfo(travel_id, function (travel_data) {

                                                        if(!(travel_data.state == "taxi_confirmed" || travel_data.state == "travel_started" || travel_data.state == "finished" || travel_data.state == "finish_commented")) {

                                                            find_taxi_and_notify(travel_id, io, function (find_and_notify_result) {});

                                                        }

                                                    });

                                                }

                                            });

                                        } else {

                                            callback(true);

                                        }

                                    });

                                }), 50000)

                            });

                        }

                    })

                });

            });

        });

    });

}

module.exports.getStationTravelsList = function (station_id, page, per_page, callback) {

    travel_dao.getStationTravelsList(station_id, page, per_page, function (travels) {

        callback(travels);

    });

}

module.exports.getStationTravelsList = function (station_id, page, per_page, callback) {

    travel_dao.getStationTravelsList(station_id, page, per_page, function (travels) {

        callback(travels);

    });

}

module.exports.getTravelFactor = function (req, callback) {

    var travel_id = req.body.travel_id;
    var taxi_id = req.body.taxi_id;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

            customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

                taxi_dao.getServiceTypeData(taxi_data.service_type_id, function (service_data) {

                    var commission_rate = parseInt(service_data.commission_rate);
                    var commission_amount = travel_data.cost * (commission_rate / 100);
                    var income = travel_data.cost - commission_amount;

                    travel_data = travel_data.toObject();
                    travel_data.commission_amount = commission_amount;
                    travel_data.income = income;
                    travel_data.customer_name = customer_data.name;
                    callback(travel_data);

                })

            });

        })


    });

};

module.exports.getTravelInfoByCode = function (req, callback) {

    var travel_code = req.params.travel_code;
    var data = {};

    travel_dao.getTravelInfoByCode(travel_code, function (travel_data) {

        taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

            customer_dao.getCustomerInfoById(travel_data.customer_id, function(customer_data){

                data.customer = customer_data;
                data.travel = travel_data;
                data.taxi = taxi_data;

                callback(data);

            });

        })

    })

};

module.exports.updateTravelOptions = function (data, io, socket, callback) {

    var travel_id = data.travel_id;
    var second_destination_lat = parseFloat(data.second_destination_lat);
    var second_destination_lan = parseFloat(data.second_destination_lan);
    var is_two_way = data.is_two_way;
    var stop_time = data.stop_time;
    var stop_time_value = parseInt(data.stop_time_value);
    var discount_code = data.discount_code;
    var discount_amount = parseInt(data.discount_amount);
    var travel_cost = parseInt(data.travel_cost);

    placeController.getLocationPlaceData(second_destination_lat, second_destination_lan, function (second_destination_place_data) {

        if (!second_destination_place_data) {

            var second_destination_place_name = "";
            var second_destination_place_id = null;

        } else {

            var second_destination_place_name = second_destination_place_data.place_name;
            var second_destination_place_id = second_destination_place_data._id;

        }

        travel_dao.updateTravelOption(travel_id, second_destination_lan, second_destination_lat, second_destination_place_name, second_destination_place_id, is_two_way, stop_time, stop_time_value, discount_code, discount_amount, travel_cost, function (result) {

            travel_dao.getTravelInfo(travel_id, function (travel_data) {

                taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

                    console.log(travel_data);

                    io.to(taxi_data.socket_id.toString()).emit('travel_options_changed', {result: travel_data});

                    callback(true);

                });

            });

        });


    });


};

module.exports.updateTravelPaymentType = function (data, io, socket, callback) {

    console.log("WE ARE IN PAYMENT CHANGE PART");
    console.log(data);
    var travel_id = data.travel_id;
    var payment_type = data.payment_type;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if(!travel_data.is_paid) {

            travel_dao.updateTravelPaymentType(travel_id, payment_type, function(result){

                if(result) {

                    travel_dao.getTravelInfo(travel_id, function (travel_data) {

                        taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

                            if(payment_type === "cash" || travel_data.state == "taxi_confirmed") {

                                var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                var now = moment();

                                var difference_time = now.diff(start_time, "seconds");

                                var remaining_time = travel_data.time_travel - difference_time;

                                if (remaining_time < 0) {

                                    remaining_time = 0;

                                }

                                var final_travel_data = travel_data.toObject();
                                final_travel_data.remaning_time = remaining_time;
                                final_travel_data.is_paid = false;

                                var data = {
                                    travel_data: final_travel_data,
                                    taxi_data: taxi_data
                                };

                                // save turn over customer

                                var date = moment().format('jYYYY/jMM/jDD');
                                var time = moment().format('HH:mm');

                                io.to(taxi_data.socket_id.toString()).emit('travel_options_changed', {result: final_travel_data});

                                callback({result: true, is_paid: false});

                            } else {

                                wallet_dao.getWalletInfoByCustomerId(travel_data.customer_id, function (wallet_data) {

                                    if ((travel_data.payment_type == 'wallet') && !travel_data.is_paid && wallet_data) {

                                        if (travel_data.discount_amount != 0) {

                                            promotion_dao.getDiscountInfoByCode(travel_data.discount_code, function (discount_data) {

                                                // calculate discount and decrease money

                                                var discount_amount = travel_data.cost * (discount_data.amount / 100);

                                                if (discount_amount >= discount_data.max_cost)
                                                    discount_amount = discount_data.max_cost;

                                                discount_amount = Math.ceil(discount_amount / 100) * 100;

                                                var final_cost = travel_data.cost - discount_amount;

                                                if ((wallet_data.money >= final_cost)) {

                                                    wallet_dao.decrementWalletMoney(wallet_data._id, final_cost, function (update_wallet_data) {

                                                        travel_dao.updateIsPaid(travel_id, true, function (update_is_paid) {

                                                            var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                                            var now = moment();

                                                            var difference_time = now.diff(start_time, "seconds");

                                                            var remaining_time = travel_data.time_travel - difference_time;

                                                            if (remaining_time < 0) {

                                                                remaining_time = 0;

                                                            }

                                                            var final_travel_data = travel_data.toObject();
                                                            final_travel_data.remaning_time = remaining_time;
                                                            final_travel_data.is_paid = true;

                                                            var data = {
                                                                travel_data: final_travel_data,
                                                                taxi_data: taxi_data
                                                            };

                                                            // save turn over customer

                                                            var date = moment().format('jYYYY/jMM/jDD');
                                                            var time = moment().format('HH:mm');

                                                            turnover_dao.createCustomerTurnover(travel_data.customer_id, travel_data.cost, "debtor", consts.TRAVEL_COST_DECREASE_EXP, date, time, function (result) {
                                                            });

                                                            smsHandler.sendDriverTravelPaidSms(taxi_data.driver_phone_number, travel_data.travel_code, travel_data.cost, function(result){});

                                                            io.to(taxi_data.socket_id.toString()).emit('travel_options_changed', {result: final_travel_data});

                                                            console.log("WE ARE HERE BASTERD 555");

                                                            callback({result: true, is_paid: true});

                                                        });
                                                    });

                                                } else {

                                                    var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                                    var now = moment();

                                                    var difference_time = now.diff(start_time, "seconds");

                                                    var remaining_time = travel_data.time_travel - difference_time;

                                                    if (remaining_time < 0) {

                                                        remaining_time = 0;

                                                    }

                                                    var final_travel_data = travel_data.toObject();
                                                    final_travel_data.remaning_time = remaining_time;

                                                    var data = {
                                                        travel_data: final_travel_data,
                                                        taxi_data: taxi_data
                                                    };


                                                    io.to(taxi_data.socket_id.toString()).emit('travel_options_changed', {result: travel_data});

                                                    callback({result: true, is_paid: false});
                                                }

                                            });

                                        } else {

                                            // decrease money without any discount

                                            if (wallet_data.money >= travel_data.cost) {

                                                wallet_dao.decrementWalletMoney(wallet_data._id, travel_data.cost, function (update_wallet_data) {

                                                    travel_dao.updateIsPaid(travel_id, true, function (update_is_paid) {

                                                        var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                                        var now = moment();

                                                        var difference_time = now.diff(start_time, "seconds");

                                                        var remaining_time = travel_data.time_travel - difference_time;

                                                        if (remaining_time < 0) {

                                                            remaining_time = 0;

                                                        }

                                                        var final_travel_data = travel_data.toObject();
                                                        final_travel_data.remaning_time = remaining_time;
                                                        final_travel_data.is_paid = true;

                                                        var data = {
                                                            travel_data: final_travel_data,
                                                            taxi_data: taxi_data
                                                        };

                                                        var date = moment().format('jYYYY/jMM/jDD');
                                                        var time = moment().format('HH:mm');

                                                        turnover_dao.createCustomerTurnover(travel_data.customer_id, travel_data.cost, "debtor", consts.TRAVEL_COST_DECREASE_EXP, date, time, function (result) {
                                                        });

                                                        smsHandler.sendDriverTravelPaidSms(taxi_data.driver_phone_number, travel_data.travel_code, travel_data.cost, function(result){});

                                                        io.to(taxi_data.socket_id.toString()).emit('travel_options_changed', {result: final_travel_data});

                                                        callback({result: true, is_paid: true});
                                                    });
                                                });

                                            } else {

                                                // can not pay

                                                var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                                var now = moment();

                                                var difference_time = now.diff(start_time, "seconds");

                                                var remaining_time = travel_data.time_travel - difference_time;

                                                if (remaining_time < 0) {

                                                    remaining_time = 0;

                                                }

                                                var final_travel_data = travel_data.toObject();
                                                final_travel_data.remaning_time = remaining_time;

                                                var data = {
                                                    travel_data: final_travel_data,
                                                    taxi_data: taxi_data
                                                };

                                                io.to(taxi_data.socket_id.toString()).emit('travel_options_changed', {result: travel_data});

                                                callback({result: true, is_paid: false});
                                            }
                                        }

                                    } else {

                                        var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                                        var now = moment();

                                        var difference_time = now.diff(start_time, "seconds");

                                        var remaining_time = travel_data.time_travel - difference_time;

                                        if (remaining_time < 0) {

                                            remaining_time = 0;

                                        }

                                        var final_travel_data = travel_data.toObject();
                                        final_travel_data.remaning_time = remaining_time;

                                        var data = {
                                            travel_data: final_travel_data,
                                            taxi_data: taxi_data
                                        };

                                        io.to(taxi_data.socket_id.toString()).emit('travel_options_changed', {result: travel_data});

                                        callback({result: true, is_paid: false});

                                    }
                                });

                            }

                        });

                    });

                } else {

                    callback({result: false, is_paid: false});

                }

            })

        } else {

            callback({result: false, is_paid: false});

        }

    })

    // travel_dao.updateTravelPaymentType(travel_id, payment_type)
    //
    // placeController.getLocationPlaceData(second_destination_lat, second_destination_lan, function (second_destination_place_data) {
    //
    //   if (!second_destination_place_data) {
    //
    //     var second_destination_place_name = "";
    //     var second_destination_place_id = null;
    //
    //   } else {
    //
    //     var second_destination_place_name = second_destination_place_data.place_name;
    //     var second_destination_place_id = second_destination_place_data._id;
    //
    //   }
    //
    //   travel_dao.updateTravelOption(travel_id, second_destination_lan, second_destination_lat, second_destination_place_name, second_destination_place_id, is_two_way, stop_time, stop_time_value, discount_code, discount_amount, travel_cost, function (result) {
    //
    //     travel_dao.getTravelInfo(travel_id, function (travel_data) {
    //
    //       taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {
    //
    //         io.to(taxi_data.socket_id.toString()).emit('travel_options_changed', {result: travel_data});
    //
    //         callback(true);
    //
    //       });
    //
    //     });
    //
    //   });
    //
    //
    // });


};

function find_taxi_and_notify(travel_id, io, callback) {

    findTaxi(travel_id, function (taxi_id) {

        console.log("WE FOUND NEW TAXI");

        if (taxi_id) {

            travel_dao.activateTravel(travel_id, function (result) {

                notify_taxi_travel(travel_id, taxi_id, io, function (result) {

                    callback(result);

                });

            });

        } else {


            io.sockets.in(travel_id).emit('taxi_not_found', {result: {travel_id: travel_id}});

            travel_dao.deactivateTravel(travel_id, function (result) {

                travel_dao.updateTravelStatus(travel_id, 'taxi_not_found', function (result) {

                    callback(false);

                });

            });
        }

    });

}

function addTravelStandByTaxi(travel_id, taxi_id, callback) {

    travel_dao.addStandByTaxi(travel_id, taxi_id, function (result) {

        callback(result);

    });

}

function addTravelRefusedTaxi(travel_id, taxi_id, callback) {

    travel_dao.addRefusedTaxi(travel_id, taxi_id, function (result) {

        callback(result);

    });

}


function handleTravelTurnOvers(travel_id, taxi_id, income, commission_amount, callback) {

    // create turnover things

    // travel_dao.getStartDateFinishTravels(function (travels) {

    // get transactions list of a travel

    var redundant_turnovers_list = [];

    turnover_dao.getTravelTurnOvers(travel_id, function (travel_turnovers) {

        var creditor_debtor_flag = false;
        var income_flag = false;

        for (var i = 0; i < travel_turnovers.length; i++) {

            if (travel_turnovers[i].turnover_type == "debtor" && !creditor_debtor_flag) {

                taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function (result) {});

                creditor_debtor_flag = true;

            }

            else if(travel_turnovers[i].turnover_type == "creditor" && !creditor_debtor_flag) {


                taxi_dao.increaseTaxiCredit(taxi_id, income, function (result) {});

                creditor_debtor_flag = true;

            }

            else if (travel_turnovers[i].turnover_type == "income" && !income_flag) {

                income_flag = true;

            }

            else {

                redundant_turnovers_list.push(travel_turnovers[i]._id);

            }

        }

        turnover_dao.removeRedundantTurnOversWithIds(redundant_turnovers_list, function(result){});

        //   if (travel_turnovers.length == 2) {
        //
        //     for (var i = 0; i < travel_turnovers.length; i++) {
        //
        //       if (travel_turnovers[i].turnover_type == "debtor") {
        //
        //         taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function (result) {
        //         });
        //
        //       }
        //
        //       if (travel_turnovers[i].turnover_type == "creditor") {
        //
        //         taxi_dao.increaseTaxiCredit(taxi_id, income, function (result) {
        //         });
        //
        //       }
        //
        //     }
        //
        //   } else if (travel_turnovers.length > 2) {
        //
        //     for (var i = 0; i < travel_turnovers.length; i++) {
        //
        //       if (travel_turnovers[i].turnover_type == "debtor" && i <= 1) {
        //
        //         taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function (result) {});
        //
        //       }
        //
        //       else if (travel_turnovers[i].turnover_type == "creditor" && i <= 1) {
        //
        //         taxi_dao.increaseTaxiCredit(taxi_id, income, function (result) {});
        //
        //       }
        //
        //       else {
        //
        //         redundant_turnovers_list.push(travel_turnovers[i]._id);
        //
        //         turnover_dao.removeRedundantTurnOversWithIds(redundant_turnovers_list, function(result){});
        //
        //       }
        //
        //     }
        //
        //   }
        //
        // });

    });


    // travel_dao.getTravelInfo(travel_id, function (travel_data) {
    //
    //   taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {
    //
    //     taxi_dao.getServiceTypeData(taxi_data.service_type_id, function (taxi_service_data) {
    //
    //       var commission_rate = parseInt(taxi_service_data.commission_rate);
    //       var commission_amount = travel_data.cost * (commission_rate/100);
    //       var income = travel_data.cost - commission_rate;
    //       var income_explanation = "درآمد سفر " + travel_data.travel_code;
    //       var cash_deptor_explanation = "کسر کمیسون سفر  " + travel_data.travel_code;
    //       var cash_creditor_explanation = "افزایش اعتبار سفر  " + travel_data.travel_code;
    //       var date = moment().format('jYYYY/jMM/jDD');
    //       var time = moment().format('HH:mm');
    //       var readable_date = moment_persian().format('dddd jD jMMMM');
    //
    //       // create turn over income
    //
    //       turnover_dao.createTaxiTurnover(taxi_id, travel_id, income, "income", income_explanation, date, readable_date, time, function(result){});
    //
    //       if(travel_data.payment_type == "cash") {
    //
    //         turnover_dao.createTaxiTurnover(taxi_id, travel_id, commission_amount, "debtor", cash_deptor_explanation, date, readable_date, time, function(result){});
    //
    //         // decrease taxi credit
    //
    //         taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function(result){});
    //
    //         // set travel paid
    //
    //         travel_dao.setTravelPaid(travel_id, function(result){});
    //
    //       } else if (travel_data.payment_type == "wallet" && travel_data.is_paid) {
    //
    //         // add a turn over transaction
    //
    //         turnover_dao.createTaxiTurnover(taxi_id, travel_id, income, "creditor", cash_creditor_explanation, date, readable_date, time, function(result){});
    //
    //         // add credit to taxi
    //
    //         taxi_dao.increaseTaxiCredit(taxi_id, income, function(result){});
    //
    //
    //       } else if (travel_data.payment_type == "wallet" && !travel_data.is_paid) {
    //
    //         turnover_dao.createTaxiTurnover(taxi_id, travel_id, commission_amount, "debtor", cash_deptor_explanation, date, readable_date, time, function(result){});
    //
    //         // decrease taxi credit
    //
    //         taxi_dao.decreaseTaxiCredit(taxi_id, commission_amount, function(result){});
    //
    //         // change travel payment type
    //
    //         travel_dao.updateTravelPaymentType(travel_id, "cash", function(result){});
    //
    //         travel_dao.setTravelPaid(travel_id, function(result){});
    //
    //       }
    //
    //     });
    //
    //   });
    //

    // });


}

module.exports.getTravelsStatusByDate = function (req, callback) {

    var start_date = req.params.start_date;
    var finish_date = req.params.finish_date;
    start_date = utils.replaceDate(start_date, "_", "/");
    finish_date = utils.replaceDate(finish_date, "_", "/");

    travel_dao.getTravelsStatusByDate(start_date, finish_date, function (travels_stat) {

        console.log(travels_stat);
        if(travels_stat) {

            callback(travels_stat);

        }

    })

}


module.exports.getTravelInfo = function (req, callback) {

    if (req.body.travel_id) {

        var travel_id = req.body.travel_id;

    } else {

        var travel_id = req.params.travel_id;

    }

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if(travel_data) {

            taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

                if (taxi_data) {

                    var now = moment();

                    var start_time = moment(travel_data.full_date, "jYYYY/jMM/jDD HH:mm:ss");

                    var taxi_found_date = moment(travel_data.taxi_found_date, "jYYYY/jMM/jDD HH:mm:ss");

                    var timer_difference_time = now.diff(taxi_found_date, "seconds");

                    var timer_time = travel_data.taxi_timer_time - timer_difference_time;

                    if (timer_time < 0) {

                        timer_time = 0;

                    }

                    var difference_time = now.diff(start_time, "seconds");

                    var remaining_time = travel_data.time_travel - difference_time;

                    if (remaining_time < 0) {

                        remaining_time = 0;

                    }

                    var final_travel_data = travel_data.toObject();

                    final_travel_data.taxi_timer_time = timer_time;

                    final_travel_data.remaning_time = remaining_time;
                    final_travel_data.taxi_location_lat = taxi_data.location_lat;
                    final_travel_data.taxi_location_lan = taxi_data.location_lan;

                    callback(final_travel_data);

                } else {

                    callback(false);

                }

            })

        }



    });

};

module.exports.getDriverRefusedTravelsListByDate = function (req , callback) {

    var start_date = req.query.start_date;
    var taxi_id = req.query.taxi_id;

    start_date = start_date ? start_date : "1";

    var finish_date = req.query.finish_date;
    finish_date = finish_date ? finish_date : moment().format('jYYYY/jM/jD');

    start_date = utils.replaceDate(start_date, "_", "/");
    finish_date = utils.replaceDate(finish_date, "_", "/");


    travel_dao.getDriverRefusedTravelsListByDate(taxi_id, start_date , finish_date , function (refused_travel_list) {

        if(refused_travel_list)

        {
            callback(refused_travel_list);

        }


    });


};

module.exports.getDriverStandByTravelsListByDate = function (req , callback) {

    var start_date = req.query.start_date;
    var taxi_id = req.query.taxi_id;

    start_date = start_date ? start_date : "1";

    var finish_date = req.query.finish_date;
    finish_date = finish_date ? finish_date : moment().format('jYYYY/jM/jD');


    travel_dao.getDriverStandByTravelsListByDate(taxi_id, start_date , finish_date , function (standby_travel_list) {

        if(standby_travel_list)

        {
            callback(standby_travel_list);

        }


    });


};
