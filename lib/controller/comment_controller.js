var soap = require('soap');
var request = require("request");
var customer_dao = require('./../dao/customer_dao');
var wallet_dao = require('./../dao/wallet_dao');
var travel_dao = require('./../dao/travel_dao');
var comment_dao = require('./../dao/comment_dao');
var moment = require('moment-jalaali');

// define comment

module.exports.defineNewComment = function (req, callback) {

    if (req.body.text) {

        var text = req.body.text;
        var star = req.body.star;

        comment_dao.defineComment(text, star, function (new_comment_data) {
           
            if (new_comment_data) 
                callback (true); 
            else 
                callback (false);
        });


    } else callback (false);

};

// get comments type by star

module.exports.getCommentsByStar = function (req, callback) {

    var star = req.body.star;

    comment_dao.getCommentsByStar(star, function (comments) {

        callback({comments: comments, result: true});
    });

};

// get all comments type

module.exports.getComments = function (req, callback) {
    
    comment_dao.getAllComments(function (comments) {

        callback({comments: comments, result: true});
    });

};

// remove comment

module.exports.removeComment = function (req, callback) {

    var comment_id = req.params.comment_id;

    comment_dao.removeComment(comment_id, function (result) {

        callback(result);

    });

};