/**
 * Created by amir on 6/13/16.
 */

var ZarinpalCheckout = require('zarinpal-checkout');
var util = require('util');
var consts = require('../config/consts');

/**
 * Create ZarinPal
 * @param {String} `xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx` [Merchant ID]
 * @param {bool} false [toggle `Sandbox` mode]
 */
var zarinpal = ZarinpalCheckout.create(consts.PAYMENT_MERCHANT_ID, false);

// RAHSAFAR merchant id

// var zarinpal = ZarinpalCheckout.create('fd49a9ec-cb41-11e6-9f26-005056a205be', false);


// pay request
exports.paymentRequest = function (amount, callback_url, description, email, phone, callback) {

    /**
     * PaymentRequest [module]
     * @return {String} URL [Payment Authority]
     */
    zarinpal.PaymentRequest({
        Amount: amount,
        CallbackURL: callback_url,
        Description: description,
        Email: email,
        Mobile: phone
    }).then(function (response) {
        if (response.status == 100) {
            console.log(response);
            callback(response.url);
        } else {
            callback (false);
            console.log(response);
        }
    }).catch(function (err) {
        console.log(err);
        callback (false);
    });
};

// verification request
exports.paymentVerification = function (amount, authority, callback) {

    zarinpal.PaymentVerification({
        Amount: amount,
        Authority: authority,
    }).then(function (response) {
        if (response.status == -21) {
            console.log('Empty!');
            callback(false);
        } else {
            console.log('Yohoooo! ' + util.inspect(response, false, null));
            callback(response.RefID);
        }
    }).catch(function (err) {
        console.log(err);
    });
};

// unverified transaction
exports.unverifiedTransaction = function (callback) {

    zarinpal.UnverifiedTransactions().then(function (response) {
        if (response.status == 100) {
            console.log(response.authorities);
            callback(response.authorities);
        }
    }).catch(function (err) {
        console.log(err);
    });
};

