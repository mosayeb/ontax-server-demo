var soap = require('soap');
var mongoose = require('mongoose');
var customer_dao = require('./../dao/customer_dao');
var travel_dao = require('./../dao/travel_dao');
var taxi_dao = require('./../dao/taxi_dao');
var support_dao = require('./../dao/support_dao');
var map_tools = require('./../tools/map_tools');

module.exports.emptyRoom = function (io, room_id) {

    io.sockets.clients(room_id).forEach(function (s) {

        s.leave(room_id);

    });

    return(true);

};