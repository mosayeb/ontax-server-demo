var place_dao = require('./../dao/place_dao');
var map_tools = require('./../tools/map_tools');
var consts = require('./../config/consts');

module.exports.getLocationPlaceID = function (lat, lan, callback) {

  place_dao.getAllPlaces(function (all_places) {

    if (all_places.length >= 1) {

      var candidate_place = all_places[0];

      for (var i = 0; i < all_places.length; i++) {

        if (map_tools.findDistance(lan, lat, candidate_place.place_lan, candidate_place.place_lat) > map_tools.findDistance(lan, lat, all_places[i].place_lan, all_places[i].place_lat)) {

          candidate_place = all_places[i];

        }
      }

      callback(candidate_place._id);

    } else {

      callback(false);

    }

  });

};


module.exports.updateBaseFactor = function (req, callback) {

    var factor = parseInt(req.body.factor);

    place_dao.updateBaseFactor(factor, function (result) {

        if(result) {

            callback(true);

        } else {

            callback(true);

        }

    })

};

module.exports.getLocationPlaceData = function (lat, lan, callback) {

  place_dao.getLocationPlaceName(lat, lan, function (place_data) {

      if (place_data !== false) {

          callback(place_data);

      } else {

          callback(false);

      }
  });

  //
  // place_dao.getAllPlaces(function (all_places) {
  //
  //   if (all_places.length >= 1) {
  //
  //     var candidate_place = all_places[0];
  //
  //     for (var i = 0; i < all_places.length; i++) {
  //
  //       if (map_tools.findDistance(lan, lat, candidate_place.place_lan, candidate_place.place_lat) > map_tools.findDistance(lan, lat, all_places[i].place_lan, all_places[i].place_lat)) {
  //
  //         candidate_place = all_places[i];
  //
  //       }
  //     }
  //
  //     callback(candidate_place);
  //
  //   } else {
  //
  //     callback(false);
  //
  //   }
  //
  // });

};

module.exports.getLocationPlaceName = function (req, callback) {

  var lat = req.body.lat;
  var lan = req.body.lan;

  place_dao.getLocationPlaceName(lat, lan, function (place_data) {

    console.log(place_data);

    if(place_data !== false && place_data !== null) {

        callback({
            result: true,
            place_name: place_data.place_name,
            place_id: place_data._id
        });

    } else {

        callback({
            result: false,
            place_name: null,
            place_id: null
        });

    }

  });

  // var bound = map_tools.getSourceBound(parseFloat(lat), parseFloat(lan), consts.PLACE_SEARCH_RADIUS);
  //
  // place_dao.getPlaceBoundsLocation(parseFloat(bound.min_lat), parseFloat(bound.max_lat), parseFloat(bound.min_lan), parseFloat(bound.max_lan), function (places_list) {
  //
  //   if (places_list.length >= 1) {
  //
  //     var candidate_place = places_list[0];
  //
  //     for (var i = 0; i < places_list.length; i++) {
  //
  //       if (places_list[i].place_lan && places_list[i].place_lat) {
  //
  //         if (map_tools.findDistance(lan, lat, candidate_place.place_lan, candidate_place.place_lat) > map_tools.findDistance(lan, lat, places_list[i].place_lan, places_list[i].place_lat)) {
  //
  //           candidate_place = places_list[i];
  //
  //         }
  //
  //       }
  //     }
  //
  //     callback({
  //       result: true,
  //       place_name: candidate_place.place_name,
  //       place_id: candidate_place._id
  //     });
  //
  //   } else {
  //
  //     callback({result: false, place_name: "", place_id: ""});
  //
  //   }
  //
  // });


  // var lat = req.body.lat;
  // var lan = req.body.lan;
  //
  // var bound = map_tools.getSourceBound(parseFloat(lat), parseFloat(lan), consts.PLACE_SEARCH_RADIUS);
  //
  // place_dao.getPlaceBoundsLocation(parseFloat(bound.min_lat), parseFloat(bound.max_lat), parseFloat(bound.min_lan), parseFloat(bound.max_lan), function (places_list) {
  //
  //   if (places_list.length >= 1) {
  //
  //     var candidate_place = places_list[0];
  //
  //     for (var i = 0; i < places_list.length; i++) {
  //
  //       if (places_list[i].place_lan && places_list[i].place_lat) {
  //
  //         if (map_tools.findDistance(lan, lat, candidate_place.place_lan, candidate_place.place_lat) > map_tools.findDistance(lan, lat, places_list[i].place_lan, places_list[i].place_lat)) {
  //
  //           candidate_place = places_list[i];
  //
  //         }
  //
  //       }
  //     }
  //
  //     callback({
  //       result: true,
  //       place_name: candidate_place.place_name,
  //       place_id: candidate_place._id
  //     });
  //
  //   } else {
  //
  //     callback({result: false, place_name: "", place_id: ""});
  //
  //   }
  //
  // });

};

module.exports.findNearestPlaces = function (first_place_lan, first_place_lat, second_place_lan, second_place_lat, callback) {

  place_dao.getAllPlaces(function (all_places) {

    if (all_places.length >= 1) {

      var first_candidate_place = all_places[0];
      var second_candidate_place = all_places[0];

      for (var i = 0; i < all_places.length; i++) {

        if (map_tools.findDistance(first_place_lan, first_place_lat, first_candidate_place.place_lan, first_candidate_place.place_lat) > map_tools.findDistance(first_place_lan, first_place_lat, all_places[i].place_lan, all_places[i].place_lat)) {

          first_candidate_place = all_places[i];

        }

        if (map_tools.findDistance(second_place_lan, second_place_lat, second_candidate_place.place_lan, second_candidate_place.place_lat) > map_tools.findDistance(second_place_lan, second_place_lat, all_places[i].place_lan, all_places[i].place_lat)) {

          second_candidate_place = all_places[i];

        }
      }

      callback(first_candidate_place._id, second_candidate_place._id);

    } else {

      callback(false, false);

    }

  });

};

module.exports.removePlace = function (req, callback) {

  var place_id = req.body.place_id;

  place_dao.removePlace(place_id, function (remove_place_info) {

    if (remove_place_info) {

      place_dao.removePlaceFactors(place_id, function (remove_place_factors_result) {

        if (remove_place_factors_result) {

          callback(true);

        }

      });

    } else {

      callback(false);

    }

  });

};

module.exports.addNewPlace = function (req, callback) {

  var place_name = req.body.place_name;
  var place_lat = req.body.place_lat;
  var place_lan = req.body.place_lan;

  place_dao.addNewPlace(place_name, place_lan, place_lat, function (add_new_place_result) {

    if (add_new_place_result) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.updatePlace = function (req, callback) {

  var place_id = req.body.place_id;
  var place_name = req.body.place_name;
  var place_lat = req.body.place_lat;
  var place_lan = req.body.place_lan;

  place_dao.updatePlaceInfo(place_id, place_name, place_lat, place_lan, function (update_place_info) {

    if (update_place_info) {

      callback(true, place_id);

    } else {

      callback(false, place_id);

    }

  });

};

module.exports.removePlace = function (req, callback) {

  var place_id = req.body.place_id;

  place_dao.removePlace(place_id, function (remove_place_info) {

    if (remove_place_info) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.createNewFactor = function (req, callback) {

  var first_place_id = req.body.first_place_id;
  var second_place_id = req.body.second_place_id;
  var cost = req.body.cost;

  place_dao.createNewFactor(first_place_id, second_place_id, cost, function (create_factor_result) {

    if (create_factor_result) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.getPlacesList = function (req, callback) {

  var page = req.params.page;
  var per_page = req.params.per_page;

  place_dao.getPlacesList(page, per_page, function (result) {

    if (result) {

      callback(result);

    }

  });
};

module.exports.getAllPlaces = function (callback) {

  place_dao.getAllPlaces(function (places) {

    callback(places);

  });

};

module.exports.addNewFactor = function (req, callback) {

  var first_place_id = req.body.first_place_id;
  var second_place_id = req.body.second_place_id;
  var cost = req.body.cost;

  place_dao.addNewFactor(first_place_id, second_place_id, cost, function (add_factor_result, state) {

    if (add_factor_result) {

      callback(true, state);

    } else {

      callback(false, state);

    }

  });

};

module.exports.getFactorsList = function (req, callback) {

  var page = req.params.page;
  var per_page = req.params.per_page;

  place_dao.getFactorsList(page, per_page, function (result) {

    if (result) {

      var factors_list = [];

      if (result.length > 0) {

        for (var i = 0; i < result.length; i++) {
          (function (cntr) {

            place_dao.getPlaceInfo(result[cntr].first_place_id, function (first_place_data) {

                console.log(first_place_data);

              if (first_place_data) {

                place_dao.getPlaceInfo(result[cntr].second_place_id, function (second_place_data) {

                  if (second_place_data) {

                    var factor_obj = result[cntr].toObject();
                    factor_obj.first_place_name = first_place_data.place_name;
                    factor_obj.second_place_name = second_place_data.place_name;

                    factors_list.push(factor_obj);

                    if (cntr == result.length - 1) {

                      callback(factors_list);

                    }

                  } else {

                      if (cntr == result.length - 1) {

                          callback(factors_list);

                      }


                  }

                });

              } else {

                  if (cntr == result.length - 1) {

                      callback(factors_list);

                  }


              }

            });

          })(i);
        }
      } else {

        callback(factors_list);

      }

    }

  });

};

module.exports.getFactorCost = function (first_place_id, second_place_id, callback) {

  place_dao.getFactorData(first_place_id, second_place_id, function (factor_data) {

    if (factor_data) {

      callback(factor_data.cost);

    }

  });

};

module.exports.getFactorDataById = function (req, callback) {

  var factor_id = req.params.factor_id;

  place_dao.getFactorDataById(factor_id, function (factor_data) {

    if (factor_data) {

      callback(factor_data);

    }

  });

};

module.exports.editFactorCost = function (req, callback) {

  var factor_id = req.body.factor_id;
  var factor_cost = req.body.cost;

  place_dao.updateFactorCost(factor_id, factor_cost, function (update_factor_cost_result) {

    if (update_factor_cost_result) {

      callback(true);

    }

  })

};

module.exports.getPlaceInfo = function (req, callback) {

  var place_id = req.body.place_id;
  place_dao.getPlaceInfo(place_id, function (place_data) {

    callback(place_data);

  });

};

module.exports.searchPlaceName = function (req, callback) {

  if (req.body) {

    var text = req.body.text;

  } else {

    var text = req.query.query;

  }

  place_dao.searchPlaceName(text, function (result) {

    if (result) {

      callback(result);

    }

  });

};

module.exports.searchPlaceName‌ByPanel = function (req, callback) {

  var text = req.query.query;
  var places_result = {
    suggestions: []
  };

  place_dao.searchPlaceName(text, function (result) {

    if (result) {

      for(var i=0; i<result.length; i++) {

        places_result.suggestions.push({"value": result[i].place_name, "data": result[i]._id});

      }

      callback(places_result);

    }

  });

};

module.exports.getServiceFactor = function (callback) {

  place_dao.getServiceFactor(function (service_factor) {

    if(service_factor) {

      callback(service_factor);

    } else {

      callback(0);

    }

  });

};


module.exports.updateServiceFactor = function (req, callback) {

  var normal_factor = parseFloat(req.body.normal_factor);
  var vip_factor = parseFloat(req.body.vip_factor);
  var lady_factor = parseFloat(req.body.lady_factor);
  var stop_factor = parseInt(req.body.stop_factor);
  var input_factor = parseInt(req.body.input_factor);
  var two_way_factor = parseFloat(req.body.two_way_factor);

  place_dao.updateServiceFactor(input_factor, stop_factor, normal_factor, vip_factor, lady_factor, two_way_factor, function (result) {

    if(result) {

      callback(true);

    } else {

      callback(true);

    }

  })

};

module.exports.setTimeFactor = function (req, callback) {

    var factor_value = req.body.factor_value;
    var factor_time = req.body.factor_time;

    place_dao.setTimeFactor(factor_time, factor_value, function (result) {

        callback(result);

    })

};

module.exports.setDistanceFactor = function (req, callback) {

    var start_distance = req.body.start_distance;
    var end_distance = req.body.end_distance;
    var factor_value = req.body.factor_value;

    place_dao.setDistanceFactor(start_distance, end_distance, factor_value, function (result) {

        callback(result);

    })

};

module.exports.getAllTimeFactors = function (callback) {

    var factors_final = {
        hours: []
    };

    place_dao.getAllTimeFactors(function (factors) {


        for (var i=0; i<factors.length; i++) {

            var this_counter = factors[i].start_time.toString();
            factors_final.hours.push(factors[i].start_time);
            factors_final[this_counter] = factors[i].factor;

        }

        console.log(factors_final);

        callback(factors_final);

    })

};

module.exports.getAllDistanceFactors = function (callback) {

    var factors_final = {
      distances: []
    };

    place_dao.getAllDistanceFactors(function (factors) {


        for (var i=0; i<factors.length; i++) {

            var this_counter = factors[i].start_distance.toString();
            factors_final.distances.push(factors[i].start_distance);
            factors_final[this_counter] = factors[i].factor;

        }

        console.log(factors_final);

        callback(factors_final);

    })

};