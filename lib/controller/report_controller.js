var customer_dao = require('./../dao/customer_dao');
var taxi_dao = require('./../dao/taxi_dao');
var travel_dao = require('./../dao/travel_dao');
var turnover_dao = require('./../dao/turnover_dao');
var travelController = require('./../controller/travel_controller');
var utils = require('./../tools/utils');
var moment = require('moment-jalaali');
var async = require('async');

module.exports.getCanceledTravels = function (req , callback) {

    var start_date = req.query.start_date;
    start_date = start_date ? start_date : "1";

    var end_date = req.query.end_date;
    end_date = end_date ? end_date : moment().format('jYYYY/jM/jD');


    travel_dao.getCanceledTravelList(start_date , end_date , function (error , canceled_travel_list) {

        if(error)
        {
            callback(error);
            return;
        }

        callback(undefined , canceled_travel_list);

    });


};

module.exports.getTopTaxisRefusedTravels = function (req , callback) {

    var start_date = req.query.start_date;
    start_date = start_date ? start_date : "1";

    var finish_date = req.query.finish_date;
    finish_date = finish_date ? finish_date : moment().format('jYYYY/jM/jD');


    travel_dao.getTopTaxisRefusedTravels(start_date , finish_date , function (error , refused_travel_list) {

        if(error)

        {
            callback(error);
            return;
        }

        callback(undefined , refused_travel_list);

    });


};

module.exports.getTopTaxisStandByTravels = function (req , callback) {

    var start_date = req.query.start_date;
    start_date = start_date ? start_date : "1";

    var finish_date = req.query.finish_date;
    finish_date = finish_date ? finish_date : moment().format('jYYYY/jM/jD');

    travel_dao.getTopTaxisStandByTravels(start_date , finish_date , function (error , standby_travel_list) {

        if(error)

        {
            callback(error);
            return;
        }

        callback(undefined , standby_travel_list);

    });


};

module.exports.getAllStatistics = function (callback) {

    var all_stat = {
        customers_number: 0,
        taxis_number: 0,
        travels_stat: 0
    };

    // get customer info

    customer_dao.getCustomersCount(function (customers_number) {

        taxi_dao.getTaxisCount(function (taxis_number) {

            getTravelsStat(function (travels_stat) {


                all_stat.travels_stat = travels_stat;
                all_stat.taxis_number = taxis_number;
                all_stat.customers_number = customers_number;

                callback(all_stat);

            });

        });

    });

};

module.exports.getDashboardAllStatistics = function (callback) {

    var all_stat = {
        customers_number: 0,
        taxis_number: 0,
        travels_stat: 0
    };

    // get customer info

    customer_dao.getCustomersCount(function (customers_number) {

        taxi_dao.getTaxisCount(function (taxis_number) {

            getTravelsStat(function (travels_stat) {

                getTravelDateStat(function (date_travels) {

                    getLatestTravels(50, function (latest_travels) {

                        all_stat.travels_stat = travels_stat;
                        all_stat.taxis_number = taxis_number;
                        all_stat.customers_number = customers_number;
                        all_stat.date_travels = date_travels;
                        all_stat.latest_travels = latest_travels;
                        callback(all_stat);

                    });

                });

            });

        });

    });

    // restaurant_dao.getRestaurantsCount(function (restaurants_count) {
    //
    //     food_dao.getFoodsCount(function (foods_count) {
    //
    //         orderController.gettravelsStat(function (travels_stat) {
    //
    //             customer_dao.getCustomersCount(function (customers_count) {
    //
    //                 var all_stat = {
    //                     restaurants_number: restaurants_count,
    //                     foods_number: foods_count,
    //                     customers_number: customers_count,
    //                     travels_stat: travels_stat
    //                 };
    //
    //                 callback(all_stat);
    //
    //             })
    //
    //         })
    //
    //     })
    //
    // })

};

module.exports.getFinanceReport = function (req, callback) {

    var start_date = req.body.start_date;
    var finish_date = req.body.finish_date;


    var finance_report = {
        travels_number: 0,
        taxis: [],
        all_cost: 0,
        all_discount: 0
    };


    travel_dao.getTravelsListByDate(start_date, finish_date, function (travels) {

        // get money stats

        for (var i = 0; i < travels.length; i++) {

            if (travels[i].taxi_id !== undefined && travels[i].taxi_id !== "" && (travels[i].state == "finish_commented" || travels[i].state == "finished") && travels[i].is_paid) {

                finance_report.travels_number = 0;
                var is_taxi_new = false;
                var current_taxi_id;

                finance_report.all_cost = travels[i].cost;
                finance_report.all_discount = travels[i].discount_amount;


                for (var j = 0; j < finance_report.taxis.length; j++) {

                    if (travels[i].taxi_id.toString() == finance_report.taxis[j]._id.toString()) {

                        is_taxi_new = true;
                        current_taxi_id = j;

                    }

                }

                if (is_taxi_new) {

                    // add travels number and cost to taxi

                    finance_report.taxis[current_taxi_id].cost = travels[i].cost + finance_report.taxis[current_taxi_id].cost;
                    finance_report.taxis[current_taxi_id].discount_amount = travels[i].discount_amount + finance_report.taxis[current_taxi_id].discount_amount;
                    finance_report.taxis[current_taxi_id].travels_number = parseInt(finance_report.taxis[current_taxi_id].travels_number) + 1;

                } else {

                    // add taxi

                    finance_report.taxis.push({
                        cost: travels[i].cost,
                        _id: travels[i].taxi_id,
                        travels_number: 1,
                        discount_amount: travels[i].discount_amount
                    });

                }

            } else {

                continue;

            }
        }

        if (finance_report.taxis.length > 0) {

            for (var i = 0; i < finance_report.taxis.length; i++) {

                (function (cntr) {

                    console.log(finance_report.taxis[cntr]._id);
                    console.log(cntr);

                    taxi_dao.getTaxiInfo(finance_report.taxis[cntr]._id, function (taxi_data) {

                        if (taxi_data) {

                            finance_report.taxis[cntr].taxi_code = taxi_data.taxi_code;
                            finance_report.taxis[cntr].driver_name = taxi_data.driver_name;
                            finance_report.taxis[cntr].station_title = taxi_data.station_title;

                        }

                        if (cntr == finance_report.taxis.length - 1) {

                            console.log(finance_report);
                            callback(finance_report);

                        }

                    })

                })(i);

            }

        } else {

            callback(finance_report);

        }


    });

};

module.exports.getTopDriversIncomeByDate = function (req, callback) {

    var start_date = req.params.start_date;
    var finish_date = req.params.finish_date;
    start_date = utils.replaceDate(start_date, "_", "/");
    finish_date = utils.replaceDate(finish_date, "_", "/");
    var taxis_final_list = [];

    turnover_dao.getTopDriversIncomeByDate(start_date, finish_date, function (drivers_income) {

        console.log(drivers_income);

        if (drivers_income.length > 0) {

            async.eachSeries(drivers_income,

                function (item, callback) {

                    taxi_dao.getTaxiInfo(item._id, function (taxi_data) {

                        var final_taxi_data = taxi_data.toObject();
                        final_taxi_data.income = item.income;

                        if (final_taxi_data) {

                            taxis_final_list.push(final_taxi_data);

                        }

                        callback(null);

                    })
                },
                
                function (err, result) {

                    if (err) {

                        callback(false);

                    } else {

                        callback(taxis_final_list);
                    }

                    //No errors, move on with your code..
                });


        } else {

            callback(taxis_final_list);

        }

    })

};

function getTravelDateStat(callback) {

    var time = moment().add(-20, 'days').format('jYYYY/jMM/jDD');
    var final_stat = [];

    travel_dao.getTravelsDateStat(time, function (stat) {

        callback(stat);

    })

}

function getLatestTravels(limit, callback) {


    travel_dao.getLatestTravels(limit, function (travels) {

        callback(travels);

    })

}

function getTravelsStat(callback) {

    travel_dao.getAllTravels(function (travels) {

        var travels_stat = {};
        travels_stat.all_cost = 0;
        travels_stat.all_discount = 0;
        travels_stat.support_canceled_number = 0;
        travels_stat.taxi_canceled_number = 0;
        travels_stat.customer_canceled_number = 0;
        travels_stat.finished_number = 0;
        travels_stat.finish_commented_number = 0;
        travels_stat.taxi_not_found_number = 0;
        travels_stat.travel_started_number = 0;
        travels_stat.taxi_founded_number = 0;
        travels_stat.none_number = 0;
        travels_stat.searching_number = 0;
        travels_stat.put_off_searching_number = 0;

        // calculate travels number

        travels_stat.travels_number = travels.length;

        // travels by state

        for (var i = 0; i < travels.length; i++) {

            if (travels[i].state == "customer_canceled") {

                travels_stat.customer_canceled_number = travels_stat.customer_canceled_number + 1;

            } else if (travels[i].state == "support_canceled") {

                travels_stat.support_canceled_number = travels_stat.support_canceled_number + 1;

            } else if (travels[i].state == "taxi_refused") {

                travels_stat.taxi_refused_number = travels_stat.taxi_refused_number + 1;

            } else if (travels[i].state == "taxi_confirmed") {

                travels_stat.taxi_refused_number = travels_stat.taxi_refused_number + 1;

            } else if (travels[i].state == "travel_started") {

                travels_stat.taxi_confirmed_number = travels_stat.taxi_confirmed_number + 1;

            } else if (travels[i].state == "searching") {

                travels_stat.searching_number = travels_stat.searching_number + 1;

            } else if (travels[i].state == "finished") {

                travels_stat.all_cost = travels_stat.all_cost + parseInt(travels[i].cost);
                travels_stat.all_discount = travels_stat.all_discount + parseInt(travels[i].discount_amount);
                travels_stat.finished_number = travels_stat.finished_number + 1;

            } else if (travels[i].state == "finish_commented") {

                travels_stat.all_cost = travels_stat.all_cost + parseInt(travels[i].cost);
                travels_stat.all_discount = travels_stat.all_discount + parseInt(travels[i].discount_amount);
                travels_stat.finish_commented_number = travels_stat.finish_commented_number + 1;

            } else if (travels[i].state == "taxi_not_found") {

                travels_stat.taxi_not_found_number = travels_stat.taxi_not_found_number + 1;

            } else if (travels[i].state == "taxi_canceled") {

                travels_stat.taxi_canceled_number = travels_stat.taxi_canceled_number + 1;

            } else if (travels[i].state == "taxi_founded") {

                travels_stat.taxi_founded_number = travels_stat.taxi_founded_number + 1;

            } else if (travels[i].state == "none") {

                travels_stat.none_number = travels_stat.none_number + 1;

            } else if (travels[i].state == "put_off_searching") {

                travels_stat.put_off_searching_number = travels_stat.put_off_searching_number + 1;

            }

        }


        callback(travels_stat);

    })

}