var promotion_dao = require('./../dao/promotion_dao');
var travel_dao = require('./../dao/travel_dao');
var customer_dao = require('./../dao/customer_dao');
var wallet_dao = require('./../dao/wallet_dao');
var place_dao = require('./../dao/place_dao');
var sms_handler = require('./../handler/sms_handler');
var moment = require('moment-jalaali');
var consts = require('../config/consts');
var shortid = require('shortid');


module.exports.newDiscount = function (req, callback) {

  console.log(req.body);

  var amount = req.body.amount;
  var discount_code = req.body.code;
  var description = req.body.description;
  var is_active = req.body.is_active;
  var place_id = req.body.place_id;
  var days = req.body.days;
  var is_global = req.body.is_global;
  var max_cost = req.body.max_cost;
  var count = req.body.count;

  promotion_dao.defineNewDiscount(amount, discount_code, description, is_active, place_id, days, max_cost, is_global, count, function (new_discount_data) {

    callback(new_discount_data);

  });

};

module.exports.checkDiscount = function (req, callback) {

  console.log("START CHECK DISCOUNT");
  console.log(req.body);

  var place_id = req.body.place_id;
  var customer_id = req.body.customer_id;
  var discount_code = req.body.discount_code;
  var travel_cost = req.body.travel_cost;
  if(!travel_cost) {

    travel_cost = req.body.cost;

  }

  console.log('there is here 1');

  if (req.body.place_id && req.body.discount_code) {

    promotion_dao.getDiscountInfoByCode(discount_code, function (discount_data) {

      if (discount_data) {

        console.log('there is here 2');

        console.log(discount_data);

        if (((Date.now() - discount_data.time_stamp) < (consts.ONE_DAY * discount_data.days)) && discount_data.is_active) {

          if(discount_data.is_global) {

            console.log(discount_data.amount);
            console.log(travel_cost);


            var discount_amount = travel_cost * (discount_data.amount / 100);

            console.log("BEFORE IF");
            console.log(discount_amount);

            if(discount_amount >= discount_data.max_cost)
              discount_amount = discount_data.max_cost;

            console.log("BEFORE ROUNDED");
            console.log(discount_amount);

            discount_amount = Math.ceil(discount_amount/100)*100;

            console.log(discount_amount);

            travel_dao.getCustomerDiscountTravels(customer_id, discount_code, function (travels) {

              if(travels) {

                console.log(travels);
                console.log("WE ARE HERE 5");

                if(discount_data.count > travels.length) {

                  callback(discount_amount);

                } else {

                  callback(false);

                }

              }

            });

          } else {

            if (discount_data.place_ids.indexOf(place_id) > -1) {

              var discount_amount = travel_cost * (discount_data.amount / 100);

              if(discount_amount >= discount_data.max_cost)
                discount_amount = discount_data.max_cost;

              discount_amount = Math.ceil(discount_amount/100)*100;

              // check discount number

              travel_dao.getCustomerDiscountTravels(customer_id, discount_code, function (travels) {

                if(travels) {

                  console.log(travels);

                  if(discount_data.count > travels.length) {

                    callback(discount_amount);

                  } else {

                    callback(false);

                  }

                }

              });


            } else {

              callback(false);
            }

          }

        } else {

          callback(false);
        }

      } else {

        callback(false);
      }
    });
  } else {

    callback(false);

  }

};

module.exports.addMoneyToCallerAndCustomerAfterFirstTravel = function (travel_data, callback) {

  customer_dao.getCustomerInfoById(travel_data.customer_id, function (customer_data) {

    console.log("WE STAY HERE 1");

    if (customer_data) {

      console.log("WE STAY HERE 2");

      travel_dao.hasCustomerAnyTravelUntilNow(customer_data._id, function (has_customer_any_travel) {

        console.log("WE STAY HERE 3");

        if ((has_customer_any_travel.length == 1) && customer_data.caller_number && (customer_data.invite_code != customer_data.caller_number)) {

          console.log("WE STAY HERE 4");

          customer_dao.getCustomerInfoByInviteCode(customer_data.caller_number, function (caller_data) {

            console.log("WE STAY HERE 5");

            if (caller_data) {

              console.log("WE STAY HERE 6");

              wallet_dao.getWalletInfoByCustomerId(caller_data._id, function (caller_wallet_data) {

                console.log("WE STAY HERE 7");

                if (caller_wallet_data) {

                  wallet_dao.saveTransaction(caller_data._id, caller_wallet_data._id, '', '', consts.CALLER_PROMOTION_GIFT, consts.CALLER_PROMOTION_DESCRIPTION, true, function (caller_transaction_data) {

                    console.log("WE STAY HERE 8");

                    if (caller_transaction_data) {

                      wallet_dao.incrementWalletMoney(caller_wallet_data._id, consts.CALLER_PROMOTION_GIFT, function (add_money_caller_data) {

                        console.log("WE STAY HERE 9");

                        if (add_money_caller_data) {

                          // send sms to caller

                          console.log("WE STAY HERE 10");

                          sms_handler.sendPromotionSmsToCaller(caller_data.phone_number, customer_data.phone_number, function (sms_data) {});



                        }
                      });
                    }
                  });
                }
              });

              wallet_dao.getWalletInfoByCustomerId(customer_data._id, function (customer_wallet_data) {

                console.log("WE STAY HERE 11");

                if (customer_wallet_data) {

                  wallet_dao.saveTransaction(customer_data._id, customer_wallet_data._id, '', '', consts.CALLER_PROMOTION_GIFT, consts.CALLER_PROMOTION_DESCRIPTION, true, function (customer_transaction_data) {

                    console.log("WE STAY HERE 12");

                    if (customer_transaction_data) {

                      wallet_dao.incrementWalletMoney(customer_wallet_data._id, consts.CALLER_PROMOTION_GIFT, function (add_money_customer_data) {

                        console.log("WE STAY HERE 13");

                        if (add_money_customer_data) {

                          // send sms to invited user

                          console.log("WE STAY HERE 14");

                          sms_handler.sendPromotionSmsToInvited(caller_data.phone_number, customer_data.phone_number, function (sms_data) {});

                          callback(true);

                        }
                        
                      });
                    }
                  });
                }
              });

            } else callback(false);
          });

        } else callback(false);
      });

    } else callback(false);
  });
};

module.exports.getDiscountsList = function (req, callback) {

  var page = req.params.page;
  var per_page = req.params.per_page;

  promotion_dao.getDiscountsList(page, per_page, function (discounts) {

    if (discounts) {

      callback(discounts);

    }

  });


};

module.exports.getDiscountInfo = function (req, callback) {

  var discount_id = req.params.discount_id;

  promotion_dao.getDiscountInfoById(discount_id, function (discount_data) {

    if (discount_data) {

      callback(discount_data);

    } else {

      callback(false);

    }

  });

};

module.exports.getDiscountFullInfo = function (req, callback) {

  var discount_id = req.params.discount_id;
  var places_list = [];

  promotion_dao.getDiscountInfoById(discount_id, function (discount_data) {

    if (discount_data) {

      var place_ids = discount_data.place_ids;

      for (var i = 0; i < place_ids.length; i++) {

        (function (cntr) {

          place_dao.getPlaceInfo(place_ids[i], function (place_data) {

            if(place_data) {

              places_list.push(place_data);

              if (cntr == place_ids.length - 1) {

                callback(discount_data, places_list);

              }

            } else {

              if (cntr == place_ids.length - 1) {

                callback(discount_data, places_list);

              }

            }

          });

        })(i);

      }

    } else {

      callback(false, false);

    }

  });

};

module.exports.editDiscount = function (req, callback) {

  var discount_id = req.body.discount_id;
  var days = req.body.days;
  var description = req.body.description;
  var is_active = req.body.is_active;
  var max_cost = req.body.max_cost;
  var is_active = req.body.is_active;

  promotion_dao.editDiscount(discount_id, days, description, is_active, max_cost, function (discount_data) {

    if (discount_data) {

      callback(true);

    } else {

      callback(false);

    }

  });

};

module.exports.addDiscountPlace = function (req, callback) {

  var discount_id = req.body.discount_id;
  var place_id = req.body.place_id;

  promotion_dao.addDiscountPlace(discount_id, place_id, function (result) {

    if (result) {

      place_dao.getPlaceInfo(place_id, function (place_data) {

        callback({result: true, place_data: place_data})

      });

    }

  });

};

module.exports.removeDiscountPlace = function (req, callback) {

  var discount_id = req.body.discount_id;
  var place_id = req.body.place_id;

  promotion_dao.removeDiscountPlace(discount_id, place_id, function (result) {

    callback(result);

  });

};