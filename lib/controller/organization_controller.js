var soap = require('soap');
var request = require("request");
var organization_dao = require('./../dao/organization_dao');
var travel_dao = require('./../dao/travel_dao');
var taxi_dao = require('./../dao/taxi_dao');
var place_dao = require('./../dao/place_dao');
var aa = require('./../aa/aa');
var support_dao = require('./../dao/support_dao');
var map_tools = require('./../tools/map_tools');
var image_tools = require('./../tools/image_tools');
var sms_handler = require('./../handler/sms_handler');
var supportController = require('./support_controller');
var travelController = require('./travel_controller');
var placeController = require('./place_controller');
var consts = require('./../config/consts');
var moment = require('moment-jalaali');
var gm = require('gm');
var easyimg = require('easyimage');
var PNG = require('pngjs').PNG;
var fs = require('fs');
var crypto = require('crypto');
var passwordHash = require("password-hash");

var imageMagick = gm.subClass({imageMagick: true});

module.exports.registerOrganization = function (req, callback) {

    var username = req.body.username;
    var password = req.body.password;
    var organization_name = req.body.organization_name;
    var email = req.body.email;
    var address = req.body.address;
    var phone_number = req.body.phone_number;
    var hashed_password = passwordHash.generate(password);
    organization_dao.registerOrganization(username,hashed_password,organization_name,email,address,phone_number,function(result){
        callback(result);
    })
};

module.exports.loginOrganization = function(req, callback){

    var username = req.body.username;
    var password = req.body.password;

    organization_dao.getOrganizationByUsername(username, function(organization_data){

        if(organization_data) {
            if(passwordHash.verify(password, organization_data.password)) {
                var token = aa.createNewToken(organization_data._id);
                console.log(token);
                organization_data = organization_data.toObject();

                organization_data.token = token;

                console.log("ORGANIZATION DATA COMMING: ");
                console.log(organization_data);

                callback({result: true, organization_data: organization_data});

            } else {

                callback({result: false, organization_data: null});

            }

        } else {

            callback({result: false, organization_data: null});

        }



    });

};

module.exports.updateCode = function (req, callback) {

    var phone_number = req.body.phone_number;

    sms_handler.sendVerifyCode(phone_number, function (result, verification_code) {

        customer_dao.updateCode(phone_number, verification_code, function (result) {

            if(result) {

                callback({result: true});

            } else {

                callback({result: false});

            }


        });

    });
};

module.exports.verifyCode = function (req, callback) {

    var phone_number = req.body.phone_number;
    var verification_code = req.body.verification_code;

    organization_dao.verifyCode(phone_number, verification_code, function (is_code_verified) {

        if (is_code_verified) {

            organization_dao.updateCode(phone_number, verification_code, function (result) {

                if (result) {

                    organization_dao.getOrganizationInfo(phone_number, function (organization_data) {

                        if (organization_data) {

                            support_dao.getCurrentNotify(function (notify) {

                                var token = aa.createNewToken(organization_data._id);

                                organization_data = organization_data.toObject();

                                organization_data.current_notify_id = notify.current_id;

                                organization_data.token = token;

                                callback({result: true, organization_data: organization_data});

                            });
                        }

                    });
                }

            });

        } else {

            callback({result: false, organization_data: {}});

        }

    });

};

module.exports.calculateTravelCost = function (req, callback) {

    var organization_id = req.body.organization_id;
    var source_lat = req.body.source_lat;
    var source_lan = req.body.source_lan;
    var destination_lat = req.body.destination_lat;
    var destination_lan = req.body.destination_lan;


    calculateCost(source_lan, source_lat, destination_lan, destination_lat, function (travel_cost) {

        if (travel_cost) {

            callback({

                result: true,
                travel_cost: travel_cost

            });
        }

    });

};

module.exports.saveTravel = function (req, callback) {

    // create travel

    findNearestTaxi(travel_id, function (taxi_id) {

        if (taxi_id) {

            calculateTimeTravel(travel_id, taxi_id, function (time_travel) {

                // taxi with taxi_id founded

                travel_dao.updateTimeTravel(travel_id, time_travel, function (result) {

                    if (result) {

                        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                            var r = taxi_data.toObject();
                            r.time_travel = time_travel;

                            var taxi_data = {
                                taxi_data: r,
                                result: true
                            };

                            callback(taxi_data);

                        });
                    }

                });


            });

        } else {

            // no taxi founded

            callback({result: false});

        }

    });

};

module.exports.travelsList = function (req, callback) {

    if (req.method == 'GET') {

        var organization_id = req.params.organization_id;
        var page = req.params.page;
        var per_page = req.params.per_page;

    } else {

        var organization_id = req.body.organization_id;
        var page = req.body.page;
        var per_page = req.body.per_page;

    }
    var final_travels_list = [];

    travel_dao.getOrganizationTravels(organization_id, page, per_page, function (travels_list) {

        if (travels_list) {

            var len = travels_list.length;

            if (len > 0) {

                for (var i = 0; i < len; i++) {
                    (function (cntr) {

                        var travel_obj = travels_list[cntr].toObject();
                        var taxi_id = travel_obj.taxi_id;

                        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

                            if (taxi_data) {

                                travel_obj.taxi_code = taxi_data.taxi_code;
                                final_travels_list.push(travel_obj);

                                if (cntr === len - 1) {
                                    callback(final_travels_list);
                                }

                            }

                        });

                    })(i);
                }

            } else {

                callback(final_travels_list);

            }

        } else {

            callback(final_travels_list);

        }

    });

};

module.exports.trackTravelTaxi = function (data, socket, callback) {

    var travel_id = data.travel_id;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data) {

            taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

                if (travel_data.state == "taxi_confirmed") {

                    var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                    var now = moment();

                    var difference_time = now.diff(start_time, "seconds");

                    var remaining_time = travel_data.time_travel - difference_time;

                    if (remaining_time < 0) {

                        remaining_time = 1;

                    }

                    var remaining_time = Math.floor(remaining_time / 60);

                    var taxi_location = {

                        _id: taxi_data._id,
                        location_lat: taxi_data.location_lat,
                        location_lan: taxi_data.location_lan,
                        remaining_time: remaining_time
                    };

                    callback({result: true, taxi_location: taxi_location});

                } else {

                    var taxi_location = {
                        _id: taxi_data._id,
                        location_lat: taxi_data.location_lat,
                        location_lan: taxi_data.location_lan,
                        remaining_time: 1
                    };

                    callback({result: false, taxi_location: taxi_location});

                }

            });

        }

    });

};

module.exports.updateInfo = function (req, callback) {




    var organization_id = req.body.organization_id;
    var email = req.body.email;
    var address = req.body.address;
    var organization_name = req.body.organization_name;
    var emergency_phone_number = req.body.emergency_phone_number;
    var phone_number = req.body.phone_number;

    organization_dao.updateOrganizationInfo(organization_id, email, address, organization_name,  emergency_phone_number,phone_number , function (result) {

        if (result) {

            callback(true);

        }

    });

};

module.exports.getOrganizationInfo = function (req, callback) {

    if (req.method == "POST") {
        var organization_id = req.body.organization_id;
    } else {
        var organization_id = req.params.organization_id;
    }
    organization_dao.getOrganizationInfo(organization_id, function (organization_data) {

        if (organization_data) {

            callback(organization_data);

        }

    });

};

module.exports.getOrganizationsList = function (req, callback) {

    var page = req.params.page;
    var per_page = req.params.per_page;

    organization_dao.getOrganizationsList(page, per_page, function (result) {

        if (result) {

            callback(result);

        }

    });

};

module.exports.editOrganization = function (req, callback) {

    var organization_id = req.body.organization_id;
    var name = req.body.name;
    var address = req.body.address;
    var email = req.body.email;

    organization_dao.updateOrganizationInfo(organization_id, email, address, name, function (edit_result) {

        if (edit_result) {
            callback(edit_result);
        }

    });

};

module.exports.search = function (req, callback) {

    var search_query = req.body.search_query;

    organization_dao.search(search_query, function (search_result) {

        if (search_result) {

            var organizations_list = [];

            var len = search_result.length;

            if (len > 0) {
                for (var i = 0; i < len; i++) {
                    (function (cntr) {

                        var organization_obj = search_result[cntr];
                        var organization_id = organization_obj._id;

                        organization_dao.getOrganizationInfoById(organization_id, function (organization_data) {

                            if (organization_data) {
                                organizations_list.push(organization_data);
                            }

                            if (cntr === len - 1) {
                                callback(organizations_list);
                            }
                        });


                    })(i);
                }
            } else {
                callback(organizations_list);
            }
        }

    });

};

module.exports.organizationOpenedReconnect = function (data, io, socket, callback) {

    var travel_id = data.travel_id;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data.state == "taxi_not_found" || travel_data.state == "none") {

            callback({travel_data: travel_data, taxi_data: {}, result: true});

        } else {

            taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

                var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                var now = moment();
                var difference_time = now.diff(start_time, "seconds");

                var remaining_time = travel_data.time_travel - difference_time;

                if (remaining_time < 0) {

                    remaining_time = 0;

                }

                travel_dao.isTravelActive(travel_id, function (travel_is_active) {


                    if (travel_is_active) {
                        socket.join(travel_id);
                    }

                    var final_travel_data = travel_data.toObject();
                    final_travel_data.remaning_time = remaining_time;

                    callback({travel_data: final_travel_data, taxi_data: taxi_data, result: true});

                });

            });
        }

    });

};

module.exports.organizationAppOpened = function (data, io, socket, callback) {

    var travel_id = data.travel_id;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data.state == "taxi_not_found" || travel_data.state == "none") {

            callback({travel_data: travel_data, taxi_data: {}, result: true});

        } else {

            taxi_dao.getTaxiInfo(travel_data.taxi_id, function (taxi_data) {

                var start_time = moment(travel_data.full_date, "jYYYY/jM/jD HH:mm:ss");

                var now = moment();

                var difference_time = now.diff(start_time, "seconds");

                var remaining_time = travel_data.time_travel - difference_time;

                if (remaining_time < 0) {

                    remaining_time = 0;

                }

                var final_travel_data = travel_data.toObject();

                final_travel_data.remaning_time = remaining_time;

                callback({travel_data: final_travel_data, taxi_data: taxi_data, result: true});

            });

        }

    });

};

module.exports.organizationClosedReconnect = function (data, io, socket) {

    var travel_id = data.travel_id;
    var organization_travel_state = data.travel_state;

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        var current_travel_state = travel_data.state;
        var taxi_id = travel_data.taxi_id;

        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

            travel_dao.isTravelActive(travel_id, function (is_travel_active) {

                if (is_travel_active) {

                    socket.join(travel_id);

                }

                if (current_travel_state !== organization_travel_state) {

                    if (current_travel_state == "taxi_confirmed") {

                        io.sockets.in(travel_id).emit('taxi_confirmed', {result: travel_data});

                    } else if (current_travel_state == "taxi_canceled") {

                        socket.emit('taxi_canceled_travel', travel_id);

                    } else if (current_travel_state == "finished") {

                        socket.emit('travel_finished', travel_id);

                    } else if (current_travel_state == "support_canceled") {

                        socket.emit('support_canceled_travel', travel_id);

                    } else if (current_travel_state == "taxi_not_found") {

                        socket.emit('taxi_not_found', {result: {travel_id: travel_id}});

                    } else if (current_travel_state == "travel_started") {

                        var data = {
                            travel_data: travel_data,
                            taxi_data: taxi_data
                        };

                        socket.emit('travel_started', {result: data});

                    } else if (current_travel_state == "put_off_searching") {

                        socket.emit('taxi_put_off_travel', travel_id);

                    }
                }

            });

        });
    });

};

module.exports.checkAppVersion = function (req, callback) {
    var customer_current_version = parseInt(req.body.current_version);

    support_dao.getSystemActivated(function (active_data) {

        if (active_data.is_active == false) {

            callback({result: "not_active", message: active_data.message});

        } else {

            supportController.getAppCurrentVersion(function (app_version) {

                if (app_version.version_code == customer_current_version) {

                    callback({result: "no_update", message: ""});

                } else if (app_version.version_code > customer_current_version) {

                    if (app_version.is_force_update) {

                        callback({result: "force_update", message: "", url: app_version.url});

                    } else if (app_version.last_force_version > customer_current_version) {

                        callback({result: "force_update", message: "", url: app_version.url});

                    } else {

                        callback({result: "normal_update", message: "", url: app_version.url});

                    }

                }

            });

        }

    });

};

// get taxis around man
module.exports.getOrganizationAroundTaxis = function (req, callback) {

    var location_lat = req.body.lat;
    var location_lan = req.body.lan;
    var organization_id = req.body.organization_id;

    // var around_taxis = [];

    // var bound = map_tools.getSourceBound(location_lat, location_lan, consts.TAXI_SEARCH_RADIUS);

    // taxi_dao.getTaxisBoundsLocation(parseFloat(bound.min_lat), parseFloat(bound.max_lat), parseFloat(bound.min_lan), parseFloat(bound.max_lan), function (taxis_list) {

    taxi_dao.getOrganizationAroundTaxis(organization_id,parseFloat(location_lat), parseFloat(location_lan), function (taxis_list) {

        if (taxis_list.length >= 1) {

            callback({result: true, taxis_list: taxis_list});

        } else {

            callback({result: false, taxis_list: false});

        }

    });

    // console.log(req.body);
    //
    // taxi_dao.getTaxisLocation(function (taxis_list) {
    //
    //     console.log(taxis_list.length);
    //
    //     if(taxis_list.length >= 1) {
    //
    //         for (var i = 0; i < taxis_list.length; i++) {
    //
    //             var taxi_distance = map_tools.findDistance(location_lat, location_lan, taxis_list[i].location_lan, taxis_list[i].location_lat);
    //
    //             if(taxi_distance < 1000000) {
    //
    //                 around_taxis.push(taxis_list[i]);
    //
    //             }
    //
    //         }
    //
    //         callback({result: true, taxis_list: taxis_list});
    //
    //     } else {
    //
    //         callback({result: false, taxis_list: false});
    //
    //     }
    //
    // });

};
module.exports.getAroundTaxis = function (req, callback) {

    var location_lat = req.body.lat;
    var location_lan = req.body.lan;

    // var around_taxis = [];

    // var bound = map_tools.getSourceBound(location_lat, location_lan, consts.TAXI_SEARCH_RADIUS);

    // taxi_dao.getTaxisBoundsLocation(parseFloat(bound.min_lat), parseFloat(bound.max_lat), parseFloat(bound.min_lan), parseFloat(bound.max_lan), function (taxis_list) {


    taxi_dao.getAroundTaxis(parseFloat(location_lat), parseFloat(location_lan), function (taxis_list) {

        if (taxis_list.length >= 1) {

            callback({result: true, taxis_list: taxis_list});

        } else {

            callback({result: false, taxis_list: false});

        }

    });

    // console.log(req.body);
    //
    // taxi_dao.getTaxisLocation(function (taxis_list) {
    //
    //     console.log(taxis_list.length);
    //
    //     if(taxis_list.length >= 1) {
    //
    //         for (var i = 0; i < taxis_list.length; i++) {
    //
    //             var taxi_distance = map_tools.findDistance(location_lat, location_lan, taxis_list[i].location_lan, taxis_list[i].location_lat);
    //
    //             if(taxi_distance < 1000000) {
    //
    //                 around_taxis.push(taxis_list[i]);
    //
    //             }
    //
    //         }
    //
    //         callback({result: true, taxis_list: taxis_list});
    //
    //     } else {
    //
    //         callback({result: false, taxis_list: false});
    //
    //     }
    //
    // });

};

module.exports.updateAvatar = function (req, callback) {

    var organization_id = req.body.organization_id;
    var photo = req.body.image;
    var photo_path = "public/images/";

    var base64Data = photo.replace(/^data:image\/png;base64,/, "");

    var seed = crypto.randomBytes(20);
    var uniqueSHA1String = crypto
        .createHash('sha256')
        .update(seed)
        .digest('hex');

    fs.writeFile(photo_path + uniqueSHA1String + ".png", base64Data, 'base64', function (err) {

        photo_path = photo_path + uniqueSHA1String + ".png";

        imageMagick(photo_path).resize(300, 300).quality(100).write(photo_path, function (err) {

            if (!err) {

                fs.createReadStream(photo_path)
                    .pipe(new PNG({
                        filterType: 4
                    }))
                    .on('parsed', function () {
                        for (var y = 0; y < this.height; y++) {
                            for (var x = 0; x < this.width; x++) {
                                var idx = (this.width * y + x) << 2;
                                var radius = this.height / 2;
                                if (y >= Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2)) + radius || y <= -(Math.sqrt(Math.pow(radius, 2) - Math.pow(x - radius, 2))) + radius) {
                                    this.data[idx + 3] = 0;
                                }
                            }
                        }

                        this.pack().pipe(fs.createWriteStream(photo_path));

                        organization_dao.updateAvatar(organization_id, photo_path, function (result) {

                            if (result) {

                                callback({result: true, avatar: result});

                            } else {

                                callback({result: false, avatar: ""});

                            }

                        });

                    });

            } else {

                callback(false);

            }

        });

    });

};



function calculateCost(source_lan, source_lat, destination_lan, destination_lat, callback) {

    placeController.getLocationPlaceData(source_lat, source_lan, function (source_place_data) {

        placeController.getLocationPlaceData(destination_lat, destination_lan, function (destination_place_data) {

            place_dao.getFactorData(source_place_data._id, destination_place_data._id, function (factor_cost) {

                if (factor_cost.cost) {

                    callback(factor_cost.cost);

                } else {

                    map_tools.findGoogleDistance(source_lan, source_lat, destination_lan, destination_lat, function (distance_in_meters) {

                        // if( 21 <= moment().hour() <= 7) {
                        //
                        //     var cost = distance_in_meters * 1.2;
                        //     cost = 500 * Math.round(cost/500);
                        //
                        // } else if( 16 <= moment().hour() <= 19 ) {
                        //
                        //     var cost = distance_in_meters * 1.2;
                        //     cost = 500 * Math.round(cost/500);
                        //
                        // } else {
                        //
                        //     var cost = distance_in_meters * 1;
                        //     cost = 500 * Math.round(cost/500);
                        //
                        // }

                        // var cost = distance_in_meters * 1;
                        // cost = 500 * Math.round(cost / 500);
                        //
                        // var final_cost = cost + consts.BASE_TRAVEL_COST;
                        //
                        // if (distance_in_meters > 8000) {
                        //
                        //   final_cost = final_cost * 0.8;
                        //
                        // }

                        var current_hour = moment().hour();

                        place_dao.getTimeFactor(current_hour, function (time_factor) {

                            place_dao.getDistanceFactor(distance_in_meters, function (distance_factor) {

                                if(distance_factor) {

                                    if(time_factor) {

                                        var cost = ( distance_in_meters / 1000 ) * time_factor.factor * distance_factor.factor;

                                        cost = 500 * Math.round(cost / 500);

                                        var final_cost = cost + consts.BASE_TRAVEL_COST;

                                        callback(final_cost);

                                    } else {


                                        place_dao.getBaseFactor(function (base_factor) {


                                            var cost = ( distance_in_meters / 1000 ) * base_factor.factor * distance_factor.factor;

                                            cost = 500 * Math.round(cost / 500);

                                            var final_cost = cost + consts.BASE_TRAVEL_COST;

                                            callback(final_cost);

                                        })

                                    }

                                } else {

                                    if(time_factor) {

                                        var cost = ( distance_in_meters / 1000 ) * time_factor.factor;

                                        cost = 500 * Math.round(cost / 500);

                                        var final_cost = cost + consts.BASE_TRAVEL_COST;

                                        callback(final_cost);

                                    } else {


                                        place_dao.getBaseFactor(function (base_factor) {


                                            var cost = ( distance_in_meters / 1000 ) * base_factor.factor;

                                            cost = 500 * Math.round(cost / 500);

                                            var final_cost = cost + consts.BASE_TRAVEL_COST;

                                            callback(final_cost);

                                        })

                                    }

                                }

                            });

                            // if(time_factor) {
                            //
                            //   var cost = ( distance_in_meters / 1000 ) * time_factor.factor;
                            //
                            //   cost = 500 * Math.round(cost / 500);
                            //
                            //   var final_cost = cost + consts.BASE_TRAVEL_COST;
                            //
                            //   callback(final_cost);
                            //
                            // } else {
                            //
                            //
                            //   place_dao.getBaseFactor(function (base_factor) {
                            //
                            //
                            //     var cost = ( distance_in_meters / 1000 ) * base_factor.factor;
                            //
                            //     cost = 500 * Math.round(cost / 500);
                            //
                            //     var final_cost = cost + consts.BASE_TRAVEL_COST;
                            //
                            //     callback(final_cost);
                            //
                            //   })
                            //
                            // }

                        });

                    });

                }

            })

        });

    });

};

function findNearestTaxi(travel_id, callback) {

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        if (travel_data) {

            var around_taxis = [];

            var source_lat = travel_data.source_lat;
            var source_lan = travel_data.source_lan;

            taxi_dao.getTaxisLocation(function (taxis_list) {

                if (taxis_list.length >= 1) {

                    for (var i = 0; i < taxis_list.length; i++) {

                        var taxi_distance = map_tools.findDistance(source_lan, source_lat, taxis_list[i].location_lan, taxis_list[i].location_lat);

                        if (taxi_distance < 6000) {

                            around_taxis.push(taxis_list[i]);

                        }

                        //if (map_tools.findDistance(organization_lan, organization_lat, candidate_taxi.location_lan, candidate_taxi.location_lat) > map_tools.findDistance(organization_lan, organization_lat, taxis_list[i].location_lan, taxis_list[i].location_lat)) {
                        //
                        //    candidate_taxi = taxis_list[i];
                        //
                        //}
                    }

                    if (around_taxis.length > 0) {

                        var candidate_taxi = around_taxis[0];

                        taxi_dao.getTaxiWaitTime(candidate_taxi._id, function (first_taxi_wait_time) {

                            var candidate_taxi_wait_time = first_taxi_wait_time;

                            for (var j = 0; j < around_taxis.length; j++) {

                                (function (cntr) {

                                    taxi_dao.getTaxiWaitTime(around_taxis[cntr]._id, function (taxi_wait_time) {

                                        if (taxi_wait_time > candidate_taxi_wait_time) {

                                            var candidate_taxi_wait_time = taxi_wait_time;
                                            candidate_taxi = around_taxis[cntr];

                                        }

                                        if (cntr === around_taxis.length - 1) {

                                            callback(candidate_taxi._id);

                                        }

                                    });

                                })(j);

                            }

                        });

                    } else {

                        callback(false);

                    }

                } else {

                    callback(false);

                }

            });
        }

    });

};

function calculateTimeTravel(travel_id, taxi_id, callback) {

    travel_dao.getTravelInfo(travel_id, function (travel_data) {

        taxi_dao.getTaxiInfo(taxi_id, function (taxi_data) {

            map_tools.timeTravelByGoogle(travel_data.source_lan, travel_data.source_lat, taxi_data.location_lan, taxi_data.location_lat, function (time_travel) {

                callback(time_travel);

            });

        });

    });
};