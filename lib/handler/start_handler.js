var customerController = require("../controller/customer_controller");
var taxi_dao = require("./../dao/taxi_dao");
var fs = require("fs");
var customer_dao = require("./../dao/customer_dao");
var place_dao = require("./../dao/place_dao");
var travel_dao = require("./../dao/travel_dao");
var taxiController = require("./../controller/taxi_controller");
var travelController = require("./../controller/travel_controller");
var supportController = require("./../controller/support_controller");
var aa = require("./../aa/aa");
var moment = require('moment-jalaali');


module.exports.setAllTaxisOffline = function (callback) {

    taxi_dao.getOnlineTaxisList(function (taxis_list) {

        if(taxis_list) {

            if(taxis_list.length > 0) {

                for (var j = 0; j < taxis_list.length; j++) {

                    (function (cntr) {

                        var taxi_id = taxis_list[cntr]._id;

                        taxi_dao.setTaxiOffline(taxi_id, function (result) {

                            if (cntr === taxis_list.length - 1) {

                                callback(true);

                            }

                        });

                        // });

                    })(j);

                }

            } else {

                callback(true);

            }

        } else {

            callback(true);

        }

    });

};

module.exports.savePlaceData = function (callback) {

    place_dao.getAllPlaces(function (places) {

        console.log(places);

        var places = JSON.stringify(JSON.parse(JSON.stringify(places)));
        fs.writeFile("./places.json", places, function(err) {
            if(err) {
                return console.log(err);
            } else {
                callback(true);
            }

        });

    })

};

module.exports.changePlaceData = function (callback) {

    var places = JSON.parse(fs.readFileSync('./places.json', 'utf8'));
    for(var i=0; i<places.length; i++) {

        place_dao.addNewPlace(places[i].place_name, places[i].place_lan, places[i].place_lat, function (result) {



        });

    }

};