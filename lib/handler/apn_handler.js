var apn = require('apn');
var consts = require('../config/consts');

var options = {
    token: {
        key: "./lib/config/certificates/passenger.p8",
        cert: "./lib/config/certificates/passenger.cer",
        keyId: consts.APPLE_KEY_ID,
        teamId: consts.APPLE_TEAM_ID
    },
    production: false
};

var taxi_options = {
    token: {
        key: "./lib/config/certificates/driver.p8",
        cert: "./lib/config/certificates/driver.cer",
        keyId: consts.APPLE_DRIVER_KEY_ID,
        teamId: consts.APPLE_TEAM_ID
    },
    production: false
};

// var passengerApnProvider = new apn.Provider(options);

// var taxiApnProvider = new apn.Provider(taxi_options);

module.exports.sendNotification = function (device_token, event_name) {

    // let notification = new apn.Notification();
    //
    // switch (event_name) {
    //
    //   case "taxi_arrived":
    //     notification.alert = "تاکسی رسید، لطفا سوار بشو دوست گرامی";
    //     notification.sound = "ping.aiff";
    //     ;
    //     notification.topic = consts.APPLE_PASSENGER_TOPIC;
    //     passengerApnProvider.send(notification, device_token).then((result) => {
    //
    //       // see documentation for an explanation of result
    //
    //       console.log(JSON.stringify(result));
    //
    //     });
    //
    //     break;
    //
    //   case "taxi_confirmed":
    //     notification.alert = "تاکسی سفر را تایید کرد، در حال آمدن به سمت شماست";
    //     notification.sound = "ping.aiff";
    //     notification.topic = consts.APPLE_PASSENGER_TOPIC;
    //     passengerApnProvider.send(notification, device_token).then((result) => {
    //
    //       // see documentation for an explanation of result
    //
    //       console.log(JSON.stringify(result));
    //
    //     });
    //
    //     break;
    //
    //   case "taxi_not_found":
    //     notification.alert = "کاربر گرامی، برای سفر شما، تاکسی یافت نشد";
    //     notification.sound = "ping.aiff";
    //     notification.topic = consts.APPLE_PASSENGER_TOPIC;
    //     passengerApnProvider.send(notification, device_token).then((result) => {
    //
    //       // see documentation for an explanation of result
    //
    //       console.log(JSON.stringify(result));
    //
    //     });
    //
    //     break;
    //
    //   case "taxi_put_off_travel":
    //     notification.alert = "تاکسی سفر را کنسل کرده، در حال یافتن تاکسی جدید هستیم";
    //     notification.sound = "ping.aiff";
    //     notification.topic = consts.APPLE_PASSENGER_TOPIC;
    //     passengerApnProvider.send(notification, device_token).then((result) => {
    //
    //       // see documentation for an explanation of result
    //
    //       console.log(JSON.stringify(result));
    //
    //     });
    //
    //     break;
    //
    //   case "taxi_canceled_travel":
    //
    //     notification.alert = "سفر توسط تاکسی، کنسل شد";
    //     notification.sound = "ping.aiff";
    //     notification.topic = consts.APPLE_PASSENGER_TOPIC;
    //     passengerApnProvider.send(notification, device_token).then((result) => {
    //
    //       // see documentation for an explanation of result
    //
    //       console.log(JSON.stringify(result));
    //
    //     });
    //
    //     break;
    //
    //   case "travel_finished":
    //     notification.alert = "سفر تمام شد، با آرزوی توفیق روز افزون";
    //     notification.sound = "ping.aiff";
    //     notification.topic = consts.APPLE_PASSENGER_TOPIC;
    //     passengerApnProvider.send(notification, device_token).then((result) => {
    //
    //       // see documentation for an explanation of result
    //
    //       console.log(JSON.stringify(result));
    //
    //     });
    //
    //     break;
    //
    //   case "travel_started":
    //     notification.alert = "سفر شما شروع شد، سفر خوشی را برای شما آرزو می‌کنم";
    //     notification.sound = "ping.aiff";
    //     notification.topic = consts.APPLE_PASSENGER_TOPIC;
    //     passengerApnProvider.send(notification, device_token).then((result) => {
    //
    //       // see documentation for an explanation of result
    //
    //       console.log(JSON.stringify(result));
    //
    //     });
    //
    //     break;
    //
    //   case "travel_is_paid":
    //     notification.alert = "هزینه‌ی سفر توسط شما پرداخت شد. ممنون";
    //     notification.sound = "ping.aiff";
    //     notification.topic = consts.APPLE_PASSENGER_TOPIC;
    //     passengerApnProvider.send(notification, device_token).then((result) => {
    //
    //       // see documentation for an explanation of result
    //
    //       console.log(JSON.stringify(result));
    //
    //     });
    //
    //     break;
    //
    // }

}


module.exports.sendTaxiNotification = function (device_id, event_name) {

    // let notification = new apn.Notification();
    //
    // switch (event_name) {
    //
    //     case "customer_canceled_travel":
    //         notification.alert = "راننده عزیز، سفر از جانب مسافر، کنسل شد";
    //         notification.sound = "ping.aiff";
    //         notification.topic = consts.APPLE_DRIVER_TOPIC;
    //         taxiApnProvider.send(notification, device_id).then((result) => {
    //
    //             // see documentation for an explanation of result
    //
    //             console.log(JSON.stringify(result));
    //
    //         });
    //
    //         break;
    //
    //     case "support_canceled_travel":
    //         notification.alert = "راننده عزیز، سفر از جانب پشتیبانی، کنسل شد";
    //         notification.sound = "ping.aiff";
    //         notification.topic = consts.APPLE_DRIVER_TOPIC;
    //         taxiApnProvider.send(notification, device_id).then((result) => {
    //
    //             // see documentation for an explanation of result
    //
    //             console.log(JSON.stringify(result));
    //
    //         });
    //
    //         break;
    //
    // }

}

