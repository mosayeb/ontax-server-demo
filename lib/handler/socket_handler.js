var customerController = require("../controller/customer_controller");
var taxi_dao = require("./../dao/taxi_dao");
var customer_dao = require("./../dao/customer_dao");
var travel_dao = require("./../dao/travel_dao");
var taxiController = require("./../controller/taxi_controller");
var travelController = require("./../controller/travel_controller");
var supportController = require("./../controller/support_controller");
var aa = require("./../aa/aa");
var moment = require('moment-jalaali');


module.exports = function (io) {

  io.on('connection', function (socket) {

    /*

     TEST PART

     */


    /*

     SUPPORT PART

     */

    socket.on('customer-connected', function (data) {

      supportController.notifyHandler(data, io, socket);

    });

    socket.on('update_socket_data', function (callback) {

      supportController.updateSupportInfo(socket, callback);

    });

    socket.on('support_confirm_travel', function (data, callback) {

      // travelController.supportConfirmedTravel(data, io, socket, callback);

    });

    // track taxi by support

    socket.on('support_track_taxi', function (data, callback) {

      taxiController.getTaxiLocation(data, socket, callback);

    });

    socket.on('support_cancel_travel', function (data, callback) {

      travelController.cancelTravelBySupport(data, io, socket, callback);

    });

    socket.on('support_finish_travel', function (data, callback) {

      travelController.travelFinished(data, io, socket, callback);

    });

    /*

     CUSTOMER PART

     */

    socket.on('find_taxi', function (data, callback) {

      if (aa.customerAuthenticate(data)) {

        customerController.findTaxi(data, function (taxi_data) {

          if (taxi_data) {

            socket.join(data.travel_id);

            callback(taxi_data);

          }
        });

      } else {

        callback({result: {result: false}});

      }

    });

    socket.on('station_confirm_travel', function (data, callback) {

          console.log("STATION CONFIRMED TRAVEL");

          console.log(data);
          console.log(data.token);

          if (aa.stationAuthenticate(data)) {

              console.log("STATION AUTHENTICATED");

              travelController.customerConfirmedTravel(data, io, socket, callback);

          } else {

              console.log("STATION NOT AUTHENTICATED");

              callback({result: false});

          }

      });

    socket.on('organization_confirm_travel', function (data, callback) {

        console.log("CUSTOMER CONFIRMED TRAVEL");

        console.log(data);
        console.log(data.token);

        if (aa.organizationAuthenticate(data)) {

            console.log("CUSTOMER AUTHENTICATED");

            travelController.customerConfirmedTravel(data, io, socket, callback);

        } else {

            console.log("CUSTOMER NOT AUTHENTICATED");

            callback({result: false});

        }

      });

    socket.on('customer_confirm_travel', function (data, callback) {

      console.log(callback);
      if (aa.customerAuthenticate(data)) {

        travelController.customerConfirmedTravelTwo(data, io, socket, callback);

      } else {

        callback({result: false});

      }

    });

    socket.on('customer_cancel_travel', function (data, callback) {

      if (aa.customerAuthenticate(data) || aa.organizationAuthenticate(data) || aa.stationAuthenticate(data)) {

        travelController.cancelTravelByCustomer(data, io, socket, callback);

      } else {

        callback(false);

      }

    });

    // track taxi by customer that is in a travel by taxi

    socket.on('track_taxi', function (data, callback) {

      if (aa.customerAuthenticate(data)) {

        customerController.trackTravelTaxi(data, socket, callback);

      } else {

        callback({result: {result: false}});

      }

    });

    // client socket connected

    socket.on('customer_opened_reconnect', function (data, callback) {

        if (aa.customerAuthenticate(data)) {

        customerController.customerOpenedReconnect(data, io, socket, callback);

      } else {

        callback({result: {result: false}});

      }
    });

    // client opened app

    socket.on('customer_app_open', function (data, callback) {

      if (aa.customerAuthenticate(data)) {

        customerController.customerAppOpened(data, io, socket, callback);

      } else {

        callback({result: {result: false}});

      }

    });

    // client socket connected, but app is closed

    socket.on('customer_closed_reconnect', function (data, callback) {

      if (aa.customerAuthenticate(data)) {

        customerController.customerClosedReconnect(data, io, socket, callback);

      } else {

      }

    });

    // customer say us: I have money now and can pay :)

    socket.on('customer_pay_cost', function (data, callback) {

      console.log("CUSTOMER_PAY_COST_TRAVEL");

      if (aa.customerAuthenticate(data)) {

        travelController.customerPayTravelCost(data, io, socket, callback);

      } else {

        callback({result: false});

      }

    });

    socket.on('update_travel_options', function (data, callback) {

        if (aa.customerAuthenticate(data)) {

            travelController.updateTravelOptions(data, io, socket, callback);

        } else {

            callback({result: false});

        }

    });

    socket.on('customer_change_payment_type', function (data, callback) {

        if (aa.customerAuthenticate(data)) {

            travelController.updateTravelPaymentType(data, io, socket, callback);

        } else {

            callback({result: false});

        }

    });

    /*

     TAXI PART

     */

    socket.on('taxi_confirm_travel', function (data, callback) {

      if (aa.taxiAuthenticate(data)) {

        travelController.taxiConfirmedTravel(data, io, socket, callback);

      } else {

        callback(false);

      }

    });

    // taxi refuse travel

    socket.on('taxi_refuse_travel', function (data, callback) {

      console.log("TAXI REFUSED TRAVEL");

      if (aa.taxiAuthenticate(data)) {

        travelController.taxiRefusedTravel(data, io, socket, callback);

      } else {

        callback(false);

      }

    });

    // taxi put off travel

    socket.on('taxi_put_off_travel', function (data, callback) {

      if (aa.taxiAuthenticate(data)) {

        travelController.taxiPutOffTravel(data, io, socket, callback);


      } else {

        callback(false);

      }

    });

    socket.on('taxi_update_location', function (data, callback) {

      console.log("TAXI UPDATE LOCATION: " + socket.id + " " + moment().format("HH:mm:ss") + " " + data.location_lan);

      if (aa.taxiAuthenticate(data)) {

        taxiController.intervalUpdateTaxiInfo(data, io, socket, callback);

      } else {


      }

    });

    socket.on('taxi_arrived', function (data, callback) {

      if (aa.taxiAuthenticate(data)) {

        travelController.taxiArrived(data, io, socket, callback);

      } else {

        callback(false);

      }

    });

    socket.on('taxi_finish_travel', function (data, callback) {

      console.log("TAXI FINISHED TRAVEL");

      if (aa.taxiAuthenticate(data)) {

          travelController.travelFinishedTwo(data, io, socket, callback);


      } else {

        console.log("HEREEEEEEE");

        callback(false);

      }

    });

    socket.on('taxi_cancel_travel', function (data, callback) {

      console.log("TAXI CANCELED TRAVEL");

      if (aa.taxiAuthenticate(data)) {

        travelController.cancelTravelByTaxi(data, io, socket, callback);

      } else {

        callback(false);

      }

    });

    // taxi start travel

    socket.on('taxi_start_travel', function (data, callback) {

      if (aa.taxiAuthenticate(data)) {

        travelController.taxiStartTravel(data, io, socket, callback);

      } else {

        callback(false);

      }

    });

    // taxi say us that user payed the travel cost by cash

    socket.on('taxi_get_cash', function (data) {

      if (aa.taxiAuthenticate(data)) {

        travelController.taxiGetCash(data, io, socket);

      } else {


      }


    });

    socket.on("disconnect", function (reason) {

      console.log("SOCKET DISCONNECTED: " + socket.id + " " + moment().format("HH:mm:ss"));
      console.log("REASON: " + reason);

      taxiController.taxiSocketDisconnected(socket);

    });

    // taxi opened app

    socket.on('taxi_app_open', function (data, callback) {

      console.log("taxi app open");

      if (aa.taxiAuthenticate(data)) {

        taxiController.taxiAppOpened(data, io, socket, callback);

      } else {

      }

    });

    // taxi socket connected

    socket.on('taxi_opened_reconnect', function (data, callback) {

      console.log("taxi open reconnect");

      if (aa.taxiAuthenticate(data)) {

        taxiController.taxiOpenedReconnect(data, io, socket, callback);

      } else {


      }

    });

    // taxi socket connected, but app is closed

    socket.on('taxi_closed_reconnect', function (data, callback) {

      if (aa.taxiAuthenticate(data)) {

        taxiController.taxiClosedReconnect(data, io, socket, callback);

      } else {

      }

    });

    // taxi reconnected but no travel it is stored

    socket.on("taxi_free_reconnect", function (data) {

      console.log("taxi free reconnect");
      if (aa.taxiAuthenticate(data)) {

        taxiController.taxiFreeReconnect(data, io, socket);

      } else {

        // callback({result: false});

      }

    });

    // this should be come every time that socket connected

    socket.on("taxi_socket_reconnect", function (data) {

      if (aa.taxiAuthenticate(data)) {

        taxiController.taxiSocketReconnect(data, socket);

      } else {


      }

    });

  });

};
