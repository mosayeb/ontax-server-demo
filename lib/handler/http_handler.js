var apn = require('apn');
var request = require("request");
var consts = require('../config/consts');

module.exports.sendBulkNotification = function (devices_list, message, callback) {

  request({
    uri: "http://148.251.34.69:9876/apn/bulk/notification/",
    method: "POST",
    contentType: 'application/json',
    form: {
      devices_list: devices_list.toString(),
      message: message,
      app_name: "ontax"
    }
  }, function(err, response, body) {

    if(!err)
      callback(true);
  });

}

// request({
//     uri: uri,
//     method: "POST",
//     form: {
//         receiver:caller_phone,
//         text:text,
//         from: from,
//         username: username,
//         password: password
//     }
// }, function(err, response, body) {
//
//     if(!err)
//         callback(true);
// });