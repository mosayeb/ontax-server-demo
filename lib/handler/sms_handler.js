"use strict";

var soap = require('soap');
var request = require("request");
var customer_dao = require('../dao/customer_dao');
var support_dao = require('../dao/support_dao');
var consts = require('../config/consts');

var username = "aflak1";
var password = "123456";
var from = "50001000000000";
var support_receiver = "989167125770";
var uri = "http://rgshop.ir/paatilsmshandler.php";

module.exports.sendPromotionSmsToCaller = function (caller_phone, customer_phone,  callback) {


    var text = "کاربر عزیز، از اینکه " + customer_phone + "را به آنتاکس دعوت کردید سپاسگزاریم. مبلغ " + consts.CALLER_PROMOTION_GIFT + "به موجودی شما افزوده شد.";

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":caller_phone}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            //"SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        client.GroupSms(args, function (err, result, body) {

            callback(true);

        });

    });

    // var text = "کاربر عزیز، از اینکه " + customer_phone + "را به رهتاک دعوت کردید سپاسگزاریم. مبلغ " + consts.CALLER_PROMOTION_GIFT + "به موجودی شما افزوده شد.";
    //
    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:caller_phone,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

};

module.exports.sendPromotionSmsToInvited = function (caller_phone, customer_phone,  callback) {

    var text = "کاربر عزیز، از اولین سفر شما طی دعوت کاربر با شماره تلفن " + caller_phone  + " از طریق آنتاکس، بسیار خرسندیم. " + " مبلغ " + consts.CALLER_PROMOTION_GIFT + "به عنوان هدیه  به موجودی شما افزوده شد.";

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":customer_phone}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            //"SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        client.GroupSms(args, function (err, result, body) {

            callback(true);

        });

    });

    // var text = "کاربر عزیز، از اینکه " + customer_phone + "را به آنتاکس دعوت کردید سپاسگزاریم. مبلغ " + consts.CALLER_PROMOTION_GIFT + "به موجودی شما افزوده شد.";
    //
    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:caller_phone,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

};

module.exports.sendVerifyCode = function(phone_number, callback) {

    console.log(phone_number);

    //var url = 'http://95.142.225.104:8080/core/MessageRelayService?wsdl';
    //var verification_code = (Math.floor(Math.random()*90000) + 10000).toString();
    //var args = {
    //    username: "4060473561",
    //    password: "hfz4033",
    //    originator: "50004633324033",
    //    destination: [phone_number],
    //    content: verification_code
    //};
    //
    //var options = {
    //    ignoredNamespaces: {
    //        namespaces: [],
    //        override: true
    //    }
    //};
    //
    //callback(true, verification_code);
    //
    //soap.createClient(url, options, function (err, client) {
    //
    //    client.sendMessageOneToMany(args, function (err, result) {
    //
    //        if(err) {
    //
    //            callback(false);
    //
    //        }
    //
    //        else if(result.return.error.errorCode == 0) {
    //
    //            customer_dao.updateCode(phone_number, verification_code, function (result) {
    //
    //                if(result)
    //                    callback(true, verification_code);
    //
    //            });
    //
    //        }
    //
    //    });
    //});

    // var url = 'http://www.linepayamak.ir/Post/Send.asmx?wsdl';

    if(phone_number == "989160000000" || phone_number == "09160000000") {

        var verification_code = 12345;

    } else {

        var verification_code = (Math.floor(Math.random()*90000) + 10000).toString();

    }

    var text = "کد فعال‌سازی شما در آنتاکس: " + verification_code;
    //
    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true, verification_code);
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            //"SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, {'escapeXML': false}, function (err, client) {

        console.log(client);

        client.GroupSms(args, function (err, result, body) {

            console.log(err);
            console.log(result);

            callback(true, verification_code);

        });

    });

    // callback(true, verification_code);

    //var args = {
    //    username: "taximo",
    //    password: "123456",
    //    from: "50001000000000",
    //    to: ["989307174247"],
    //    text: "salam"
    //};
    //
    //var options = {
    //    ignoredNamespaces: {
    //        namespaces: [],
    //        override: true
    //    }
    //};
    //
    ////callback(true, verification_code);
    //
    //soap.createClient(url, options, function (err, client) {
    //
    //    client.SendSimpleSMS(args, function (err, result) {
    //
    //        console.log(result);
    //        console.log(args);
    //
    //        customer_dao.updateCode(phone_number, verification_code, function (result) {
    //
    //            if(result)
    //                callback(true, verification_code);
    //
    //        });
    //
    //    });
    //});
};

// module.exports.sendVerifyCode = function(phone_number, callback) {
//
//     //var url = 'http://95.142.225.104:8080/core/MessageRelayService?wsdl';
//     //var verification_code = (Math.floor(Math.random()*90000) + 10000).toString();
//     //var args = {
//     //    username: "4060473561",
//     //    password: "hfz4033",
//     //    originator: "50004633324033",
//     //    destination: [phone_number],
//     //    content: verification_code
//     //};
//     //
//     //var options = {
//     //    ignoredNamespaces: {
//     //        namespaces: [],
//     //        override: true
//     //    }
//     //};
//     //
//     //callback(true, verification_code);
//     //
//     //soap.createClient(url, options, function (err, client) {
//     //
//     //    client.sendMessageOneToMany(args, function (err, result) {
//     //
//     //        if(err) {
//     //
//     //            callback(false);
//     //
//     //        }
//     //
//     //        else if(result.return.error.errorCode == 0) {
//     //
//     //            customer_dao.updateCode(phone_number, verification_code, function (result) {
//     //
//     //                if(result)
//     //                    callback(true, verification_code);
//     //
//     //            });
//     //
//     //        }
//     //
//     //    });
//     //});
//
//     // var url = 'http://www.linepayamak.ir/Post/Send.asmx?wsdl';
//
//
//     var verification_code = (Math.floor(Math.random()*90000) + 10000).toString();
//     var text = "کد فعال‌سازی شما در آنتاکس: " + verification_code;
//
//     request({
//         uri: uri,
//         method: "POST",
//         form: {
//             receiver:phone_number,
//             text:text,
//             from: from,
//             username: username,
//             password: password
//         }
//     }, function(err, response, body) {
//
//         if(!err)
//             callback(true, verification_code);
//     });
//
//     //var args = {
//     //    username: "taximo",
//     //    password: "123456",
//     //    from: "50001000000000",
//     //    to: ["989307174247"],
//     //    text: "salam"
//     //};
//     //
//     //var options = {
//     //    ignoredNamespaces: {
//     //        namespaces: [],
//     //        override: true
//     //    }
//     //};
//     //
//     ////callback(true, verification_code);
//     //
//     //soap.createClient(url, options, function (err, client) {
//     //
//     //    client.SendSimpleSMS(args, function (err, result) {
//     //
//     //        console.log(result);
//     //        console.log(args);
//     //
//     //        customer_dao.updateCode(phone_number, verification_code, function (result) {
//     //
//     //            if(result)
//     //                callback(true, verification_code);
//     //
//     //        });
//     //
//     //    });
//     //});
// };

module.exports.sendTaxiFoundedSms = function (phone_number, travel_code, driver_name, driver_phone_number, car, callback) {

    var text = "کاربر عزیز،  " + driver_name + " با شماره تلفن " + driver_phone_number + " با ماشین " + car + " درخواست سفر شما با کد " + travel_code +  "  را تایید کرده و برای خدمت رسانی به شما در راه است.";

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            //"SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        client.GroupSms(args, function (err, result, body) {

            callback(true);

        });

    });

    // callback(true);

};

module.exports.sendTaxiArrivedSms = function (phone_number, callback) {

    var text = "کاربر عزیز، راننده شما به مقصد مورد نظر رسیده است، لطفا برای سوار شدن تشریف ببرید.";

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            //"SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        client.GroupSms(args, function (err, result, body) {

            callback(true);

        });

    });

    // callback(true);

};

module.exports.sendTravelCanceledByTaxiSms = function (phone_number, travel_code, callback) {



    var text = "کاربر عزیز، سفر شما با کد " + travel_code + " توسط راننده لغو شد.";

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            //"SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        client.GroupSms(args, function (err, result, body) {

            callback(true);

        });

    });

    // callback(true);

};

module.exports.sendTravelCanceledBySupportSms = function (phone_number, travel_code, callback) {



    var text = "کاربر عزیز، سفر شما با کد " + travel_code + " توسط پشتیبانی لغو شد.";

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            //"SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        client.GroupSms(args, function (err, result, body) {

            callback(true);

        });

    });

    // callback(true);

};

module.exports.sendNewTravelSms = function (driver_phone_number, travel_code, callback) {



    var text = "راننده عزیز، سفر جدیدی با کد " + travel_code + " برای شما آمده، لطفا تایید فرمایید.";

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:driver_phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":driver_phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            // "SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        if(client) {

            client.GroupSms(args, function (err, result, body) {

                callback(true);

            });

        }

    });

    // callback(true);

};

module.exports.sendCustomerIncreaseMoney = function (customer_phone_number, money, callback) {



    var text = "کاربر عزیز، مبلغ " + money + " تومان توسط پشتیبانی آنتاکس، به موجودی کیف پول شما اضافه گردید.";

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:driver_phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":customer_phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            // "SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        if(client) {

            client.GroupSms(args, function (err, result, body) {

                callback(true);

            });

        }

    });

    // callback(true);

};

module.exports.sendTravelCostChange = function (customer_phone_number, money, callback) {

    var text = "کاربر عزیز، هزینه‌ی سفر توسط پشتیبانی به مبلغ " + money + " تومان تغییر داده شد. سپاس از همراهی شما";

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:driver_phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":customer_phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            // "SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        if(client) {

            client.GroupSms(args, function (err, result, body) {

                callback(true);

            });

        }

    });

    // callback(true);

};

module.exports.sendDriverWelcomeMessage = function (driver_phone_number, driver_name, username, password, callback) {

    var text = " به خانواده آنتاکس خوش آمدید\n " + driver_name + " اطلاعات کاربری شما در سیستم آنتاکس ثبت شد.\n" +  "نام کاربری: " + username + "\nرمز عبور: " + password;

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:driver_phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    // var args={
    //   "security":  {
    //     "Username":"09167125770", "Password":"mosayeb_sonofagun"
    //   },
    //   "model": {
    //     "Message": text,
    //     "SenderNumber": "9830002530000002",
    //     "Numbers": [{"string":driver_phone_number}],
    //     "SendType": "Normal",
    //     "YourMessageId": [{"long":"1"}]
    //     // "SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
    //   }
    // };
    //
    // var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";
    //
    // soap.createClient(url, function (err, client) {
    //
    //   if(client) {
    //
    //     client.GroupSms(args, function (err, result, body) {
    //
    //       callback(true);
    //
    //     });
    //
    //   }
    //
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":driver_phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            // "SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        if(client) {

            client.GroupSms(args, function (err, result, body) {

                callback(true);

            });

        }

    });

    // callback(true);

};

module.exports.sendDriverTravelPaidSms = function (phone_number, travel_code, cost, callback) {

    var text = "راننده عزیز، هزینه سفر شما با کد " + travel_code + " توسط مسافر پرداخت شد.\n" + " مبلغ پرداخت شده: " + cost + " تومان";

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            // "SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        if(client) {

            client.GroupSms(args, function (err, result, body) {

                callback(true);

            });

        }

    });

};

module.exports.sendDriverChangePasswordMessage = function (driver_phone_number, driver_name, username, password, callback) {

    var text = "راننده گرامی، " + driver_name + " اطلاعات کاربری شما در سیستم آنتاکس بروز‌رسانی شد.\n" +  "نام کاربری: " + username + "\nرمز عبور: " + password;

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:driver_phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    // var args={
    //   "security":  {
    //     "Username":"09167125770", "Password":"mosayeb_sonofagun"
    //   },
    //   "model": {
    //     "Message": text,
    //     "SenderNumber": "9830002530000002",
    //     "Numbers": [{"string":driver_phone_number}],
    //     "SendType": "Normal",
    //     "YourMessageId": [{"long":"1"}]
    //     // "SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
    //   }
    // };
    //
    // var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";
    //
    // soap.createClient(url, function (err, client) {
    //
    //   if(client) {
    //
    //     client.GroupSms(args, function (err, result, body) {
    //
    //       callback(true);
    //
    //     });
    //
    //   }
    //
    // });

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":driver_phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            // "SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        if(client) {

            client.GroupSms(args, function (err, result, body) {

                callback(true);

            });

        }

    });

    // callback(true);

};

module.exports.sendCustomerCancelTravel = function (phone_number, travel_code, callback) {

    console.log(phone_number);
    console.log(travel_code);

    var text = "راننده عزیز، سفر شما با کد " + travel_code + " توسط مسافر لغو شد.";

    // request({
    //     uri: uri,
    //     method: "POST",
    //     form: {
    //         receiver:phone_number,
    //         text:text,
    //         from: from,
    //         username: username,
    //         password: password
    //     }
    // }, function(err, response, body) {
    //
    //     if(!err)
    //         callback(true);
    // });

    // var args={
    //   "security":  {
    //     "Username":"09167125770", "Password":"mosayeb_sonofagun"
    //   },
    //   "model": {
    //     "Message": text,
    //     "SenderNumber": "9830002530000002",
    //     "Numbers": [{"string":phone_number}],
    //     "SendType": "Normal",
    //     "YourMessageId": [{"long":"1"}]
    //     //"SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
    //   }
    // };
    //
    // var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";
    //
    // // soap.createClient(url, function (err, client) {
    // //
    // //   client.GroupSms(args, function (err, result, body) {
    // //
    //     callback(true);
    //
    //   });
    //
    // });

    // callback(true);

    var args={
        "security":  {
            "Username":"09167125770", "Password":"mosayeb_sonofagun"
        },
        "model": {
            "Message": text,
            "SenderNumber": "9830002530000002",
            "Numbers": [{"string":phone_number}],
            "SendType": "Normal",
            "YourMessageId": [{"long":"1"}]
            // "SendOn": "2016-06-22T15:01:00.000Z" in parameter optional ast
        }
    };

    var url = "http://niksms.com:1370/NiksmsWebservice.svc?wsdl";

    soap.createClient(url, function (err, client) {

        if(client) {

            client.GroupSms(args, function (err, result, body) {

                callback(true);

            });

        }

    });

};

