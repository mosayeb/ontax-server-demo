/**
 * Created by amir on 12/12/15.
 */

module.exports = {
    PAYMENT_CALL_BACK_URL: 'http://148.251.34.69:5555/customer/paymentresult',
    ZARINPAL_URL: 'https://www.zarinpal.com/pg/StartPay/',
    ONE_DAY: 1000 * 60 * 60 * 24, // one day in milli second
    CALLER_PROMOTION_GIFT: 5000, // tomans
    CALLER_PROMOTION_DESCRIPTION: 'افزایش اعتبار هدیه به مبلغ',
    IPG_PAYMENT_DESCRIPTION: 'افزایش اعتبار به مبلغ'
}