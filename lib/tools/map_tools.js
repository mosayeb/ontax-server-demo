var geolib = require('geolib');

var googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyCt_c22Yex1Ju9u9BysXCaLfHf3Xg4FvVQ'
});

module.exports.findDistance = function (sourceLan, sourceLat, destinationLan, destinationLat) {

    sourceLan = parseFloat(sourceLan);
    sourceLat = parseFloat(sourceLat);
    destinationLan = parseFloat(destinationLan);
    destinationLat = parseFloat(destinationLat);

    var distance = geolib.getDistance(
        {latitude: sourceLat, longitude: sourceLan},
        {latitude: destinationLat, longitude: destinationLan}
    );

    return(distance);
};

module.exports.findGoogleDistance = function (sourceLan, sourceLat, destinationLan, destinationLat, callback) {

    sourceLan = parseFloat(sourceLan);
    sourceLat = parseFloat(sourceLat);
    destinationLan = parseFloat(destinationLan);
    destinationLat = parseFloat(destinationLat);

    googleMapsClient.distanceMatrix({
        origins: [
            [sourceLat, sourceLan]
        ],
        destinations: [
            [destinationLat, destinationLan]
        ]
    }, function (err, result) {

        console.log(err);
        console.log(result);
        callback(result.json.rows[0].elements[0].distance.value);

    });
};

module.exports.timeTravel = function (sourceLan, sourceLat, destinationLan, destinationLat) {

    sourceLan = parseFloat(sourceLan);
    sourceLat = parseFloat(sourceLat);
    destinationLan = parseFloat(destinationLan);
    destinationLat = parseFloat(destinationLat);

    var distance = this.findDistance(sourceLan, sourceLat, destinationLan, destinationLat);

    var time_travel = (distance * 0.9).toFixed();

    if(time_travel === 0) {

        time_travel = 1;

    }

    return(time_travel);

};

module.exports.timeTravelByGoogle = function (sourceLan, sourceLat, destinationLan, destinationLat, callback) {

    sourceLan = parseFloat(sourceLan);
    sourceLat = parseFloat(sourceLat);
    destinationLan = parseFloat(destinationLan);
    destinationLat = parseFloat(destinationLat);

    googleMapsClient.distanceMatrix({
        origins: [
            [sourceLat, sourceLan]
        ],
        destinations: [
            [destinationLat, destinationLan]
        ]
    }, function (err, result) {

        var duration = result.json.rows[0].elements[0].duration.value;

        if(duration === 0) {

            duration = 1;

        }

        callback(duration);

    });

};

module.exports.getSourceBound = function (lat, lan, distance) {

    var north_east_point = geolib.computeDestinationPoint({lat: lat, lon: lan}, distance, 45);
    var south_west_point = geolib.computeDestinationPoint({lat: lat, lon: lan}, distance, 225);

    var bound = {
        min_lat: south_west_point.latitude,
        max_lat: north_east_point.latitude,
        min_lan: south_west_point.longitude,
        max_lan: north_east_point.longitude
    };

    return(bound);
};


module.exports.findGoogleDistanceDuration = function (sourceLan, sourceLat, destinationLan, destinationLat, callback) {

    sourceLan = parseFloat(sourceLan);
    sourceLat = parseFloat(sourceLat);
    destinationLan = parseFloat(destinationLan);
    destinationLat = parseFloat(destinationLat);

    googleMapsClient.distanceMatrix({
        origins: [
            [sourceLat, sourceLan]
        ],
        destinations: [
            [destinationLat, destinationLan]
        ]
    }, function (err, result) {

        callback(result.json.rows[0].elements[0].distance.value, result.json.rows[0].elements[0].duration.value);

    });
};


function deg2rad(deg) {
    var rad = deg * Math.PI/180;
    return rad;
};

function round(x) {
    return Math.round( x * 1000) / 1000;
};
