var backup = require('mongodb-backup');
var restore = require('mongodb-restore');
var moment = require('moment-jalaali');
var zipFolder = require('zip-folder');
var homedir = require('homedir');
var tar = require('tar-fs');
var fs = require('fs');
var nodeExcel = require('excel-export');
var fsExtra = require('fs-extra');
var JSFtp = require("jsftp");
var customer_dao = require("../dao/customer_dao");
var turnover_dao = require("../dao/turnover_dao");
var travel_dao = require("../dao/travel_dao");
var taxi_dao = require("../dao/taxi_dao");

var config = require("../config/config");

// var ftp = new JSFtp({
//   host: "148.251.34.69",
//   port: 21, // defaults to 21
//   user: "ecab_demo", // defaults to "anonymous"
//   pass: "ecab_demo_sonofagun" // defaults to "@anonymous"
// });

module.exports.replaceDate = function (str, find, replace) {

  return str.replace(new RegExp(find, 'g'), replace);

};

module.exports.periodicBackUp = function (callback) {

  var CronJob = require('cron').CronJob;

  // get every hour backup

  new CronJob('53 * * * * *', function () {

    var time_now = moment().format("jYYYY_jMM_jDD_HH");
    var back_up_path = homedir() + '/backups/ontax/' + time_now + "/";

    if (!fs.existsSync(back_up_path)) {

      fs.mkdirSync(back_up_path);

      backup({
        uri: config.database, // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
        root: back_up_path,
        tar: "db.tar",
        callback: function (err) {

          if (!err) {

            tar.pack('./public/images').pipe(fs.createWriteStream(back_up_path + 'images.tar'));

          } else {

            console.log(err);

          }

        }
      });

    }

  }, null, true, 'Asia/Tehran');

  // get daily backup and send to ftp server

  new CronJob('1 1 * * *', function () {

    var time_now = moment().format("jYYYY_jMM_jDD_HH");
    var back_up_path = homedir() + '/backups/ontax/' + time_now + "D/";

    if (!fs.existsSync(back_up_path)) {

      fs.mkdirSync(back_up_path);

      backup({
        uri: config.database, // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
        root: back_up_path,
        tar: "db.tar",
        callback: function (err) {

          if (!err) {

            tar.pack('./public/images').pipe(fs.createWriteStream(back_up_path + 'images.tar'));

            // sending to ftp server here

            // ftp.put(homedir() + '/backups/ontax/' + time_now + "/db.tar", time_now + '/file.txt', function(hadError) {


            // ftp.raw('mkd', time_now + '/', function (hadError) {
            //
            //
            //     if (!hadError)
            //
            //
            //         ftp.put(homedir() + '/backups/ontax/' + time_now + "/db.tar", function (hadError) {
            //
            //
            //             if (!hadError)
            //
            //                 ftp.put(homedir() + '/backups/ontax/' + time_now + "/images.tar", function (hadError) {
            //
            //
            //                     console.log(hadError);
            //
            //                     if (!hadError)
            //
            //                         console.log("File transferred successfully!");
            //
            //                 });
            //
            //         });
            //
            // });


          } else {

            console.log(err);

          }

        }
      });

    }

  }, null, true, 'Asia/Tehran');

  // remove older backups two daysgetLatestTravels

  new CronJob('30 1 */2 * *', function () {

    var two_days_ago = moment().subtract(2, 'd').format("jYYYY_jMM_jDD");
    var one_day_ago = moment().subtract(1, 'd').format("jYYYY_jMM_jDD");

    var two_back_up_path = homedir() + '/backups/ontax/' + two_days_ago + "*";
    var one_back_up_path = homedir() + '/backups/ontax/' + one_day_ago + "*";

    fsExtra.remove(two_back_up_path, err => {

      if (err) return console.error(err);

      console.log('success!');

      fsExtra.remove(one_back_up_path, err => {

        if (err) return console.error(err);

        console.log('success!'); // I just deleted my entire HOME directory.


      })

    })

  }, null, true, 'Asia/Tehran');

};


// restore all of the data

module.exports.restore = function (backup_version, callback) {

  // restore database

  var back_up_path = homedir() + '/backups/ontax/' + backup_version + "/";

  restore({
    uri: config.database, // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
    root: back_up_path,
    tar: "db.tar",
    drop: true,
    callback: function (err) {

      if (!err) {

        if (!err) {

          // tar.pack('./public/images').pipe(fs.createWriteStream(back_up_path + 'images.tar'));

          fs.createReadStream(back_up_path + 'images.tar').pipe(tar.extract('./public/images'));

          callback(true);

        } else {

          console.log(err);

        }

      }

    }
  });

};


module.exports.getLatestBackups = function (callback) {

  var back_up_path = homedir() + '/backups/ontax/';
  var backup_list = [];

  fs.readdir(back_up_path, function (err, items) {

    console.log(items);

    for (var i = 0; i < items.length; i++) {

      backup_list.push(items[i]);

      console.log(items[i]);
    }

    callback(backup_list);
  });

};

module.exports.excelExportCustomerPhones = function (customers, callback) {

  var conf = {};
  var rows = [];

  // conf.stylesXmlFile = "styles.xml";
  conf.name = "phone_sheets";

  conf.cols = [{
    caption: 'Name',
    type: 'string',
    beforeCellWrite: function (row, cellData) {
      return cellData.toUpperCase();
    },
    width: 28.7109375
  }, {
    caption: 'Phone',
    type: 'number',
    width: 48.7109375
  }];

  for (var i = 0; i < customers.length; i++) {

    rows.push([customers[i].name, customers[i].phone_number]);

  }

  conf.rows = rows;

  var result = nodeExcel.execute(conf);

  callback(result);

};
module.exports.exportTaxisNumbers = function (taxis, callback) {

  var conf = {};
  var rows = [];

  // conf.stylesXmlFile = "styles.xml";
  conf.name = "phone_sheets";

  conf.cols = [{
    caption: 'Name',
    type: 'string',
    beforeCellWrite: function (row, cellData) {
      return cellData.toUpperCase();
    },
    width: 28.7109375
  }, {
    caption: 'Phone',
    type: 'number',
    width: 48.7109375
  }];

  for (var i = 0; i < taxis.length; i++) {

    rows.push([taxis[i].driver_name, taxis[i].driver_phone_number]);

  }

  conf.rows = rows;

  var result = nodeExcel.execute(conf);

  callback(result);

};

module.exports.makeid = function (length) {

  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  for (var i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};


function fixTurnOverPart() {

  // turnover_dao.fixTemp(function    (result) {
  //
  //     console.log("\n\n\n\n\n")
  //     console.log("trun overs fixed");
  //     console.log("\n\n\n\n\n")

  taxi_dao.fixTemp();

  // });
}

function removeRedundantTurnovers() {

  turnover_dao.removeRedundantTurnOvers(function (result) {


  });

}

function setInviteCode() {

  customer_dao.setInviteCode(function (result) {

    console.log(result);

  })

}

function syncTaxiData() {

  taxi_dao.setBankData(function (result) {

    console.log("FINISHED");

  })

}

function updateCallerNumbers() {

  customer_dao.updateCallerNumbersTwelveDigits(function (result) {

    console.log("FINISHED");

  })

}

function syncTransactions() {

  // get all travels list from first of month


  // check travels if have more than 1 transaction, create another one

  turnover_dao.syncTransactions(function (result) {

  });

}

function ontaxSync() {

  // update customer part

  // customer_dao.ontaxSync(function(result){
  //
  //
  //
  // });

  // update travels data

  travel_dao.ontaxSyncDate(function (result) {



  });

  // update taxi new fields

  // taxi_dao.ontaxSync(function (result) {
  //
  //
  //
  // });

}

// ontaxSync();

// syncTransactions();



// updateCallerNumbers();

// syncTaxiData();

// fixTurnOverPart();

// setInviteCode();

// removeRedundantTurnovers();