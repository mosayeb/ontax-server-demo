var base64 = require('./base64');
var fs = require('fs');
var crypto = require('crypto');

module.exports.uploadTaxiImage = function (image, cb) {

    // Regular expression for image type:
    // This regular image extracts the "jpeg" from "image/jpeg"

    var imageTypeRegularExpression = /\/(.*?)$/;

    // Generate random string

    var seed = crypto.randomBytes(20);
    var uniqueSHA1String = crypto
        .createHash('sha256')
        .update(seed)
        .digest('hex');

    var ImageEncodedString = image;
    var imageBuffer = base64.decodeBase64Image(ImageEncodedString);
    var userUploadedLocation = 'public/images/';

    var uniqueRandomImageName = 'image-' + uniqueSHA1String;

    // This variable is actually an array which has 5 values,
    // The [1] value is the real image extension

    var imageTypeDetected = imageBuffer.type.match(imageTypeRegularExpression);

    var userUploadedImagePath = userUploadedLocation + uniqueRandomImageName + '.' + imageTypeDetected[1];
    var returnUserAddress = 'public/images/' + uniqueRandomImageName + '.' + imageTypeDetected[1];

    fs.writeFile(userUploadedImagePath, imageBuffer.data, function (err) {
        if (err)

            cb(0);

        else {
            console.log('File saved.');
            cb(returnUserAddress);
        }

    });
};